<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Skynet Core Module.
 *
 * @package HostCMS 6\Module
 * @version 7.x
 * @author Борисов Михаил Юрьевич
 * @copyright © mikeborisov, m.u.borisov@gmail.com
 */
class Skynetcore_Sanitize
{
	public static function properties()
	{
		$aModels = [
			'Property_Value_Int',
			'Property_Value_String',
			'Property_Value_Text',
			'Property_Value_Float',
			'Property_Value_File',
			'Property_Value_Datetime',
		];
		foreach ($aModels as $modelName)
		{
			$aEntities = Core_Entity::factory($modelName)->getAllByEntity_id(0, false);
			foreach ($aEntities as $entity)
			{
				$entity->delete();
			}
		}
	}
}
