<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');

use Skynetcore_Utils as utl;
/**
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
class Skynetcore_Command_Controller extends Core_Command_Controller
{
	protected $_instance = null;
	protected $_response = null;
	protected $_status_code = 404;
	protected $_status = 'ERROR';
	protected $_message = "Неизвестная ошибка";
	protected $_context_html = "";
	protected $_html = "";
	protected $_trigger = [];
	protected $_js_func = [];
	protected $_referer = '/';
	protected $_data = [];
	protected $_suggestions = false;
	protected $_query = false;
	protected $_data_only = false;
    protected $_url = "";

	public function __construct()
	{
		$this->_instance = Core_Page::instance();
		$this->_context_html = Core_Array::getPost('context_html', false);
	}

	/**
	 * @return bool
	 */
	public function isDataOnly(): bool
	{
		return $this->_data_only;
	}

	/**
	 * @param bool $data_only
	 */
	public function setDataOnly(bool $data_only = true)
	{
		$this->_data_only = $data_only;
		return $this;
	}

	protected function checkCSRF() {
		$this->_response = new Core_Response();
		$this->_response
			->header('Content-type', 'application/json; charset=utf-8')
		;
		try {
			if (Core_Array::getRequest('csrf', Core_Array::getRequest('csrf_key')) != utl::getSimpleToken()) {
				throw new \Exception('Невозможно');
			} else {
				return true;
			}
		} catch (\Exception $e) {
			$aResult['message'] = $e->getMessage();
			$this->_response
				->status(404)
				->body(json_encode($aResult));
			$this->_response
				->sendHeaders()
				->showBody();
			die();
		}
	}

	protected function sendResponse() {
		/** @var Core_Page $instance */
		$instance = Core_Page::instance();
		$this->_response = new Core_Response();
		$this->_response
			->header('Content-type', 'application/json; charset=utf-8')
		;
		if(in_array($this->_status_code, [301, 302])) {
			$referer = Core_Array::get($_SERVER, 'HTTP_REFERER', $this->_referer);
			$this->_response
				->header('Location', $referer)
			;
			$this->_response->sendHeaders();
			exit();
		}
		$instance->response($this->_response);

		$aResult = [
			'status' => $this->_status,
			'message' => $this->_message,
		];
		if($this->_html != '') {
			$aResult['html'] = $this->_html;
		}
		if(count($this->_trigger) > 0) {
			$aResult['trigger'] = $this->_trigger;
		}
		if(count($this->_js_func) > 0) {
			$aResult['js_func'] = $this->_js_func;
		}
		if(count($this->_data)) {
			$aResult['data'] = $this->_data;
		}
		if(is_array($this->_suggestions)) {
			$aResult['suggestions'] = $this->_suggestions;
		}
		if($this->_query != false) {
			$aResult['query'] = $this->_query;
		}
		if($this->_url != '') {
			$aResult['url'] = $this->_url;
		}

		Core_Event::notify(get_class($this) . '.onBeforeSendResponse', $this, array($aResult));
		$eventResult = Core_Event::getLastReturn();
		$aResult = !is_null($eventResult) ? $eventResult : $aResult;

		if($this->_data_only) {
			$this->_response = $aResult;
		} elseif(is_object($this->_response) && method_exists($this->_response, 'sendHeaders')) {
			$this->_response
				->header('X-Robots-Tag', 'none')
				->status($this->_status_code)
				->body(json_encode($aResult))
				->sendHeaders()
				->showBody()
			;
			exit();
		}

		return $this->_response;
	}

	protected function checkSiteuser() {
		if(isset($this->_instance->skynet->siteuser)
			&& $this->_instance->skynet->siteuser === false)
		{
			$this->_message = "Ошибка аутентификации";
			$this->_status_code = 503;
			$this->sendResponse();
			$this->_response
				->sendHeaders()
				->showBody();
			die();
		}
		return $this;
	}

	public function __call($name, $arguments) {
		$name = preg_replace("/\-/ui", "", $name);
		if(method_exists($this, $name)) {
			return call_user_func([$this, $name], $arguments);
		} else {
			$this->_message = 'Маршрут не найден';
			return $this->sendResponse();
		}
	}
}