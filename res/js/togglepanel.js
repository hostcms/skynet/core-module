class TogglePanel {
	constructor() {
		this.initialized = false;
		this.hostname = window.location.hostname;
		this.cookieOptions = {
			expires: 31536000,
			// domain: "."+window.location.hostname,
			domain: window.location.hostname,
			path: "/"
		};
		this.selectors = ".hostcmsPanel:not(.togglePanel), .hostcmsSectionWidgetPanel, .hostcmsSectionPanel, .drag-handle, .customToggle";
		this.panels = [...document.querySelectorAll(this.selectors)];
		this.status = '0';
		this.handler = this.handler.bind(this);
	}
	init(){
		if(!this.initialized) {
			this.initialized = true;
			document.addEventListener('keydown', this.handler);
			this.firstCheckStatus();

			this.clearEmptyPanels();
		}else {
			console.log('toggle panel инициализирован, возможно, на сайте подключен два раза скрипт togglePanel.js')
		}
	}
	handler(event){
		if(event.keyCode === 192){
			if(this.status === '1'){
				this.hide();
			}else if(this.status === '0'){
				this.show();
			}
		}

		// console.log(event.keyCode);
		if(event.altKey && event.keyCode === 83){
			const frame = document.querySelector('.hostcmsWindow iframe');
			const doc = frame.contentWindow.document;
			const saveKey = doc.querySelector('.formButtons .save-and-close');
			saveKey && saveKey.click();
		}
		if(event.altKey && event.keyCode === 67){
			const frame = document.querySelector('.hostcmsWindow iframe');
			const doc = frame.contentWindow.document;
			const tabSpecKey = doc.querySelector('li[name="TehnicCat"] a');
			tabSpecKey && tabSpecKey.click();
		}
		if(event.altKey && event.keyCode === 81){
			const photoGalKey = document.querySelector('.good__wrapper a.photoGalleries_key.editGalleries');
			photoGalKey && photoGalKey.click();
		}
		if(event.altKey && event.keyCode === 65){
			const editKey = document.querySelector('.good__wrapper a.editItem_key.editItem');
			editKey && editKey.click();
		}
		if(event.altKey && event.keyCode === 90){
			const techKey = document.querySelector('.good__wrapper a.techs_key.editTechnique');
			techKey && techKey.click();
		}
	}
	show(){
		this.panels.forEach(el => {
			el.classList.add('skycls-show');
			el.classList.remove('skycls-hide');
		});
		this.setCookie(`skynet.toggle.${this.hostname}`, 1, this.cookieOptions);
		this.status = '1';
	}
	hide(){
		this.panels.forEach(el => {
			el.classList.remove('skycls-show');
			el.classList.add('skycls-hide');
		});

		this.setCookie(`skynet.toggle.${this.hostname}`, 0, this.cookieOptions);
		this.status = '0';
	}
	update(){
		this.panels = [...document.querySelectorAll(this.selectors)];
	}
	setCookie(name, value, options){
		options = options || {};
		let expires = options.expires;
		if (typeof expires == "number" && expires) {
			var d = new Date();
			d.setTime(d.getTime() + expires * 1000);
			expires = options.expires = d;
		}
		if (expires && expires.toUTCString) {
			options.expires = expires.toUTCString();
		}
		value = encodeURIComponent(value);
		let updatedCookie = name + "=" + value;
		for (let propName in options) {
			updatedCookie += "; " + propName;
			let propValue = options[propName];
			if (propValue !== true) {
				updatedCookie += "=" + propValue;
			}
		}
		document.cookie = updatedCookie;
	}
	getCookie(name){
		let matches = document.cookie.match(new RegExp(
			"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
		));
		return matches ? decodeURIComponent(matches[1]) : undefined;
	}

	firstCheckStatus(){
		if(this.getCookie(`skynet.toggle.${this.hostname}`) === undefined){
			this.setCookie(`skynet.toggle.${this.hostname}`, 1, this.cookieOptions);
			this.status = '1';
			this.show();
		}else if(this.getCookie(`skynet.toggle.${this.hostname}`) === '0'){
			this.hide();
		}
	}

	clearEmptyPanels(){
		document.querySelectorAll('.hostcmsPanel .hostcmsSubPanel').forEach(el => {
			if(el.innerHTML.trim() === '') {
				const wrap = el.closest('.hostcmsPanel');
				wrap.parentNode.removeChild(wrap)
			}
		})
	}
}

if(window.tp === undefined){
	window.tp = new TogglePanel();
	window.tp.init();
}