<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Skynetcore_Siteuser_Model
 */
class Skynetcore_Siteuser_Model extends Siteuser_Model
{
	const RETURN_CODE_OK = 0;
	const RETURN_CODE_USER_EXISTS = 10;
	const RETURN_CODE_WRONG_EMAIL = 20;
	const RETURN_CODE_VOID_EMAIL = 30;
	const RETURN_CODE_VOID_PASSWORD = 40;
	const RETURN_CODE_WRONG_PASSWORD_CONFIRMATION = 50;
	const RETURN_CODE_VOID_NAME = 60;

	protected $_errorCode = self::RETURN_CODE_OK;
	protected $_errorMessage = "";
	protected $_siteuserFounded = null;
	public $password_source = "";
	public $password_confirm = "";
	public $fio = "";

	/**
	 * Model name
	 * @var mixed
	 */
	protected $_modelName = 'siteuser';

	private function setErrorCode($errorCode) {
		$this->_errorCode = $errorCode;
		switch ($errorCode) {
			case self::RETURN_CODE_OK:
				$this->_errorMessage = "Выполнено";
				break;
			case self::RETURN_CODE_USER_EXISTS:
				$tmpSiteuserPerson = Core_Array::get($this->_siteuserFounded->siteuser_people->findAll(), 0, json_decode('{"surname": "", "name": "", "patronymic": ""}'));
//				Skynetcore_Utils::p($tmpSiteuserPerson);
				$this->_errorMessage = "Пользователь `".trim("{$tmpSiteuserPerson->surname} {$tmpSiteuserPerson->name} {$tmpSiteuserPerson->patronymic}")."` существует";
				break;
			case self::RETURN_CODE_WRONG_EMAIL:
				$this->_errorMessage = "Неверный email";
				break;
			case self::RETURN_CODE_VOID_EMAIL:
				$this->_errorMessage = "Пустой email";
				break;
			case self::RETURN_CODE_VOID_PASSWORD:
				$this->_errorMessage = "Пустой пароль";
				break;
			case self::RETURN_CODE_WRONG_PASSWORD_CONFIRMATION:
				$this->_errorMessage = "Пароли не совпадают";
				break;
			case self::RETURN_CODE_VOID_NAME:
				$this->_errorMessage = "Имя не заполнено";
				break;
		}
		return $this;
	}

	/**
	 * @return var
	 */
	public function getErrorCode()
	{
		return $this->_errorCode;
	}

	/**
	 * @return string
	 */
	public function getErrorMessage(): string
	{
		return $this->_errorMessage;
	}

	public function getByLoginAndPassword($login, $password, $removeUnaccessedIp = false)
	{
		$oLocalSiteuser = parent::getByLoginAndPassword($login, $password);
		if(is_null($oLocalSiteuser) && $removeUnaccessedIp) {
			$ip = Core::getClientIp();
			// Save attempt
			$aSiteuser_Accessdenied = Core_Entity::factory('Siteuser_Accessdenied')->getAllByIp($ip, false);
			if(count($aSiteuser_Accessdenied)) {
				foreach ($aSiteuser_Accessdenied as $oSiteuserAccessDenied) {
					$oSiteuserAccessDenied->delete();
				}
			}
		}

		return $oLocalSiteuser;
	}

	public function addSiteuser() {
		if( !is_null($this->id) ) {
			$this->setErrorCode($this::RETURN_CODE_USER_EXISTS);
			return $this;
		}
		if( !Core_Valid::email($this->email) ) {
			$this->setErrorCode($this::RETURN_CODE_WRONG_EMAIL);
			return $this;
		}
		if( trim($this->email) == '' ) {
			$this->setErrorCode($this::RETURN_CODE_VOID_EMAIL);
			return $this;
		}
		if( trim($this->password_source) == '' ) {
			$this->setErrorCode($this::RETURN_CODE_VOID_EMAIL);
			return $this;
		}
		if( $this->password_source != $this->password_confirm ) {
			$this->setErrorCode($this::RETURN_CODE_WRONG_PASSWORD_CONFIRMATION);
			return $this;
		}
		if( $this->name ) {
			$this->setErrorCode($this::RETURN_CODE_VOID_NAME);
			return $this;
		}

		/** @var Siteuser_Model $qLookupSiteuser */
		$qLookupSiteuser = Core_Entity::factory(preg_replace("/_Model$/ui", "", get_class($this)));
		$qLookupSiteuser
			->queryBuilder()
			->where('site_id', '=', CURRENT_SITE)
		;
		$this->_siteuserFounded = $qLookupSiteuser->getByEmail($this->email, false);
		if(is_null($this->_siteuserFounded)) {
			$this->site_id = CURRENT_SITE;
			$this->siteuser_type_id = 1;
			$this->siteuser_status_id = 0;
			$this->crm_source_id = 0;
			$this->login = $this->email;
			$this->password = Core_Hash::instance()->hash($this->password_source);
			$this->guid =  Core_Guid::get();
			$this->active = 0;

			$splites = explode(' ', $this->fio);
			$this->surname = Core_Array::get($splites, 0, '');
			$this->name = Core_Array::get($splites, 1, '');
			$this->patronymic = Core_Array::get($splites, 2, '');
			$this->save();
			/** @var Siteuser_Group_Model $qGroup */
			$qDefaultGroup = Core_Entity::factory('Siteuser_Group');
			$qDefaultGroup
				->queryBuilder()
				->where('site_id', '=', CURRENT_SITE)
			;
			//-- Добавляем пользователя в группу по умолчанию ----------------------------------------------------------
			/** @var Siteuser_Group_List_Model $oDefaultGroup */
			$oDefaultGroup = $qDefaultGroup->getByDefault(1, false);
			if(isset($oDefaultGroup) && $oDefaultGroup->id > 0
				&& isset($this->id) && $this->id > 0
			) {
				$oSiteuser_Group_List = Core_Entity::factory('Siteuser_Group_List');
				$oSiteuser_Group_List->siteuser_group_id = $oDefaultGroup->id;
				$oSiteuser_Group_List->siteuser_id = $this->id;
				$oSiteuser_Group_List->save();
			}
			$this->setErrorCode($this::RETURN_CODE_OK);
		} else {
//			$this->_siteuserFounded = $this;
//			Skynetcore_Utils::p($this->_siteuserFounded->siteuser_person[0]->toArray());
			$this->setErrorCode($this::RETURN_CODE_USER_EXISTS);
			return $this;
		}
	}

	public function setCurrent($expires = 2678400)
	{
		$oSiteUser = parent::setCurrent($expires);
		$instance = Core_Page::instance();
		if(isset($oSiteUser->id) && $oSiteUser->id > 0) {
			$instance->skynet->siteuser = $oSiteUser;
			/** @var Siteuser_Group_Model $oSiteuserGroups */
			$oSiteuserGroups = Core_Entity::factory('Siteuser_Group');
			$siteuser_groups = Skynetcore_Utils::array_map_assoc(
				function($groupKey, $oGroup) {
					return [$oGroup->id, $oGroup];
				}, $oSiteuserGroups->getForSiteuser($oSiteUser->id)
			);
			//-- Группа Администраторы (пользователь не в группе Администраторы) --
			if(!array_key_exists(53, $siteuser_groups)) {
				$ip = Core_Array::get($_SERVER, 'REMOTE_ADDR', '127.0.0.1');
				$sessionId = session_id();

				$userAgent = Core_Array::get($_SERVER, 'HTTP_USER_AGENT', NULL);
				$oDataBase = Core_QueryBuilder::update('skynetcore_siteuser_sessions')
					->join(['sessions', 's'], 's.id', '=', 'skynetcore_siteuser_sessions.session_id')
					->set('skynetcore_siteuser_sessions.siteuser_id', $oSiteUser->id)
					->set('skynetcore_siteuser_sessions.siteuser_pass', $oSiteUser->password)
					->set('skynetcore_siteuser_sessions.time', time())
					->set('skynetcore_siteuser_sessions.user_agent', $userAgent)
					->set('skynetcore_siteuser_sessions.ip', $ip)
					->where('skynetcore_siteuser_sessions.session_id', '=', $sessionId)
//					->where('skynetcore_siteuser_sessions.time', '>=', Core_QueryBuilder::expression('s.time'))
					->where('skynetcore_siteuser_sessions.exit_time', '>', Core_Date::timestamp2sql(time()))
					->where('skynetcore_siteuser_sessions.active', '=', 1)
					->execute();
//				Skynetcore_Utils::p( $oDataBase->getLastQuery() ); die();
				// Returns the number of rows affected by the last SQL statement
				// If nothing's really was changed affected rowCount will return 0.
				if ($oDataBase->getAffectedRows() == 0)
				{
					Core_QueryBuilder::insert('skynetcore_siteuser_sessions')
						->ignore()
						->columns('session_id', 'siteuser_id', 'siteuser_pass', 'time', 'user_agent', 'ip', 'active', 'exit_type', 'value')
						->values($sessionId, $oSiteUser->id, $oSiteUser->password, time(), $userAgent, $ip, 1, '', '')
						->execute();
				}

//				$billing = new Wbs_Billing_Controller();
//				$oWbs = $billing->getWbsSiteUser($oSiteUser->id);
//				Skynetcore_Utils::p($oWbs->tarif->max_auth_sessions, "max_auth_sessions - ".$oSiteUser->id); die();
			}
		}

		return $this;
	}

//	public function setCurrent($expires = 2678400)
//	{
//		$oSiteUser = parent::setCurrent($expires);
//
//		if(isset($oSiteUser->id) && $oSiteUser->id > 0) {
//			/** @var Siteuser_Group_Model $oSiteuserGroups */
//			$oSiteuserGroups = Core_Entity::factory('Siteuser_Group');
//			$siteuser_groups = Skynetcore_Utils::array_map_assoc(
//				function($groupKey, $oGroup) {
//					return [$oGroup->id, $oGroup];
//				}, $oSiteuserGroups->getForSiteuser($oSiteUser->id)
//			);
//			//-- Группа Администраторы (пользователь не в группе Администраторы) --
//			if(!array_key_exists(53, $siteuser_groups)) {
//				$ip = Core_Array::get($_SERVER, 'REMOTE_ADDR', '127.0.0.1');
//				$sessionId = session_id();
//
//				$userAgent = Core_Array::get($_SERVER, 'HTTP_USER_AGENT', NULL);
//				$oDataBase = Core_QueryBuilder::update('skynetcore_siteuser_sessions')
//					->set('siteuser_id', $oSiteUser->id)
//					->set('siteuser_pass', $oSiteUser->password)
//					->set('time', time())
//					->set('user_agent', $userAgent)
//					->set('ip', $ip)
//					->where('session_id', '=', $sessionId)
//					->where('active', '=', 1)
//					->execute();
//
//				// Returns the number of rows affected by the last SQL statement
//				// If nothing's really was changed affected rowCount will return 0.
//				if ($oDataBase->getAffectedRows() == 0)
//				{
//					Core_QueryBuilder::insert('skynetcore_siteuser_sessions')
//						->ignore()
//						->columns('session_id', 'siteuser_id', 'siteuser_pass', 'time', 'user_agent', 'ip', 'active', 'exit_time', 'exit_type', 'value')
//						->values($sessionId, $oSiteUser->id, $oSiteUser->password, time(), $userAgent, $ip, 1, '0000-00-00 00:00:00', '', '')
//						->execute();
//				}
//
////				$billing = new Wbs_Billing_Controller();
////				$oWbs = $billing->getWbsSiteUser($oSiteUser->id);
////				Skynetcore_Utils::p($oWbs->tarif->max_auth_sessions, "max_auth_sessions - ".$oSiteUser->id); die();
//			}
//		}
//
//		return $this;
//	}

	/**
	 * Backend callback method
	 * @return string
	 */
	public function userLoginBackend()
	{
		$isOnline = $this->isOnline();

		$sStatus = $isOnline ? 'online' : 'offline';

		$lng = $isOnline ? 'siteuser_active' : 'siteuser_last_activity';

		$sStatusTitle = !is_null($this->last_activity)
			? Core::_('Siteuser.' . $lng, Core_Date::sql2datetime($this->last_activity))
			: '';

		$sResult = htmlspecialchars($this->login)
			. '&nbsp;<span title="' . htmlspecialchars($sStatusTitle) . '" class="' . htmlspecialchars($sStatus) . '"></span>';

		if ($this->crm_source_id)
		{
			$oCrm_Source = $this->Crm_Source;

			$sResult .= '&nbsp;<i title="' . htmlspecialchars((string) $oCrm_Source->name) . '" class="fa ' . htmlspecialchars((string) $oCrm_Source->icon) . ' fa-small" style="color: ' . htmlspecialchars((string) $oCrm_Source->color) . '"></i>';
		}

		// fix bug with model Siteuser_Group_Siteuser
		$aSiteuser_Groups = Core_Entity::factory('Siteuser', $this->id)->Siteuser_Groups->findAll(FALSE);

		if (count($aSiteuser_Groups))
		{
			$sListSiteuserGroups = '';

			foreach ($aSiteuser_Groups as $oSiteuser_Group)
			{
				$sListSiteuserGroups .= '<i class="fa-regular fa-folder-open" style="margin-right: 5px"></i><a onclick="'
					. "$.adminLoad({path: hostcmsBackend + '/siteuser/group/list/index.php', additionalParams: 'siteuser_group_id=" . $oSiteuser_Group->id . "', windowId: 'id_content'}); return false"
					. '">' . htmlspecialchars($oSiteuser_Group->name) . "</a><br/>";
			}

			//data-trigger="focus"
			$sResult .= '<a id="siteuser_' . $this->id . '"  class="siteuser_group_list_link" style="margin-right: 5px;" tabindex="0" role="button" data-toggle="popover" data-placement="right" data-content="' . htmlspecialchars($sListSiteuserGroups) . '" data-title="' . Core::_('Siteuser_Group.title') . '" data-titleclass="bordered-darkorange" data-container="#siteuser_' . $this->id . '"><i class="fa fa-folder-o gray"></i></a>';
		}

		$sResult .= ' ' . ($this->Siteuser_Type->name
				? '<span class="badge badge-square badge-max-width margin-right-5" title="' . htmlspecialchars($this->Siteuser_Type->name) . '" style="background-color: ' . htmlspecialchars($this->Siteuser_Type->color) . '">' . htmlspecialchars($this->Siteuser_Type->name) . '</span>'
				: '')
			. ($this->Siteuser_Status->name
				? '<span class="badge badge-square badge-max-width" title="' . htmlspecialchars($this->Siteuser_Status->name) . '" style="background-color: ' . htmlspecialchars($this->Siteuser_Status->color) . '">' . htmlspecialchars($this->Siteuser_Status->name) . '</span>' : '');

		return $sResult;
	}

	/**
	 * Get avatar with name
	 * @return string|NULL
	 */
	public function getAvatarWithName()
	{
		if ($this->id)
		{
			/** @var Siteuser_Person_Model $oPeople */
			$oPeople = $this->siteuser_people->getFirst();
			if(isset($oPeople->id) && $oPeople->id > 0) {
				return $oPeople->getProfilePopupBlock();
			}

			return $this->userLoginBackend();
		}

		return NULL;
	}

	/**
	 * Show avatar with name
	 */
	public function showAvatarWithName()
	{
		return $this->getAvatarWithName();
	}

	public function unsetCurrent()
	{
		$sessionClass = isset(Core::$mainConfig['session']['class'])
			? Core::$mainConfig['session']['class']
			: self::_getDriverName(Core::$mainConfig['session']['driver']);
		$instance = Core_Page::instance();
		/** @var Core_Session_Database $session */
		$session = new $sessionClass();
		$sesionId = session_id();

		$currentSession = $session->sessionRead(session_id());
		/** @var Skynetcore_Siteuser_Session_Model $oSiteuserSession */
		$aSiteuserSessions = Core_Entity::factory('Skynetcore_Siteuser_Session')->getAllBySession_id($sesionId);
		if(count($aSiteuserSessions)) {
			foreach ($aSiteuserSessions as $oSiteuserSessionTmp) {
				$oSiteuserSessionTmp->active = 0;
				if(Core_Array::getRequest('action', false) == 'exit') {
					$oSiteuserSessionTmp->exit_type = 'exit';
				}
				$oSiteuserSessionTmp->exit_time = Core_Date::timestamp2sql(time());
				$oSiteuserSessionTmp->value = base64_encode($currentSession);
				$oSiteuserSessionTmp->save();
				$session->sessionDestroyer($sesionId);
			}
		}
		$instance->skynet->siteuser = false;
		return parent::unsetCurrent();
	}

	public function getCurrent($bCheckSite = FALSE)
	{
		$oSiteuser = null;
		if(isset(Siteuser_Controller::getCurrent()->id) && ($sid=Siteuser_Controller::getCurrent()->id) > 0) {
			/** @var Skynetcore_Siteuser_Model $oSiteuser */
			$oSiteuser = Core_Entity::factory('Skynetcore_Siteuser')->getById($sid);
		}
		return $oSiteuser;
	}


}