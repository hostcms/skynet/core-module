<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				xmlns:hostcms="http://www.hostcms.ru/"
				xmlns:zsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:string="http://symphony-cms.com/functions"
				exclude-result-prefixes="hostcms string xlink">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>
	<xsl:include href="strings.xsl"/>

	<xsl:decimal-format name="moneyFormat" decimal-separator="," grouping-separator=" "/>

	<xsl:variable name="skynet_pathinfo" select="/*/pathinfo" />
	<xsl:variable name="skynet_path" select="$skynet_pathinfo/path_no_page" />
	<xsl:variable name="skynet_get" select="/*/get" />
<!--	<xsl:variable name="img_mode" select="false()" />-->
	<xsl:variable name="img_mode" select="true()" />

	<xsl:template name="pagination">
		<xsl:param name="base_url" select="false()" />
		<xsl:param name="add_pagination_number" select="true()" />
		<xsl:param name="vis_pages" select="'5'" />
		<xsl:param name="class" select="''" />
		<xsl:param name="attrtype" select="''" />
		<xsl:param name="show_custom_buttons" select="false()" />
		<xsl:param name="limit" select="limit"/>
		<xsl:param name="page" select="page"/>
		<xsl:param name="total" select="total"/>

		<xsl:variable name="current_selected"><xsl:choose>
			<xsl:when test="$limit > 0"><xsl:value-of select="$limit" /></xsl:when>
			<xsl:when test="$skynet_get/on_page != ''"><xsl:value-of select="$skynet_get/on_page" /></xsl:when>
			<xsl:when test="/*/limit > 0"><xsl:value-of select="/*/limit" /></xsl:when>
			<xsl:otherwise>10</xsl:otherwise>
		</xsl:choose></xsl:variable>
		<!-- pagination BEGIN -->
		<xsl:variable name="pagination">
<!--			<xsl:if test="$attrtype != ''"><xsl:attribute name="data-type"><xsl:value-of select="$attrtype" /></xsl:attribute></xsl:if>-->
			<div class="pagination__list">
				<xsl:if test="$total &gt; 0 and $limit &gt; 0">

					<xsl:variable name="count_pages" select="ceiling($total div $limit)"/>

					<xsl:variable name="visible_pages" select="number($vis_pages)"/>

					<xsl:variable name="real_visible_pages"><xsl:choose>
						<xsl:when test="$count_pages &lt; $visible_pages"><xsl:value-of select="$count_pages"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="$visible_pages"/></xsl:otherwise>
					</xsl:choose></xsl:variable>

					<!-- Считаем количество выводимых ссылок перед текущим элементом -->
					<xsl:variable name="pre_count_page"><xsl:choose>
						<xsl:when test="$page - (floor($real_visible_pages div 2)) &lt; 0">
							<xsl:value-of select="$page"/>
						</xsl:when>
						<xsl:when test="($count_pages - $page - 1) &lt; floor($real_visible_pages div 2)">
							<xsl:value-of select="$real_visible_pages - ($count_pages - $page - 1) - 1"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="round($real_visible_pages div 2) = $real_visible_pages div 2">
									<xsl:value-of select="floor($real_visible_pages div 2) - 1"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="floor($real_visible_pages div 2)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose></xsl:variable>

					<!-- Считаем количество выводимых ссылок после текущего элемента -->
					<xsl:variable name="post_count_page"><xsl:choose>
						<xsl:when test="0 &gt; $page - (floor($real_visible_pages div 2) - 1)">
							<xsl:value-of select="$real_visible_pages - $page - 1"/>
						</xsl:when>
						<xsl:when test="($count_pages - $page - 1) &lt; floor($real_visible_pages div 2)">
							<xsl:value-of select="$real_visible_pages - $pre_count_page - 1"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$real_visible_pages - $pre_count_page - 1"/>
						</xsl:otherwise>
					</xsl:choose></xsl:variable>

					<xsl:variable name="i"><xsl:choose>
						<xsl:when test="$page + 1 = $count_pages"><xsl:value-of select="$page - $real_visible_pages + 1"/></xsl:when>
						<xsl:when test="$page - $pre_count_page &gt; 0"><xsl:value-of select="$page - $pre_count_page"/></xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose></xsl:variable>

					<xsl:call-template name="forshop">
						<xsl:with-param name="limit" select="$limit"/>
						<xsl:with-param name="page" select="$page"/>
						<xsl:with-param name="items_count" select="$total"/>
						<xsl:with-param name="i" select="$i"/>
						<xsl:with-param name="post_count_page" select="$post_count_page"/>
						<xsl:with-param name="pre_count_page" select="$pre_count_page"/>
						<xsl:with-param name="visible_pages" select="$real_visible_pages"/>
						<xsl:with-param name="base_url" select="$base_url"/>
						<xsl:with-param name="add_pagination_number" select="$add_pagination_number"/>
						<xsl:with-param name="show_custom_buttons" select="$show_custom_buttons"/>
					</xsl:call-template>
				</xsl:if>
			</div>
			<xsl:if test="$show_custom_buttons">
				<xsl:if test="$limit &lt; $total">
					<div class="pagination__onpage">
						<select name="on_page">
							<option>...</option>
							<option><xsl:choose><xsl:when test="$current_selected = 10"><xsl:attribute name="selected">selected</xsl:attribute></xsl:when></xsl:choose>
								10
							</option>
							<option><xsl:choose><xsl:when test="$current_selected = 20"><xsl:attribute name="selected">selected</xsl:attribute></xsl:when></xsl:choose>
								20
							</option>
							<option><xsl:choose><xsl:when test="$current_selected = 30"><xsl:attribute name="selected">selected</xsl:attribute></xsl:when></xsl:choose>
								30</option>
							<option><xsl:choose><xsl:when test="$current_selected = 50"><xsl:attribute name="selected">selected</xsl:attribute></xsl:when></xsl:choose>
								50</option>
							<option><xsl:choose><xsl:when test="$current_selected = 100"><xsl:attribute name="selected">selected</xsl:attribute></xsl:when></xsl:choose>
								100</option>
							<option><xsl:choose><xsl:when test="$current_selected = 200"><xsl:attribute name="selected">selected</xsl:attribute></xsl:when></xsl:choose>
								200</option>
							<option><xsl:choose><xsl:when test="$current_selected = 500"><xsl:attribute name="selected">selected</xsl:attribute></xsl:when></xsl:choose>
								500</option>
							<option><xsl:choose><xsl:when test="$current_selected = 1000"><xsl:attribute name="selected">selected</xsl:attribute></xsl:when></xsl:choose>
								1000</option>
						</select>
					</div>
				</xsl:if>
				<!--			<xsl:apply-templates select="." mode="custom_buttons" />-->
				<div class="pagination__buttons">
					<button name="panelaction" value="apply" type="submit">Применить</button>
					<a href="." class="button">Сбросить</a>
					<xsl:apply-templates select="." mode="custom_buttons" />
				</div>
			</xsl:if>
		</xsl:variable>
		<xsl:if test="normalize-space($pagination) != ''">
			<div id="pagination">
				<xsl:if test="$attrtype != ''"><xsl:attribute name="data-type"><xsl:value-of select="$attrtype" /></xsl:attribute></xsl:if>
				<xsl:attribute name="class">pagination<xsl:if test="$class != ''"><xsl:text> </xsl:text><xsl:value-of select="$class" /></xsl:if></xsl:attribute>
				<xsl:copy-of select="$pagination"/>
			</div>
		</xsl:if>
		<!-- pagination END -->
	</xsl:template>

	<xsl:template match="*" mode="custom_buttons">
	</xsl:template>

	<xsl:template name="forshop">

		<xsl:param name="limit"/>
		<xsl:param name="page"/>
		<xsl:param name="pre_count_page"/>
		<xsl:param name="post_count_page"/>
		<xsl:param name="i" select="0"/>
		<xsl:param name="items_count"/>
		<xsl:param name="visible_pages"/>
		<xsl:param name="base_url"/>
		<xsl:param name="add_pagination_number" />
		<xsl:param name="show_custom_buttons" select="false()" />

		<xsl:variable name="n" select="ceiling($items_count div $limit)"/>

		<xsl:variable name="start_page"><xsl:choose>
			<xsl:when test="$page + 1 = $n"><xsl:value-of select="$page - $visible_pages + 1"/></xsl:when>
			<xsl:when test="$page - $pre_count_page &gt; 0"><xsl:value-of select="$page - $pre_count_page"/></xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose></xsl:variable>

		<!-- Передаем фильтр -->
		<xsl:variable name="filter"><xsl:if test="/shop/filter/node()">?filter=1&amp;sorting=<xsl:value-of select="/shop/sorting"/>&amp;price_from=<xsl:value-of select="/shop/price_from"/>&amp;price_to=<xsl:value-of select="/shop/price_to"/><xsl:for-each select="/shop/*"><xsl:if test="starts-with(name(), 'property_')">&amp;<xsl:value-of select="name()"/>[]=<xsl:value-of select="."/></xsl:if></xsl:for-each></xsl:if></xsl:variable>

		<xsl:variable name="on_page"><xsl:if test="/shop/on_page/node() and /shop/on_page > 0"><xsl:choose><xsl:when test="/shop/filter/node()">&amp;</xsl:when><xsl:otherwise>?</xsl:otherwise></xsl:choose>on_page=<xsl:value-of select="/shop/on_page"/></xsl:if></xsl:variable>

		<xsl:variable name="getparamsTmp"><xsl:for-each select="$skynet_get/*"><xsl:if
				test="    name(.) != 'query'
				      and name(.) != 'queryshort'
				      and name(.) != 'filter'
				      and name(.) != 'sorting'
				      and name(.) != 'price_from'
				      and name(.) != 'price_to'
				      and name(.) != 'ajax'">&amp;<xsl:value-of
				select="name(.)" />=<xsl:value-of
				select="text()" /></xsl:if></xsl:for-each></xsl:variable>
<!--		<xsl:variable name="getparamsTmp"><xsl:if test="$skynet_get/query">?<xsl:value-of-->
<!--				select="string:replace($skynet_get/query, '&amp;ajax=1', '')" /><xsl:value-of-->
<!--				select="string:replace($skynet_get/query, 'filter=1', '')" /><xsl:value-of-->
<!--				select="string:replace($skynet_get/query, '&amp;filter=1', '')" /></xsl:if></xsl:variable>-->
<!--		<xsl:variable name="getparamsTmp"><xsl:if test="$skynet_get/query">?<xsl:value-of-->
<!--				select="string:replace($skynet_get/query, '&amp;ajax=1')" /></xsl:if></xsl:variable>-->
		<xsl:variable name="sGetparams"><xsl:choose>
			<xsl:when test="$getparamsTmp != '' and $getparamsTmp != '?'"><xsl:value-of select="$getparamsTmp" /></xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose></xsl:variable>
		<xsl:variable name="getparams"><xsl:choose>
			<xsl:when test="$sGetparams != '' and substring-before($sGetparams, '&amp;') = ''"><xsl:value-of select="concat('?', substring-after($sGetparams, '&amp;'))" /></xsl:when>
			<xsl:otherwise><xsl:value-of select="$sGetparams" /></xsl:otherwise>
		</xsl:choose></xsl:variable>

<!--		===query==<xsl:value-of select="string:replace($skynet_get/query, 'filter=1', '-')" />=-->
<!--		===query==<xsl:value-of select="$skynet_get/query" />=-->
<!--		===getparamsTmp&#45;&#45;<xsl:value-of select="$getparamsTmp" />-->
<!--		=====<xsl:value-of select="substring-before($getparams, '&amp;')" />=-->

		<xsl:if test="$items_count &gt; $limit and ($page + $post_count_page + 1) &gt; $i">
			<!-- Заносим в переменную $group идентификатор текущей группы -->
			<xsl:variable name="group" select="/shop/group"/>

			<!-- Путь для тэга -->
			<xsl:variable name="tag_path"></xsl:variable>
<!--			<xsl:variable name="tag_path"><xsl:if test="count(/shop/tag) != 0">tag/<xsl:value-of select="/shop/tag/urlencode"/>/</xsl:if></xsl:variable>-->

			<!-- Путь для сравнения товара -->
			<xsl:variable name="shop_producer_path"><xsl:if test="count(/shop/shop_producer)">producer-<xsl:value-of
					select="/shop/shop_producer/@id"/>/</xsl:if></xsl:variable>

			<!-- Определяем группу для формирования адреса ссылки -->
			<xsl:variable name="group_link"><xsl:choose>
				<xsl:when test="$base_url != false()"><xsl:choose>
					<xsl:when test="$add_pagination_number = false() and $page = 0"><xsl:value-of
							select="substring-before($base_url, '/entity/')"/>/<xsl:if test="$i != 0">page-<xsl:value-of
							select="$i + 1" />/</xsl:if>entity/<xsl:value-of
							select="substring-after($base_url, '/entity/')"/></xsl:when>
					<xsl:when test="$add_pagination_number = false() and $page > 0"><xsl:value-of
							select="substring-before($base_url, concat('/page-', $page+1, '/'))"/>/<xsl:choose>
								<xsl:when test="$i != 0">page-<xsl:value-of
										select="$i + 1" />/</xsl:when>
							</xsl:choose><xsl:value-of
							select="substring-after($base_url, concat('/page-', $page+1, '/'))"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="$base_url"/></xsl:otherwise>
				</xsl:choose></xsl:when>
				<xsl:when test="pathinfo/path_no_page != ''"><xsl:value-of select="pathinfo/path_no_page"/></xsl:when>
				<xsl:when test="$group != 0"><xsl:value-of select="/shop//shop_group[@id=$group]/url"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="/shop/url"/></xsl:otherwise></xsl:choose></xsl:variable>

			<!-- Определяем адрес ссылки -->
			<xsl:variable name="number_link"><xsl:choose>
				<xsl:when test="$i != 0 and $add_pagination_number">page-<xsl:value-of select="$i + 1"/>/</xsl:when>
			</xsl:choose></xsl:variable>

			<!-- Выводим ссылку на первую страницу -->
			<xsl:if test="$page - $pre_count_page &gt; 0 and $i = $start_page">
				<xsl:variable name="corrected_group_link"><xsl:choose>
					<xsl:when test="$add_pagination_number = false()"><xsl:value-of
							select="substring-before($base_url, concat('/page-', $page+1, '/'))"/>/<xsl:value-of
							select="substring-after($base_url, concat('/page-', $page+1, '/'))"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="$group_link" /></xsl:otherwise>
				</xsl:choose></xsl:variable>
				<div class="pagination__item">
					<a href="{$corrected_group_link}{$tag_path}{$shop_producer_path}{$filter}{$on_page}{$getparams}" class="pagination__link js-pagination-link" data-page="1">1</a>
				</div>
				<div class="pagination__item">
					<span class="pagination__dummy">
						...
					</span>
				</div>
			</xsl:if>

			<!-- Ставим ссылку на страницу-->
			<xsl:if test="$i != $page">
				<xsl:if test="($page - $pre_count_page) &lt;= $i and $i &lt; $n">
					<!-- Выводим ссылки на видимые страницы -->
					<div class="pagination__item">
						<a href="{$group_link}{$number_link}{$tag_path}{$shop_producer_path}{$filter}{$on_page}{$getparams}" class="pagination__link js-pagination-link" data-page="{$i + 1}">
							<xsl:value-of select="$i + 1"/>
						</a>
					</div>
				</xsl:if>

				<!-- Выводим ссылку на последнюю страницу -->
				<xsl:if test="$i+1 &gt;= ($page + $post_count_page + 1) and $n &gt; ($page + 1 + $post_count_page)">
					<!-- Выводим ссылку на последнюю страницу -->
					<div class="pagination__item">
						<span class="pagination__dummy">
							...
						</span>
					</div>
					<div class="pagination__item">
						<xsl:variable name="corrected_group_link_last"><xsl:choose>
							<xsl:when test="$add_pagination_number = false()"><xsl:value-of
									select="substring-before($base_url, concat('/page-', $page+1, '/'))"/>/page-<xsl:value-of
									select="$n" />/<xsl:value-of
									select="substring-after($base_url, concat('/page-', $page+1, '/'))"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="$group_link" />page-<xsl:value-of select="$n" />/</xsl:otherwise>
						</xsl:choose></xsl:variable>
						<a href="{$corrected_group_link_last}{$tag_path}{$shop_producer_path}{$filter}{$on_page}{$getparams}" class="pagination__link js-pagination-link" data-page="{$n}">
							<xsl:value-of select="$n" />
						</a>
					</div>
				</xsl:if>
			</xsl:if>

			<xsl:if test="$show_custom_buttons">
				<!-- Ссылка на предыдущую страницу для Ctrl + влево -->
				<xsl:if test="$i = $start_page and $page != 0">
					<xsl:variable name="prev_number_link"><xsl:choose>
						<xsl:when test="$page &gt; 1 and $add_pagination_number">page-<xsl:value-of select="$page"/>/</xsl:when>
					</xsl:choose></xsl:variable>

					<a href="{$group_link}{$prev_number_link}{$tag_path}{$shop_producer_path}{$filter}{$on_page}{$getparams}" class="pagination__prev">
						<svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M14 6L2 6" stroke="#172A3F" stroke-width="1.5" />
							<path d="M7.07227 0.90918L1.98136 6.00009L7.07227 11.091" stroke="#172A3F" stroke-width="1.5" />
						</svg>
					</a>
				</xsl:if>

				<!-- Ссылка на следующую страницу для Ctrl + вправо -->
				<xsl:if test="($n - 1) > $page and $i = $page">
					<a href="{$group_link}page-{$page+2}/{$tag_path}{$shop_producer_path}{$filter}{$on_page}{$getparams}"
					   class="pagination__next"
					>
						<span>Дальше</span>
						<svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M0.00195312 6.09082L12.002 6.09082" stroke="#172A3F" stroke-width="1.5" />
							<path d="M6.92969 1L12.0206 6.09091L6.92969 11.1818" stroke="#172A3F" stroke-width="1.5" />
						</svg>
					</a>
				</xsl:if>
			</xsl:if>

			<!-- Не ставим ссылку на страницу-->
			<xsl:if test="$i = $page">
				<div class="pagination__item">
					<span class="pagination__current js-pagination-current" data-page="{$i+1}">
						<xsl:value-of select="$i+1"/>
					</span>
				</div>
			</xsl:if>

			<!-- Рекурсивный вызов шаблона. НЕОБХОДИМО ПЕРЕДАВАТЬ ВСЕ НЕОБХОДИМЫЕ ПАРАМЕТРЫ! -->
			<xsl:call-template name="forshop">
				<xsl:with-param name="i" select="$i + 1"/>
				<xsl:with-param name="limit" select="$limit"/>
				<xsl:with-param name="page" select="$page"/>
				<xsl:with-param name="items_count" select="$items_count"/>
				<xsl:with-param name="pre_count_page" select="$pre_count_page"/>
				<xsl:with-param name="post_count_page" select="$post_count_page"/>
				<xsl:with-param name="visible_pages" select="$visible_pages"/>
				<xsl:with-param name="base_url" select="$base_url"/>
				<xsl:with-param name="add_pagination_number" select="$add_pagination_number"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template name="column_arrows">
		<xsl:param name="field" select="'id'" />
		<div class="arrows">
			<a href="./?sort={$field},desc">
				<xsl:if test="$skynet_get/sort[node()=concat($field,',desc')]" >
					<xsl:attribute name="class">selected</xsl:attribute>
				</xsl:if>
				<i class="fa fa-arrow-up" aria-hidden="true"></i>
			</a>
			<a href="./?sort={$field},asc">
				<xsl:if test="$skynet_get/sort[node()=concat($field,',asc')]" >
					<xsl:attribute name="class">selected</xsl:attribute>
				</xsl:if>
				<i class="fa fa-arrow-down" aria-hidden="true"></i>
			</a>
		</div>
	</xsl:template>

	<xsl:template name="sky_image_src_small"><xsl:param
			name="noimage_default" select="'/upload/noimage_small.png'" /><xsl:choose>
		<xsl:when test="image_small != ''"><xsl:value-of select="dir" /><xsl:value-of select="image_small" /></xsl:when>
		<xsl:otherwise><xsl:choose>
			<xsl:when test="$noimage_default = false()"></xsl:when>
			<xsl:otherwise><xsl:value-of select="$noimage_default" /></xsl:otherwise>
		</xsl:choose></xsl:otherwise>
	</xsl:choose></xsl:template>

	<xsl:template name="sky_image_small">
		<xsl:param name="noimage_default" select="'/upload/noimage.png'" />
		<xsl:variable name="image_src"><xsl:call-template name="sky_image_src_small" >
			<xsl:with-param name="noimage_default" select="$noimage_default" />
		</xsl:call-template></xsl:variable>

		<xsl:if test="normalize-space($image_src) != ''">
			<img src="{$image_src}" />
		</xsl:if>
	</xsl:template>

	<xsl:template name="sky_image_src"><xsl:param
			name="noimage_default" select="'/upload/noimage.png'" /><xsl:choose>
		<xsl:when test="image_large != ''"><xsl:value-of select="dir" /><xsl:value-of select="image_large" /></xsl:when>
		<xsl:otherwise><xsl:choose>
			<xsl:when test="$noimage_default = false()"></xsl:when>
			<xsl:otherwise><xsl:value-of select="$noimage_default" /></xsl:otherwise>
		</xsl:choose></xsl:otherwise>
	</xsl:choose></xsl:template>

	<xsl:template name="sky_image">
		<xsl:param name="noimage_default" select="'/upload/noimage.png'" />
		<xsl:variable name="image_src"><xsl:call-template name="sky_image_src" >
			<xsl:with-param name="noimage_default" select="$noimage_default" />
		</xsl:call-template></xsl:variable>

		<xsl:if test="normalize-space($image_src) != ''">
			<img src="{$image_src}" />
		</xsl:if>
	</xsl:template>

	<xsl:template name="userbars">
		<xsl:param name="id" select="@id" />
		<xsl:param name="data_checked" />
		<xsl:param name="icon_type" />
		<xsl:param name="no_svg_icon" select="false()" />
		<xsl:param name="is_common" />
		<xsl:param name="class" select="''" />
		<xsl:param name="context" select="''" />
		<xsl:param name="data_template_small" select="'undefined'" />
		<xsl:param name="data_template_large" select="'undefined'" />

		<xsl:variable name="post_text_0" select="''" />
		<xsl:variable name="post_text_1" select="''" />
		<xsl:variable name="class_int">button-<xsl:value-of select="$icon_type" /><xsl:if
				test="$class != ''"><xsl:text> </xsl:text><xsl:value-of
				select="$class" /></xsl:if></xsl:variable>
		<xsl:variable name="context_int"><xsl:if test="$context != ''">,<xsl:value-of select="$context" /></xsl:if></xsl:variable>

		<xsl:choose>
			<xsl:when test="$icon_type = 'favorites' or $icon_type = 'compares'">
				<a href="javascript:;"
				   data-id="{$id}"
				   data-url="/skynet/ajax/shop/{$icon_type}/{$id}/"
				>
					<xsl:attribute name="data-context">#items_<xsl:value-of select="$icon_type" />_main,.def_<xsl:value-of select="$icon_type" />_<xsl:value-of select="$id" /><xsl:if
							test="$is_common">,.<xsl:value-of select="$icon_type" />_common_<xsl:value-of select="$id" />,.userbar_<xsl:value-of select="$icon_type" /></xsl:if><xsl:if test="$context != ''">,<xsl:value-of
							select="$context" /></xsl:if></xsl:attribute>
					<xsl:attribute name="class"><xsl:value-of select="$class_int" /> js-skynet-send-data <xsl:choose>
						<xsl:when test="not($is_common)"> def_<xsl:value-of select="$icon_type" />_<xsl:value-of
								select="$id" /></xsl:when>
						<xsl:otherwise><xsl:value-of select="$icon_type" />_common_<xsl:value-of select="$id" /></xsl:otherwise>
					</xsl:choose><xsl:if
							test="$data_checked = 1"> is-active</xsl:if></xsl:attribute>
					<!--			<xsl:value-of select="$fav_common" />-->
					<!--			<xsl:call-template name="icon_types_code">-->
					<!--				<xsl:with-param name="icon_type" select="$icon_type" />-->
					<!--				<xsl:with-param name="data_checked" select="$data_checked" />-->
					<!--				<xsl:with-param name="post_text_0" select="$post_text_0" />-->
					<!--				<xsl:with-param name="post_text_1" select="$post_text_1" />-->
					<!--			</xsl:call-template>-->
					<xsl:variable name="icon_name"><xsl:choose>
						<xsl:when test="$icon_type = 'compares'">compare</xsl:when>
						<xsl:when test="$icon_type = 'favorites'">heart</xsl:when>
					</xsl:choose></xsl:variable>
					<xsl:variable name="is_fill"><xsl:choose>
						<xsl:when test="$icon_name = 'heart'">-fill</xsl:when>
						<xsl:when test="$icon_name = 'compare'">-fill</xsl:when>
					</xsl:choose></xsl:variable>
					<span class="icon-{$icon_type}">
						<xsl:choose>
							<xsl:when test="$no_svg_icon"></xsl:when>
							<xsl:when test="$data_checked = 1">
								<svg>
									<use xlink:href="/assets/icons/icons.svg#{$icon_name}{$is_fill}"></use>
								</svg>
							</xsl:when>
							<xsl:otherwise>
								<svg>
									<use xlink:href="/assets/icons/icons.svg#{$icon_name}"></use>
								</svg>
							</xsl:otherwise>
						</xsl:choose>
					</span>
					<xsl:choose>
						<xsl:when test="$post_text_0 != '' and $data_checked = 0"><span><xsl:value-of select="$post_text_0" /></span></xsl:when>
						<xsl:when test="$post_text_1 != '' and $data_checked = 1"><span><xsl:value-of select="$post_text_1" /></span></xsl:when>
					</xsl:choose>
				</a>
			</xsl:when>
			<xsl:when test="$icon_type = 'little_cart' or $icon_type = 'cart'">
				<button class="{$class} js-skynet-send-data"
						type="button"
						data-action="remove"
						data-tmpl="{$data_template_small}"
						data-tmpl_cart="{$data_template_large}"
						data-context=".sky-little-cart:f,.sky-cart:f,.sky-little-cart{$context_int}"
						data-quantity="1"
						data-additional_class="open"
						data-id="{$id}"
						data-url="/skynet/ajax/shop/cart/{$id}/"
				>
					<svg>
						<xsl:choose>
							<xsl:when test="$icon_type = 'little_cart'">
								<use xlink:href="/assets/icons/icons.svg#x"></use>
							</xsl:when>
							<xsl:when test="$icon_type = 'cart'">
								<use xlink:href="/assets/icons/icons.svg#trash"></use>
							</xsl:when>
						</xsl:choose>
					</svg>
				</button>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="showPrice">
		<xsl:param name="price" select="'0'" />
		<xsl:param name="price_discount" select="'0'" />
		<xsl:param name="original_price" select="'0'" />
		<xsl:param name="wrapper_prefix" select="'good'" />

		<div class="{$wrapper_prefix}__prices">
			<div class="prices">
				<xsl:choose>
					<xsl:when test="$price_discount &gt; 0">
						<div class="prices__price"><xsl:value-of select="format-number($price, '### ##0', 'siteMoneyFormat')" /> ₽</div>
						<div class="prices__price-old"><xsl:value-of select="format-number($original_price, '#####0', 'siteMoneyFormat')" /> ₽</div>
					</xsl:when>
					<xsl:otherwise>
						<div class="prices__price"><xsl:value-of select="format-number($price, '### ##0', 'siteMoneyFormat')" /> ₽</div>
					</xsl:otherwise>
				</xsl:choose>

<!--				<xsl:value-of select="format-number($price, '#####0', 'siteMoneyFormat')" />-->
			</div>
		</div>

	</xsl:template>

</xsl:stylesheet>