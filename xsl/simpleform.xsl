<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:hostcms="http://www.hostcms.ru/"
				xmlns:zsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:string="http://symphony-cms.com/functions"
				exclude-result-prefixes="hostcms string">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>

	<xsl:template match="/form">
		<xsl:param name="h_level" select="'1'" />
		<xsl:param name="show_description" select="true()" />
		<xsl:param name="show_ok_message" select="false()" />

		<xsl:variable name="localCsrf"><xsl:choose>
			<xsl:when test="/*/sky_csrf != ''"><xsl:value-of select="/*/sky_csrf" /></xsl:when>
			<xsl:when test="/*/data_csrf != ''"><xsl:value-of select="/*/data_csrf" /></xsl:when>
		</xsl:choose></xsl:variable>
		<div class="form-content">
			<xsl:choose>
				<xsl:when test="$h_level = false()">
					<h1><xsl:value-of select="name" disable-output-escaping="yes" /></h1>
				</xsl:when>
				<xsl:when test="$h_level = false()">
					<h2><xsl:value-of select="name" disable-output-escaping="yes" /></h2>
				</xsl:when>
				<xsl:when test="$h_level = false()">
					<div><xsl:value-of select="name" disable-output-escaping="yes" /></div>
				</xsl:when>
				<xsl:otherwise>
					<div class="shop__headline h1" style="margin-bottom: 1.5rem"><xsl:value-of select="name" disable-output-escaping="yes" /></div>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$show_description and description != ''">
				<xsl:value-of select="description" disable-output-escaping="yes" />
			</xsl:if>
		</div>
		<form method="post" name="form{@id}" id="form{@id}" enctype="multipart/form-data"
			data-form_id="{@id}"
			data-csrf_key="{$localCsrf}"
			data-url="/skynet/process/sendform/"
			data-ajax="1"
			data-action="processform"
			data-usersend="1"
			data-xsl="{$formXSL}"
			data-form="1"
		>
			<xsl:if test="$show_ok_message">
				<xsl:attribute name="data-show_message">1</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="class"><xsl:choose>
				<xsl:when test="form_class != ''"><xsl:value-of select="form_class" /></xsl:when>
				<xsl:otherwise>form-h js-form</xsl:otherwise>
			</xsl:choose></xsl:attribute>
			<xsl:apply-templates select="form_field" mode="form_field" />
			<div class="form-h__group">
				<div class="form-h__label"></div>
				<div class="form-h__field">
					<button name="{button_name}"
							class="button form-h__submit"
							value="{button_value}"
							type="submit"
							data-form="1"
					>
						<xsl:value-of select="button_value" disable-output-escaping="yes" />
					</button>
					<xsl:call-template name="policy" />
				</div>
			</div>
		</form>
	</xsl:template>

	<xsl:template match="form_field" mode="form_field">
		<div class="form-h__group">
			<div class="form-h__label">
				<!-- если это чекбокс, то слева в label не выводим текст, текст выводится в самом чекбоксе -->
				<xsl:if test="type != 4">
					<xsl:if test="caption != 'Предупреждение'">
						<xsl:value-of select="caption" />
					</xsl:if>
					<xsl:if test="obligatory = 1">
						<font color="red">*</font>
					</xsl:if>
				</xsl:if>
			</div>
			<div class="form-h__field">
				<!-- Не скрытое поле и не надпись -->
				<xsl:if test="type != 7 and type != 8">
					<!-- Поле загрузки файла -->
					<xsl:if test="type = 2">
						<div class="dropzone js-dropzone" data-name="{name}"></div>
					</xsl:if>
					<!-- Текстовые поля -->
					<xsl:if test="type = 0 or type = 1">
						<xsl:choose>
							<xsl:when test="name = 'tel' or name = 'phone'">
								<div class="form-drop">
									<button class="form-drop__toggle js-form-drop-toggle js-dropdown" type="button">
										<img src="/templates/template26/img/flag/flag-ru.png" alt="" />
									</button>
									<input type="tel" name="{name}" class="form-drop__control form__control required js-code-tel cq-form-tel" title="Введите номер телефона" minlength="16" />
									<div class="form-drop__drop js-dropdown-sub">
										<label class="form-drop__item">
											<input type="radio" class="form-drop__control js-form-drop-control" name="code" value="+7" checked="checked" />
											<span class="form-drop__pic"><img src="/templates/template26/img/flag/flag-ru.png" alt="" /></span>
											<span class="form-drop__text">Россия <span>+7</span></span>
										</label>
										<label class="form-drop__item">
											<input type="radio" class="form-drop__control js-form-drop-control" name="code" value="+375" />
											<span class="form-drop__pic"><img src="/templates/template26/img/flag/flag-by.png" alt="" /></span>
											<span class="form-drop__text">Беларусь <span>+375</span></span>
										</label>
										<label class="form-drop__item">
											<input type="radio" class="form-drop__control js-form-drop-control" name="code" value="+7" />
											<span class="form-drop__pic"><img src="/templates/template26/img/flag/flag-kz.png" alt="" /></span>
											<span class="form-drop__text">Казахстан <span>+7</span></span>
										</label>
										<div class="form-drop__sep"></div>
										<label class="form-drop__item">
											<input type="radio" class="form-drop__control js-form-drop-control" name="code" value="+994" />
											<span class="form-drop__pic"><img src="/templates/template26/img/flag/flag-az.png" alt="" /></span>
											<span class="form-drop__text">Азербайджан <span>+994</span></span>
										</label>
										<label class="form-drop__item">
											<input type="radio" class="form-drop__control js-form-drop-control" name="code" value="+374" />
											<span class="form-drop__pic"><img src="/templates/template26/img/flag/flag-am.png" alt="" /></span>
											<span class="form-drop__text">Армения <span>+374</span></span>
										</label>
										<label class="form-drop__item">
											<input type="radio" class="form-drop__control js-form-drop-control" name="code" value="+995" />
											<span class="form-drop__pic"><img src="/templates/template26/img/flag/flag-ge.png" alt="" /></span>
											<span class="form-drop__text">Грузия <span>+995</span></span>
										</label>
										<label class="form-drop__item">
											<input type="radio" class="form-drop__control js-form-drop-control" name="code" value="+996" />
											<span class="form-drop__pic"><img src="/templates/template26/img/flag/flag-kg.png" alt="" /></span>
											<span class="form-drop__text">Кыргызстан <span>+996</span></span>
										</label>
										<label class="form-drop__item">
											<input type="radio" class="form-drop__control js-form-drop-control" name="code" value="+373" />
											<span class="form-drop__pic"><img src="/templates/template26/img/flag/flag-md.png" alt="" /></span>
											<span class="form-drop__text">Молдова <span>+373</span></span>
										</label>
										<label class="form-drop__item">
											<input type="radio" class="form-drop__control js-form-drop-control" name="code" value="+380" />
											<span class="form-drop__pic"><img src="/templates/template26/img/flag/flag-ua.png" alt="" /></span>
											<span class="form-drop__text">Украина <span>+380</span></span>
										</label>
										<label class="form-drop__item">
											<input type="radio" class="form-drop__control js-form-drop-control" name="code" value="+992" />
											<span class="form-drop__pic"><img src="/templates/template26/img/flag/flag-tj.png" alt="" /></span>
											<span class="form-drop__text">Таджикистан <span>+992</span></span>
										</label>
										<label class="form-drop__item">
											<input type="radio" class="form-drop__control js-form-drop-control" name="code" value="+998" />
											<span class="form-drop__pic"><img src="/templates/template26/img/flag/flag-uz.png" alt="" /></span>
											<span class="form-drop__text">Узбекистан <span>+998</span></span>
										</label>
									</div>
								</div>
							</xsl:when>
							<xsl:otherwise>
								<input type="text" name="{name}" value="{value}" size="{size}"
									   placeholder="{description}"
								>
									<xsl:choose>
										<!-- Поле для ввода пароля -->
										<xsl:when test="type = 1">
											<xsl:attribute name="type">password</xsl:attribute>
										</xsl:when>
										<!-- Текстовое поле -->
										<xsl:otherwise>
											<xsl:attribute name="type">text</xsl:attribute>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:attribute name="class">form-h__control<xsl:if
											test="obligatory = 1"> required</xsl:if><xsl:if
											test="name='email'"> js-email cq-form-email</xsl:if><xsl:if
											test="name='fio'"> cq-form-fio</xsl:if><xsl:if
											test="name='tirag'"> cq-form-tirag</xsl:if><xsl:if
											test="name='item_name'"> cq-form-item_name</xsl:if><xsl:if
											test="name='company-name'"> cq-form-company-name</xsl:if><xsl:if
											test="name='company-email'"> cq-form-company-email</xsl:if><xsl:if
											test="name='company-address'"> cq-form-company-address</xsl:if><xsl:if
											test="name='company-tel'"> cq-form-company-tel</xsl:if><xsl:if
											test="name='address'"> cq-form-address</xsl:if>
									</xsl:attribute>
									<xsl:if test="obligatory = 1">
										<xsl:attribute name="minlength">1</xsl:attribute>
										<xsl:attribute name="title">Заполните поле <xsl:value-of select="caption" /></xsl:attribute>
									</xsl:if>
								</input>
							</xsl:otherwise>
						</xsl:choose>

					</xsl:if>

					<!-- Радиокнопки -->
					<xsl:if test="type = 3 or type = 9">
						<xsl:apply-templates select="list/list_item" mode="skynet_form_list_item" />
						<label class="input_error" for="{name}" style="display: none">Выберите, пожалуйста, значение.</label>
					</xsl:if>

					<!-- Checkbox -->
					<xsl:if test="type = 4">
						<label class="checkbox" style="margin-top:8px;margin-bottom:8px;">
							<input type="checkbox" class="checkbox__input" name="{name}">
								<xsl:if test="checked = 1 or value = 1">
									<xsl:attribute name="checked">checked</xsl:attribute>
								</xsl:if>
							</input>
							<span class="checkbox__mark"></span>
							<span class="checkbox__text"><xsl:value-of select="caption"/><xsl:if
									test="obligatory = 1"><font color="red">*</font></xsl:if></span>
						</label>
					</xsl:if>

					<!-- Textarea -->
					<xsl:if test="type = 5">
						<textarea name="{name}" cols="{cols}" rows="{rows}" wrap="off">
							<xsl:attribute name="class">form-h__control<xsl:if
									test="obligatory = 1"> required</xsl:if><xsl:if
									test="name = 'message'"> cq-form-message</xsl:if></xsl:attribute>
							<xsl:if test="obligatory = 1">
								<xsl:attribute name="minlength">1</xsl:attribute>
								<xsl:attribute name="title">Заполните поле <xsl:value-of select="caption" /></xsl:attribute>
							</xsl:if>
							<xsl:value-of select="value" />
						</textarea>
					</xsl:if>

					<!-- Список -->
					<xsl:if test="type = 6">
						<select name="{name}">
							<xsl:if test="obligatory = 1">
								<xsl:attribute name="class">form__control required</xsl:attribute>
								<xsl:attribute name="title">Заполните поле <xsl:value-of select="caption" /></xsl:attribute>
							</xsl:if>
<!--							<option value="">...</option>-->
							<xsl:apply-templates select="list/list_item" mode="skynet_form_list_item" />
						</select>
					</xsl:if>
				</xsl:if>

				<!-- скрытое поле -->
				<xsl:if test="type = 7">
					<input type="hidden" name="{name}" value="{value}" />
				</xsl:if>

				<!-- Надпись -->
				<xsl:if test="type = 8">
					<div class="warn">
						<div class="warn__text"><xsl:value-of select="caption" /></div>
					</div>
				</xsl:if>
<!--			<input type="text" name="number" class="form-h__control required" title="Введите номер заказа" />-->
			</div>
		</div>
	</xsl:template>

	<!-- Формируем радиогруппу или выпадающий список -->
	<xsl:template match="list/list_item" mode="skynet_form_list_item">
		<xsl:choose>
			<xsl:when test="../../type = 3">
				<input id="{../../name}_{@id}" type="radio" name="{../../name}" value="{value}">
					<xsl:if test="value = ../../value">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:if>
					<xsl:if test="../../obligatory = 1">
						<xsl:attribute name="class">required</xsl:attribute>
						<xsl:attribute name="minlength">1</xsl:attribute>
						<xsl:attribute name="title">Заполните поле <xsl:value-of select="caption" /></xsl:attribute>
					</xsl:if>
				</input><xsl:text> </xsl:text>
				<label for="{../../name}_{@id}"><xsl:value-of disable-output-escaping="yes" select="value" /></label>
				<br/>
			</xsl:when>
			<xsl:when test="../../type = 6">
				<option value="{value}">
					<xsl:if test="value = ../../value">
						<xsl:attribute name="selected">selected</xsl:attribute>
					</xsl:if>
					<xsl:value-of disable-output-escaping="yes" select="value" />
				</option>
			</xsl:when>
			<xsl:when test="../../type = 9">
				<xsl:variable name="currentValue" select="@id" />
				<input id="{../../name}_{@id}" type="checkbox" name="{../../name}_{@id}" value="{value}">
					<xsl:if test="../../values[value=$currentValue]/node()">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:if>
					<xsl:if test="../../obligatory = 1">
						<xsl:attribute name="class">required</xsl:attribute>
						<xsl:attribute name="minlength">1</xsl:attribute>
						<xsl:attribute name="title">Заполните поле <xsl:value-of select="caption" /></xsl:attribute>
					</xsl:if>
				</input><xsl:text> </xsl:text>
				<label for="{../../name}_{@id}"><xsl:value-of disable-output-escaping="yes" select="value" /></label>
				<br/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="policy">
		<div class="form-h__term">
			Нажимая кнопку "Отправить", я даю <a href="{$policyHref}" class="js-modal" data-src="#modal-privacy">согласие на обработку своих персональных данных</a>
		</div>
	</xsl:template>
</xsl:stylesheet>
