<?php
use Skynetcore_Utils as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Dadata services command controller.
 *
 * @version 6.x
 * @copyright © MikeBorisov
 */
class Skynetcore_Dadata_Controller extends Core_Command_Controller
{
	public function showAction()
	{
		$count_results = 10;
		/** @var Core_Page $instance */
		$instance = Core_Page::instance();
		$oCore_Response = new Core_Response();
		$oCore_Response
			->header('Content-type', 'application/json; charset=utf-8')
			->header('Pragma', 'no-cache')
			->header('Cache-Control', 'private, no-cache')
			->header('Vary', 'Accept')
			->header('Last-Modified', gmdate('D, d M Y H:i:s', time()) . ' GMT')
			->header('X-Powered-By', 'Skynetcore_Module');
		$instance->response($oCore_Response);
		$aResult = array(
			'status' => 'ERROR',
			'message' => 'Неизвестная ошибка',
		);
		try {
			if(Core_Array::getRequest('csrf') != utl::getSimpleToken()) {
				throw new \Exception('Невозможно');
			}
			$service = new \Dadata\DadataClient(
				Core_Page::instance()->skynet->config['integration']['dadata']['key'],
				Core_Page::instance()->skynet->config['integration']['dadata']['secret']
			);
			$query = Core_Array::getRequest('query');
			$aResult['query'] = $query;
			switch($this->service) {
				case 'inn-complex':
					$aSuggestions = $service->suggest("party", $query, $count_results);
//					Skynetcore_Utils::p($aSuggestions);
					$aSuggestionsReturn = array_map(function ($a) {
						$suggestionData = json_decode(json_encode($a['data']));
						return [
							'value' => trim(Core_Array::get($a['data'], 'inn', '').' '.Core_Array::get($a, 'value', '')),
							'object' => $suggestionData,
						];
					}, $aSuggestions);

					$aResult['suggestions'] = $aSuggestionsReturn;
					$aResult['status'] = 'OK';
					$aResult['message'] = 'OK';
					break;
				case 'fio':
					$aSuggestions = $service->suggest("fio", $query, $count_results);
					$aSuggestionsReturn = array_map(function ($a) {
						$suggestionData = json_decode(json_encode($a['data']));
						return [
							'value' => implode(' ', [$suggestionData->surname, $suggestionData->name, $suggestionData->patronymic]),
							'object' => $suggestionData,
						];
					}, $aSuggestions);

					$aResult['suggestions'] = $aSuggestionsReturn;
					$aResult['status'] = 'OK';
					$aResult['message'] = 'OK';
					break;
				case 'address':
					$aSuggestionsReturn = $aAddress = [];
					$aSuggestions = $service->suggest("address", $query, $count_results);
					foreach ($aSuggestions as $suggestion) {
						$addr = [];
						$addrId = [];
						$aSuggestionData = Core_Array::get($suggestion, 'data', false);
						if(is_array($aSuggestionData) && count($aSuggestionData)) {
							$suggestionData = json_decode(json_encode($aSuggestionData));
							$aCountries = Core_Entity::factory('Shop_Country')->getAllByName($suggestionData->country);
							$oCountry = Core_Array::get($aCountries, 0, json_decode(json_encode(['id' => 0])));
							$oLocation = new stdClass();
							$oLocation->id = 0;
							$oLocation->name = 'Undefined';
							$oCity = new stdClass();
							$oCity->id = 0;
							$oCity->name = 'Undefined';
							if($oCountry instanceof Shop_Country_Model) {
								$locationName = $suggestionData->region_type_full . ' ' . $suggestionData->region;
								$revLocationName = $suggestionData->region . ' ' . $suggestionData->region_type_full;
								$qLocations = $oCountry->shop_country_locations;
								$qLocations
									->queryBuilder()
									->where('name', 'IN', [$locationName, $revLocationName])
								;
								$aLocations = $qLocations->findAll(false);
								if(count($aLocations) == 0) {
									$oLocation = Core_Entity::factory('Shop_Country_Location');
									$oLocation->shop_country_id = $oCountry->id;
									$oLocation->name = $revLocationName;
									$oLocation->save();
								} else {
									$oLocation = $aLocations[0];
								}
							}

							if($oLocation instanceof Shop_Country_Location_Model) {
								$Shop_Country_Location_CityName = $suggestionData->city == '' ? $suggestionData->settlement_with_type : $suggestionData->city;
								$qCities = $oLocation->shop_country_location_cities;
								$qCities
									->queryBuilder()
									->where('name', 'IN', [$Shop_Country_Location_CityName])
								;
								$aCities = $qCities->findAll(false);
								if(count($aCities) == 0) {
									$oCity = Core_Entity::factory('Shop_Country_Location_City');
									$oCity->shop_country_location_id = $oLocation->id;
									$oCity->name = $Shop_Country_Location_CityName;
									$oCity->save();
								} else {
									$oCity = $aCities[0];
								}
							}

							$addr[] = [
								'infull' => false,
								'type' => 'shop_country_id',
								'value' => $suggestionData->country,
								'id' => $oCountry->id
							];
							$addr[] = [
								'type' => 'postcode',
								'value' => $suggestionData->postal_code != '' ? $suggestionData->postal_code : '',
							];
							if($suggestionData->region != '') {
								$addr[] = [
									'type' => 'shop_country_location_id',
									'value' => trim("{$suggestionData->region_type} {$suggestionData->region}"),
									'id' => $oLocation->id, //-- $oCity->shop_country_location->id,
								];
							}
							if($suggestionData->city != '') {
								$addr[] = [
									'type' => 'shop_country_location_city_id',
									'value' => trim("{$suggestionData->city_type} {$suggestionData->city}"),
									'id' => $oCity->id,
								];
							}
							if($suggestionData->area != '') {
								$addr[] = [
									'type' => 'ar',
									'value' => trim("{$suggestionData->area_type} {$suggestionData->area}"),
								];
							}
							if($suggestionData->settlement != '') {
								$addr[] = [
									'type' => 'shop_country_location_city_id',
									'value' => trim("{$suggestionData->settlement_type} {$suggestionData->settlement}"),
									'id' => $oCity->id,
								];
							}
							if(($streetValue = trim("{$suggestionData->street_type} {$suggestionData->street}")) != ''
								|| $suggestionData->house_type_full != ''
							) {
								$addr[] = [
									'type' => 'address',
									'value' => trim(($streetValue != '') ? $streetValue : $suggestionData->house_type_full),
								];
							}
							if($suggestionData->house_type != '') {
								$addr[] = [
									'type' => 'house',
									'value' => trim("{$suggestionData->house_type} {$suggestionData->house}"),
								];
							}
							if($suggestionData->flat_type != '') {
								$addr[] = [
									'type' => 'flat',
									'value' => trim("{$suggestionData->flat_type} {$suggestionData->flat}"),
								];
							}
						}
						$aAddress[] = $addr;
						$aAddressIDs[] = $addrId;
					}
					if(is_array($aAddress)) {
						$aSuggestionsReturn = array_map(function ($a) {
							$asAddr = [];
							foreach ($a as $v) {
								if(Core_Array::get($v, 'infull', true)) {
									$asAddr[] = trim($v['value'], ', ');
								}
							}
							return [
								'value' => implode(', ', $asAddr),
								'object' => $a,
							];
						}, $aAddress);
					}
					$aResult['suggestions'] = $aSuggestionsReturn;
					$aResult['status'] = 'OK';
					$aResult['message'] = 'OK';
					break;
				default:
					throw new \Exception('Неизвестный сервис');
			}
			$oCore_Response
				->status(200)
				->body(json_encode($aResult));
		} catch (\Exception $e) {
			$aResult['message'] = $e->getMessage();
			$oCore_Response
				->status(404)
				->body(json_encode($aResult));
		}
		return $oCore_Response;
	}
}