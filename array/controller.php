<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');

use Skynetcore_Utils as utl;
use KubAT\PhpSimple\HtmlDomParser;

class Skynetcore_Array_Controller extends Skynetcore_Core_Servant_Properties
{
	public static function applyKeysOnIndexes($arInput, $keyField = 'id') {
		$arOutput = [];
		if(!is_array($arInput)) {
			$arInput = [$arInput[$keyField]];
		}
		foreach ($arInput as $inputValue) {
			switch (true) {
				case is_object($inputValue) && isset($inputValue->$keyField):
					$arOutput[$inputValue->$keyField] = $inputValue;
					break;
				case is_array($inputValue) && isset($inputValue[$keyField]):
					$arOutput[$inputValue[$keyField]] = $inputValue;
					break;
			}
		}
		return $arOutput;
	}
}