<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Browser_Info extends Skynetcore_Core_Servant_Properties {

	/**
	 * Allowed object properties
	 * @var array
	 */
	protected $_allowedProperties = array(
//		'mobile',
//		'realmobile',
//		'w',
//		'h',
		'devicetype',
		'user_agent',
	);

	protected $_tagName = 'sky_browser';

	/**
	 * Utils_Browser_Info constructor.
	 * @param array $_allowedProperties
	 */
	public function __construct()
	{
		parent::__construct();

		$detect = new \Detection\MobileDetect();
		$this->devicetype = 'desktop';
		$detect->isMobile() && $this->devicetype = 'mobile';
		$detect->isTablet() && $this->devicetype = 'tablet';

		if(isset($_SERVER) && is_array($_SERVER)) {
			$this->user_agent = Core_Array::get($_SERVER, 'HTTP_USER_AGENT', 'undefined');
		}
	}
}