<?php
//use Gam6itko\GoodsSeller\Service\V1\CategoriesService;
use GuzzleHttp\Client as GuzzleClient;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use Http\Factory\Guzzle\RequestFactory;
use Http\Factory\Guzzle\StreamFactory;
//use Gam6itko\GoodsSeller\Service\V2 as psV2;
//use Gam6itko\GoodsSeller\Service\V2\ProductService;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Skynetcore_Mp_Goods_Controller
 */
class Skynetcore_Mp_Goods_Controller extends Skynetcore_Mp_Controller
{
//	public static function writeCategoriesToDB($categories, $parent_id=0) {
//		foreach ($categories as $category) {
//			$localSaveItem = false;
//			/** @var Skynetcore_Mp_Goods_Category_Model $dbCategory */
//			$dbCategory = Core_Entity::factory('Skynetcore_Mp_Goods_Category')->getById($category['category_id'], false);
//			if(!(isset($dbCategory->id) && $dbCategory->id > 0)) {
//				$dbCategory = Core_Entity::factory('Skynetcore_Mp_Goods_Category');
//				$dbCategory->id = $category['category_id'];
//				$dbCategory->parent_id = $parent_id;
//				$dbCategory->title = '';
//				$dbCategory = $dbCategory->create();
//			}
//			if($parent_id != $dbCategory->parent_id) {
//				$dbCategory->parent_id = $parent_id;
//				$localSaveItem = true;
//			}
//			if($category['title'] != $dbCategory->title) {
//				$dbCategory->title = $category['title'];
//				$localSaveItem = true;
//			}
//			$localSaveItem && $dbCategory = $dbCategory->save();
//			if(isset($category['children'])
//				&& is_array($category['children'])
//				&& count($category['children']) > 0
//			) {
//				self::writeCategoriesToDB($category['children'], $dbCategory->id);
//			}
//		}
//	}

//	public static function syncCategories() {
//		$instance = Core_Page::instance();
//
//		$client = new GuzzleAdapter(new GuzzleClient());
//		$requestFactory = new RequestFactory();
//		$streamFactory = new StreamFactory();
//		$adapter = new GuzzleAdapter(new GuzzleClient());
//		/** @var \Gam6itko\GoodsSeller\Service\V2\CategoryService $svcCategories */
//		$svcCategories = new psV2\CategoryService(
//			$instance->skynet->config['integration']['Goods'][$instance->skynet->request->envType],
//			$adapter,
//			$requestFactory,
//			$streamFactory
//		);
//		$aCategoriesTree = $svcCategories->tree(0);
//		self::writeCategoriesToDB($aCategoriesTree);
//	}

//	public static function syncItems() {
//		$instance = Core_Page::instance();
//		$propertyIdToUnload = $instance->skynet->config['integration']['Goods']['properties']['id_to_upload'];
//
//		/** @var Core_QueryBuilder_Select $qItemsToUploadTmp */
//		$qItemsToUploadTmp = Core_QueryBuilder::select('si.id')
//			->select('si.marking')
//			->select('si.name')
//			->from(['getGroupsIerarchyUp', 'giu'])
//			->join(['shop_items', 'si'], 'si.id', '=', 'giu.id')
//			->leftJoin(['shop_items', 'siparent'], 'siparent.id', '=', 'si.modification_id')
//			->leftJoin(['property_value_ints', 'items_to_unload'], 'items_to_unload.entity_id', '=', Core_QueryBuilder::expression('COALESCE(siparent.id, si.id)'), [
//				['AND' => ['items_to_unload.property_id', '=', $propertyIdToUnload]]
//			])
//			->where(Core_QueryBuilder::expression('COALESCE(items_to_unload.value, 0)'), '=', 1)
////			->where('si.marking', 'IN', ['1060214458970'])
////			->where('si.marking', 'IN', ['1099927410003', '1042270584992', '1042560680498', '1060127126159','1057198985159'])
//		;
//		$aItemsToUploadTmp = $qItemsToUploadTmp->asAssoc()->execute()->result();
//		if(count($aItemsToUploadTmp)) {
//			$client = new GuzzleAdapter(new GuzzleClient());
//			$requestFactory = new RequestFactory();
//			$streamFactory = new StreamFactory();
//			$adapter = new GuzzleAdapter(new GuzzleClient());
//			/** @var \Gam6itko\GoodsSeller\Service\V2\ProductService $svcProduct */
//			$svcProduct = new psV2\ProductService($instance->skynet->config['integration']['Goods'][$instance->skynet->request->envType],
//				$adapter,
//				$requestFactory,
//				$streamFactory
//			);
//			$aChunkedRecords = array_chunk($aItemsToUploadTmp, 999);
//			$aOffers = [];
//			foreach ($aChunkedRecords as $chunkedRecord => $aChunkedRecord) {
//				$aFilterRecords = [];
//				foreach ($aChunkedRecord as $oChunkedRecord) {
//					$aFilterRecords[$oChunkedRecord['marking']] = $oChunkedRecord;
//				}
////				$aListTmp = $svcProduct->list(
////					[
////						'filter' => [
////							'offer_id' => array_keys($aFilterRecords)
////						]
////					]
////				);
//				$aListTmp = $svcProduct->infoList(
//					[
//						"offer_id" => array_keys($aFilterRecords),
//						"product_id" => [ ],
//						"sku" => [ ]
//					]
//				);
//				if(isset($aListTmp['items']) && is_array($aListTmp['items']) && count($aListTmp['items'])) {
//					foreach ($aListTmp['items'] as $listTmp) {
//						$aOffers[$listTmp['offer_id']] = $listTmp;
//					}
//				}
//				usleep(0.3 * 1000000);
//			}
//			foreach ($aItemsToUploadTmp as $itemsToUploadTmp) {
//				/** @var Skynetcore_Mp_Goods_Item_Model $tryMpGoodsItemTmp */
//				$tryMpGoodsItemTmp = Core_Entity::factory('Skynetcore_Mp_Goods_Item');
////				$tryMpGoodsItemTmp
////					->queryBuilder()
////					->where('shop_item_id', '=', $itemsToUploadTmp['id'])
////				;
//				$GoodsOfferId = '';
//				$GoodsOffer = false;
//				if(isset($aOffers[$itemsToUploadTmp['marking']]['id'])) {
//					$GoodsOfferId = $aOffers[$itemsToUploadTmp['marking']]['id'];
//					$GoodsOffer = $aOffers[$itemsToUploadTmp['marking']];
//				}
//				$tryMpGoodsItem = $tryMpGoodsItemTmp
//					->getByShop_item_id($itemsToUploadTmp['id'], false);
////				Skynetcore_Utils::p($tryMpGoodsItemTmp->queryBuilder()->build(), $GoodsOfferId);
//				if(!(isset($tryMpGoodsItem->id) && $tryMpGoodsItem->id > 0)) {
//					$tryMpGoodsItem = Core_Entity::factory('Skynetcore_Mp_Goods_Item');
//					$tryMpGoodsItem->shop_item_id = $itemsToUploadTmp['id'];
//					$tryMpGoodsItem->create();
//				}
//				if(isset($GoodsOffer['id']) && $GoodsOffer['id'] != '') {
//					$tryMpGoodsItem->product_id = $GoodsOfferId;
//					$tryMpGoodsItem->state = $GoodsOffer['status']['state'];
//					$tryMpGoodsItem->state_failed = $GoodsOffer['status']['state_failed'];
//					$tryMpGoodsItem->validation_state = $GoodsOffer['status']['validation_state'];
//					$tryMpGoodsItem->state_name = $GoodsOffer['status']['state_name'];
//					$tryMpGoodsItem->state_description = $GoodsOffer['status']['state_description'];
//					$tryMpGoodsItem->is_failed = $GoodsOffer['status']['is_failed']*1;
//					$tryMpGoodsItem->is_created = $GoodsOffer['status']['is_created']*1;
//					$tryMpGoodsItem->item_errors = json_encode(Core_Array::get($GoodsOffer['status'], 'item_errors', []), JSON_UNESCAPED_UNICODE);
//					$tryMpGoodsItem->state_updated_at = Core_Date::timestamp2sql(strtotime($GoodsOffer['status']['state_updated_at']));
//					$tryMpGoodsItem->save();
//				}
//			}
//		}
//	}

	public function syncStocks() {
		$instance = Core_Page::instance();
		$envType = $instance->skynet->request->envType;
		$GoodsConfig = $instance->skynet->config['integration']['goods'][$envType];

		$qbCurrentChangedID = Core_QueryBuilder::select([Core_QueryBuilder::expression('MAX(id)'), 'max_id'])
			->from('skynetcore_history_shop_warehouse_items');
		$currentChangedID = Core_Array::get($qbCurrentChangedID->asAssoc()->execute()->result(false), 0, ['max_id' => 0])['max_id'];

		/** @var Skynetcore_Mp_Goods_History_Model $qbLastChangedID */
		$qbLastChangedID = Core_Entity::factory('Skynetcore_Mp_Goods_History');
		$qbLastChangedID
			->queryBuilder()
			->limit(1)
			->orderBy('id', 'DESC')
		;
		$lastChangedID = Core_Array::get($qbLastChangedID->findAll(false), 0, json_decode('{"last_warehouse_history_id": 0}'));

		$qbLastChanged = Core_QueryBuilder::select('swi.*')
			->select([Core_QueryBuilder::expression("MAX(sh.id)"), 'datawarehouse_sh_id'])
			->select([Core_QueryBuilder::expression("COALESCE(sw.name, '')"), 'datawarehouse_name'])
			->select([Core_QueryBuilder::expression("COALESCE(si.marking, '')"), 'dataitem_marking'])
			->select([Core_QueryBuilder::expression("COALESCE(si.name, '')"), 'dataitem_name'])
			->from(['skynetcore_history_shop_warehouse_items', 'sh'])
			->join(['shop_warehouse_items', 'swi'], 'swi.id', '=', 'sh.shop_warehouse_item_id')
			->join(['shop_warehouses', 'sw'], 'sw.id', '=', 'swi.shop_warehouse_id')
			->join(['shop_items', 'si'], 'si.id', '=', 'sh.shop_item_id')
			->join(['getGroupsIerarchyUp', 'giu'], 'si.id', '=', 'giu.id')
			->join(['property_value_ints', 'pvigoods'], 'pvigoods.entity_id', '=','giu.id',[
				['AND' => ['pvigoods.property_id', '=', 1862]]
			])
			->where('sh.id', '>', $lastChangedID->last_warehouse_history_id*1)
			->where('swi.shop_warehouse_id', 'IN', array_keys(Moto_Controller_Market_Goods::$_active_warehouses)) // $instance->skynet->config['active_warehouses'])
			->where('swi.count', '>=', 0)
			->where('pvigoods.value', '=', 1)
			->where('sh.date', '>', Core_QueryBuilder::expression('NOW() - INTERVAL 2 DAY'))
//			->where('sh.date', '>', Core_Date::timestamp2date(time()).' 00:00:00')
//			->where('si.marking', 'IN', ["1058417618111", "1039019778171", "4041320932047", "4041100313816", "4041745616252",
//				"1039654920663", "1039203441721"])
//			->where('si.marking', 'IN', ["1042571591600", "1039019778171"])
//			->where('si.marking', 'IN', ['1099927410003','1099046475794','1099108194523'])
			->groupBy('swi.id')
			->orderBy(Core_QueryBuilder::expression('MAX(sh.id)'), 'DESC')
		;
//		$qbLastChanged->open();
//		//-- Ограничение по складу и по разделу
//		//-- ... AND ( giu.dirpath REGEXP '^/bikes/' AND swi.shop_warehouse_id IN (19) OR giu.dirpath NOT REGEXP '^/bikes/' )
//		$qbLastChanged
//			->open()
//			->where('giu.dirpath', 'REGEXP', '^/bikes/')
//			->where('swi.shop_warehouse_id', 'IN', [19])
//			->setOr()
//			->where('giu.dirpath', 'NOT REGEXP', '^/bikes/')
//			->close()
//		;
//		//-- Ограничение по складу и по разделу
//		//-- ... AND ( giu.dirpath REGEXP '^/sup-boards/' AND swi.shop_warehouse_id IN (19, 20) OR giu.dirpath NOT REGEXP '^/sup-boards/' )
//		$qbLastChanged
//			->open()
//			->where('giu.dirpath', 'REGEXP', '^/sup-boards/')
//			->where('swi.shop_warehouse_id', 'IN', [19, 20])
//			->setOr()
//			->where('giu.dirpath', 'NOT REGEXP', '^/sup-boards/')
//			->close()
//		;
//		$qbLastChanged->close();
//		Skynetcore_Utils::p($qbLastChanged->build()); die();

		$aLastChanged = $qbLastChanged->asObject('Shop_Warehouse_Item_Model')->execute()->result(false);

		$lastChangedRecords = $lastChangedRecordsTmp = [];
		foreach ($aLastChanged as $lastChangedTmp) {
			$lastChangedRecordsTmp[$lastChangedTmp->dataitem_marking][] = $lastChangedTmp->count - $lastChangedTmp->moto_reserve_count;
		}
		foreach ($lastChangedRecordsTmp as $lastChangedRecordItem_id => $aLastChangedRecord) {
			$lastChangedRecords[$lastChangedRecordItem_id] = array_sum($aLastChangedRecord);
		}
		$textResult = 'none';
//		Skynetcore_Utils::p($qbLastChanged->build()); die();
		if(count($lastChangedRecords)) {
			$aChunkedRecords = array_chunk(array_keys($lastChangedRecords), 299);
			$aOffers = [];
			foreach ($aChunkedRecords as $chunkedRecord => $aChunkedRecord) {
				foreach ($aChunkedRecord as $chunkedMarking) {
//					Skynetcore_Utils::p($lastChangedRecords[$chunkedMarking], $chunkedMarking);
					if($lastChangedRecords[$chunkedMarking] >= 0) {
						$aOffers[$chunkedRecord][] = [
							"offerId" => $chunkedMarking.'',
							"quantity" => $lastChangedRecords[$chunkedMarking],
						];
					}
				}
			}
			$apiHref = $instance->skynet->config['integration']['goods'][$instance->skynet->request->envType]['apihref_merch'];
			if(count($aOffers)) {
				foreach ($aOffers as $offerKey => $offerChunked) {
					$offerStocksToSend = [
						"meta" => [],
						"data" => [
							"token" => $instance->skynet->config['integration']['goods'][$instance->skynet->request->envType]['auth_token'],
							"stocks" => $offerChunked
						],
					];
//					Skynetcore_Utils::p(json_encode($offerStocksToSend));
					/** @var GuzzleClient $client */
					$client = new GuzzleClient([
						'headers' => [ 'Content-Type' => 'application/json' ]
					]);
					$response = $client->post($apiHref.'/offerService/stock/update', [
						GuzzleHttp\RequestOptions::JSON => $offerStocksToSend
					]);
//					Skynetcore_Utils::p($response->getBody()->getContents(), 'X-'.$apiHref);
					usleep(0.3 * 1000000);
				}
			}
		}
		echo Core_Date::timestamp2sql(time()) . ': ' . $textResult . "\n";

	}
}