<?php
//use Gam6itko\OzonSeller\Service\V1\CategoriesService;
use GuzzleHttp\Client as GuzzleClient;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use Http\Factory\Guzzle\RequestFactory;
use Http\Factory\Guzzle\StreamFactory;


// Работа с API Ozon
//use Modules\Skynetcore\Mp\Wb\WbApiConnect;
//use Modules\Skynetcore\Mp\Wb\WbApiConnect;
//use Skynetcore\Mp\Wb\WbApiConnect;



// Работа с API Ozon
use Gam6itko\OzonSeller\Service\V2 as psV2;
use Gam6itko\OzonSeller\Service\V3 as psV3;

require_once(CMS_FOLDER . 'modules/skynetcore/mp/wb/WbApiConnect.php');


defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Skynetcore_Mp_Wb_Controller
 */
class Skynetcore_Mp_Wb_Controller extends Skynetcore_Mp_Controller
{
	public static function writeCategoriesToDB($categories, $parent_id=0) {
		foreach ($categories as $category) {
			$localSaveItem = false;
			/** @var Skynetcore_Mp_Wb_Category_Model $dbCategory */
			$dbCategory = Core_Entity::factory('Skynetcore_Mp_Wb_Category')->getById($category['category_id'], false);
			if(!(isset($dbCategory->id) && $dbCategory->id > 0)) {
				$dbCategory = Core_Entity::factory('Skynetcore_Mp_Wb_Category');
				$dbCategory->id = $category['category_id'];
				$dbCategory->parent_id = $parent_id;
				$dbCategory->title = '';
				$dbCategory = $dbCategory->create();
			}
			if($parent_id != $dbCategory->parent_id) {
				$dbCategory->parent_id = $parent_id;
				$localSaveItem = true;
			}
			if($category['title'] != $dbCategory->title) {
				$dbCategory->title = $category['title'];
				$localSaveItem = true;
			}
			$localSaveItem && $dbCategory = $dbCategory->save();
			if(isset($category['children'])
				&& is_array($category['children'])
				&& count($category['children']) > 0
			) {
				self::writeCategoriesToDB($category['children'], $dbCategory->id);
			}
		}
	}




    public static function addMainImgToCard() {

        $entity_id = 557169; // zzzzzzzz получать через параметр

        $instance = Core_Page::instance();
        $shop_id = $instance->skynet->config['integration']['wb']['shop']['id'];


        $structure = []; // Всё храним в этой структуре

// Данные из карточки товара
        $qItems = Core_QueryBuilder::select('marking')->select('image_large')

            ->from(['shop_items'])
            ->where('id', '=', $entity_id)
            ->limit(0, 1);
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        $structure['entity_data'] = $aItems[0];

// Формируем путь до картинки
        $photo_url = $entity_id;
        if($entity_id < 10){
            $photo_url = "00" . $photo_url;
        } elseif($entity_id < 100) {
            $photo_url = "0" . $photo_url;
        }
        $photo_url = substr($photo_url, 0, 3);
        $photo_url = implode("/", str_split($photo_url));
        $photo_url = "/upload/shop_" . $shop_id . "/" . $photo_url . "/item_" . $entity_id . "/" . $structure['entity_data']['image_large'];

// Добавление главной картинки в КТ
        $vendor_code    = $structure['entity_data']['marking'];
        $photo_number   = 1;
        $photo_url      = "/upload/shop_3/5/5/7/item_557169/item_557169.jpg";
        $a = new Skynetcore_Mp_Wb_WbApiConnect($instance);
        $allCharacteristicsInDb = [];
        $object_characteristics = $a->media_file($vendor_code, $photo_number, $photo_url);

echo "object_characteristics = " . "<br>\n";
echo json_encode($object_characteristics, JSON_UNESCAPED_UNICODE) . "<br>\n";

    }

    public static function createCards() {


        $entity_id = 557169; // zzzzzzzz получать через параметр

        $instance = Core_Page::instance();
        $shop_id = $instance->skynet->config['integration']['wb']['shop']['id'];


        $structure = []; // Всё храним в этой структуре


// Данные из карточки товара
        $qItems = Core_QueryBuilder::select('id')
            ->select('shop_group_id')->select('name')->select('marking')->select('price')->select('cache_url')
            ->select('description')->select('text')

            ->from(['shop_items'])
            ->where('id', '=', $entity_id)
            ->limit(0, 1);
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        $structure['entity_data'] = $aItems[0];

        // В описании режем html теги
        $structure['entity_data']['text'] = preg_replace('/\<[^\>]+\>/', '', $structure['entity_data']['text']);
        $structure['entity_data']['text'] = preg_replace('/[\n\r]+/', ' ', $structure['entity_data']['text']);

// Бренд
        $qItems = Core_QueryBuilder::select('name')
            ->from(['shop_groups'])
            ->where('id', '=', $structure['entity_data']['shop_group_id'])
            ->limit(0, 1);
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        $structure['entity_data']['group_name'] = $aItems[0]['name'];


echo "structure['entity_data']['group_name'] = " . $structure['entity_data']['group_name'] . "<br>\n";
        


// Дерево категорий
        $qItems = Core_QueryBuilder::select('dirpath_ids')
            ->from(['shop_group_queries'])
            ->where('group_id', '=', $structure['entity_data']['shop_group_id'])
            ->limit(0, 1);
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        $structure['dirpath_ids'] = array_reverse(explode("/", $aItems[0]['dirpath_ids']));


// Находим название категорий в WB (objectName)
        $qItems = Core_QueryBuilder::select('objectName')->select('group_id')
            ->from(['skynetcore_mp_wb_categories'])
            ->where('group_id', 'IN', $structure['dirpath_ids'])
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        foreach ($aItems as $item) {
            $structure['wb_objectName_ot_group_id'][$item['group_id']] = $item['objectName'];
        }


// Находим самую ближайшую категорию
        foreach ($structure['dirpath_ids'] as $item) {
            if (!empty($structure['wb_objectName_ot_group_id'][$item]) && empty($structure['wb_objectName'])) {
                $structure['wb_objectName'] = $structure['wb_objectName_ot_group_id'][$item];
                $structure['group_id']      = $item;
            }
        }

// Находим id характеристик, отвечающих за размер в WB
        $qItems = Core_QueryBuilder::select('group_id')->select('property_id')
            ->from(['skynetcore_mp_wb_category_sizes_property'])
            ->where('group_id', 'IN', $structure['dirpath_ids'])
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        foreach ($aItems as $item) {
            $structure['properties_id_to_wb_size'][$item['group_id']] = $item['property_id'];
        }

// Берем id характеристики, отвечающих за размер в WB из самой ближайшей категории
        $structure['property_id_to_wb_size'] = "";
        foreach ($structure['dirpath_ids'] as $item) {
            if (!empty($structure['properties_id_to_wb_size'][$item]) && empty($structure['property_id_to_wb_size'])) {
                $structure['property_id_to_wb_size'] = $structure['properties_id_to_wb_size'][$item];
            }
        }


// Находим названия свойств в WB
        $qItems = Core_QueryBuilder::select('name')->select('property_id')->select('maxCount')
            ->from(['skynetcore_mp_wb_category_characteristics'])
            ->where('objectName', '=', $structure['wb_objectName'])
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        foreach ($aItems as $item) {

            if ($item['name'] == "Наименование") {      $item['property_id'] = "name";}
            if ($item['name'] == "Бренд") {             $item['property_id'] = "brand";}
            if ($item['name'] == "Описание") {          $item['property_id'] = "description";}

            if ($item['name'] == "Высота упаковки") {   $item['property_id'] = "packing_height";}
            if ($item['name'] == "Длина упаковки") {    $item['property_id'] = "packing_length";}
            if ($item['name'] == "Ширина упаковки") {   $item['property_id'] = "packing_width";}

            if ($item['property_id']) {
                $structure['wb_property_name_ot_property_id'][$item['property_id']]['name'] = $item['name'];
                $structure['wb_property_name_ot_property_id'][$item['property_id']]['maxCount'] = $item['maxCount'];
            }
        }


// СТРОКОВЫЕ дополнительные свойства
        $qItems = Core_QueryBuilder::select('id')->select('property_id')->select('value')->select('sorting')
            ->from(['property_value_strings'])
            ->where('entity_id', '=', $entity_id)
//            ->limit(0, 10);
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        foreach ($aItems as $item) {
            $structure['properties'][$item['property_id']][$item['sorting']] = $item;
        }

// ТЕКСТОВЫЕ дополнительные свойства
        $qItems = Core_QueryBuilder::select('id')->select('property_id')->select('value')->select('sorting')
            ->from(['property_value_texts'])
            ->where('entity_id', '=', $entity_id)
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        foreach ($aItems as $item) {
            $structure['properties'][$item['property_id']][$item['sorting']] = $item;
        }

// ЧИСЛОВЫЕ (floats) дополнительные свойства
        $qItems = Core_QueryBuilder::select('id')->select('property_id')->select('value')->select('sorting')
            ->from(['property_value_floats'])
            ->where('entity_id', '=', $entity_id)
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        foreach ($aItems as $item) {
            $structure['properties'][$item['property_id']][$item['sorting']] = $item;
        }


// ЧИСЛОВЫЕ (ints) дополнительные свойства
        $qItems = Core_QueryBuilder::select('id')->select('property_id')->select('value')->select('sorting')
            ->from(['property_value_ints'])
            ->where('entity_id', '=', $entity_id)
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        foreach ($aItems as $item) {
            $item['value_id'] = $item['value'];
            if (!empty($item['value'])) {
                $structure['list_items_id'][] = $item['value'];
            }
            $structure['properties'][$item['property_id']][$item['sorting']] = $item;
        }


// Значения "СПИСОЧНЫХ" свойств
        $qItems = Core_QueryBuilder::select('id')->select('value')
            ->from(['list_items'])
            ->where('id', 'IN', $structure['list_items_id'])
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        foreach ($aItems as $item) {
            $structure['list_items'][$item['id']] = $item['value'];
        }

// Узнаем Высоту, Щирину и Длину упаковки
        $qItems = Core_QueryBuilder::select('field_id')->select('value')
            ->from(['field_value_ints'])
            ->where('entity_id', '=', $entity_id)
        ;
        $aItems = $qItems->asAssoc()->execute()->result();
        foreach ($aItems as $item) {
            if ($item['field_id'] == 3) {
                $structure['packing_length'] = $item['value'];
            } 
            elseif($item['field_id'] == 4) {
                $structure['packing_width'] = $item['value'];
            }
            elseif($item['field_id'] == 5) {
                $structure['packing_height'] = $item['value'];
            }
        }


// Находим "Размер" ("Рос. размер")
        $techSize = "";
        if ($structure['property_id_to_wb_size'] && $structure['properties'][$structure['property_id_to_wb_size']]) {
            foreach ($structure['properties'][$structure['property_id_to_wb_size']] as $item_prop) {
                // Если списочное свойство
                if (!empty($item_prop['value_id'])) {
                    $item_prop['value'] = $structure['list_items'][$item_prop['value_id']];
                }
                $techSize = strval($item_prop['value']); // число переводим в строку, иначе WB не принимает
            }
        }

        $request_boby = []; // Формируем JSON для запроса к WB
        $request_boby['vendorCode'] = $structure['entity_data']['marking'];

        $request_boby['sizes'][0]['techSize']   = $techSize;
        $request_boby['sizes'][0]['wbSize']     = $techSize;
        $request_boby['sizes'][0]['price']      = intval($structure['entity_data']['price']); // intval(42); 
        $request_boby['sizes'][0]['skus'][0]    = $structure['entity_data']['marking'];

        $request_boby['characteristics'][0]['Предмет'] = $structure['wb_objectName'];


        foreach ($structure['wb_property_name_ot_property_id'] as $key => $item) {



            if ($key == "name") {
                $request_boby['characteristics'][0]['Наименование'] = $structure['entity_data']['name'];
            } 
            elseif($key == "brand") {
                $request_boby['characteristics'][0]['Бренд'] = $structure['entity_data']['group_name'];
            }
            elseif($key == "description") {
                $request_boby['characteristics'][0]['Описание'] = $structure['entity_data']['text'];
            }
            elseif($key == "packing_height") {
                $request_boby['characteristics'][0]['Высота упаковки'] = intval($structure['packing_height'] / 10); // переводим из мм в см
            }
            elseif($key == "packing_length") {
                $request_boby['characteristics'][0]['Длина упаковки'] = intval($structure['packing_length'] / 10); // переводим из мм в см
            }
            elseif($key == "packing_width") {
                $request_boby['characteristics'][0]['Ширина упаковки'] = intval($structure['packing_width'] / 10); // переводим из мм в см
            }
            
            else {
                $k = 0;
                foreach ($structure['properties'][$key] as $item_prop) {
                    if (!empty($item_prop['value_id'])) {
                        $item_prop['value'] = $structure['list_items'][$item_prop['value_id']];
                    }

                    if($item['maxCount'] == 0){
                        $request_boby['characteristics'][0][$item['name']] = $item_prop['value'];
                    } else {     
                        $k++;
                        if ($k <= $item['maxCount']) {
                            $request_boby['characteristics'][0][$item['name']][] = $item_prop['value'];
                        }
                    }//else        
                            
                }
            }

        }


// $request_boby = '[[{"vendorCode": "6000000002","characteristics": [{"ТНВЭД": ["6403993600"]},{"Пол": ["Мужской"]},{"Стилистика": ["casual"]},{"Предмет": "Платья"}],"sizes": [{"techSize": "40-41","wbSize": "","price": 3999,"skus": ["1000000001"]}]}]]';

        $request_boby = "[[". json_encode($request_boby, JSON_UNESCAPED_UNICODE) ."]]";
        $a = new Skynetcore_Mp_Wb_WbApiConnect($instance);
        $allCharacteristicsInDb = [];
        $object_characteristics = $a->cards_upload($request_boby);




echo "object_characteristics = " . "<br>\n";
echo json_encode($object_characteristics) . "<br>\n";
exit();



    }




    public static function syncCategoryCharacteristics() {

        $instance = Core_Page::instance();
        $logs = Utils_Core_Log::instance('/universalmotors.ru/mp/wb');

/*
        // Запоминаем имеющиеся категории
        $structure = [];
        $ItemsQuery = Core_QueryBuilder::select('objectName')->from(['skynetcore_mp_wb_category_characteristics']);
        $Items = $ItemsQuery->asAssoc()->execute()->result();

        foreach ($Items as $Item) {
            $structure[$Item['objectName']] = 1;
        }

*/

        $wb_connections_limit = 90; // В WB Допускается максимум 100 запросов в минуту на методы контента в целом.
        // zzzzzzzzzzzzz 
        $wb_connections_limit = 100; // В WB Допускается максимум 100 запросов в минуту на методы контента в целом.

        $qCategoriesTmp = Core_QueryBuilder::select('objectName')->select('id')
            ->from('skynetcore_mp_wb_categories')
//            ->where('objectName', '=',$objectName)
            ->orderBy('timestampUpdCharacteristics', 'ASC')
            ->limit(0, $wb_connections_limit);
        ;

        $qCategories = $qCategoriesTmp->asAssoc()->execute()->result();

        foreach ($qCategories as $qCategory) {
echo "qCategory['objectName'] = " . $qCategory['objectName'] . "<br>\n";



            // Импортируем Характеристики для категории товара
            $objectName = $qCategory['objectName'];
            $allCharacteristicsInDb = [];
            $a = new Skynetcore_Mp_Wb_WbApiConnect($instance);
            $object_characteristics = $a->object_characteristics($objectName);

            $qCharacteristicsTmp = Core_QueryBuilder::select('name')->select('id')
                ->from('skynetcore_mp_wb_category_characteristics')
                ->where('objectName', '=',$objectName)
            ;
            $qCharacteristics = $qCharacteristicsTmp->asAssoc()->execute()->result();

            foreach ($qCharacteristics as $qCharacteristic) {
                $allCharacteristicsInDb[$qCharacteristic['name']] = $qCharacteristic['id'];
            }

            foreach ($object_characteristics['data'] as $Item) {
                $tryMpWpItem = Core_Entity::factory('Skynetcore_Mp_Wb_Category_Characteristic');

    /*
                // Если нужен Update
                if(isset($allCharacteristicsInDb[$Item['name']])) {
                    $tryMpWpItem = $tryMpWpItem->getById($allCharacteristicsInDb[$Item['name']], false);
                }
    */
                // Если нужен только Inset
                if(!isset($allCharacteristicsInDb[$Item['name']])) {

                    $tryMpWpItem->objectName        = $Item['objectName'];
                    $tryMpWpItem->name              = $Item['name'];
                    $tryMpWpItem->unitName          = $Item['unitName'];
                    $tryMpWpItem->required          = $Item['required'];
                    $tryMpWpItem->popular           = $Item['popular'];
                    $tryMpWpItem->maxCount          = $Item['maxCount'];
                    $tryMpWpItem->charcType         = $Item['charcType'];
                    $tryMpWpItem->isMetaCardGroup   = $Item['isMetaCardGroup'];
                    $tryMpWpItem->isMetaCardJoin    = $Item['isMetaCardJoin'];

                    $tryMpWpItem->save();

                }

            }


            // Запоминаем время обновление характеристик
            $oCore_QueryBuilder = Core_QueryBuilder::update('skynetcore_mp_wb_categories')
            ->set('timestampUpdCharacteristics', time())
            ->where('id', '=', $qCategory['id'])
            ->execute();

        }


    }

    public static function syncCategories() {

        $instance = Core_Page::instance();
        
        $logs = Utils_Core_Log::instance('/universalmotors.ru/mp/wb');


/*
        // Запоминаем имеющиеся категории
        $structure = [];
        $ItemsQuery = Core_QueryBuilder::select('objectName')->from(['skynetcore_mp_wb_categories']);
        $Items = $ItemsQuery->asAssoc()->execute()->result();

        foreach ($Items as $Item) {
            $structure[$Item['objectName']] = 1;
        }
*/

        // Импортируем Категории товаров
        $name = "";
        $top = 50000; // Импортируем все (было около 7500 шт.)
        $a = new Skynetcore_Mp_Wb_WbApiConnect($instance);
        $object_all = $a->object_all($name, $top);

        foreach ($object_all['data'] as $Item) {

            $tryMpWpCategoryTmp = Core_Entity::factory('Skynetcore_Mp_Wb_Category');
            $tryMpWpCategory = $tryMpWpCategoryTmp->getByObjectName($Item['objectName'], false);

            if(!(isset($tryMpWpCategory->id) && $tryMpWpCategory->id > 0)) {
                $tryMpWpItem = Core_Entity::factory('Skynetcore_Mp_Wb_Category');
                $tryMpWpItem->objectName = $Item['objectName'];
                $tryMpWpItem->parentName = $Item['parentName'];

//                $tryMpWpItem->create();
                $tryMpWpItem->save();
            

echo "Item['objectName'] = " . $Item['objectName'] . "<br>\n";
        
            }


        }

    }




	public static function syncCategoryAtributes() {
		$instance = Core_Page::instance();

		/** @var Skynetcore_Mp_Wb_Category_Model $qCategories */
		$qCategories = Core_Entity::factory('Skynetcore_Mp_Wb_Category');
		$qCategories
			->queryBuilder()
			->join(['property_value_strings', 'pvs'], 'pvs.value', '=', 'skynetcore_mp_wb_categories.id', [
				['AND' => ['pvs.property_id', 'IN', [
					$instance->skynet->config['integration']['wb']['properties']['wb_category'],
					$instance->skynet->config['integration']['wb']['properties']['wb_item_category'],
				]]]
			])
			->leftJoin(['skynetcore_mp_wb_category_links', 'ocl'], 'skynetcore_mp_wb_categories.id', '=', 'ocl.wb_category_id')
			->where('ocl.id', 'is', null)
			->groupBy('skynetcore_mp_wb_categories.id')
		;
//		Skynetcore_Utils::p($qCategories->queryBuilder()->build());
//		die();
		$oCategories = $qCategories->findAll(false);
		$aCategories = array_map(function($oCategory) {
			return $oCategory->id;
		}, $oCategories);

		$client = new GuzzleAdapter(new GuzzleClient());
		$requestFactory = new RequestFactory();
		$streamFactory = new StreamFactory();
		$adapter = new GuzzleAdapter(new GuzzleClient());
		/** @var \Gam6itko\OzonSeller\Service\V3\CategoryService $svcCategories */
		$svcCategories = new psV3\CategoryService(
			$instance->skynet->config['integration']['wb'][$instance->skynet->request->envType],
			$adapter,
			$requestFactory,
			$streamFactory
		);
		$aChunkedCategories = array_chunk($aCategories, 20);
		foreach ($aChunkedCategories as $aChunkedCategoriesNum => $aChunkedCategories) {
//			Skynetcore_Utils::p($aChunkedCategories, $aChunkedCategoriesNum . ' = aChunkedCategories');
			$aCategoryAttributes = $svcCategories->attribute($aChunkedCategories);
			foreach ($aCategoryAttributes as $aCategoryAttribute) {
				$categoryID = Core_Array::get($aCategoryAttribute, 'category_id', 0);
				$categoryAttributes = Core_Array::get($aCategoryAttribute, 'attributes', []);
				foreach ($categoryAttributes as $categoryAttribute) {
					/** @var Skynetcore_Mp_Wb_Category_Atribute_Model $oCategoryAtribute */
					$oCategoryAtribute = Core_Entity::factory('Skynetcore_Mp_Wb_Category_Atribute')->getById($categoryAttribute['id'], false);
					if(!(isset($oCategoryAtribute->id) && $oCategoryAtribute->id > 0)) {
						$oCategoryAtribute = Core_Entity::factory('Skynetcore_Mp_Wb_Category_Atribute');
						$oCategoryAtribute->id = $categoryAttribute['id'];
						$oCategoryAtribute = $oCategoryAtribute->create();
					}
					$oCategoryAtribute->name = $categoryAttribute['name'];
					$oCategoryAtribute->description = $categoryAttribute['description'];
					$oCategoryAtribute->type = $categoryAttribute['type'];
					$oCategoryAtribute->is_collection = $categoryAttribute['is_collection']*1;
					$oCategoryAtribute->is_required = $categoryAttribute['is_required']*1;
					$oCategoryAtribute->group_id = $categoryAttribute['group_id']*1;
					$oCategoryAtribute->group_name = $categoryAttribute['group_name'];
					$oCategoryAtribute->dictionary_id = $categoryAttribute['dictionary_id']*1;
					$oCategoryAtribute->save();
				}
				foreach ($categoryAttributes as $categoryAttribute) {
					$attributeID = Core_Array::get($categoryAttribute, 'id', 0);
					/** @var Skynetcore_Mp_Wb_Category_Link_Model $qCategoryAtributeLink */
					$qCategoryAtributeLink = Core_Entity::factory('Skynetcore_Mp_Wb_Category_Link');
					$qCategoryAtributeLink
						->queryBuilder()
						->where('wb_category_id', '=', $categoryID)
					;
					/** @var Skynetcore_Mp_Wb_Category_Link_Model $qCategoryAtributeLink */
					$oCategoryAtributeLink = $qCategoryAtributeLink->getByWp_atribute_id($attributeID);
					if(!(isset($oCategoryAtributeLink->id) && $oCategoryAtributeLink->id > 0)) {
						$oCategoryAtributeLink = Core_Entity::factory('Skynetcore_Mp_Wb_Category_Link');
						$oCategoryAtributeLink->wb_category_id = $categoryID;
						$oCategoryAtributeLink->wb_atribute_id = $attributeID;
						$oCategoryAtributeLink = $oCategoryAtributeLink->create();
					}
				}
			}
		}
	}

	public static function syncItems() {

/*
    В таблицу skynetcore_mp_wb_items импортируем данные из WB
    https://suppliers-api.wildberries.ru/content/v1/cards/cursor/list

    Для каждого размера формирует отдельную запись в таблице.
    
    nmID                    -> shop_item_id
    sizes.skus = marking    -> product_id    по размеру находим артикул, по артикулу находим ID товара

*/

        $instance = Core_Page::instance();
        $propertyIdToUnload = $instance->skynet->config['integration']['wb']['properties']['id_to_upload'];


/*
    Запоминает всю информацию из базы UM (id, name, marking) и
    строим структуру с ключами marking

    Берутся товары и их модификации, 
    у которых у родителя стоит Галка "Опубликовать в Goods"
*/
        /** @var Core_QueryBuilder_Select $qItemsToUploadTmp */
        $qItemsToUploadTmp = Core_QueryBuilder::select('si.id')
            ->select('si.marking')
            ->select('si.name')
            ->from(['getGroupsIerarchyUp', 'giu'])
            ->join(['shop_items', 'si'], 'si.id', '=', 'giu.id')
            ->leftJoin(['shop_items', 'siparent'], 'siparent.id', '=', 'si.modification_id')
            ->leftJoin(['property_value_ints', 'items_to_unload'], 'items_to_unload.entity_id', '=', Core_QueryBuilder::expression('COALESCE(siparent.id, si.id)'), [
                ['AND' => ['items_to_unload.property_id', '=', $propertyIdToUnload]]
            ])
            ->where(Core_QueryBuilder::expression('COALESCE(items_to_unload.value, 0)'), '=', 1)
//          ->where('si.marking', 'IN', ['1060655125172', '1060341783391']) // Велосипед AIST Rocky 2.0 Disc 29 (2021) размер рамы 21.5 цвет черно-желтый
//            ->limit(0, 100);
        ;


		$aItemsToUploadTmpQuery = $qItemsToUploadTmp->asAssoc()->execute()->result();
		$aItemsToUploadTmp = [];


		foreach ($aItemsToUploadTmpQuery as $itemToUploadTmpQuery) {
			$aItemsToUploadTmp[$itemToUploadTmpQuery['marking']] = $itemToUploadTmpQuery;
		}


/*
    // Получение информации о ценах    
    $a = new Skynetcore_Mp_Wb_WbApiConnect($instance);
    $info = $a->info();
echo "info = " . "<br>\n";
echo json_encode($info) . "<br>\n";


    // Список НМ v2
    $a = new Skynetcore_Mp_Wb_WbApiConnect($instance);
    $limit      = "";
    $updatedAt  = "";
    $nmID       = "";

    $cards_cursor_list = $a->cards_cursor_list($limit, $updatedAt, $nmID);
echo "cards_cursor_list = " . "<br>\n";
echo json_encode($cards_cursor_list) . "<br>\n";

*/



    $logs = Utils_Core_Log::instance('/universalmotors.ru/mp/wb');

    // Список НМ v2
    $a = new Skynetcore_Mp_Wb_WbApiConnect($instance);
    $limit      = 1000;
    $total      = $limit;

    $updatedAt  = "";
    $nmID       = "";

    $max_requests_numbers = 200; // защита от зацикливания (максимальное количество обращений к WB)
    $request_number = 0;

    // Запрашиваем по $limit < 1000 товаров
    while ($total == $limit) {

        $request_number++;
        if($request_number >= $max_requests_numbers){

            $log_message = "Ошибка. Слишком большое количество обращений \"cards_cursor_list\" к API WB: \n";
            $log_message .= "request_number =" . $request_number . "\n";
            $log_message .= "nmID = " . $nmID . "\n";
            $log_message .= "updatedAt = " . $updatedAt . "\n";
            $log_message .= "total = " . $total . "\n\n";
            $logs->write($log_message);

            echo $log_message;
            exit();
        }


        $cards_cursor_list = $a->cards_cursor_list($limit, $updatedAt, $nmID);

/*
 {
    "sizes": [{
        "techSize": "M",
        "skus": ["2015495608035"]
    }, {
        "techSize": "XL",
        "skus": ["2015495608011"]
    }, {
        "techSize": "XXL",
        "skus": ["2015495608004"]
    }, {
        "techSize": "L",
        "skus": ["2015495608028"]
    }],
    "mediaFiles": ["https:\/\/img1.wbstatic.net\/big\/new\/51790000\/51797442-11.jpg", "https:\/\/img1.wbstatic.net\/big\/new\/51790000\/51797442-13.jpg", "https:\/\/img1.wbstatic.net\/big\/new\/51790000\/51797442-7.jpg", "https:\/\/img1.wbstatic.net\/big\/new\/51790000\/51797442-22.jpg"],
    "colors": ["\u0447\u0435\u0440\u043d\u044b\u0439", "\u0441\u0435\u0440\u044b\u0439", "\u0431\u0435\u043b\u044b\u0439"],
    "updateAt": "2022-12-15T07:14:47Z",
    "vendorCode": "1057129765504FRDRIVE",
    "brand": "FIANRO RACING",
    "object": "\u041a\u0443\u0440\u0442\u043a\u0438",
    "nmID": 51797442
}, 
*/



        $total      = $cards_cursor_list['data']['cursor']['total'];
        $updatedAt  = $cards_cursor_list['data']['cursor']['updatedAt'];
        $nmID       = $cards_cursor_list['data']['cursor']['nmID'];


        foreach ($cards_cursor_list['data']['cards'] as $OfferTmp) {

            // Допускаем, что у карточки товара всегда есть хотя бы один размер!!!
            foreach ($OfferTmp['sizes'] as $SizeTmp) {

                // Если товар есть в импортируемых
                if (!empty($aItemsToUploadTmp[$SizeTmp['skus'][0]])) {
                    
                    $product_id = $aItemsToUploadTmp[$SizeTmp['skus'][0]]['id'];

                    $tryMpWpItemTmp = Core_Entity::factory('Skynetcore_Mp_Wb_Item');
    //              $tryMpWpItemTmp
    //                  ->queryBuilder()
    //                  ->where('shop_item_id', '=', $itemsToUploadTmp['id'])
    //              ;
                    $tryMpWpItem = $tryMpWpItemTmp
//                        ->getByShop_item_id($OfferTmp['nmID'], false);
                        ->getByProduct_id($product_id, false);


                    if(!(isset($tryMpWpItem->id) && $tryMpWpItem->id > 0)) {
                        $tryMpWpItem = Core_Entity::factory('Skynetcore_Mp_Wb_Item');
                        $tryMpWpItem->product_id = $product_id;
//                        $tryMpWpItem->shop_item_id = $OfferTmp['nmID'];
                        $tryMpWpItem->create();

                    }


                    $tryMpWpItem->techSize      = $SizeTmp['techSize'];

                    $tryMpWpItem->product_id    = $product_id;
                    $tryMpWpItem->shop_item_id  = $OfferTmp['nmID'];

                    $tryMpWpItem->sizes         = json_encode($OfferTmp['sizes'], JSON_UNESCAPED_UNICODE);
                    $tryMpWpItem->colors        = json_encode($OfferTmp['colors'], JSON_UNESCAPED_UNICODE);
                    $tryMpWpItem->mediaFiles    = json_encode($OfferTmp['mediaFiles'], JSON_UNESCAPED_UNICODE);

                    $tryMpWpItem->vendorCode    = $OfferTmp['vendorCode'];
                    $tryMpWpItem->brand         = $OfferTmp['brand'];
                    $tryMpWpItem->object        = $OfferTmp['object'];

                    $tryMpWpItem->updateAt = Core_Date::timestamp2sql(strtotime($OfferTmp['updateAt']));
                    
                    $tryMpWpItem->save();

                }


            }

        }


        usleep(0.7 * 1000000); // Допускается максимум 100 запросов в минуту

    }//while



/*
    foreach ($cards_cursor_list as $chunkedRecord => $aChunkedRecord) {
        $aFilterRecords = [];
        foreach ($aChunkedRecord as $oChunkedRecord) {
            $aFilterRecords[$oChunkedRecord['marking']] = $oChunkedRecord;
        }
*/


exit();






//		Skynetcore_Utils::p($aItemsToUploadTmp);
		if(count($aItemsToUploadTmp)) {
			$client = new GuzzleAdapter(new GuzzleClient());
			$requestFactory = new RequestFactory();
			$streamFactory = new StreamFactory();
			$adapter = new GuzzleAdapter(new GuzzleClient());
			/** @var \Gam6itko\OzonSeller\Service\V2\ProductService $svcProduct */
			$svcProduct = new psV2\ProductService($instance->skynet->config['integration']['wb'][$instance->skynet->request->envType],
				$adapter,
				$requestFactory,
				$streamFactory
			);
			
// WB 
// https://suppliers-api.wildberries.ru/content/v1/cards/cursor/list


            $aChunkedRecords = array_chunk($aItemsToUploadTmp, 999);



			$aOffers = [];
			foreach ($aChunkedRecords as $chunkedRecord => $aChunkedRecord) {
				$aFilterRecords = [];
				foreach ($aChunkedRecord as $oChunkedRecord) {
					$aFilterRecords[$oChunkedRecord['marking']] = $oChunkedRecord;
				}


//				$aListTmp = $svcProduct->list(
//					[
//						'filter' => [
//							'offer_id' => array_keys($aFilterRecords)
//						]
//					]
//				);


/* 
    Ключи - marking (артикулы UM)
aFilterRecords = 
{
    "1058333797214": {
        "id": "6087",
        "marking": "1058333797214",
        "name": "\u041c\u0430\u0441\u043b\u043e"
    },
    "1057755670546": {
        "id": "18297",
        "marking": "1057755670546",
        "name": "\u0417\u0430\u0449\u0438\u0442\u0430"
    }
}

*/



				$aListTmp = $svcProduct->infoList(
					[
						"offer_id" => array_keys($aFilterRecords),
						"product_id" => [ ],
						"sku" => [ ]
					]
				);

//				Skynetcore_Utils::p($aListTmp);
				if(isset($aListTmp['items']) && is_array($aListTmp['items']) && count($aListTmp['items'])) {
					foreach ($aListTmp['items'] as $listTmp) {
						$aOffers[$listTmp['offer_id']] = $listTmp;
					}
				}
				usleep(0.3 * 1000000);
			}


/*
Содержание смотри в файле 
    aOffers.json
*/


//echo "aOffers = " . "<br>\n";
//echo json_encode($aOffers) . "<br>\n";
exit();



//			Skynetcore_Utils::p($aOffers);
//			Skynetcore_Utils::p($aItemsToUploadTmp);
			foreach ($aItemsToUploadTmp as $itemsToUploadTmpMarking => $itemsToUploadTmp) {
				/** @var Skynetcore_Mp_Wb_Item_Model $tryMpWpItemTmp */
				$tryMpWpItemTmp = Core_Entity::factory('Skynetcore_Mp_Wb_Item');
//				$tryMpWpItemTmp
//					->queryBuilder()
//					->where('shop_item_id', '=', $itemsToUploadTmp['id'])
//				;
				$tryMpWpItem = $tryMpWpItemTmp
					->getByShop_item_id($itemsToUploadTmp['id'], false);

				$wpOfferId = '';
				$wpOffer = false;
				if(isset($aOffers[$itemsToUploadTmpMarking]['id'])
					&& isset($aOffers[$itemsToUploadTmpMarking]['status']['state'])
//					&& $aOffers[$itemsToUploadTmpMarking]['status']['state'] != ''
				) {
					$wpOfferId = $aOffers[$itemsToUploadTmpMarking]['id'];
					$wpOffer = $aOffers[$itemsToUploadTmpMarking];
				} else {
					if(isset($tryMpWpItem->id) && $tryMpWpItem->id > 0) {
						$tryMpWpItem->delete();
					}
				}
//				Skynetcore_Utils::p($tryMpWpItemTmp->queryBuilder()->build(), $wpOfferId);
				if(!(isset($tryMpWpItem->id) && $tryMpWpItem->id > 0)) {
					$tryMpWpItem = Core_Entity::factory('Skynetcore_Mp_Wb_Item');
					$tryMpWpItem->shop_item_id = $itemsToUploadTmp['id'];
					$tryMpWpItem->create();
				}
//				Skynetcore_Utils::p($tryMpWpItem->toArray());
				if(isset($wpOffer['id']) && $wpOffer['id'] != '') {
					$tryMpWpItem->product_id = $wpOfferId;
					$tryMpWpItem->state = $wpOffer['status']['state'];
					$tryMpWpItem->state_failed = $wpOffer['status']['state_failed'];
					$tryMpWpItem->validation_state = $wpOffer['status']['validation_state'];
					$tryMpWpItem->state_name = $wpOffer['status']['state_name'];
					$tryMpWpItem->state_description = $wpOffer['status']['state_description'];
					$tryMpWpItem->is_failed = $wpOffer['status']['is_failed']*1;
					$tryMpWpItem->is_created = $wpOffer['status']['is_created']*1;
					$tryMpWpItem->visible = Core_Array::get($wpOffer, 'visible', 0);
					$tryMpWpItem->visibility_details = json_encode(Core_Array::get($wpOffer, 'visibility_details', []), JSON_UNESCAPED_UNICODE);
					$tryMpWpItem->item_errors = json_encode(Core_Array::get($wpOffer['status'], 'item_errors', []), JSON_UNESCAPED_UNICODE);
					$tryMpWpItem->state_updated_at = Core_Date::timestamp2sql(strtotime($wpOffer['status']['state_updated_at']));
					$tryMpWpItem->save();



					/** @var Skynetcore_Mp_Wb_Item_Sync_Model $oTaskItem */
					$oTaskItem = Core_Entity::factory('Skynetcore_Mp_Wb_Item_Sync')
						->getByShop_item_id($tryMpWpItem->shop_item_id, false);
//					Skynetcore_Utils::p($oTaskItem->toArray());
					if(isset($oTaskItem->id) && $oTaskItem->id > 0) {
						$oTaskItem->active = 0;
						$oTaskItem->save();
					}




				}



			}
		}
	}



	public function syncStocks() {
		$instance = Core_Page::instance();
		$envType = $instance->skynet->request->envType;
		$wpConfig = $instance->skynet->config['integration']['wb'][$envType];

		$qbCurrentChangedID = Core_QueryBuilder::select([Core_QueryBuilder::expression('MAX(id)'), 'max_id'])
			->from('skynetcore_history_shop_warehouse_items');
		$currentChangedID = Core_Array::get($qbCurrentChangedID->asAssoc()->execute()->result(false), 0, ['max_id' => 0])['max_id'];

		/** @var Skynetcore_Mp_Wb_History_Model $qbLastChangedID */
		$qbLastChangedID = Core_Entity::factory('Skynetcore_Mp_Wb_History');
		$qbLastChangedID
			->queryBuilder()
			->limit(1)
			->orderBy('id', 'DESC')
		;
		$lastChangedID = Core_Array::get($qbLastChangedID->findAll(false), 0, json_decode('{"last_warehouse_history_id": 0}'));

		$qbLastChanged = Core_QueryBuilder::select('swi.*')
			->select([Core_QueryBuilder::expression("MAX(sh.id)"), 'datawarehouse_sh_id'])
			->select([Core_QueryBuilder::expression("COALESCE(sw.name, '')"), 'datawarehouse_name'])
			->select([Core_QueryBuilder::expression("COALESCE(si.marking, '')"), 'dataitem_marking'])
			->select([Core_QueryBuilder::expression("COALESCE(si.name, '')"), 'dataitem_name'])
			->from(['skynetcore_history_shop_warehouse_items', 'sh'])
			->join(['shop_warehouse_items', 'swi'], 'swi.id', '=', 'sh.shop_warehouse_item_id')
			->join(['shop_warehouses', 'sw'], 'sw.id', '=', 'swi.shop_warehouse_id')
			->join(['shop_items', 'si'], 'si.id', '=', 'sh.shop_item_id')
			->join(['getGroupsIerarchyUp', 'giu'], 'si.id', '=', 'giu.id')
			->where('sh.id', '>', $lastChangedID->last_warehouse_history_id*1)
			->where('swi.shop_warehouse_id', 'IN', $instance->skynet->config['active_warehouses'])
			->where('swi.count', '>=', 0)
//			->where('si.marking', 'IN', ["1060508487600", "1099927410003"])
//			->where('si.marking', 'IN', ['1099927410003','1099046475794','1099108194523'])
			->groupBy('swi.id')
		;
		$qbLastChanged->open();
		//-- Ограничение по складу и по разделу
		//-- ... AND ( giu.dirpath REGEXP '^/bikes/' AND swi.shop_warehouse_id IN (19) OR giu.dirpath NOT REGEXP '^/bikes/' )
		$qbLastChanged
			->open()
			->where('giu.dirpath', 'REGEXP', '^/bikes/')
			->where('swi.shop_warehouse_id', 'IN', [19])
			->setOr()
			->where('giu.dirpath', 'NOT REGEXP', '^/bikes/')
			->close()
		;
//		//-- Ограничение по складу и по разделу
//		//-- ... AND ( giu.dirpath REGEXP '^/sup-boards/' AND swi.shop_warehouse_id IN (19, 20) OR giu.dirpath NOT REGEXP '^/sup-boards/' )
//		$qbLastChanged
//			->open()
//			->where('giu.dirpath', 'REGEXP', '^/sup-boards/')
//			->where('swi.shop_warehouse_id', 'IN', [19, 20])
//			->setOr()
//			->where('giu.dirpath', 'NOT REGEXP', '^/sup-boards/')
//			->close()
//		;
		$qbLastChanged->close();
//		Skynetcore_Utils::p($qbLastChanged->build()); die();

		$aLastChanged = $qbLastChanged->asObject('Shop_Warehouse_Item_Model')->execute()->result(false);

		$lastChangedRecords = $lastChangedRecordsTmp = [];
		foreach ($aLastChanged as $lastChangedTmp) {
			$lastChangedRecordsTmp[$lastChangedTmp->dataitem_marking][] = $lastChangedTmp->count - $lastChangedTmp->moto_reserve_count;
		}
		foreach ($lastChangedRecordsTmp as $lastChangedRecordItem_id => $aLastChangedRecord) {
			$lastChangedRecords[$lastChangedRecordItem_id] = array_sum($aLastChangedRecord);
		}
		$textResult = 'none';
		if(count($lastChangedRecords)) {
			$instance = Core_Page::instance();
			$client = new GuzzleAdapter(new GuzzleClient());
			$requestFactory = new RequestFactory();
			$streamFactory = new StreamFactory();
			$adapter = new GuzzleAdapter(new GuzzleClient());
			/** @var \Gam6itko\OzonSeller\Service\V2\ProductService $svcProduct */
			$svcProduct = new psV2\ProductService($instance->skynet->config['integration']['wb'][$instance->skynet->request->envType],
				$adapter,
				$requestFactory,
				$streamFactory
			);

			$aChunkedRecords = array_chunk(array_keys($lastChangedRecords), 999);
			$aOffers = [];
			foreach ($aChunkedRecords as $chunkedRecord => $aChunkedRecord) {
				$list = $svcProduct->list(
					[
						'filter' => [
							'offer_id' => $aChunkedRecord
						]
					]
				);
				if(isset($list['items']) && is_array($list['items']) && count($list['items'])) {
					foreach ($list['items'] as $list_item) {
						$aOffers[$list_item['offer_id']] = $list_item;
						$aOffers[$list_item['offer_id']]['quantity'] = $lastChangedRecords[$list_item['offer_id']];
					}
				}
				usleep(0.3 * 1000000);
			}
			/** @var \Gam6itko\OzonSeller\Service\V2\ProductService $svcProductOffer */
			$svcProductOffer = new psV2\ProductService($instance->skynet->config['integration']['wb'][$instance->skynet->request->envType],
				$adapter,
				$requestFactory,
				$streamFactory
			);
			if(count($aOffers)) {
				$aStocks = [];
				//-- UM --
				foreach ($aOffers as $offerID => $offer) {
					$aStocks[] = [
						"offer_id" => $offer['offer_id'],
						"product_id" => $offer['product_id'],
						"stock" => $offer['quantity']*1,
						"warehouse_id" => 23824253095000,
					];
				}
				//-- UM 2 --
				foreach ($aOffers as $offerID => $offer) {
					$aStocks[] = [
						"offer_id" => $offer['offer_id'],
						"product_id" => $offer['product_id'],
						"stock" => 0,
						"warehouse_id" => 23834506690000,
					];
				}
				$aChunkedStocks = array_chunk($aStocks, 100);
				$aUpdateResults = [
					'ok' => [],
					'error' => [],
				];
				foreach ($aChunkedStocks as $aStocks) {
					$aStockResults = $svcProductOffer->importStocks($aStocks);
					foreach ($aStockResults as $tmpStockResult) {
						$aUpdateResults[(count($tmpStockResult['errors']) == 0 ? 'ok' : 'error')][] = $tmpStockResult;
					}
					sleep(2);
				}
				$textResult = json_encode([
					'processed' => count($aUpdateResults['ok']) / 2,
					'errors' => $aUpdateResults['error'],
				]);
				$oNewHistoryRecord = Core_Entity::factory('Skynetcore_Mp_Wb_History');
				$oNewHistoryRecord->date = Core_Date::timestamp2sql(time());
				$oNewHistoryRecord->last_warehouse_history_id = $currentChangedID;
				$oNewHistoryRecord->status = (count($aUpdateResults['error']) == 0) * 1;
				$oNewHistoryRecord->result = $textResult;
				$oNewHistoryRecord->save();
			}
		}
		echo Core_Date::timestamp2sql(time()) . ': ' . $textResult . "\n";

	}
}