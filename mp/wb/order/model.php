<?php
use Skynetcore_Utils as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/*
CREATE TABLE skynetcore_mp_wb_orders (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  date datetime NOT NULL,
  lastchangedate datetime NOT NULL,
  supplierarticle varchar(255) NOT NULL,
  techsize varchar(255) NOT NULL,
  barcode varchar(255) NOT NULL,
  totalprice double NOT NULL DEFAULT 0,
  discountpercent int(11) NOT NULL DEFAULT 0,
  warehousename varchar(255) NOT NULL,
  oblast varchar(255) NOT NULL,
  incomeid varchar(255) NOT NULL,
  odid bigint(20) NOT NULL DEFAULT 0,
  nmid varchar(255) NOT NULL,
  subject varchar(255) NOT NULL,
  category varchar(255) NOT NULL,
  brand varchar(255) NOT NULL,
  iscancel varchar(255) NOT NULL,
  cancel_dt datetime NOT NULL,
  gnumber varchar(255) NOT NULL,
  sticker varchar(255) NOT NULL,
  srid varchar(255) NOT NULL,
  ordertype varchar(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

ALTER TABLE skynetcore_mp_wb_orders
ADD UNIQUE INDEX IDX_skynetcore_mp_wb_orders_odid (gnumber, odid);

ALTER TABLE skynetcore_mp_wb_orders
ADD INDEX IDX_wb_supplier_orders (supplierarticle, techsize);

ALTER TABLE skynetcore_mp_wb_orders
ADD INDEX IDX_wb_supplier_orders_brand (brand);

ALTER TABLE skynetcore_mp_wb_orders
ADD INDEX IDX_wb_supplier_orders_category (category);

ALTER TABLE skynetcore_mp_wb_orders
ADD INDEX IDX_wb_supplier_orders_date (date);

ALTER TABLE skynetcore_mp_wb_orders
ADD INDEX IDX_wb_supplier_orders_supplierArticle (supplierarticle);

ALTER TABLE skynetcore_mp_wb_orders
ADD INDEX IDX_wbs_supplier_orders_nmid (nmid);

ALTER TABLE skynetcore_mp_wb_orders
ADD INDEX IDX_wb_supplier_orders_lastchangedate (lastchangedate);
*/
class Skynetcore_Mp_Wb_Order_Model extends Core_Entity
{

	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

	public function fillData($stat_token, $days_diff = 3) : array {
		$aReturn = [
			'status' => 'ERROR',
			'message' => 'Undefined Error',
			'orders' => [],
		];
		$guzzleAdapter = new \GuzzleHttp\Client();
		try {
			/** @var Skynetcore_Mp_Wb_Order_Model $qTmpModelMaxObject */
			$qTmpModelMaxObject = Core_Entity::factory(str_replace('_Model', '', self::class));
			/** @var Core_QueryBuilder_Select $qbTmpModelMaxObject */
			$qbTmpModelMaxObject = $qTmpModelMaxObject->queryBuilder();
			$qbTmpModelMaxObject
				->clearSelect()
				->select([Core_QueryBuilder::expression('MAX(lastchangedate)'), 'ldate'])
			;
			$tmpModelMaxObject = $qbTmpModelMaxObject->asAssoc()->execute()->result(false);
			$dateFrom = date(DATE_ATOM, strtotime("now -{$days_diff} days"));
			if(isset($tmpModelMaxObject[0]) && isset($tmpModelMaxObject[0]['ldate'])) {
				$dateFrom = date(DATE_ATOM, strtotime($tmpModelMaxObject[0]['ldate']));
			}
			$wbStatResponce = $guzzleAdapter
				->get(
					'https://statistics-api.wildberries.ru/api/v1/supplier/orders',
					[
						'query' => [
							'dateFrom' => $dateFrom,
						],
						'headers' => [
							'Accept'     => 'application/json',
							'Authorization'      => "Bearer {$stat_token}",
						]
					]
				);
			if($wbStatResponce->getStatusCode() == 200) {
				$aWbContents = json_decode($wbStatResponce->getBody()->getContents());
				if(json_last_error() == JSON_ERROR_NONE) {
					foreach ($aWbContents as $wbContent) {
						/** @var Skynetcore_Mp_Wb_Order_Model $qTmpModelObject */
						$qTmpModelObject = Core_Entity::factory(str_replace('_Model', '', self::class));
						/** @var Skynetcore_Mp_Wb_Order_Model $oTmpModelObject */
						$oTmpModelObject = $qTmpModelObject->getBySrid($wbContent->srid);
						if(is_null($oTmpModelObject)) {
							$oTmpModelObject = Core_Entity::factory(str_replace('_Model', '', self::class));
							$oTmpModelObject->date = $wbContent->date;
							$oTmpModelObject->lastchangedate = $wbContent->lastChangeDate;
							$oTmpModelObject->supplierarticle = $wbContent->supplierArticle;
							$oTmpModelObject->techsize = $wbContent->techSize;
							$oTmpModelObject->barcode = $wbContent->barcode;
							$oTmpModelObject->totalprice = $wbContent->totalPrice;
							$oTmpModelObject->discountpercent = $wbContent->discountPercent;
							$oTmpModelObject->warehousename = $wbContent->warehouseName;
							$oTmpModelObject->oblastOkrugName = $wbContent->oblastOkrugName;
							$oTmpModelObject->regionName = $wbContent->regionName;
							$oTmpModelObject->incomeid = $wbContent->incomeID;
							$oTmpModelObject->srid = $wbContent->srid;
							$oTmpModelObject->nmid = $wbContent->nmId;
							$oTmpModelObject->subject = $wbContent->subject;
							$oTmpModelObject->category = $wbContent->category;
							$oTmpModelObject->brand = $wbContent->brand;
							$oTmpModelObject->iscancel = $wbContent->isCancel;
							$oTmpModelObject->cancelDate = $wbContent->cancelDate;
							$oTmpModelObject->gnumber = $wbContent->gNumber;
							$oTmpModelObject->sticker = $wbContent->sticker;
							$oTmpModelObject->srid = $wbContent->srid;
							$oTmpModelObject->ordertype = $wbContent->orderType;
							$oTmpModelObject = $oTmpModelObject->save();
						}
						$aReturn['orders'][$oTmpModelObject->gNumber][$oTmpModelObject->nmid][$oTmpModelObject->srid] = $oTmpModelObject;
					}
					$aReturn['status'] = 'OK';
				}
			} else {
				Skynetcore_Utils::p($wbStatResponce->getBody(), $wbStatResponce->getStatusCode());
			}
		} catch (\Exception $e) {
			Skynetcore_Utils::p($e->getMessage());
			Skynetcore_Chat_Telegram_Controller::sendOk($e->getMessage());
			$aReturn['message'] = $e->getMessage();
		}
		return $aReturn;
	}
}