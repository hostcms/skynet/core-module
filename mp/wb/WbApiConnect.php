<?php

//namespace Modules\Skynetcore\Mp\Wb\WbApiConnect;

use GuzzleHttp\Client as GuzzleClient;
//use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
//use Http\Factory\Guzzle\RequestFactory;
//use Http\Factory\Guzzle\StreamFactory;


defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * WbApiConnect.php
 */

class AbstractRequest {
    
    public $func;
    public $headers = [];
    public $method;
    public $url;
    public $query = [];
    public $body;
    public $multipart = [];

    
}

class Skynetcore_Mp_Wb_WbApiConnect
{
    
    protected $config;
    
    public function __construct($config) 
    {
		
        $this->config = $config;

    }
    
    /**
     * @param send $request
     */
    public function send($request) 
    {


		$logs = Utils_Core_Log::instance('/universalmotors.ru/mp/wb');
		$log_message = "";

		$host 		= $this->config->skynet->config['integration']['wb']['dev']['host'];
		$key_header = $this->config->skynet->config['integration']['wb']['dev']['apiKeyHeader'];


		$options = [];

		$options['headers']['Authorization'] = $key_header;
		foreach ($request->headers as $key => $item) {
			$options['headers'][$key] = $item;
		}//foreach

        // По умолчанию GET
        if(empty($request->method)){
            $request->method = 'GET';
        }

		$request->url = $host . $request->url;
        $query_str = http_build_query($request->query);
        if (!empty($query_str)) {
            $request->url = $request->url . '?' . $query_str;
        }

        if (!empty($request->body)) {
            $options['body'] = $request->body;
        }

        if (!empty($request->multipart)) {
            $options['multipart'] = $request->multipart;
        }



echo "request->method = " . $request->method . "<br>\n";
echo "request->url = " . $request->url . "<br>\n";
		


		$client = new GuzzleClient();

// zzzzzzzzz после отдалки стереть, чтобы не засорять логи
		$log_message .= "request = \n" . json_encode($request) . "\n\n";
		$log_message .= "options = \n" . json_encode($options) . "\n\n";
		$logs->write($log_message);

        try {
	        $res = $client->request($request->method, $request->url, $options);
        } 

        ///////////////// Ошибка соединения
        catch (\GuzzleHttp\Exception\ConnectException $e) {

            $getMessage 	= $e->getMessage();

			$log_message  = "GuzzleHttp Ошибка соединения\n";
			$log_message .= "getMessage = \n" . $getMessage . "\n\n";
			$logs->write($log_message);

			echo $log_message;
			exit();
        } 

        ///////////////// Ошибка партнера (4хх)
        catch (\GuzzleHttp\Exception\ClientException $e) {

            $res = $e->getResponse();

            $getMessage 	= $e->getMessage();
            $getStatusCode 	= $res->getStatusCode();
            $getContents 	= $res->getBody()->getContents();

			$log_message  = "GuzzleHttp - Ошибка партнера (4хх) <br>\n";
			$log_message .= "getStatusCode = " . $getStatusCode . "<br>\n\n";
			$log_message .= "getMessage = " . $getMessage . "<br>\n\n";
			$log_message .= "getContents = " . $getContents . "<br>\n\n";
			$logs->write($log_message);

			echo $log_message;
			exit();

        }


        $output =  json_decode($res->getBody()->__toString(), true);
        // Проверка ответа на JSON
        if (!$output) {
        	$getBody = $res->getBody()->__toString();

			$log_message  = "Неправильный формат ответа: <br>\n";
			$log_message .= "getBody = " . $getBody . "<br>\n\n";
			$logs->write($log_message);

			echo $log_message;
			exit();
	

        }

// zzzzzzzzz после отдалки стереть, чтобы не засорять логи
		$log_message  = "output = \n" . json_encode($output) . "\n\n";
		$logs->write($log_message);


    	return $output;

    }


    // Добавление медиа контента в КТ
    public function media_file($vendor_code, $photo_number, $photo_url) 
    {
        /**
         * @var $request object_all
         */

        $request = new AbstractRequest();

        $request->func  = __FUNCTION__;
        $request->method 	= 'POST';
        $request->url   = "content/v1/media/file";

        $headers = [];
//         $headers['Content-Type']	= "multipart/form-data"; // заработало, когда отключил
        $headers['X-Vendor-Code']   = $vendor_code;
        $headers['X-Photo-Number']  = $photo_number;
        $request->headers = $headers;

        $multipart = [];
        $multipart[0]['name'] 		= "uploadfile";
        $multipart[0]['filename'] 	= "uploadfile.jpg";
        $multipart[0]['contents'] 	= file_get_contents($_SERVER['DOCUMENT_ROOT'] . $photo_url);
        $request->multipart   = $multipart;

        $res = $this->send($request);

        return $res;
    }

    // Получение информации о ценах
    public function info() 
    {
        /**
         * @var $request info
         */

        $request = new AbstractRequest();

        $request->func  = __FUNCTION__;
        $request->url   = "public/api/v1/info";

        $res = $this->send($request);

        return $res;
    }



    // ТНВЭД код
    // 28.01.2023 метод не работал, на всё отдадал пустой ответ даже на "кривой JSON"
    public function directory_tnved($request_boby) 
    {
        /**
         * @var $request directory_tnved
         */

//        $request_boby = '{"data": [{"subjectName": "Блузки","tnvedsLike": "42031","description": "Предметы одежды из натуральной кожи","isKiz": true}],"error": false,"errorText": "","additionalErrors": ""}';

        $request = new AbstractRequest();

        $request->func  = __FUNCTION__;
        $request->method 	= 'GET';
        $request->url   = "content/v1/directory/tnved";
        $request->body   = $request_boby;

        $res = $this->send($request);

        return $res;
    }



    // Список несозданных НМ с ошибками
    public function cards_error_list() 
    {
        /**
         * @var $request object_all
         */

        $request = new AbstractRequest();

        $request->func  = __FUNCTION__;
        $request->method 	= 'GET';
        $request->url   = "content/v1/cards/error/list";

        $res = $this->send($request);

/*
{
	"data": [{
		"errors": ["Для данной категории товара необходимо заполнить поле Размер", "Для данной категории товара необходимо заполнить поле Рос. размер", "Поле Высота упаковки должно быть целочисленным", "Поле Ширина упаковки должно быть целочисленным", "Поле Длина упаковки должно быть целочисленным"],
		"updateAt": "2023-02-18T06:06:46Z",
		"vendorCode": "1060984694197",
		"object": "Велосипеды двухколесные"
	}],
	"error": false,
	"errorText": "",
	"additionalErrors": null
}*/

        return $res;
    }

    // Создание КТ
    public function cards_upload($request_boby) 
    {
        /**
         * @var $request object_all
         */

        $request = new AbstractRequest();

        $request->func  = __FUNCTION__;
        $request->method 	= 'POST';
        $request->url   = "content/v1/cards/upload";
        $request->body   = $request_boby;

        $res = $this->send($request);

        return $res;
    }

    // Характеристики для для категории товара
    public function object_characteristics($objectName) 
    {
        /**
         * @var $request object_all
         */

        $objectName = urlencode($objectName);

        $request = new AbstractRequest();

        $request->func  = __FUNCTION__;
        $request->method 	= 'GET';
        $request->url   = "content/v1/object/characteristics/" . $objectName;

        $res = $this->send($request);

        return $res;
    }

    // Категории товаров
    public function object_all($name, $top) 
    {
        /**
         * @var $request object_all
         */

        #if(!$top){ $top = 10000; } // по умолчанию

        $request = new AbstractRequest();

        $request->func  = __FUNCTION__;
        $request->method 	= 'GET';
        $request->url   = "content/v1/object/all";

        $query = [];

        if($name){
	        $query['name']= $name;
        }

        if($top){
	        $query['top']= $top;
        }

        $request->query   = $query;

        $res = $this->send($request);

        return $res;
    }

    // Получение информации о созданных КТ, НМ и размерах
    public function cards_cursor_list($limit, $updatedAt, $nmID) 
    {
        /**
         * @var $request info
         */

        if(!$limit){ $limit = 1000; } // по умолчанию

        $request = new AbstractRequest();

        $request->func  	= __FUNCTION__;
        $request->method 	= 'POST';
        $request->url   	= "content/v1/cards/cursor/list";

        $body = [];

        $body['sort']['cursor'] = [];
        if($limit){
	        $body['sort']['cursor']['limit'] = $limit;
        }
        if($updatedAt){
	        $body['sort']['cursor']['updatedAt'] = $updatedAt;
        }
        if($nmID){
	        $body['sort']['cursor']['nmID'] = $nmID;
        }

        $body['sort']['filter'] = [];
        $body['sort']['filter']['withPhoto'] = -1;

        $body['sort']['sort']['sortColumn'] = "updateAt";

        $request->body   = json_encode($body, JSON_UNESCAPED_UNICODE);

        $res = $this->send($request);

/*

{
	"sort": {
		"cursor": {
			"limit": 1000
		},
		"filter": {
			"withPhoto": -1
		}
	}
}
s
$res = 
[{
	"nmId": 51790643,
	"price": 14500,
	"discount": 33,
	"promoCode": 0
}, {
	"nmId": 52843740,
	"price": 33000,
	"discount": 33,
	"promoCode": 0
}]
*/

        return $res;
    }



}

