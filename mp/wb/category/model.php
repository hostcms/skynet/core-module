<?php
use Skynetcore_Utils as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Mp_Wb_Category_Model extends Core_Entity
{

	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

	/**
	 * Belongs to relations
	 * @var array
	 */
	protected $_belongsTo = array(
		'parent_category' => array(
			'model' => 'skynetcore_mp_wb_category',
			'foreign_key' => 'parent_id'
		),
	);
}