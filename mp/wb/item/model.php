<?php
use Skynetcore_Utils as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Mp_Wb_Item_Model extends Core_Entity
{

	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

	/**
	 * Get XML for entity and children entities
	 * @return string
	 * @hostcms-event shop_item.onBeforeRedeclaredGetXml
	 */
	public function getXml()
	{
		Core_Event::notify($this->_modelName . '.onBeforeRedeclaredGetXml', $this);

		$this->_prepareData();

		return parent::getXml();
	}

	/**
	 * Prepare entity and children entities
	 * @return self
	 */
	protected function _prepareData()
	{
		$aItemErrors = json_decode($this->item_errors, true);
		$this->addForbiddenTag('item_errors');
		if(json_last_error() == JSON_ERROR_NONE && count($aItemErrors)) {
			$this->addEntity(new Skynetcore_Convert_Array_Xml_Entity($aItemErrors, 'item_error', 'item_errors'));
		}
		return $this;
	}
}