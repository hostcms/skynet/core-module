<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Mp_Ozon_Item_Controller extends Hostcms_Ozon_Item_Controller
{

	/**
	 * Get shop items
	 * @return array
	 */
	public function getOzonShopItems($aShopItemIDs = [-1])
	{
		// Выгружать товары из подгрупп
		$groupTableName = isset($this->_config['subgroups']) && $this->_config['subgroups']
			? 'hostcms_ozon_subgroups_tmp'
			: 'hostcms_ozon_group_shop_groups';

		$oShop_Items = $this->Shop->Shop_Items;
		$oShop_Items->queryBuilder()
			->clearSelect()
			->select(
				'shop_items.*',
				array('hostcms_ozon_groups.id', 'data_hostcms_ozon_group_id'),
				array('hostcms_ozon_groups.ozon_group_id', 'data_ozon_group_id')
			)
			->join($groupTableName, 'shop_items.shop_group_id', '=', $groupTableName . '.shop_group_id')
			->join('hostcms_ozon_groups', $groupTableName . '.hostcms_ozon_group_id', '=', 'hostcms_ozon_groups.id')
			->where($groupTableName . '.shop_group_id', '!=', 0)
			->where('shop_items.active', '=', 1)
			// Для модификации при выгрузке нужна группа, установленная родительскому товару. Они добавляются при выгузке основного товара
			->where('shop_items.modification_id', '=', 0)
			->where('shop_items.shortcut_id', '=', 0)
			->where('shop_items.id', 'IN', $aShopItemIDs)
			->clearOrderBy()
			->orderBy('shop_items.id', 'ASC');

		// Выгружать товары только в наличии
		if (isset($this->_config['in_stock']) && $this->_config['in_stock'])
		{
			$this->applyInStockConditions($oShop_Items);
		}

		return $oShop_Items->findAll(FALSE);
	}

	public function getAttributes(Shop_Item_Model $oShop_Item, $hostcms_ozon_group_id, $ozon_group_id) {
//		Skynetcore_Utils::p($hostcms_ozon_group_id, '$hostcms_ozon_group_id');
		$aTmpAttributes = [];
		$oShop = $oShop_Item->shop;
		$sMeasureWeight = rtrim($oShop->Shop_Measure->name, '. ');

		switch ($sMeasureWeight)
		{
			case 'кг':
				$weight_unit = 'kg';
				break;
			case 'фунт':
				$weight_unit = 'lb';
				break;
			default:
				$weight_unit = 'g';
		}
		// Коэффициент для пересчета стандартных единиц измерения, которые не принимает Озон (метры и футы)
		$dimensionCoefficient = 1;

		switch ($oShop->size_measure)
		{
			case 0:
			default:
				$dimension_unit = 'mm';
				$sMeasureDimension = 'мм';
				break;
			case 1:
				$dimension_unit = 'cm';
				$sMeasureDimension = 'см';
				break;
			case 2: // метры
				$dimension_unit = 'cm';
				$dimensionCoefficient = 100;
				$sMeasureDimension = 'м';
				break;
			case 3:
				$dimension_unit = 'in';
				$sMeasureDimension = 'дюйм';
				break;
			case 4: // футы
				$dimension_unit = 'cm';
				$dimensionCoefficient = 30.48;
				$sMeasureDimension = 'фут';
				break;
		}
		$oHostcms_Ozon_Group_Attributes = Core_Entity::factory('Hostcms_Ozon_Group_Attribute');
		$oHostcms_Ozon_Group_Attributes->queryBuilder()
			->select('hostcms_ozon_group_attributes.*')
			->where('hostcms_ozon_group_attributes.hostcms_ozon_group_id', '=', $hostcms_ozon_group_id)
			->join('hostcms_ozon_attributes', 'hostcms_ozon_attributes.id', '=', 'hostcms_ozon_group_attributes.hostcms_ozon_attribute_id')
			->where('hostcms_ozon_attributes.shop_id', '=', $this->Shop->id);

		$aHostcms_Ozon_Group_Attributes = $oHostcms_Ozon_Group_Attributes->findAll(FALSE);

		foreach ($aHostcms_Ozon_Group_Attributes as $oHostcms_Ozon_Group_Attribute)
		{
			$oHostcms_Ozon_Attribute = $oHostcms_Ozon_Group_Attribute->Hostcms_Ozon_Attribute;

			// "Вес, г", "Длина, см"
			$aAttributeMeasure = explode(',', $oHostcms_Ozon_Attribute->name);

			// Название единицы измерения, требуемой Озону
			$sAttributeMeasure = trim(end($aAttributeMeasure));

			$value = NULL;

			if ($oHostcms_Ozon_Group_Attribute->property_id < 0)
			{
				switch ($oHostcms_Ozon_Group_Attribute->property_id)
				{
					case -1: // Название товара
						$value = $oShop_Item->name;
						break;
					case -2: // Название родительского товара модификации
						$oShop_Item = $oShop_Item->modification_id
							? $oShop_Item->Modification
							: $oShop_Item;

						$value = $oShop_Item->name;
						break;
					case -3: // 'Производитель'
						if ($oShop_Item->shop_producer_id)
						{
							$oShop_Producer = $oShop_Item->Shop_Producer;

							$value = $oShop_Producer->name;
						}
						break;
					case -4: // Ширина
						$value = $oShop_Item->width;
						$value = intval(Core_Str::convertDimension($sMeasureDimension, $sAttributeMeasure, $value));
						break;
					case -5: // Длина
						$value = $oShop_Item->length;
						$value = intval(Core_Str::convertDimension($sMeasureDimension, $sAttributeMeasure, $value));
						break;
					case -6: // Высота
						$value = $oShop_Item->height;
						$value = intval(Core_Str::convertDimension($sMeasureDimension, $sAttributeMeasure, $value));
						break;
					case -7: // Вес
						$value = $oShop_Item->weight;
						$value = intval(Core_Str::convertWeight($sMeasureWeight, $sAttributeMeasure, $value));
						break;
					case -8: // Ширина упаковки
						$value = $oShop_Item->package_width;
						$value = intval(Core_Str::convertDimension($sMeasureDimension, $sAttributeMeasure, $value));
						break;
					case -9: // Длина упаковки
						$value = $oShop_Item->package_length;
						$value = intval(Core_Str::convertDimension($sMeasureDimension, $sAttributeMeasure, $value));
						break;
					case -10: // Высота упаковки
						$value = $oShop_Item->package_height;
						$value = intval(Core_Str::convertDimension($sMeasureDimension, $sAttributeMeasure, $value));
						break;
					case -11: // Вес с упаковкой
						$value = $oShop_Item->package_weight;
						$value = intval(Core_Str::convertWeight($sMeasureWeight, $sAttributeMeasure, $value));
						break;
					case -12: // Описание
						$value = $oShop_Item->description;
						break;
					case -13: // Артикул
						$value = $oShop_Item->marking;
						break;
					case -14: // Код товара от производителя
						$value = $oShop_Item->vendorcode;
						break;
					case -15: // Текст
						$value = $oShop_Item->text;
						break;
					case -16: // Продавец
						if ($oShop_Item->shop_seller_id)
						{
							$oShop_Seller = $oShop_Item->Shop_Seller;

							$value = $oShop_Seller->name;
						}
						break;
					case -17: // Продавец
						if ($oShop_Item->shop_measure_id)
						{
							$oShop_Measure = $oShop_Item->Shop_Measure;

							$value = $oShop_Measure->name;
						}
						break;
					case -18: // Страна производства
						$value = $oShop_Item->country_of_origin;
						break;
					case -19: // Отличие товара от других (значение тега <sales_notes>)
						$value = $oShop_Item->yandex_market_sales_notes;
						break;
					case -20: // Название группы
						$oShop_Item = $oShop_Item->modification_id
							? $oShop_Item->Modification
							: $oShop_Item;

						$value = $oShop_Item->shop_group_id
							? $oShop_Item->Shop_Group->name
							: '';
						break;
					case -21: // Название группы Озон
						$oHostcms_Ozon_Group = Core_Entity::factory('Hostcms_Ozon_Group')->getByOzon_group_id($ozon_group_id, FALSE);

						if (!is_null($oHostcms_Ozon_Group))
						{
							$value = $oHostcms_Ozon_Group->name;
						}
						break;
				}
			}
			else
			{
				// $oProperty = $oHostcms_Ozon_Attribute->ozon_attribute_id
				// 	? $this->_getProperty($oHostcms_Ozon_Attribute->ozon_attribute_id)
				// 	: NULL;
				$oProperty = $oHostcms_Ozon_Group_Attribute->property_id
					? $oHostcms_Ozon_Group_Attribute->Property
					: NULL;

				if (!is_null($oProperty))
				{
					$aProperty_Values = $oProperty->getValues($oShop_Item->id, FALSE);

					if (isset($aProperty_Values[0]) && $aProperty_Values[0]->value != '')
					{
						$oProperty_Value = $aProperty_Values[0];

						switch ($oProperty->type)
						{
							case 0: // Int
							case 1: // String
							case 4: // Textarea
							case 6: // Wysiwyg
							case 7: // Checkbox
							case 10: // Hidden field
							case 11: // Float
								$value = $oProperty_Value->value;
								break;
							case 2: // File
								$value = $oProperty_Value->file == ''
									? ''
									: $oProperty_Value->setHref($oShop_Item->getItemHref())->getLargeFileHref();
								break;
							case 3:
								if (Core::moduleIsActive('list'))
								{
									$value = $oProperty_Value->List_Item->value;
								}
								break;
							case 5: // Informationsystem
								$value = $oProperty_Value->value
									? $oProperty_Value->Informationsystem_Item->name
									: '';
								break;
							case 8: // Date
								$value = Core_Date::sql2date($oProperty_Value->value);
								break;
							case 9: // Datetime
								$value = Core_Date::sql2datetime($oProperty_Value->value);
								break;
							case 12: // Shop
								$value = $oProperty_Value->value
									? $oProperty_Value->Shop_Item->name
									: '';
								break;
							default:
								$value = $oProperty_Value->value;
						}
					}
				}
			}

			if ($value == '')
			{
				$value = trim($oHostcms_Ozon_Group_Attribute->default_value);
			}

			Core_Event::notify('Hostcms_Ozon_Item_Controller.onBeforeSetAttributeValue', $this, array($value, $oHostcms_Ozon_Group_Attribute, $oShop_Item, $ozon_group_id, $hostcms_ozon_group_id));

			$eventResult = Core_Event::getLastReturn();
			!is_null($eventResult) && $value = $eventResult;

			if ($value != '')
			{
				$aTmpAttributes[] = array(
					// 'complex_id' => 0,
					'id' => intval($oHostcms_Ozon_Attribute->ozon_attribute_id),
					'values' => array(
						array(
							'dictionary_value_id' => $this->_getDictionaryValueId($oHostcms_Ozon_Attribute->dictionary_id, $value),
							'value' => strval($value)
						)
					)
				);
			}
//			// Не заполнен обязательный атрибут
//			elseif ($oHostcms_Ozon_Group_Attribute->is_required && !in_array($oHostcms_Ozon_Attribute->name, array('Изображение')))
//			{
//				Core_Log::instance()->clear()
//					->status(Core_Log::$MESSAGE)
//					->write("Hostcms_Ozon_Item_Controller::_addItem() не заполнен обязательный атрибут {$oHostcms_Ozon_Attribute->name} для товара '{$oShop_Item->name}', id {$oShop_Item->id}");
//
//				return $this;
//			}
		}

//		// Характеристики
//		$oHostcms_Ozon_Attributes = Core_Entity::factory('Hostcms_Ozon_Attribute');
//		$oHostcms_Ozon_Attributes->queryBuilder()
//			->select('hostcms_ozon_attributes.*')
//			->join('hostcms_ozon_group_attributes', 'hostcms_ozon_group_attributes.hostcms_ozon_attribute_id', '=', 'hostcms_ozon_attributes.id')
//			->where('hostcms_ozon_group_attributes.hostcms_ozon_group_id', '=', $hostcms_ozon_group_id)
//			->where('hostcms_ozon_attributes.shop_id', '=', $oShop->id);

//		$aHostcms_Ozon_Attributes = $oHostcms_Ozon_Attributes->findAll(FALSE);
//
//		foreach ($aHostcms_Ozon_Attributes as $oHostcms_Ozon_Attribute)
//		{
//			// "Вес, г", "Длина, см"
//			$aAttributeMeasure = explode(',', $oHostcms_Ozon_Attribute->name);
//			// Название единицы измерения, требуемой Озону
//			$sAttributeMeasure = trim(end($aAttributeMeasure));
//
//			$value = NULL;
//
//			if ($oHostcms_Ozon_Attribute->property_id < 0)
//			{
//				switch ($oHostcms_Ozon_Attribute->property_id)
//				{
//					case -1: // Название товара
//						$value = $oShop_Item->name;
//						break;
//					case -2: // Название родительского товара модификации
//						$oShop_Item = $oShop_Item->modification_id
//							? $oShop_Item->Modification
//							: $oShop_Item;
//
//						$value = $oShop_Item->name;
//						break;
//					case -3: // 'Производитель'
//						if ($oShop_Item->shop_producer_id)
//						{
//							$oShop_Producer = $oShop_Item->Shop_Producer;
//
//							$value = $oShop_Producer->name;
//						}
//						break;
//					case -4: // Ширина
//						$value = intval($oShop_Item->width);
//						$value = Core_Str::convertDimension($sMeasureDimension, $sAttributeMeasure, $value);
//						break;
//					case -5: // Длина
//						$value = intval($oShop_Item->length);
//						$value = Core_Str::convertDimension($sMeasureDimension, $sAttributeMeasure, $value);
//						break;
//					case -6: // Высота
//						$value = intval($oShop_Item->height);
//						$value = Core_Str::convertDimension($sMeasureDimension, $sAttributeMeasure, $value);
//						break;
//					case -7: // Вес
//						$value = intval($oShop_Item->weight);
//						$value = Core_Str::convertWeight($sMeasureWeight, $sAttributeMeasure, $value);
//						break;
//					case -8: // Ширина упаковки
//						$value = intval($oShop_Item->package_width);
//						$value = Core_Str::convertDimension($sMeasureDimension, $sAttributeMeasure, $value);
//						break;
//					case -9: // Длина упаковки
//						$value = intval($oShop_Item->package_length);
//						$value = Core_Str::convertDimension($sMeasureDimension, $sAttributeMeasure, $value);
//						break;
//					case -10: // Высота упаковки
//						$value = intval($oShop_Item->package_height);
//						$value = Core_Str::convertDimension($sMeasureDimension, $sAttributeMeasure, $value);
//						break;
//					case -11: // Вес с упаковкой
//						$value = intval($oShop_Item->package_weight);
//						$value = Core_Str::convertWeight($sMeasureWeight, $sAttributeMeasure, $value);
//						break;
//					case -12: // Описание
//						$value = $oShop_Item->description;
//						break;
//				}
//			}
//			else
//			{
//				$oProperty = $oHostcms_Ozon_Attribute->ozon_attribute_id
//					? $this->_getProperty($oHostcms_Ozon_Attribute->ozon_attribute_id)
//					: NULL;
//
//				if (!is_null($oProperty))
//				{
//					$aProperty_Values = $oProperty->getValues($oShop_Item->id, FALSE);
//
//					if (isset($aProperty_Values[0]) && $aProperty_Values[0]->value != '')
//					{
//						switch ($oProperty->type)
//						{
//							case 3:
//								if (Core::moduleIsActive('list'))
//								{
//									$value = $aProperty_Values[0]->List_Item->value;
//								}
//								break;
//							default:
//								$value = $aProperty_Values[0]->value;
//						}
//					}
//				}
//			}
//
//			if ($value == '')
//			{
//				$value = $oHostcms_Ozon_Attribute->default_value;
//			}
//
//			$value != '' && $aTmpAttributes[] = array(
//				// 'complex_id' => 0,
//				'id' => intval($oHostcms_Ozon_Attribute->ozon_attribute_id),
//				'values' => array(
//					array(
//						'dictionary_value_id' => $this->_getDictionaryValueId($oHostcms_Ozon_Attribute->dictionary_id, $value),
//						'value' => $value
//					)
//				)
//			);
//		}
		return $aTmpAttributes;
	}

	public function getConformity(Shop_Item_Model $oShop_Item, $name, $defaultValue)
	{
		return parent::_getConformity($oShop_Item, $name, $defaultValue);
	}
}