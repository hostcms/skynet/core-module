<?php
//use Gam6itko\OzonSeller\Service\V1\CategoriesService;
use GuzzleHttp\Client as GuzzleClient;
//use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use Http\Factory\Guzzle\RequestFactory;
use Http\Factory\Guzzle\StreamFactory;
use Gam6itko\OzonSeller\Service\V2 as psV2;
use Gam6itko\OzonSeller\Service\V3 as psV3;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Skynetcore_Mp_Ozon_Controller
 */
class Skynetcore_Mp_Ozon_Controller extends Skynetcore_Mp_Controller
{
	public static function writeCategoriesToDB($categories, $parent_id=0) {
		foreach ($categories as $category) {
			$localSaveItem = false;
			/** @var Skynetcore_Mp_Ozon_Category_Model $dbCategory */
			$dbCategory = Core_Entity::factory('Skynetcore_Mp_Ozon_Category')->getById($category['category_id'], false);
			if(!(isset($dbCategory->id) && $dbCategory->id > 0)) {
				$dbCategory = Core_Entity::factory('Skynetcore_Mp_Ozon_Category');
				$dbCategory->id = $category['category_id'];
				$dbCategory->parent_id = $parent_id;
				$dbCategory->title = '';
				$dbCategory = $dbCategory->create();
			}
			if($parent_id != $dbCategory->parent_id) {
				$dbCategory->parent_id = $parent_id;
				$localSaveItem = true;
			}
			if($category['title'] != $dbCategory->title) {
				$dbCategory->title = $category['title'];
				$localSaveItem = true;
			}
			$localSaveItem && $dbCategory = $dbCategory->save();
			if(isset($category['children'])
				&& is_array($category['children'])
				&& count($category['children']) > 0
			) {
				self::writeCategoriesToDB($category['children'], $dbCategory->id);
			}
		}
	}

	static public function createAdminOrder($posting, $posting_statuses = [
		'awaiting_packaging' => 0,
		'awaiting_deliver' => 0,
		'delivering' => 0,
		'company_id' => 0,
		'status_id' => 0,
		'shop_id' => 0,
	]) {
		$retValues = new stdClass();
		$retValues->status = -1;
		$retValues->message = "Неизвестная ошибка";
		if(isset($posting->products) && is_array($posting->products) && count($posting->products)) {
			/** @var Skynetcore_Shop_Order_Model $ozonOrder */
			$ozonOrder = Core_Entity::factory('Skynetcore_Shop_Order')
				->getByInvoice($posting->order_number);
			$possibleName = '';
			if(isset($posting->addressee->name)) {
				$possibleName = explode(' ', trim($posting->addressee->name));
			}
			$tmpSurname = Core_Array::get($possibleName, 0, '');
			$tmpName = Core_Array::get($possibleName, 1, '');
			$tmpPatronymic = Core_Array::get($possibleName, 2, '');
			if(trim($tmpSurname.' '.$tmpName.' '.$tmpPatronymic) == '') {
				$tmpName = 'Ozon';
			}
			$customer = $posting->customer;

			if(is_null($ozonOrder)) {
				/** @var Skynetcore_Shop_Order_Model $newOzonOrder */
				$newOzonOrder = Core_Entity::factory('Skynetcore_Shop_Order');
				$newOzonOrder->shop_id = Core_Array::get($posting_statuses, 'shop_id', 0);
				$ozonOrder = $newOzonOrder->save();
				$retValues->status = 1;
			} else {
				$retValues->status = 2;
			}
			if(isset($customer->customer_email)) {
				$ozonOrder->email = $customer->customer_email;
			}
			$ozonOrder->invoice = $posting->order_number;
			$ozonOrder->guid = $ozonOrder->id;
			$ozonOrder->datetime = Core_Date::timestamp2sql(strtotime($posting->in_process_at));
			$ozonOrder->company_id = Core_Array::get($posting_statuses, 'company_id', 0);
			$ozonOrder->name = $tmpName;
			$ozonOrder->surname = $tmpSurname;
			$ozonOrder->patronymic = $tmpPatronymic;
			$ozonOrder->shop_order_status_id = Core_Array::get($posting_statuses, $posting->status, Core_Array::get($posting_statuses, 'posting_status', 0));

			$ozonOrder->save();
			$aProperties = Core_Entity::factory('Property')->getAllByTag_name([
				'order-ozon-delivery-id',
				'order-ozon-id',
				'order-ozon-shipment-date',
				'order-ozon-source-code',
			], false, 'IN');
			/** @var Property_Model $oProperty */
			foreach ($aProperties as $oProperty) {
				$aPropertyValues = $oProperty->getValues($ozonOrder->id);
				$oTmpPropertyValue = Core_Array::get($aPropertyValues, 0, false);
				if($oTmpPropertyValue === false) {
					$oTmpPropertyValue = $oProperty->createNewValue($ozonOrder->id);
				}
				switch ($oProperty->tag_name) {
					case 'order-ozon-source-code':
						$oTmpPropertyValue->value = json_encode($posting, JSON_UNESCAPED_UNICODE);
						break;
					case 'order-ozon-delivery-id':
						$oTmpPropertyValue->value = $posting->posting_number;
						break;
					case 'order-ozon-id':
						$oTmpPropertyValue->value = $posting->order_id;
						break;
					case 'order-ozon-shipment-date':
						$oTmpPropertyValue->value = Core_Date::timestamp2sql(strtotime($posting->shipment_date));
						break;
				}
				$oTmpPropertyValue->save();
			}
			$aProducts = $posting->products;
			foreach ($aProducts as $product) {
				/** @var Skynetcore_Shop_Item_Model $oProduct */
				$oProduct = Core_Entity::factory('Skynetcore_Shop_Item')
					->getByMarking($product->offer_id);
				$productPrice = $product->price;
				/** @var Skynetcore_Shop_Order_Item_Model $oTmpItems */
				$oTmpItems = $ozonOrder->shop_order_items;
				$oTmpItem = $oTmpItems->getByMarking($product->offer_id);
				if(!(isset($oTmpItem->id)
					&& $oTmpItem->id > 0)
				) {
					/** @var Skynetcore_Shop_Order_Item_Model $oNewTmpItem */
					$oNewTmpItem = Core_Entity::factory('Skynetcore_Shop_Order_Item');
					$oNewTmpItem->shop_order_id = $ozonOrder->id;
					$oTmpItem = $oNewTmpItem->save();

					$oTmpItem->shop_item_id = (isset($oProduct->id)) ? $oProduct->id : 0;
					$oTmpItem->shop_measure_id = (isset($oProduct->shop->measure_id)) ? $oProduct->shop->measure_id : 0;
					$oTmpItem->name = (isset($oProduct->name) && $oProduct->name != '') ? $oProduct->name : ((trim($product->name) != '') ? $product->name : 'Неизвестный товар');
					$oTmpItem->quantity = $product->quantity;
					$oTmpItem->price = $productPrice;
					$oTmpItem->marking = $product->offer_id;
					$oTmpItem->save();
				}
			}
			$retValues->message = "OK";
		}
		return $retValues;
	}

	public static function syncCategories() {
		$instance = Core_Page::instance();

		$requestFactory = new RequestFactory();
		$streamFactory = new StreamFactory();
		$adapter = new GuzzleClient();
		/** @var \Gam6itko\OzonSeller\Service\V2\CategoryService $svcCategories */
		$svcCategories = new psV2\CategoryService(
			$instance->skynet->config['integration']['ozon'][$instance->skynet->request->envType],
			$adapter,
			$requestFactory,
			$streamFactory
		);
		$aCategoriesTree = $svcCategories->tree(0);
		self::writeCategoriesToDB($aCategoriesTree);
	}

	public static function syncCategoryAtributes() {
		$instance = Core_Page::instance();

		/** @var Skynetcore_Mp_Ozon_Category_Model $qCategories */
		$qCategories = Core_Entity::factory('Skynetcore_Mp_Ozon_Category');
		$qCategories
			->queryBuilder()
			->join(['property_value_strings', 'pvs'], 'pvs.value', '=', 'skynetcore_mp_ozon_categories.id', [
				['AND' => ['pvs.property_id', 'IN', [
					$instance->skynet->config['integration']['ozon']['properties']['ozon_category'],
					$instance->skynet->config['integration']['ozon']['properties']['ozon_item_category'],
				]]]
			])
			->leftJoin(['skynetcore_mp_ozon_category_links', 'ocl'], 'skynetcore_mp_ozon_categories.id', '=', 'ocl.ozon_category_id')
			->where('ocl.id', 'is', null)
			->groupBy('skynetcore_mp_ozon_categories.id')
		;
		$oCategories = $qCategories->findAll(false);
		$aCategories = array_map(function($oCategory) {
			return $oCategory->id;
		}, $oCategories);

		$requestFactory = new RequestFactory();
		$streamFactory = new StreamFactory();
		$adapter = new GuzzleClient();
		/** @var \Gam6itko\OzonSeller\Service\V3\CategoryService $svcCategories */
		$svcCategories = new psV3\CategoryService(
			$instance->skynet->config['integration']['ozon'][$instance->skynet->request->envType],
			$adapter,
			$requestFactory,
			$streamFactory
		);
		$aChunkedCategories = array_chunk($aCategories, 20);
		foreach ($aChunkedCategories as $aChunkedCategoriesNum => $aChunkedCategories) {
//			Skynetcore_Utils::p($aChunkedCategories, $aChunkedCategoriesNum . ' = aChunkedCategories');
			$aCategoryAttributes = $svcCategories->attribute($aChunkedCategories);
			foreach ($aCategoryAttributes as $aCategoryAttribute) {
				$categoryID = Core_Array::get($aCategoryAttribute, 'category_id', 0);
				$categoryAttributes = Core_Array::get($aCategoryAttribute, 'attributes', []);
				foreach ($categoryAttributes as $categoryAttribute) {
					/** @var Skynetcore_Mp_Ozon_Category_Atribute_Model $oCategoryAtribute */
					$oCategoryAtribute = Core_Entity::factory('Skynetcore_Mp_Ozon_Category_Atribute')->getById($categoryAttribute['id'], false);
					if(!(isset($oCategoryAtribute->id) && $oCategoryAtribute->id > 0)) {
						$oCategoryAtribute = Core_Entity::factory('Skynetcore_Mp_Ozon_Category_Atribute');
						$oCategoryAtribute->id = $categoryAttribute['id'];
						$oCategoryAtribute = $oCategoryAtribute->create();
					}
					$oCategoryAtribute->name = $categoryAttribute['name'];
					$oCategoryAtribute->description = $categoryAttribute['description'];
					$oCategoryAtribute->type = $categoryAttribute['type'];
					$oCategoryAtribute->is_collection = $categoryAttribute['is_collection']*1;
					$oCategoryAtribute->is_required = $categoryAttribute['is_required']*1;
					$oCategoryAtribute->group_id = $categoryAttribute['group_id']*1;
					$oCategoryAtribute->group_name = $categoryAttribute['group_name'];
					$oCategoryAtribute->dictionary_id = $categoryAttribute['dictionary_id']*1;
					$oCategoryAtribute->save();
				}
				foreach ($categoryAttributes as $categoryAttribute) {
					$attributeID = Core_Array::get($categoryAttribute, 'id', 0);
					/** @var Skynetcore_Mp_Ozon_Category_Link_Model $qCategoryAtributeLink */
					$qCategoryAtributeLink = Core_Entity::factory('Skynetcore_Mp_Ozon_Category_Link');
					$qCategoryAtributeLink
						->queryBuilder()
						->where('ozon_category_id', '=', $categoryID)
					;
					/** @var Skynetcore_Mp_Ozon_Category_Link_Model $qCategoryAtributeLink */
					$oCategoryAtributeLink = $qCategoryAtributeLink->getByOzon_atribute_id($attributeID);
					if(!(isset($oCategoryAtributeLink->id) && $oCategoryAtributeLink->id > 0)) {
						$oCategoryAtributeLink = Core_Entity::factory('Skynetcore_Mp_Ozon_Category_Link');
						$oCategoryAtributeLink->ozon_category_id = $categoryID;
						$oCategoryAtributeLink->ozon_atribute_id = $attributeID;
						$oCategoryAtributeLink = $oCategoryAtributeLink->create();
					}
				}
			}
		}
	}

	public static function syncShopItems() {
//		Skynetcore_Utils::p('Hi');
	}

	public static function syncItems() {
		$instance = Core_Page::instance();
		$propertyIdToUnload = $instance->skynet->config['integration']['ozon']['properties']['id_to_upload'];

		/** @var Core_QueryBuilder_Select $qItemsToUploadTmp */
		$qItemsToUploadTmp = Core_QueryBuilder::select('si.id')
			->select('si.marking')
			->select('si.name')
			->from(['getGroupsIerarchyUp', 'giu'])
			->join(['shop_items', 'si'], 'si.id', '=', 'giu.id')
			->leftJoin(['shop_items', 'siparent'], 'siparent.id', '=', 'si.modification_id')
			->leftJoin(['property_value_ints', 'items_to_unload'], 'items_to_unload.entity_id', '=', Core_QueryBuilder::expression('COALESCE(siparent.id, si.id)'), [
				['AND' => ['items_to_unload.property_id', '=', $propertyIdToUnload]]
			])
			->where(Core_QueryBuilder::expression('COALESCE(items_to_unload.value, 0)'), '=', 1)
//			->where('si.marking', 'IN', ['1060214458970','1060198437603'])
//			->where('si.marking', 'IN', ['1010864287070'])
//			->where('si.marking', 'IN', ['1060214458970'])
//			->where('si.marking', 'IN', ['1099927410003', '1042270584992', '1042560680498', '1060127126159','1057198985159'])
		;
		$aItemsToUploadTmpQuery = $qItemsToUploadTmp->asAssoc()->execute()->result();
		$aItemsToUploadTmp = [];
		foreach ($aItemsToUploadTmpQuery as $itemToUploadTmpQuery) {
			$aItemsToUploadTmp[$itemToUploadTmpQuery['marking']] = $itemToUploadTmpQuery;
		}
		if(count($aItemsToUploadTmp)) {
			$requestFactory = new RequestFactory();
			$streamFactory = new StreamFactory();
			$adapter = new GuzzleClient();
			/** @var \Gam6itko\OzonSeller\Service\V2\ProductService $svcProduct */
			$svcProduct = new psV2\ProductService($instance->skynet->config['integration']['ozon'][$instance->skynet->request->envType],
				$adapter,
				$requestFactory,
				$streamFactory
			);
			$aChunkedRecords = array_chunk($aItemsToUploadTmp, 999);
			$aOffers = [];
			foreach ($aChunkedRecords as $chunkedRecord => $aChunkedRecord) {
				$aFilterRecords = [];
				foreach ($aChunkedRecord as $oChunkedRecord) {
					$aFilterRecords[$oChunkedRecord['marking']] = $oChunkedRecord;
				}
				$aListTmp = $svcProduct->infoList(
					[
						"offer_id" => array_keys($aFilterRecords),
						"product_id" => [ ],
						"sku" => [ ]
					]
				);
				if(isset($aListTmp['items']) && is_array($aListTmp['items']) && count($aListTmp['items'])) {
					foreach ($aListTmp['items'] as $listTmp) {
						$aOffers[$listTmp['offer_id']] = $listTmp;
					}
				}
				usleep(0.3 * 1000000);
			}
			foreach ($aItemsToUploadTmp as $itemsToUploadTmpMarking => $itemsToUploadTmp) {
				/** @var Skynetcore_Mp_Ozon_Item_Model $tryMpOzonItemTmp */
				$tryMpOzonItemTmp = Core_Entity::factory('Skynetcore_Mp_Ozon_Item');
				$tryMpOzonItem = $tryMpOzonItemTmp
					->getByShop_item_id($itemsToUploadTmp['id'], false);

				$ozonOfferId = '';
				$ozonOffer = false;
				if(isset($aOffers[$itemsToUploadTmpMarking]['id'])
					&& isset($aOffers[$itemsToUploadTmpMarking]['status']['state'])
				) {
					$ozonOfferId = $aOffers[$itemsToUploadTmpMarking]['id'];
					$ozonOffer = $aOffers[$itemsToUploadTmpMarking];
				} else {
					if(isset($tryMpOzonItem->id) && $tryMpOzonItem->id > 0) {
						$tryMpOzonItem->delete();
					}
				}
				if(!(isset($tryMpOzonItem->id) && $tryMpOzonItem->id > 0)) {
					$tryMpOzonItem = Core_Entity::factory('Skynetcore_Mp_Ozon_Item');
					$tryMpOzonItem->shop_item_id = $itemsToUploadTmp['id'];
					$tryMpOzonItem->create();
				}
				if(isset($ozonOffer['id']) && $ozonOffer['id'] != '') {
					$tryMpOzonItem->product_id = $ozonOfferId;
					$tryMpOzonItem->state = $ozonOffer['status']['state'];
					$tryMpOzonItem->state_failed = $ozonOffer['status']['state_failed'];
					$tryMpOzonItem->validation_state = $ozonOffer['status']['validation_state'];
					$tryMpOzonItem->state_name = $ozonOffer['status']['state_name'];
					$tryMpOzonItem->state_description = $ozonOffer['status']['state_description'];
					$tryMpOzonItem->is_failed = $ozonOffer['status']['is_failed']*1;
					$tryMpOzonItem->is_created = $ozonOffer['status']['is_created']*1;
					$tryMpOzonItem->visible = Core_Array::get($ozonOffer, 'visible', 0);
					$tryMpOzonItem->visibility_details = json_encode(Core_Array::get($ozonOffer, 'visibility_details', []), JSON_UNESCAPED_UNICODE);
					$tryMpOzonItem->item_errors = json_encode(Core_Array::get($ozonOffer['status'], 'item_errors', []), JSON_UNESCAPED_UNICODE);
					$tryMpOzonItem->state_updated_at = Core_Date::timestamp2sql(strtotime($ozonOffer['status']['state_updated_at']));
					$tryMpOzonItem->save();

					/** @var Skynetcore_Mp_Ozon_Item_Sync_Model $oTaskItem */
					$oTaskItem = Core_Entity::factory('Skynetcore_Mp_Ozon_Item_Sync')
						->getByShop_item_id($tryMpOzonItem->shop_item_id, false);
					if(isset($oTaskItem->id) && $oTaskItem->id > 0) {
						$oTaskItem->active = 0;
						$oTaskItem->save();
					}
				}
			}
		}
	}

	public function syncStocks() {
		$instance = Core_Page::instance();
		$envType = $instance->skynet->request->envType;
		$ozonConfig = $instance->skynet->config['integration']['ozon'][$envType];

		$qbCurrentChangedID = Core_QueryBuilder::select([Core_QueryBuilder::expression('MAX(id)'), 'max_id'])
			->from('skynetcore_history_shop_warehouse_items');
		$currentChangedID = Core_Array::get($qbCurrentChangedID->asAssoc()->execute()->result(false), 0, ['max_id' => 0])['max_id'];

		/** @var Skynetcore_Mp_Ozon_History_Model $qbLastChangedID */
		$qbLastChangedID = Core_Entity::factory('Skynetcore_Mp_Ozon_History');
		$qbLastChangedID
			->queryBuilder()
			->limit(1)
			->orderBy('id', 'DESC')
		;
		$lastChangedID = Core_Array::get($qbLastChangedID->findAll(false), 0, json_decode('{"last_warehouse_history_id": 0}'));

		$qbLastChanged = Core_QueryBuilder::select('swi.*')
			->select([Core_QueryBuilder::expression("giu.url"), 'dataurl'])
			->select([Core_QueryBuilder::expression("MAX(sh.id)"), 'datawarehouse_sh_id'])
			->select([Core_QueryBuilder::expression("COALESCE(sw.name, '')"), 'datawarehouse_name'])
			->select([Core_QueryBuilder::expression("COALESCE(si.marking, '')"), 'dataitem_marking'])
			->select([Core_QueryBuilder::expression("COALESCE(si.name, '')"), 'dataitem_name'])
			->from(['skynetcore_history_shop_warehouse_items', 'sh'])
			->join(['shop_warehouse_items', 'swi'], 'swi.id', '=', 'sh.shop_warehouse_item_id')
			->join(['shop_warehouses', 'sw'], 'sw.id', '=', 'swi.shop_warehouse_id')
			->join(['shop_items', 'si'], 'si.id', '=', 'sh.shop_item_id')
			->join(['getGroupsIerarchyUp', 'giu'], 'si.id', '=', 'giu.id')
			->where('sh.id', '>', $lastChangedID->last_warehouse_history_id*1)
			->where('swi.shop_warehouse_id', 'IN', $instance->skynet->config['active_warehouses'])
			->where('swi.count', '>=', 0)
//			->where('si.marking', 'IN', ["1060180785395", "1060469915354"])
//			->where('si.marking', 'IN', ["1052768158434"])
//			->where('si.marking', 'IN', ["1052163484599"])
//			->where('si.marking', 'IN', ["1051210960559"])
//			->where('si.marking', 'IN', ["1043209750942"])
//			->where('si.marking', 'IN', ["1057701324052"])
//			->where('si.marking', 'IN', ["1052122046300", "1051210960559", "1043209750942", "1057701324052"])
//			->where('si.marking', 'IN', ["1060508487600", "1099927410003"])
//			->where('si.marking', 'IN', ['1099927410003','1099046475794','1099108194523'])
			->groupBy('swi.id')
		;
//		$qbLastChanged->open();
		//-- Ограничение по складу и по разделу
		//-- ... AND ( giu.dirpath REGEXP '^/bikes/' AND swi.shop_warehouse_id IN (19) OR giu.dirpath NOT REGEXP '^/bikes/' )
//		$qbLastChanged
//			->open()
//			->where('giu.dirpath', 'REGEXP', '^/bikes/')
//			->where('swi.shop_warehouse_id', 'IN', [19])
//			->setOr()
//			->where('giu.dirpath', 'NOT REGEXP', '^/bikes/')
//			->close()
//		;
//		//-- Ограничение по складу и по разделу
//		//-- ... AND ( giu.dirpath REGEXP '^/sup-boards/' AND swi.shop_warehouse_id IN (19, 20) OR giu.dirpath NOT REGEXP '^/sup-boards/' )
//		$qbLastChanged
//			->open()
//			->where('giu.dirpath', 'REGEXP', '^/sup-boards/')
//			->where('swi.shop_warehouse_id', 'IN', [19, 20])
//			->setOr()
//			->where('giu.dirpath', 'NOT REGEXP', '^/sup-boards/')
//			->close()
//		;
//		$qbLastChanged->close();

		$aLastChanged = $qbLastChanged->asObject('Shop_Warehouse_Item_Model')->execute()->result(false);
		$lastChangedRecords = $lastChangedWarehousesTmp = $lastChangedRecordsTmp = [];
		foreach ($aLastChanged as $lastChangedTmp) {
			$oLastChangedTmp = $lastChangedTmp->getStdObject();
			$lastChangedRecordsTmp[$lastChangedTmp->dataitem_marking] =
				[
					'marking' => $oLastChangedTmp->dataitem_marking,
					'name' => $oLastChangedTmp->dataitem_name,
					'cat' => preg_replace('/^\/(.*?)\/(.*)$/ui', '$1', $oLastChangedTmp->dataurl),
					'path' => $oLastChangedTmp->dataurl,
				];
			$lastChangedWarehousesTmp[$lastChangedTmp->dataitem_marking][$lastChangedTmp->shop_warehouse_id] = [
					'whname' => $oLastChangedTmp->datawarehouse_name,
					'quantity' => ($lastChangedTmp->count - $lastChangedTmp->moto_reserve_count)
				];
		}
		foreach ($lastChangedRecordsTmp as $lastChangedRecordItem_id => $aLastChangedRecord) {
			$localSum = 0;
			foreach ($lastChangedWarehousesTmp[$lastChangedRecordItem_id] as $lastChangedRecordWhTmp) {
				$localSum += $lastChangedRecordWhTmp['quantity'];
			}
			$lastChangedRecords[$lastChangedRecordItem_id] = [
				'item' => $aLastChangedRecord,
				'warehouses' => $lastChangedWarehousesTmp[$lastChangedRecordItem_id],
				'sum' => $localSum,
			];
		}
		$textResult = 'none';
		if(count($lastChangedRecords)) {
			$confOzonWarehouses = $instance->skynet->config['integration']['ozon']['stocks'];

			$instance = Core_Page::instance();
			$requestFactory = new RequestFactory();
			$streamFactory = new StreamFactory();
			$adapter = new GuzzleClient();
			/** @var \Gam6itko\OzonSeller\Service\V2\ProductService $svcProduct */
			$svcProduct = new psV2\ProductService($instance->skynet->config['integration']['ozon'][$instance->skynet->request->envType],
				$adapter,
				$requestFactory,
				$streamFactory
			);

			$aChunkedRecords = array_chunk(array_keys($lastChangedRecords), 990);
			$aOffers = [];
			foreach ($aChunkedRecords as $chunkedRecord => $aChunkedRecord) {
				$list = $svcProduct->list(
					[
						'filter' => [
							'offer_id' => $aChunkedRecord
						]
					]
				);
				if(isset($list['items']) && is_array($list['items']) && count($list['items'])) {
					foreach ($list['items'] as $list_item) {
						if(isset($lastChangedRecords[$list_item['offer_id']])) {
							$aOffers[$list_item['offer_id']] = $list_item;
							$changed = $lastChangedRecords[$list_item['offer_id']];
							foreach ($confOzonWarehouses as $ozonWarehouseID => $confOzonWh) {
								$allowOzon = ($confOzonWh['allow']===true
									|| (is_array($confOzonWh['allow']) && in_array($changed['item']['cat'], $confOzonWh['allow']))
								) && ($confOzonWh['deny']===false
									|| ($confOzonWh['deny']===true
											&& (is_array($confOzonWh['allow']) && in_array($changed['item']['cat'], $confOzonWh['allow'])))
									|| (is_array($confOzonWh['deny']) && !in_array($changed['item']['cat'], $confOzonWh['deny'])));
								if($allowOzon) {
									$sumToSend = -1;
									foreach ($changed['warehouses'] as $umWarehouseId => $umWarehouse) {
										if(in_array($umWarehouseId, $confOzonWh['um_wh_ids'])) {
											if($sumToSend == -1) {
												$sumToSend = $umWarehouse['quantity'];
											} else {
												$sumToSend += $umWarehouse['quantity'];
											}
										}
									}
										$aOffers[$list_item['offer_id']]['ozon_warehouses'][$ozonWarehouseID] =
											$sumToSend >= 0 ? $sumToSend : 0;
								}
							}
						}
					}
				}
				usleep(0.3 * 1000000);
			}

			/** @var \Gam6itko\OzonSeller\Service\V2\ProductService $svcProductOffer */
			$svcProductOffer = new psV2\ProductService($instance->skynet->config['integration']['ozon'][$instance->skynet->request->envType],
				$adapter,
				$requestFactory,
				$streamFactory
			);
			if(count($aOffers)) {
				$aStocks = [];

				foreach ($aOffers as $offerID => $offer) {
					foreach (Core_Array::get($offer, 'ozon_warehouses', []) as $ozWhId => $ozWhQuantity) {
						$aStocks[] = [
							"offer_id" => $offer['offer_id'],
							"product_id" => $offer['product_id'],
							"stock" => $ozWhQuantity*1,
							"warehouse_id" => $ozWhId,
						];
					}
				}
				$aChunkedStocks = array_chunk($aStocks, 100);
				$aUpdateResults = [
					'ok' => [],
					'error' => [],
				];
				foreach ($aChunkedStocks as $aStocks) {
					$aStockResults = $svcProductOffer->importStocks($aStocks);
					foreach ($aStockResults as $tmpStockResult) {
						$aUpdateResults[(count($tmpStockResult['errors']) == 0 ? 'ok' : 'error')][] = $tmpStockResult;
					}
					sleep(2);
				}
				$textResult = json_encode([
					'processed' => count($aUpdateResults['ok']) / 2,
					'errors' => $aUpdateResults['error'],
				]);
				$oNewHistoryRecord = Core_Entity::factory('Skynetcore_Mp_Ozon_History');
				$oNewHistoryRecord->date = Core_Date::timestamp2sql(time());
				$oNewHistoryRecord->last_warehouse_history_id = $currentChangedID;
				$oNewHistoryRecord->status = (count($aUpdateResults['error']) == 0) * 1;
				$oNewHistoryRecord->result = $textResult;
				$oNewHistoryRecord->save();
			}
		}
		echo Core_Date::timestamp2sql(time()) . ': ' . $textResult . "\n";

	}
}