<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Skynetcore_Mp_Controller
 */
class Skynetcore_Mp_Controller extends Core_Controller
{
	protected $_instance = null;
	protected $_env = null;
	protected $_local_config = null;

	public function __construct(Core_Entity $oEntity)
	{
		parent::__construct($oEntity);
		$this->_instance = Core_Page::instance();
		$this->_env = $this->_instance->skynet->request->envType;
	}

	static public function create(Core_Entity $oEntity)
	{
		$className = static::class;

		if (!class_exists($className))
		{
			throw new Core_Exception("Class '%className' does not exist",
				array('%className' => $className));
		}

		return new $className($oEntity);
	}

	static public function makeSellMarketPrice($price, $conditions) {
		$vTotalPrice = 0;
		foreach ($conditions as $conditionKey => $condition) {
			if(isset($condition['condition']) && is_array($localCond = $condition['condition'])) {
				$localPrice = 0;
				if(isset($localCond['default'])) {
					switch (true) {
						case isset($localCond['default']['percent']):
							$localPrice = $price * $localCond['default']['percent'];
							break;
						case isset($localCond['default']['money']):
							$localPrice = $localCond['default']['money'];
							break;
					}
				}
				$vTotalPrice += $localPrice;
				$conditions[$conditionKey]['value'] = round($localPrice);
//				Skynetcore_Utils::p($localCond);
			}
		}
		$conditions['totalPrice']['value'] = $vTotalPrice;
//		Skynetcore_Utils::p($conditions);
		return $conditions;
	}
}