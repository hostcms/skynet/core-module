<?php
use GuzzleHttp\Client as GuzzleClient;
use Http\Factory\Guzzle\RequestFactory;
use Http\Factory\Guzzle\StreamFactory;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Skynetcore_Mp_Yamarket_Controller
 */
class Skynetcore_Mp_Yamarket_Controller extends Skynetcore_Mp_Controller
{
	protected $_client = null;
	protected $_businessID = 0;
	protected $_options = [];

	public function __construct(Core_Entity $oEntity)
	{
		parent::__construct($oEntity);

		$this->_local_config = $this->_instance->skynet->config['integration']['yamarket'][$this->_env];
		$this->_client = new GuzzleClient();
		$this->_businessID = $this->_local_config['businessID'];
		$this->_options = [
			'headers' => [
				'Authorization' => 'Bearer ' . $this->_local_config['apiKey'],
				'Accept' => 'application/json',
			]
		];
	}

	public static function getSellsMarketPrice($oShopItem) {
//		-Цена
//		- минус 1% обработка платежа
//		- минус комиссия (%) за продажу (зависит от категории - по умолчанию 14%, но в настройках наших категорий на сайте нужно сделать поле которое будет отвечать за назначение процента)
//		- минус логистика 5,5% (минимум 40р максимум 400р)
//		- минус обработка заказа 25р
//		- минус региональная доставка (далее скрин и ссылка справки ЯМ с расчетами региональной доставки)
		$aMarkingTypes = [
			'itemPrice'         => $oShopItem->price,
			'totalPrice'        => [
				'name' => 'Цена',
				'condition' => false,
			],
			'paymentProcessing' => [
				'name' => 'Обработка платежа, 1%',
				'condition' => [
					'default' => ['percent' => 0.01],
				],
			],
			'saleComission'     => [
				'name' => 'Комиссия за продажу',
				'condition' => [
					'default' => ['percent' => 0.14]
				],
			],
			'logisticsComission' => [
				'name' => 'Логистика',
				'condition' => [
					'default' => ['percent' => 0.055]
				],
			],
			'orderProcessing' => [
				'name' => 'Обработка заказа',
				'condition' => [
					'default' => ['money' => 25]
				],
			],
			'orderDelivery' => [
				'name' => 'Доствака',
				'condition' => [],
			],
		];
		$aMarkingTypes = parent::makeSellMarketPrice($oShopItem->price, $aMarkingTypes);
		return $aMarkingTypes;
	}

	public function syncMarketItemsToCms()
	{

	}

	public function syncShopItems() {
		/** @var Skynetcore_Mp_Yamarket_Item_Sync_Model $qItemsForUpdate */
		$qItemsForUpdate = Core_Entity::factory('Skynetcore_Mp_Yamarket_Item_Sync');
		$qItemsForUpdate
			->queryBuilder()
			->where('active', '=', 1)
		;
		$tmpLastChangedMarkingsToUpload = $qItemsForUpdate->getAllByActive(1, false);
		$lastChangedMarkingsToUpload = Skynetcore_Utils::array_map_assoc(function($oKey, $oItem) {
			return [$oItem->shop_item->marking, $oItem];
		}, $tmpLastChangedMarkingsToUpload);

		if(count($lastChangedMarkingsToUpload)) {
			$aSyncMarketToInsertItems = [];
			if(count($lastChangedMarkingsToUpload)) {
				/**
				 * @var  $tmpLastChangedMarking
				 * @var Umotors_Shop_Item_Model $lastChangedMarkingToUpload
				 */
				foreach ($lastChangedMarkingsToUpload as $tmpLastChangedMarking => $lastChangedMarkingToUpload) {
					$oTmpOfferToUpload = $lastChangedMarkingToUpload->shop_item;
//					Skynetcore_Utils::p($oTmpOfferToUpload->id);
					if(is_object($oTmpOfferToUpload)
						&& method_exists($oTmpOfferToUpload, 'getMarketExportJson')
						&& $oTmpOfferToUpload->id > 0
					) {
						$aTmpOfferToUpload = $oTmpOfferToUpload->getMarketExportJson();
						$aTmpOfferToUpload['price'] = $oTmpOfferToUpload->getMpPrice('yamarket');
						$aSyncMarketToInsertItems[]['offer'] = $aTmpOfferToUpload;
					}
				}
			}
			if(count($aSyncMarketToInsertItems)) {
				$aSplitedMarketToInsertItems = array_chunk($aSyncMarketToInsertItems, 200);
				foreach ($aSplitedMarketToInsertItems as $splitedMarketToInsertItems) {
					$aMarketToInsertOffers = [
						'offerMappings' => $splitedMarketToInsertItems
					];
					$aPricesForUpload = [];
					foreach ($splitedMarketToInsertItems as $splitedMarketToInsertItemKey => $splitedMarketToInsertItem) {
						if(isset($splitedMarketToInsertItem['offer']['price'])) {
							unset($splitedMarketToInsertItems[$splitedMarketToInsertItemKey]['offer']['price']);
							$aPricesForUpload[] = [
								'offerId' => $splitedMarketToInsertItem['offer']['offerId'],
								'price' => [
									'value' => $splitedMarketToInsertItem['offer']['price']*1.0,
									'currencyId' => 'RUR'
								],
							];
						}
					}
					try {
						$responseOffersUpdate = $this->_client
							->request('POST',
								$this->_local_config['host'] . '/businesses/' . $this->_businessID . '/offer-mappings/update',
								array_merge($this->_options, [
									GuzzleHttp\RequestOptions::JSON => $aMarketToInsertOffers,
//								'debug' => true,
								])
							);
						if($responseOffersUpdate->getStatusCode()  == 200
							&& ($responseOffersBody = json_decode($responseOffersUpdate->getBody()->getContents())) instanceof stdClass
							&& isset($responseOffersBody->status)
							&& $responseOffersBody->status == 'OK'
						) {
							foreach ($splitedMarketToInsertItems as $aMarketToInsertOffer) {
								if(isset($lastChangedMarkingsToUpload[$aMarketToInsertOffer['offer']['offerId']])
									&& ($tmpSyncItem = $lastChangedMarkingsToUpload[$aMarketToInsertOffer['offer']['offerId']]) instanceof Skynetcore_Mp_Yamarket_Item_Sync_Model
								) {
									$tmpSyncItem->active = 0;
									$tmpSyncItem->response = json_encode($responseOffersBody);
									$tmpSyncItem->save();
									echo("\nВыполнено успешно, " . $aMarketToInsertOffer['offer']['offerId']);
								}
							}
						} else {
							foreach ($splitedMarketToInsertItems as $aMarketToInsertOffer) {
								if(isset($lastChangedMarkingsToUpload[$aMarketToInsertOffer['offer']['offerId']])
									&& ($tmpSyncItem = $lastChangedMarkingsToUpload[$aMarketToInsertOffer['offer']['offerId']]) instanceof Skynetcore_Mp_Yamarket_Item_Sync_Model
								) {
									$tmpSyncItem->response = json_encode($responseOffersBody);
									$tmpSyncItem->save();
									echo("\nОшибка, " . $aMarketToInsertOffer['offer']['offerId']).', '
										. $tmpSyncItem->response;
								}
							}
							throw new Exception(json_encode($responseOffersBody));
						}
						if(count($aPricesForUpload)) {
							$aMarketToUploadPrices = [
								'offers' => $aPricesForUpload
							];
							$responseOfferPricesUpdate = $this->_client
								->request('POST',
									$this->_local_config['host'].'/businesses/'.$this->_businessID.'/offer-prices/updates',
									array_merge($this->_options, [
										GuzzleHttp\RequestOptions::JSON => $aMarketToUploadPrices,
//							'debug' => true,
									])
								)
							;
							$aOffersUpdatePricesResult = json_decode($responseOfferPricesUpdate->getBody()->getContents());
						}
					} catch (\Exception $e) {
						Skynetcore_Chat_Telegram_Controller::sendError($e->getMessage());
					}
				}
			}
		}
	}

	public function syncStocks() {
		$qbCurrentChangedID = Core_QueryBuilder::select([Core_QueryBuilder::expression('MAX(id)'), 'max_id'])
			->from('skynetcore_history_shop_warehouse_items');
		$currentChangedID = Core_Array::get($qbCurrentChangedID->asAssoc()->execute()->result(false), 0, ['max_id' => 0])['max_id'];

		/** @var Skynetcore_Mp_Yamarket_History_Model $qbLastChangedID */
		$qbLastChangedID = Core_Entity::factory('Skynetcore_Mp_Yamarket_History');
		$qbLastChangedID
			->queryBuilder()
			->limit(1)
			->orderBy('id', 'DESC')
		;
		//-- "2023-06-19T15:22:00Z" --
		$currentDateTime = new DateTime(date('Y-m-d H:i:s', time()));
		$lastChangedID = Core_Array::get($qbLastChangedID->findAll(false), 0, json_decode('{"last_warehouse_history_id": 0}'));
		$qbLastChanged = Core_QueryBuilder::select('swi.id')
			->select('swi.shop_warehouse_id')
			->select('swi.shop_item_id')
//			->select([Core_QueryBuilder::expression("CASE WHEN item_top.id IS NULL THEN 0 ELSE `swi`.`count` END"), 'count'])
			->select([Core_QueryBuilder::expression("CASE WHEN item_top.id IS NULL AND COUNT(DISTINCT items_mod_related.id) > 0 THEN 0 ELSE `swi`.`count` END"), 'count'])
			->select('swi.moto_reserve_count')
			->select('swi.user_id')
			->select([Core_QueryBuilder::expression("giu.url"), 'dataurl'])
			->select([Core_QueryBuilder::expression("MAX(sh.id)"), 'datawarehouse_sh_id'])
			->select([Core_QueryBuilder::expression("COALESCE(sw.name, '')"), 'datawarehouse_name'])
			->select([Core_QueryBuilder::expression("COALESCE(si.marking, '')"), 'dataitem_marking'])
			->select([Core_QueryBuilder::expression("COALESCE(si.name, '')"), 'dataitem_name'])
			->from(['skynetcore_history_shop_warehouse_items', 'sh'])
			->join(['shop_warehouse_items', 'swi'], 'swi.id', '=', 'sh.shop_warehouse_item_id')
			->join(['shop_warehouses', 'sw'], 'sw.id', '=', 'swi.shop_warehouse_id')
			->join(['shop_items', 'si'], 'si.id', '=', 'sh.shop_item_id')
			->join(['getGroupsIerarchyUp', 'giu'], 'si.id', '=', 'giu.id')
			->leftJoin(['shop_items', 'item_top'], 'item_top.id', '=', 'si.modification_id', [
				['AND' => ['item_top.active', '=', 1]],
				['AND' => ['item_top.deleted', '=', 0]],
			])
			->leftJoin(['shop_items', 'items_mod_related'], 'items_mod_related.modification_id', '=', 'si.id', [
				['AND' => ['si.active', '=', 1]],
				['AND' => ['si.deleted', '=', 0]],
			])
//			  LEFT JOIN shop_items AS items_mod_related ON items_mod_related.modification_id = si.id
			->where('sh.id', '>', $lastChangedID->last_warehouse_history_id*1)
			->where('swi.shop_warehouse_id', 'IN', $this->_instance->skynet->config['active_warehouses'])
			->where('swi.count', '>=', 0)
//			->where('si.id', 'IN', [572715, 572716, 572717, 572718, 572719, 572720, 572736, 621390])
//			->where('si.marking', 'IN', ["1057644272945", "1057652868278"])
//			->where('si.marking', 'IN', ["1042181133080"])
			->leftJoin(['property_value_ints', 'yamarket'], 'yamarket.entity_id', '=', 'si.id', [
				['AND' => ['yamarket.property_id', '=', 1874]],
				['AND' => ['yamarket.value', '=', 1]],
			])
//			->leftJoin(['field_value_ints', 'fviw'], 'fviw.entity_id', '=', Core_QueryBuilder::expression('COALESCE(item_top.id, si.id)'), [
//				['AND' => ['fviw.field_id', '=', 4]]
//			])
//			->leftJoin(['field_value_ints', 'fvih'], 'fvih.entity_id', '=', Core_QueryBuilder::expression('COALESCE(item_top.id, si.id)'), [
//				['AND' => ['fvih.field_id', '=', 5]]
//			])
//			->leftJoin(['field_value_ints', 'fvil'], 'fvil.entity_id', '=', Core_QueryBuilder::expression('COALESCE(item_top.id, si.id)'), [
//				['AND' => ['fvil.field_id', '=', 3]]
//			])
//			->leftJoin(['field_value_floats', 'fviwgt'], 'fviwgt.entity_id', '=', Core_QueryBuilder::expression('COALESCE(item_top.id, si.id)'), [
//				['AND' => ['fviwgt.field_id', '=', 6]]
//			])
//			->where(Core_QueryBuilder::expression('CASE WHEN (COALESCE(fviw.value, 0) > 0 AND COALESCE(fvih.value, 0) > 0 AND COALESCE(fvil.value, 0) > 0 AND COALESCE(fviwgt.value, 0)) > 0 THEN 1 ELSE 0 END'), '=', 1)
			->where(Core_QueryBuilder::expression('CASE WHEN (item_top.package_width > 0 AND item_top.package_height > 0 AND item_top.package_length > 0 AND item_top.package_weight > 0) THEN 1 ELSE 0 END'), '=', 1)
			->groupBy('swi.id')
			->groupBy('si.id')
			->orderBy('si.id')
		;
//		Skynetcore_Utils::p($qbLastChanged->build(), date("Y-m-d H:i:s")); die();
		$aLastChanged = $qbLastChanged->asObject('Shop_Warehouse_Item_Model')->execute()->result(false);
		Skynetcore_Utils::p("Получен ответ SQL:", date("Y-m-d H:i:s"));
		$lastChangedRecords = $lastChangedMarkings =
		$lastChangedWarehousesTmp = $lastChangedRecordsTmp =
		$aYandexMarketItems = [];
		foreach ($aLastChanged as $lastChangedTmp) {
			$oLastChangedTmp = $lastChangedTmp->getStdObject();
			$lastChangedRecordsTmp[$lastChangedTmp->dataitem_marking] =
				[
					'marking' => $oLastChangedTmp->dataitem_marking,
					'name' => $oLastChangedTmp->dataitem_name,
					'cat' => preg_replace('/^\/(.*?)\/(.*)$/ui', '$1', $oLastChangedTmp->dataurl),
					'path' => $oLastChangedTmp->dataurl,
				];
			$lastChangedWarehousesTmp[$lastChangedTmp->dataitem_marking][$lastChangedTmp->shop_warehouse_id] = [
					'whname' => $oLastChangedTmp->datawarehouse_name,
					'quantity' => ($lastChangedTmp->count - $lastChangedTmp->moto_reserve_count)
				];
		}
		foreach ($lastChangedRecordsTmp as $lastChangedRecordItem_id => $aLastChangedRecord) {
			$localSum = 0;
			foreach ($lastChangedWarehousesTmp[$lastChangedRecordItem_id] as $lastChangedRecordWhTmp) {
				$localSum += $lastChangedRecordWhTmp['quantity'];
			}
			$lastChangedRecords[$lastChangedRecordItem_id] = [
				'item' => $aLastChangedRecord,
				'warehouses' => $lastChangedWarehousesTmp[$lastChangedRecordItem_id],
				'sum' => $localSum,
			];
		}
//		Skynetcore_Utils::p($lastChangedRecords);
//		die();
		$textResult = 'none';
		$lastChangedMarkings = array_unique(array_keys($lastChangedRecords));
		Skynetcore_Utils::p("Найдено элементов: ".count($lastChangedMarkings), Core_Date::timestamp2datetime(time()));
//		Skynetcore_Utils::p($lastChangedMarkings); die();
		if(count($lastChangedRecords)) {
			$confYamarketWarehouses = $this->_instance->skynet->config['integration']['yamarket']['stocks'];
			$aWarehousesIdsForRequest = [];
			print_r('Начинаем цикл $confYamarketWarehouses:'."\n");
			foreach ($confYamarketWarehouses as $aConfYamarketWarehouseKey => $aConfYamarketWarehouse) {
				foreach ($aConfYamarketWarehouse as $confYamarketWarehouseKey => $confYamarketWarehouse) {
					print_r("aConfYamarketWarehouseKey={$aConfYamarketWarehouseKey}, confYamarketWarehouseKey={$confYamarketWarehouseKey}, aWarehousesIdsForRequest". count($aWarehousesIdsForRequest) ."\n");
					$aWarehousesIdsForRequest = array_merge($aWarehousesIdsForRequest, $confYamarketWarehouse['um_wh_ids']);
					$aWarehousesIdsForRequest = array_unique($aWarehousesIdsForRequest);
				}
			}
			$aWarehousesIdsForRequest = array_unique($aWarehousesIdsForRequest);
			print_r('Завершился цикл $confYamarketWarehouses: '.count($aWarehousesIdsForRequest)."\n");

			print_r('Начинаем цикл $lastChangedRecords:'.count($lastChangedRecords)."\n");
			$num = 0;
			foreach ($lastChangedRecords as $lastChangedRecordMarking => $lastChangedRecord) {
				/** @var Umotors_Shop_Item_Model $oTmpItemMarking */
				$oTmpItemMarking = Core_Entity::factory('Moto_Shop_Item')->getByMarking($lastChangedRecordMarking, false);
				if(!is_null($oTmpItemMarking)) {
					if(($num % 500) == 0) {
						print_r('$aTmpRestWarehouses Start: '.$num.' of '.count($lastChangedRecords).', '.$lastChangedRecordMarking."");
					}
					$aTmpRestWarehouses = $oTmpItemMarking->getRestCountWithSizes($aWarehousesIdsForRequest, 'NOT IN', 'warehouses');
					if(($num % 500) == 0) {
						print_r(' End: ' . $lastChangedRecordMarking . "\n");
					}
					foreach ($aTmpRestWarehouses as $tmpRestWarehouseID => $tmpRestWarehouse) {
						if(!isset($lastChangedRecords[$lastChangedRecordMarking]['warehouses'][$tmpRestWarehouseID])) {
							$lastChangedRecords[$lastChangedRecordMarking]['warehouses'][$tmpRestWarehouseID] = [
								'whname' => $tmpRestWarehouse['whname'],
								'quantity' => $tmpRestWarehouse['counts']*1,
							];
						}
					}
					$num++;
				}
			}
			print_r('Цикл $lastChangedRecords завершен:'.count($lastChangedRecords)."\n");
//------------------- Получаем каталог ---------------------------------------------------------------------------------
			$lastChangedMarkingsLength = 200; //-- MaxLimit - https://yandex.ru/dev/market/partner-api/doc/ru/reference/business-assortment/getOfferMappings
			$chunkedChangedMarkings = array_chunk($lastChangedMarkings, $lastChangedMarkingsLength);

			//-- Размещаем в магазинах маркета -------------------------------------------------------------------------
			$limitIterations = 1000;
			$queryNoCampaignOffers = [
				'limit' => $lastChangedMarkingsLength, //-- Максимальный лимит 200
			];
			$aNoCampaignOffersTmp = [];
			Skynetcore_Utils::p("Начинаем получение каталога");
			do {
				$aGoodsStatuses = [
					"NO_CARD_ADD_TO_CAMPAIGN",
					"HAS_CARD_CAN_UPDATE",
				];
				//-- https://yandex.ru/dev/market/partner-api/doc/ru/reference/content/getOfferCardsContentStatus -
				$responseNoCampaignOffers = $this->_client
					->request('POST',
					$this->_local_config['host'].'/businesses/'.$this->_businessID.'/offer-cards'.Skynetcore_Entity_Request::queryFromArray(
							$queryNoCampaignOffers
						),
						array_merge($this->_options, [
							GuzzleHttp\RequestOptions::JSON => [
								"cardStatuses" => $aGoodsStatuses
							],
//							'debug' => true,
						])
					)
				;
				print_r(", получен ответ: ".$responseNoCampaignOffers->getStatusCode()."\n");
				if($responseNoCampaignOffers->getStatusCode() == 200) {
					$oYamarketNoCampaign = json_decode($responseNoCampaignOffers->getBody()->getContents());
					if(json_last_error() == JSON_ERROR_NONE) {
						$nextPageToken = (isset($oYamarketNoCampaign->result->paging->nextPageToken) ? $oYamarketNoCampaign->result->paging->nextPageToken : '');
						if(isset($oYamarketNoCampaign->result->offerCards)) {
							foreach ($oYamarketNoCampaign->result->offerCards as $offerCard) {
								$aNoCampaignOffersTmp[$offerCard->offerId] = $offerCard;
							}
						}
						if($nextPageToken == '') break;
						$queryNoCampaignOffers['page_token'] = $nextPageToken;
					}
					print_r("Следующий токен для получения данных: ".$responseNoCampaignOffers->getStatusCode()."");
				} else {
					Skynetcore_Utils::p('Ошибка : '.$responseNoCampaignOffers->getStatusCode().', '.$responseNoCampaignOffers->getBody());
				}
				$limitIterations--;
			} while ($limitIterations > 0);
			if(count($aNoCampaignOffersTmp)) {
				Skynetcore_Utils::p("Не привязано товаров в маркете к магазинам: ".count($aNoCampaignOffersTmp), '');
				$chunkedNoCampaignOffersTmp = array_chunk($aNoCampaignOffersTmp, 200, true);
				$aNoCampaignStocksTmp = [];
				foreach ($chunkedNoCampaignOffersTmp as $noCampaignOfferPage => $aNoCampaignOffers) {
					$aShop_Items = Core_Entity::factory('Umotors_Shop_Item')->getAllByMarking(array_keys($aNoCampaignOffers), false, 'IN');
					/** @var Umotors_Shop_Item_Model $oShopItem */
					foreach ($aShop_Items as $oShopItem) {
						$mpStocksTmp = $oShopItem->getMpStocks('Yamarket', $this->_instance->skynet->config['integration']['yamarket']['stocks']);
						foreach ($mpStocksTmp as $campaignId => $yandexWarehouses) {
							foreach ($yandexWarehouses as $yaWarehouseId => $yaWarehouseItem) {
								$itemCounts = Core_Array::get($yaWarehouseItem, 'um_wh_all_stocks', 0)*1;
								if($itemCounts >= 0) {
									$aNoCampaignStocksTmp[$campaignId][] = [
										"sku" => $yaWarehouseItem['marking'],
										"warehouseId" => $yaWarehouseId,
										"items" => [
											[
												"count" => $itemCounts,
												"type" => "FIT",
												"updatedAt" => $currentDateTime->format(DateTime::ATOM)
											]
										]
									];
								}
							}
						}
					}
				}

				foreach ($aNoCampaignStocksTmp as $campaignId => $noCampaignStocks) {
					$chunkedNoCampaignStocksTmp = array_chunk($noCampaignStocks, 200, true);
					foreach ($chunkedNoCampaignStocksTmp as $hoCampaignPage => $aNoCampaignStocks) {
						$requestForNoCampaignStocks = ['skus' => array_values($aNoCampaignStocks)];
						$putOptions = array_merge($this->_options, [
							'json' => $requestForNoCampaignStocks
						]);
						try {
							$responseStocks = $this->_client
								->request('PUT',
									$this->_local_config['host'].'/campaigns/'.$campaignId.'/offers/stocks',
									$putOptions
								)
							;
							$aResponced[$campaignId][] = json_decode($responseStocks->getBody()->getContents(), true);
						} catch (\Exception $re) {
							Skynetcore_Utils::p($putOptions, 'Ошибка : $campaignId='.$campaignId.', '.$re->getMessage());
						}
					}
				}
			}

			foreach ($chunkedChangedMarkings as $aChunkedChangedMarkingKey => $aChunkedChangedMarkings) {
				$responseOffers = $this->_client
					->request('POST',
						$this->_local_config['host'].'/businesses/'.$this->_businessID.'/offer-mappings'.Skynetcore_Entity_Request::queryFromArray([
							'limit' => $lastChangedMarkingsLength
						]),
						array_merge($this->_options, [
							GuzzleHttp\RequestOptions::JSON => ["offerIds" => $aChunkedChangedMarkings],
//							'debug' => true,
						])
					)
				;

				if($responseOffers->getStatusCode() == 200) {
					$aOffers = json_decode($responseOffers->getBody()->getContents())->result->offerMappings;
					echo "\nПрилетело товаров от маркета на итерации ".$aChunkedChangedMarkingKey.': '.count($aOffers);
					foreach ($aOffers as $stdOffer) {
						$aYandexMarketItems[$stdOffer->offer->offerId] = $stdOffer->offer;
					}
				} else {
					Skynetcore_Utils::p('Ошибка на итерации '.$aChunkedChangedMarkingKey.': '.$responseOffers->getStatusCode().', '.$responseOffers->getBody());
				}
			}
			$lastChangedMarkingsToUpload = $lastChangedPricesToUpload = [];
			foreach ($lastChangedMarkings as $tmpLastChangedMarking) {
				if(!isset($aYandexMarketItems[$tmpLastChangedMarking])) {
					$lastChangedMarkingsToUpload[$tmpLastChangedMarking] = $lastChangedRecordsTmp[$tmpLastChangedMarking];
				}
			}
			$aMarketToInsertItemsTmp = [];
			if(count($lastChangedMarkingsToUpload)) {
				foreach ($lastChangedMarkingsToUpload as $tmpLastChangedMarking => $lastChangedMarkingToUpload) {
					/** @var Umotors_Shop_Item_Model $oEntity */
					$oEntity = Core_Entity::factory('Umotors_Shop_Item')->getByMarking($tmpLastChangedMarking);
					$aTmpOfferToUpload = $oEntity->getMarketExportJson();
					$aMarketToInsertItemsTmp[]['offer'] = $aTmpOfferToUpload;
				}
			}
			if(count($aMarketToInsertItemsTmp)) {
				$aSplitedMarketToInsertOffers = array_chunk($aMarketToInsertItemsTmp, 200);
				foreach ($aSplitedMarketToInsertOffers as $aSplitedMarketToInsertOffersPage => $aMarketToInsertItems) {
					$aMarketToInsertOffers = [
						'offerMappings' => $aMarketToInsertItems
					];
					$responseOffersUpdate = $this->_client
						->request('POST',
							$this->_local_config['host'].'/businesses/'.$this->_businessID.'/offer-mappings/update',
							array_merge($this->_options, [
								GuzzleHttp\RequestOptions::JSON => $aMarketToInsertOffers,
//							'debug' => true,
							])
						)
					;
					$aOffersUpdateResult = json_decode($responseOffersUpdate->getBody()->getContents());
				}
			}
//------------------- Загружаем цены ---------------------------------------------------------------------------------
			Skynetcore_Utils::p('', 'Загружаем цены, товаров: '.count($lastChangedMarkings));
			$pricesLastChangedMarkings = array_chunk($lastChangedMarkings, 100);
			foreach ($pricesLastChangedMarkings as $tmpPricePage => $aChunkedPricesLastChangedMarkings) {
				$aPricesForUpload = [];
				/** @var Umotors_Shop_Item_Model $oEntity */
				$aShopItems = Core_Entity::factory('Umotors_Shop_Item')->getAllByMarking($aChunkedPricesLastChangedMarkings, false, 'IN');
				/** @var Umotors_Shop_Item_Model $oTmpPriceItem */
				foreach ($aShopItems as $oTmpPriceItem) {
					/** @var Umotors_Shop_Item_Model $oPriceItem */
					$oPriceItem = $oTmpPriceItem;
					if($oTmpPriceItem->modification_id > 0) {
						$oPriceItem = Core_Entity::factory('Umotors_Shop_Item')->getById($oTmpPriceItem->modification_id, false);
					}
					$tmpItemPrice = $oPriceItem->getMpPrice('yamarket')*1;

					if(isset($aYandexMarketItems[$oPriceItem->marking])
						&& (!isset($aYandexMarketItems[$oPriceItem->marking]->basicPrice->value)
						|| (isset($aYandexMarketItems[$oPriceItem->marking]->basicPrice->value)
							  && $aYandexMarketItems[$oPriceItem->marking]->basicPrice->value != $tmpItemPrice)
						)
					) {
						if($tmpItemPrice > 0) {
							$aPricesForUpload[] = [
								'offerId' => $oTmpPriceItem->marking,
								'price' => [
									'value' => $tmpItemPrice*1.0,
									'currencyId' => 'RUR'
								],
							];
						}
					}
				}
				if(count($aPricesForUpload)) {
					$aMarketToUploadPrices = [
						'offers' => $aPricesForUpload
					];
					$responseOfferPricesUpdate = $this->_client
						->request('POST',
							$this->_local_config['host'].'/businesses/'.$this->_businessID.'/offer-prices/updates',
							array_merge($this->_options, [
								GuzzleHttp\RequestOptions::JSON => $aMarketToUploadPrices,
//							'debug' => true,
							])
						)
					;
					$aOffersUpdatePricesResult = json_decode($responseOfferPricesUpdate->getBody()->getContents());
				}
			}
//------------------- Получаем каталог ---------------------------------------------------------------------------------
			$aQuery = [
				'limit' => 200, //-- Максимальный лимит 200
			];
			$aLisOfCampaignItems = $aLisOfMarketItems = [];
			foreach ($this->_local_config['campaigns'] as $campaignID) {
				$limitIterations = 1000;
				$aQuery['page_token'] = '';
				do {
					$aGoodsStatuses = [
						"statuses" => [
							"PUBLISHED",
							"CHECKING",
							"DISABLED_BY_PARTNER",
							"DISABLED_AUTOMATICALLY",
							"REJECTED_BY_MARKET",
							"CREATING_CARD",
							"NO_CARD",
							"NO_STOCKS",
						]
					];
					$query = Skynetcore_Entity_Request::queryFromArray($aQuery);
					$response = $this->_client
						->request('POST',
							$this->_local_config['host'].'/campaigns/'.$campaignID.'/offers'.$query,
							array_merge($this->_options, [
								GuzzleHttp\RequestOptions::JSON => $aGoodsStatuses,
//							'debug' => true,
							])
						)
					;
					if($response->getStatusCode() == 200) {
						$oYamarketItems = json_decode($response->getBody()->getContents());
						if(json_last_error() == JSON_ERROR_NONE) {
							$nextPageToken = (isset($oYamarketItems->result->paging->nextPageToken) ? $oYamarketItems->result->paging->nextPageToken : '');
							$tmpItems = $oYamarketItems->result->offers;
							// Skynetcore_Utils::p("-{$nextPageToken}-, Получено товаров при синхронизации с маркетом: ".count($tmpItems), $this->_local_config['host'].'/campaigns/'.$campaignID.'/offer-mapping-entries'.$query);
							foreach ($tmpItems as $tmpItem) {
								$aLisOfCampaignItems[$campaignID][$tmpItem->offerId] = $tmpItem;
							}
							if($nextPageToken == '') break;
							$aQuery['page_token'] = $nextPageToken;
						}
					} else {
						Skynetcore_Utils::p('Ошибка : '.$responseOffers->getStatusCode().', '.$responseOffers->getBody());
					}
					$limitIterations--;
					echo "-{$limitIterations}=";
				} while ($limitIterations > 0);
			}

			$aOffers = [];
			Skynetcore_Utils::p("Выход из цикла, готовим остатки к синхронизации: ".count($aLisOfCampaignItems));
			if(count($aLisOfCampaignItems)) {
//				Skynetcore_Utils::p($aLisOfCampaignItems);
				foreach ($aLisOfCampaignItems as $campaignID => $aLisOfItems) {
					$aChunkedRecords = array_chunk(array_keys($lastChangedRecords), 1);
					foreach ($aChunkedRecords as $chunkedRecordIndex => $aChunkedRecord) {
						foreach ($aChunkedRecord as $chunkedMarking) {
							if(isset($aLisOfItems[$chunkedMarking])
								&& isset($confYamarketWarehouses[$campaignID])
								&& isset($aLisOfItems[$chunkedMarking])
								&& isset($aLisOfItems[$chunkedMarking]->name)
							) {
								$yaMarketListItem = $aLisOfItems[$chunkedMarking];
								$aOffers[$campaignID][$chunkedMarking]['name'] = $yaMarketListItem->name;
								$aOffers[$campaignID][$chunkedMarking]['marking'] = $yaMarketListItem->offerId;
								$changed = $lastChangedRecords[$chunkedMarking];
								$aConfYamarketWarehouses = $confYamarketWarehouses[$campaignID];

								foreach ($aConfYamarketWarehouses as $yamarketWarehouseID => $confYamarketWh) {
									$allowYamarket = ($confYamarketWh['allow'] === true
											|| (is_array($confYamarketWh['allow']) && in_array($changed['item']['cat'], $confYamarketWh['allow']))
										) && ($confYamarketWh['deny'] === false
											|| ($confYamarketWh['deny'] === true
												&& (is_array($confYamarketWh['allow']) && in_array($changed['item']['cat'], $confYamarketWh['allow'])))
											|| (is_array($confYamarketWh['deny']) && !in_array($changed['item']['cat'], $confYamarketWh['deny'])));

									if ($allowYamarket) {
										$sumToSend = -1;
										foreach ($changed['warehouses'] as $umWarehouseId => $umWarehouse) {
											if (in_array($umWarehouseId, $confYamarketWh['um_wh_ids'])) {
												if ($sumToSend == -1) {
													$sumToSend = $umWarehouse['quantity'];
												} else {
													$sumToSend += $umWarehouse['quantity'];
												}
											}
										}
										$aOffers[$campaignID][$chunkedMarking]['yamarket_warehouses'][$yamarketWarehouseID] =
											$sumToSend >= 0 ? $sumToSend : 0;
									}
								}
							}
						}
					}
				}
				Skynetcore_Utils::p("Выход из цикла, синхронизируем остатки: ".count($aOffers));
				if(count($aOffers)) {
					$aStocks = [];
					foreach ($aOffers as $campaignID => $aCampaignOffers) {
						foreach ($aCampaignOffers as $offerID => $offer) {
							foreach (Core_Array::get($offer, 'yamarket_warehouses', []) as $yaWhId => $yaWhQuantity) {
//							$offerID == 1060207148156 &&
								$aStocks[$campaignID][] = [
									"sku" => $offerID,
									"warehouseId" => $yaWhId,
									"items" => [
										[
											"count" => $yaWhQuantity * 1,
											"type" => "FIT",
											"updatedAt" => $currentDateTime->format(DateTime::ATOM)
										],
									],
								];
							}
						}
					}
					Core_Event::notify('Skynetcore_Mp_Yamarket_Controller.onAfterSetStocks', $this, [$aStocks]);
					$lastReturnedValue = Core_Event::getLastReturn();
					if(!is_null($lastReturnedValue)) {
						$aStocks = $lastReturnedValue;
					}
					echo "Просчитали остатки. Записываем в маркет: ".count($aStocks);
					if(true && count($aStocks)) {
						$aResponced = [];
						$aResponcedErrors = [];
						foreach ($aStocks as $campaignID => $stocks) {
							echo "Просчитали остатки. Записываем в маркет: {$campaignID}".count($stocks);
							$aChunkedStocks = array_chunk($stocks, 500);
							foreach ($aChunkedStocks as $aTmpStocksKey => $aTmpStocks) {
								$decodedResponce = '';
								$requestForStocks = ['skus' => $aTmpStocks];
								echo "\n{$campaignID}: ".count($aTmpStocks);
//								echo "\n".json_encode($requestForStocks, JSON_UNESCAPED_UNICODE);
								$putOptions = array_merge($this->_options, [
									'json' => $requestForStocks
								]);
								try {
									$responseStocks = $this->_client
										->request('PUT',
											$this->_local_config['host'].'/campaigns/'.$campaignID.'/offers/stocks',
											$putOptions
										)
									;
									if($response->getStatusCode() == 200) {
										$decodedResponce = json_decode($responseStocks->getBody()->getContents(), true);
										$aResponced[$campaignID][] = $decodedResponce;
										($aTmpStocksKey == 0) && Skynetcore_Utils::p($decodedResponce);
										echo ", получено: ".count($decodedResponce);
									} else {
										throw new Exception($responseOffers->getStatusCode().', '.$responseOffers->getBody());
									}
								} catch (\Exception $re) {
									$aResponcedErrors[$campaignID][] = $requestForStocks;
									Skynetcore_Utils::p('Ошибка : '.$re->getMessage());
									Skynetcore_Utils::p($decodedResponce);
//									$filesource = Core_Array::get($f = debug_backtrace(), 0, []);
//									$sFileSource = "== [".Core_Array::get($filesource, 'line', '').'] '.Core_Array::get($filesource, 'file', '').' ==';
//									echo $sFileSource."\n";
								}
							}
						}
						Skynetcore_Utils::p($aResponcedErrors, '$aResponcedErrors');
						if(count($aResponced)) {
							/** @var Skynetcore_Mp_Yamarket_History_Model $oNewHistoryRecord */
							$oNewHistoryRecord = Core_Entity::factory('Skynetcore_Mp_Yamarket_History');
							$oNewHistoryRecord->date = Core_Date::timestamp2sql(time());
							$oNewHistoryRecord->last_warehouse_history_id = $currentChangedID;
							$oNewHistoryRecord->status = 1;
							$oNewHistoryRecord->result = ''; // json_encode($aResponced);
							// Skynetcore_Utils::p($oNewHistoryRecord->toArray(), $lastChangedID);
							$oNewHistoryRecord->save();
						}
					}
				}
			}
		}
	}
}