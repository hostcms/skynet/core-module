<?php
use Skynetcore_Utils as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Mp_History_Model extends Core_Entity
{

	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;
}