<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');

use Skynetcore_Utils as utl;

class Skynetcore_Controller_Process extends Skynetcore_Command_Controller
{
	public function testhtmlAction() {
		ob_start();
		?>
		<table class="mp-table-small">
			<thead>
			<tr>
				<th colspan="12">2022</th>
			</tr>
			<tr>
				<th>Декабрь</th>
				<th>Ноябрь</th>
				<th>Октябрь</th>
				<th>Сентябрь</th>
				<th>Август</th>
				<th>Июль</th>
				<th>Июнь</th>
				<th>Май</th>
				<th>Апрель</th>
				<th>Март</th>
				<th>Февраль</th>
				<th>Январь</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>7 131 452</td>
				<td>6 430 790</td>
				<td>3 962 698</td>
				<td>2 978 761</td>
				<td>2 145 254</td>
				<td>718 639</td>
				<td>-</td>
				<td>315 263</td>
				<td>357 435</td>
				<td>-</td>
				<td>-</td>
				<td>-</td>
			</tr>
			</tbody>
		</table>
		<?php
		$localHtml = ob_get_clean();
		$this->_status_code = 200;
        $this->_status = "OK";
        $this->_message = "Тестовый ответ";
		$this->_html = $localHtml;
		return $this->sendResponse();
	}

	public function showFormAction() {
		$instance = Core_Page::instance();
		$formId = Core_Array::getRequest('form_id', Core_Array::getRequest('form', 0));
		/** @var Form_Model $oForm */
		$oForm = Core_Entity::factory('Form')->getById($formId);
		if(isset($oForm->id) && $oForm->id > 0) {
			$oXsl = Core_Entity::factory('Xsl')->getByName(Core_Array::getRequest('xsl', 'undefined'));
			if(isset($oXsl->id) && $oXsl->id > 0) {
				/** @var Form_Controller_Show $Form_Controller_Show */
				$Form_Controller_Show = new Skynetcore_Form_Controller_Show($oForm);
				ob_start();
				$Form_Controller_Show
					->xsl($oXsl)
					->show();
				$content = ob_get_clean();
				$this->_html = [ 'modal' => "<div class='modal'><div class='modal__body'>{$content}</div></div>" ];
				$this->_status = 'OK';
				$this->_message = 'OK';
			} else {
				$this->_message = 'Макет `'.Core_Array::getRequest('xsl', 'undefined').'` для формы `'.$oForm->name.'` не найден';
			}
		} else {
			$this->_message = 'Форма не найдена';
		}
		$this->_status_code = 200;
		return $this->sendResponse();
	}

	public function sendFormAction() {
		$this->checkCSRF();
		$instance = Core_Page::instance();
		$formId = Core_Array::getRequest('form_id', Core_Array::getRequest('form', 0));
		/** @var Form_Model $oForm */
		$oForm = Core_Entity::factory('Form')->getById($formId);
		if(isset($oForm->id) && $oForm->id > 0) {
			/** @var Form_Controller_Show $Form_Controller_Show */
			$Form_Controller_Show = new Skynetcore_Form_Controller_Show($oForm);
			if (!is_null(Core_Array::getPost($oForm->button_name)))
			{
				$sXslName = 'undefined';
				$oField = Core_Entity::factory('Field')->getByTag_name('xsl_template');
				if(isset($oField->id) && $oField->id > 0) {
					$oFormUserTemplate = Core_Array::get($oField->getValues($oForm->id), 0, false);
					if(isset($oFormUserTemplate->value) && $oFormUserTemplate->value != '') {
						$sXslName = $oFormUserTemplate->value;
					}
				}
				$oXsl = Core_Entity::factory('Xsl')->getByName($sXslName);
				if(is_null($oXsl)) {
					$oXsl = Core_Entity::factory('Xsl')->getByName(Core_Array::getRequest('xsl', 'undefined'), false);
				}
				ob_start();

				$Form_Controller_Show
					->values($_POST + $_FILES)
					->mailType(0)
					->mailXsl($oXsl)
					->mailFromFieldName('email')
					->process()
				;
				/** @var Core_Mail_Smtp $mi */
				$mi = Core_Mail::instance()->getStatus();
				$error = '';
				$errID = $Form_Controller_Show->getError();
				is_null($errID) && $errID = 10000;
				$aErrors = [
					0 => 'Вы неверно ввели число подтверждения отправки формы!',
					1 => 'Заполните все обязательные поля!',
					2 => 'Прошло слишком мало времени с момента последней отправки Вами формы!',
					3 => 'Ошибка! Форма не может быть обработана!',
					10000 => 'Форма зарегистрирована успешно!'
				];
				echo $aErrors[$errID];
				$resp = ob_get_clean();

				$this->_html = trim($resp);
				$this->_status = $errID == 10000 ? 'OK' : 'ERROR';
				$this->_message = $aErrors[$errID];
			} else {
				$this->_message = 'Неверное название кнопки';
			}
		} else {
			$this->_message = 'Форма не найдена';
		}
		$this->_status_code = 200;
//		Skynetcore_Utils::p('Hi');
		return $this->sendResponse();
	}

	public static function clearSessionDeliveryConditionOrder() {
//		Skynetcore_Utils::p($_SESSION); die();
		if(isset($_SESSION['hostcmsOrder'])
			&& isset($_SESSION['hostcmsOrder']['shop_delivery_condition_id'])
		) {
			$_SESSION['hostcmsOrder']['shop_delivery_condition_id'] = 0;
		}
		if(isset($_SESSION['hostcmsOrder'])
			&& isset($_SESSION['hostcmsOrder']['shop_delivery_id'])
		) {
			$_SESSION['hostcmsOrder']['shop_delivery_id'] = 0;
		}
		if(isset($_SESSION['hostcmsOrder'])
			&& isset($_SESSION['hostcmsOrder']['shop_payment_system_id'])
		) {
			$_SESSION['hostcmsOrder']['shop_payment_system_id'] = 0;
		}
		self::applyRequestToSessionOrder();
		return true;
	}

	public static function applyRequestToSessionOrder($asEntity = false) {
		Core_Session::start();
//		Skynetcore_Utils::p($_SESSION['hostcmsOrder']);
		$hostcmsOrderSession = Core_Array::getSession('hostcmsOrder', [
			'shop_country_id' => 0,
			'shop_country_location_id' => 0,
			'shop_country_location_city_id' => 0,
			'shop_country_location_city_area_id' => 0,
			'shop_delivery_condition_id' => 0,
			'shop_delivery_id' => 0,
			'shop_payment_system_id' => 0,
			'partial_payment_by_personal_account' => 0,
			'postcode' => '',
			'address' => '',
			'house' => '',
			'flat' => '',
			'surname' => '',
			'name' => '',
			'patronymic' => '',
			'phone' => '',
			'email' => '',
			'description' => '',
			'organization_payment' => 'off',
			'store_properties' => [],
			'coupon_text' => '',
		]);
		$currentCountryID = Core_Array::get($hostcmsOrderSession, 'shop_country_id', 0);
		$currentCountryLocationID = Core_Array::get($hostcmsOrderSession, 'shop_country_location_id', 0);
		$currentCountryLocationCityID = Core_Array::get($hostcmsOrderSession, 'shop_country_location_city_id', 0);
		$currentDeliveryConditionID = Core_Array::get($hostcmsOrderSession, 'shop_delivery_condition_id', 0);
		$currentPaymentSystemID = Core_Array::get($hostcmsOrderSession, 'shop_payment_system_id', 0);

		$aProperties = Core_Array::get($hostcmsOrderSession, 'store_properties', ['property' => []]);
//		Skynetcore_Utils::p($hostcmsOrderSession);
//		Skynetcore_Utils::p($aProperties);
		if(isset($_REQUEST) && is_array($_REQUEST)) {
			foreach ($_REQUEST as $rqKey => $rqValue) {
				$propertiesMatches = [];
				if(preg_match('/^property_(\d+)$/ui', $rqKey, $propertiesMatches)) {
					$property_id = Core_Array::get($propertiesMatches, 1, 0);
					if($property_id > 0) {
						$aProperties['property'][$property_id] = [
							'@attributes' => [
								'id' => Core_Array::get($propertiesMatches, 1, 0),
								'name' => $rqKey
							],
							'@value' => $rqValue,
						];
					}
				}
			}
		}

		if(isset($hostcmsOrderUpdateSession['json'])) unset($hostcmsOrderUpdateSession['json']);
//		$requestFullName = Core_Array::getRequest('full_name', '');
//		$surname = $name = $patronymic = '';
//		if($requestFullName != '') {
//			$aSplFullName = explode(' ', trim($requestFullName));
//			if(count($aSplFullName)) {
//				if(count($aSplFullName) == 1) {
//					$name = trim($requestFullName);
//				} else {
//					list($surname, $name, $patronymic) = $aSplFullName;
//				}
//			}
//		}
//		if(isset($_REQUEST['site_users_surname']) && $_REQUEST['site_users_surname'] == '') unset($_REQUEST['site_users_surname']);
//		if(isset($_REQUEST['site_users_name']) && $_REQUEST['site_users_name'] == '') unset($_REQUEST['site_users_name']);
//		if(isset($_REQUEST['site_users_patronymic']) && $_REQUEST['site_users_patronymic'] == '') unset($_REQUEST['site_users_patronymic']);
//		if(isset($_REQUEST['surname']) && $_REQUEST['surname'] == '') unset($_REQUEST['surname']);
//		if(isset($_REQUEST['name']) && $_REQUEST['name'] == '') unset($_REQUEST['name']);
//		if(isset($_REQUEST['patronymic']) && $_REQUEST['patronymic'] == '') unset($_REQUEST['patronymic']);

//		Skynetcore_Utils::p($_REQUEST, '$requestFullName');
//		Skynetcore_Utils::tp(
//				Core_Str::stripTags(
//						strval(Core_Array::getRequest(
//								'site_users_surname',
//								Core_Array::getRequest(
//										'surname',
//										Core_Array::get(
//												$hostcmsOrderSession,
//												'surname',
//												$surname
//										)
//								)
//						)
//						)
//				), 'surname XXX surname');
//		Skynetcore_Utils::p($_REQUEST, $requestFullName);
//		Skynetcore_Utils::p($hostcmsOrderSession, "{$surname} {$name} {$patronymic}");
//		die();
		$hostcmsOrderUpdateSession = [
			'shop_country_id' => intval(Core_Array::getRequest('country', Core_Array::getPost('shop_country_id', Core_Array::get($hostcmsOrderSession, 'shop_country_id', 175)))),
			'shop_country_location_id' => intval(Core_Array::getRequest('location', Core_Array::getPost('shop_country_location_id', Core_Array::get($hostcmsOrderSession, 'shop_country_location_id', 0)))),
			'shop_country_location_city_id' => intval(Core_Array::getRequest('sel_city', Core_Array::getPost('shop_country_location_city_id', Core_Array::get($hostcmsOrderSession, 'shop_country_location_city_id', 0)))),
			'shop_country_location_city_area_id' => intval(Core_Array::getRequest('area', Core_Array::getPost('shop_country_location_city_area_id', Core_Array::get($hostcmsOrderSession, 'shop_country_location_city_area_id', 0)))),
			'shop_delivery_condition_id' => intval(Core_Array::getRequest('cond_of_delivery', Core_Array::getPost('shop_delivery_condition_id', Core_Array::get($hostcmsOrderSession, 'shop_delivery_condition_id', 0)))),
			'shop_delivery_id' => 0,
			'shop_payment_system_id' => intval(Core_Array::getRequest('shop_payment_system', Core_Array::getPost('shop_payment_system_id', Core_Array::get($hostcmsOrderSession, 'shop_payment_system_id', 0)))),
			'postcode' => Core_Str::stripTags(strval(Core_Array::getRequest('postcode', Core_Array::getRequest('index', Core_Array::get($hostcmsOrderSession, 'postcode', ''))))),
			'address' => Core_Str::stripTags(strval(Core_Array::getRequest('address', Core_Array::get($hostcmsOrderSession, 'address', '')))),
			'house' => Core_Str::stripTags(strval(Core_Array::getRequest('house', Core_Array::get($hostcmsOrderSession, 'house', '')))),
			'flat' => Core_Str::stripTags(strval(Core_Array::getRequest('flat', Core_Array::get($hostcmsOrderSession, 'flat', '')))),
			'address-autocomplete' => Core_Str::stripTags(strval(Core_Array::getRequest('address-autocomplete', Core_Array::get($hostcmsOrderSession, 'address-autocomplete', '')))),
			'surname' => Core_Str::stripTags(strval(Core_Array::getRequest('site_users_surname', Core_Array::getRequest('surname', Core_Array::get($hostcmsOrderSession, 'surname', ''))))),
			'name' => Core_Str::stripTags(strval(Core_Array::getRequest('site_users_name', Core_Array::getRequest('name', Core_Array::get($hostcmsOrderSession, 'name', ''))))),
			'patronymic' => Core_Str::stripTags(strval(Core_Array::getRequest('site_users_patronymic', Core_Array::getRequest('patronymic', Core_Array::get($hostcmsOrderSession, 'patronymic', ''))))),
			'phone' => Core_Str::stripTags(strval(Core_Array::getRequest('site_users_phone', Core_Array::getRequest('phone', Core_Array::get($hostcmsOrderSession, 'phone', ''))))),
			'email' => Core_Str::stripTags(strval(Core_Array::getRequest('site_users_email', Core_Array::getRequest('email', Core_Array::get($hostcmsOrderSession, 'email', ''))))),
			'description' => Core_Str::stripTags(strval(Core_Array::getRequest('description', Core_Array::get($hostcmsOrderSession, 'description', '')))),
			'organization_payment' => Core_Array::getRequest('organization_payment', 'off'),
			'store_properties' => $aProperties,
			'coupon_text' => Core_Str::stripTags(strval(Core_Array::getRequest('coupon_text', Core_Array::get($hostcmsOrderSession, 'coupon_text', '')))),
		];
		if(($localShop_delivery_condition_id = Core_Array::get($hostcmsOrderUpdateSession, 'shop_delivery_condition_id', 0)) > 0) {
			/** @var Shop_Delivery_Condition_Model $oDeliveryCondition */
			$oDeliveryCondition = Core_Entity::factory('Shop_Delivery_Condition')->getById($localShop_delivery_condition_id, false);
			if(isset($oDeliveryCondition->id) && $oDeliveryCondition->id > 0) {
				$hostcmsOrderUpdateSession['shop_delivery_id'] = $oDeliveryCondition->shop_delivery_id;
			}
		}

		$requestFullName = Core_Array::getRequest('full_name', '');
		$surname = $name = $patronymic = '';
		if($requestFullName != '') {
			$aSplFullName = explode(' ', trim($requestFullName));
			if(count($aSplFullName)) {
				if(count($aSplFullName) == 1) {
					$name = trim($requestFullName);
				} elseif(count($aSplFullName) == 2) {
					$surname = trim($aSplFullName[0]);
					$name = trim($aSplFullName[1]);
				} else {
					list($surname, $name, $patronymic) = $aSplFullName;
				}
			}
		}
		if(isset($hostcmsOrderUpdateSession['surname']) && $hostcmsOrderUpdateSession['surname'] == '') $hostcmsOrderUpdateSession['surname'] = $surname;
		if(isset($hostcmsOrderUpdateSession['name']) && $hostcmsOrderUpdateSession['name'] == '') $hostcmsOrderUpdateSession['name'] = $name;
		if(isset($hostcmsOrderUpdateSession['patronymic']) && $hostcmsOrderUpdateSession['patronymic'] == '') $hostcmsOrderUpdateSession['patronymic'] = $patronymic;

		if(intval($hostcmsOrderUpdateSession['shop_country_location_city_id']) == 0
			|| intval($currentCountryID) != intval($hostcmsOrderUpdateSession['shop_country_id'])
			|| intval($currentCountryLocationID) != intval($hostcmsOrderUpdateSession['shop_country_location_id'])
			|| intval($currentCountryLocationCityID) != intval($hostcmsOrderUpdateSession['shop_country_location_city_id'])
		) {
			$hostcmsOrderUpdateSession['shop_delivery_condition_id'] = $hostcmsOrderUpdateSession['shop_delivery_id'] = 0;
		}
//		Skynetcore_Utils::tp($hostcmsOrderUpdateSession, $currentDeliveryConditionID);
		if(intval($hostcmsOrderUpdateSession['shop_delivery_condition_id']) == 0
			|| intval($currentDeliveryConditionID) != intval($hostcmsOrderUpdateSession['shop_delivery_condition_id'])
		) {
			$hostcmsOrderUpdateSession['shop_payment_system_id'] = 0;
//			Skynetcore_Utils::tp($hostcmsOrderUpdateSession, 'YYYYYY');
		}
		if(intval($hostcmsOrderUpdateSession['shop_delivery_condition_id']) > 0) {
			$oShop_Delivery_Condition = Core_Entity::factory('Shop_Delivery_Condition', $hostcmsOrderUpdateSession['shop_delivery_condition_id']);
			$hostcmsOrderUpdateSession['shop_delivery_id'] = $oShop_Delivery_Condition->shop_delivery_id * 1;
		} else {
			$hostcmsOrderUpdateSession['shop_delivery_id'] = 0;
		}
//		Skynetcore_Utils::tp($hostcmsOrderUpdateSession, 'XXXXX');
		$_SESSION['hostcmsOrder'] = $hostcmsOrderUpdateSession;
		$_SESSION['hostcmsOrder']['json'] = json_encode($hostcmsOrderUpdateSession, JSON_UNESCAPED_UNICODE);
		if($asEntity) {
//			Skynetcore_Utils::p($_SESSION['hostcmsOrder']);
			return Skynetcore_Convert_Array_Xml_Entity::createInstance($_SESSION['hostcmsOrder'], '', 'hostcms_order');
		}
//		Skynetcore_Utils::tp($_REQUEST, $requestFullName);
//		Skynetcore_Utils::tp($hostcmsOrderSession, "{$surname} {$name} {$patronymic}");
//		Skynetcore_Utils::tp($_SESSION['hostcmsOrder'], $requestFullName);
//		die();
		return $_SESSION['hostcmsOrder'];
	}

	public static function showDeliveryType($shop_id) {
		$return = false;
		$Shop_Delivery_Controller_Show = new Skynetcore_Shop_Delivery_Controller_Show(Core_Entity::factory('Shop')->getById($shop_id));
		if(!is_null($Shop_Delivery_Controller_Show)) {
			if(isset($_SESSION['hostcmsOrder']['shop_country_id']) && $_SESSION['hostcmsOrder']['shop_country_id'] > 0) {
				$Shop_Delivery_Controller_Show->shop_country_id($_SESSION['hostcmsOrder']['shop_country_id']);
			}
			if(isset($_SESSION['hostcmsOrder']['shop_country_location_id']) && $_SESSION['hostcmsOrder']['shop_country_location_id'] > 0) {
				$Shop_Delivery_Controller_Show->shop_country_location_id($_SESSION['hostcmsOrder']['shop_country_location_id']);
			}
			if(isset($_SESSION['hostcmsOrder']['shop_country_location_city_id']) && $_SESSION['hostcmsOrder']['shop_country_location_city_id'] > 0) {
				$Shop_Delivery_Controller_Show->shop_country_location_city_id($_SESSION['hostcmsOrder']['shop_country_location_city_id']);
			}
			if(isset($_SESSION['hostcmsOrder']['shop_country_location_city_area_id']) && $_SESSION['hostcmsOrder']['shop_country_location_city_area_id'] > 0) {
				$Shop_Delivery_Controller_Show->shop_country_location_city_area_id($_SESSION['hostcmsOrder']['shop_country_location_city_area_id']);
			}
			if(isset($_SESSION['hostcmsOrder']['postcode']) && $_SESSION['hostcmsOrder']['shop_country_id'] != '') {
				$Shop_Delivery_Controller_Show->postcode($_SESSION['hostcmsOrder']['postcode']);
			}

			ob_start();
//			Skynetcore_Utils::p($_SESSION['hostcmsOrder']);
//			Shop_Delivery_Condition_Model

			$Shop_Delivery_Controller_Show
				->couponText(
					Core_Str::stripTags(
						Core_Array::get(Core_Array::get($_SESSION, 'hostcmsOrder', array()), 'coupon_text')
					)
				)
				->setUp()
				->xsl(
					Core_Entity::factory('Xsl')->getById(399)
				)
				->show();
			$return = ob_get_clean();
		}
		return $return;
	}

	public static function showPaymentType($shop_id) {
		$return = false;
		ob_start();
		$Shop_Payment_System_Controller_Show = new Skynetcore_Shop_Payment_System_Controller_Show(Core_Entity::factory('Shop')->getById($shop_id));

		if(!is_null($Shop_Payment_System_Controller_Show)) {
//		$shop_delivery_condition_id = strval(Core_Array::getPost('shop_delivery_condition_id', 0));
//		if (is_numeric($shop_delivery_condition_id))
//		{
//			$_SESSION['hostcmsOrder']['shop_delivery_condition_id'] = intval($shop_delivery_condition_id);
//
//			$oShop_Delivery_Condition = Core_Entity::factory('Shop_Delivery_Condition', $_SESSION['hostcmsOrder']['shop_delivery_condition_id']);
//			$_SESSION['hostcmsOrder']['shop_delivery_id'] = $oShop_Delivery_Condition->shop_delivery_id;
//		}
//		else
//		{
//			$_SESSION['hostcmsOrder']['shop_delivery_condition_id'] = 0;
//
//			// в shop_delivery_condition_id тогда "10-123#", ID элемента массива в сессии, в котором
//			// хранится стоимость доставки, налог, название специфичного условия доставки
//			list($shopDeliveryInSession) = explode('#', $shop_delivery_condition_id);
//
//			list($shop_delivery_id, $position) = explode('-', $shopDeliveryInSession);
//
//			$oShop_Delivery = $oShop->Shop_Deliveries->getById($shop_delivery_id);
//
//			if (!is_null($oShop_Delivery))
//			{
//				$oShop_Delivery_Handler = Shop_Delivery_Handler::factory($oShop_Delivery);
//				$oShop_Delivery_Handler->process($position);
//			}
//		}
			$shopDeliveryId = Core_Array::get($_SESSION['hostcmsOrder'], 'shop_delivery_id', 0);
//			Skynetcore_Utils::p($shopDeliveryId);
//			$_SESSION['hostcmsOrder']['shop_payment_system_id'] = 0;
//			self::applyRequestToSessionOrder();
//			Skynetcore_Utils::p($_SESSION['hostcmsOrder'], $shopDeliveryId);
			$Shop_Payment_System_Controller_Show
				->shop_delivery_id($shopDeliveryId == 0 ? -1 : $shopDeliveryId)
				->xsl(
					Core_Entity::factory('Xsl')->getById(
						400
					)
				)
				->show();
			$return = ob_get_clean();
		}
		return $return;
	}

	public function saveOrderParamsAction() {
		$this->checkCSRF();
		self::applyRequestToSessionOrder();
		$this->_status = 'OK';
		$this->_message = 'OK';
		$this->_status_code = 200;
		return $this->sendResponse();
	}

	public function getDeliveryTypeAction() {
		$this->checkCSRF();
		$instance = Core_Page::instance();
		$shop = Core_Array::getRequest('shop', 0);
		self::applyRequestToSessionOrder();
		$deliveryType = self::showDeliveryType($shop);
		$paymentType = self::showPaymentType($shop);
		if($deliveryType !== false) {
			$tmpHtml = [
				'#order-delivery' => $deliveryType,
				'#order-payment' => $paymentType,
				'_tmp' => $_SESSION['hostcmsOrder'],
			];
			$this->_html = $tmpHtml;

			$this->_status = 'OK';
			$this->_message = 'OK';
			$this->_status_code = 200;
		} else {
			$this->_message = 'Не найдено';
		}

		return $this->sendResponse();
	}

	public function getPaymentTypeAction() {
		$this->checkCSRF();
		$instance = Core_Page::instance();
		$shop = Core_Array::getRequest('shop', 0);
		self::applyRequestToSessionOrder();
		$returnHtml = self::showPaymentType($shop);
		if($returnHtml !== false) {
			$tmpHtml = [
				'#order-payment' => $returnHtml,
			];
			$this->_html = $tmpHtml;

			$this->_status = 'OK';
			$this->_message = 'OK';
			$this->_status_code = 200;
		} else {
			$this->_message = 'Не найдено';
		}

		return $this->sendResponse();
	}

	public function setGeoAction() {
		$this->checkCSRF();

		$cityId = Core_Array::getRequest('id', 0);
		if($cityId > 0) {
			/** @var Shop_Country_Location_City_Model $oCity */
			$oCity = Core_Entity::factory('Shop_Country_Location_City')->getById($cityId, false);
			$txtSkynetCity = implode(', ', [
				$oCity->name,
				$oCity->shop_country_location->name,
				$oCity->shop_country_location->shop_country->name
			]);
			Core::setCookie("skynet_city", $txtSkynetCity, strtotime( "+1 month"), '/');
			$_COOKIE['skynet_city'] = $txtSkynetCity;

			Core::setCookie("skynet_region", $oCity->shop_country_location->name, strtotime( "+1 month"), '/');
			$_COOKIE['skynet_region'] = $oCity->shop_country_location->name;

			$geoip = Array
			(
				'city' => Array (
					'id' => $oCity->id,
//					'lat' => 0,
//					'lon' => 0,
					'name_ru' => $oCity->name,
					'name_en' => $oCity->name_en,
				),
				'region' => Array (
					'id' => $oCity->shop_country_location->id,
					'name_ru' => $oCity->shop_country_location->name,
					'name_en' => $oCity->shop_country_location->name_en,
					'iso' => ''
				),
				'country' => Array (
					'id' => $oCity->shop_country_location->shop_country->id,
					'iso' => $oCity->shop_country_location->shop_country->alpha2,
					'lat' => 0,
					'lon' => 0,
					'name_ru' => $oCity->shop_country_location->shop_country->name,
					'name_en' => $oCity->shop_country_location->shop_country->name_en,
				)
			);

			Core::setCookie("skynet_geolocation", json_encode($geoip), strtotime( "+1 month"), '/');
			$_COOKIE['skynet_geolocation'] = json_encode($geoip);
		}
//		Core::setCookie("skynet_region", $txtSkynetCity, strtotime( "+1 month"), '/');
//		$_COOKIE['skynet_city'] = $txtSkynetCity;

		$this->_status = 'OK';
		$this->_message = 'OK';
		$this->_status_code = 200;

		return $this->sendResponse();
	}
}