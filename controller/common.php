<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Skynetcore Module.
 *
 * @package HostCMS
 * @subpackage Redwings
 * @version 6.x
 * @author GoldDraft
 * @copyright © 2021 GoldDraft (info@golddraft.ru)
 */

class Skynetcore_Controller_Common
{

	public static function redirectByConditions($oEntity) {
		$pathToRedirect = false;
		$baseEntityType = preg_replace('/^(.*)(Shop|Informationsystem)(_Model)$/ui', '$2', get_class($oEntity->getEntity()));
		$settings = Core_Array::get(Core_Page::instance()->skynet->config, 'settings', []);

		$currentGroup = $currentItem = false;
		switch (true) {
			case ($oEntity->group == 0 && $oEntity->item == 0): //-- Выбран головной раздел --
				/** @var Structure_Model $oStructure */
				$oStructure = $oEntity->getEntity()->structure;
				if(is_object($oStructure) && isset($oStructure->id) && $oStructure->id > 0) {
					$aStructurePropertyValues = $oStructure->getPropertyValues();
					foreach ($aStructurePropertyValues as $structurePropertyValue) {
						if($structurePropertyValue->property->tag_name == 'redirect-path'
							&& $structurePropertyValue->value != ''
						) {
							$pathToRedirect = $structurePropertyValue->value;
						}
					}
				}
				break;
			case ($oEntity->group > 0 && $oEntity->item == 0): //-- Выбрана группа --
				$currentGroup = Core_Entity::factory("{$baseEntityType}_Group")->getById($oEntity->group, false);

				if($currentGroup->indexing == 0 && Core_Array::get($settings, 'check_indexing', false)) {
					$localParent = $currentGroup->getParent();
					$localStructure = $currentGroup->informationsystem->structure;

					if(method_exists($localStructure, 'getPath')) {
						$pathToRedirect = $localStructure->getPath();
					}
					if(method_exists($localParent, 'getPath')) {
						$pathToRedirect .= $localParent->getPath();
					}
				}
				break;
			case ($oEntity->group == 0 && $oEntity->item > 0): //-- Выбран элемент --
				$currentItem = Core_Entity::factory("{$baseEntityType}_Item")->getById($oEntity->item, false);
				break;
			case ($oEntity->group > 0 && $oEntity->item > 0): //-- Выбран элемент --
				$currentGroup = Core_Entity::factory("{$baseEntityType}_Group")->getById($oEntity->group, false);
				$currentItem = Core_Entity::factory("{$baseEntityType}_Item")->getById($oEntity->item, false);
				break;
		}

		if($pathToRedirect !== false) {
			Core_Page::instance()->response
				->status(301)
				->header('Location', $pathToRedirect)
				->sendHeaders();
			exit();
		}

		return [
			'group' => $currentGroup,
			'item' => $currentItem,
		];
	}
}