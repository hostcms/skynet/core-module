<?php
use Skynetcore_Utils as utl;
use Skynetcore_Convert_Array_Xml_Entity as xmls;

class Skynetcore_Controller_Market_Base extends Shop_Controller_YandexMarket
{
	protected	$_instance = false;
	private		$_market_property_id = 0;
	private		$_pvi_nompunload = 0;
	private		$_aProperty_dir_id = [];
	private		$_aProperties = [];
	protected	$offerIdTagType = "id";
	protected	$checkBarcodes = true;
	protected	$excludeShopGroups = false;

	/**
	 * Moto_Controller_Market_Goods constructor.
	 */
	public function __construct(\Shop_Model $oShop, $market_property_id = 0, $aProperty_dir_id = 0, $aProperties = [], $pvi_nompunload = 0)
	{
		$this->_instance = Core_Page::instance();
		$this->_market_property_id = $market_property_id;
		$this->_pvi_nompunload = $pvi_nompunload;
		if(!is_array($aProperty_dir_id)) {
			$aProperty_dir_id = [$aProperty_dir_id];
		}
		$this->_aProperty_dir_id = $aProperty_dir_id;

		if(!is_array($aProperties)) {
			$aProperties = [$aProperties];
		}
		$this->_aProperties = $aProperties;

		parent::__construct($oShop);
	}

	/**
	 * @return array
	 */
	public function getExcludeShopGroups(): array
	{
		return $this->excludeShopGroups;
	}

	/**
	 * @param array $excludeShopGroups
	 */
	public function setExcludeShopGroups(array $excludeShopGroups)
	{
		$this->excludeShopGroups = $excludeShopGroups;
		return $this;
	}

	public function getMarketProperty() {
		return $this->_market_property_id;
	}

	public function setMarketProperty($property_id) {
		$this->_market_property_id = $property_id;
	}

	public function getMarketNotUploadProperty() {
		return $this->_pvi_nompunload;
	}

	public function setMarketNotUploadProperty($property_id) {
		$this->_pvi_nompunload = $property_id;
	}

	public function setAMarketPropertyDirs($aProperty_dir_id) {
		$this->_aProperty_dir_id = $aProperty_dir_id;
		return $this;
	}

	protected function _setShopGroups()
	{
		/** @var Core_QueryBuilder_Select $qbGroups */
		$qbGroups = clone $this->_Shop_Items->queryBuilder();
		$qbGroups
			->clearOrderBy()
			->clearLimit()
			->clearSelect()
			->select('shop_items.shop_group_id')
			->from('shop_items')
			->groupBy('shop_items.shop_group_id')
		;
		$aaGroups = $qbGroups->asAssoc()->execute()->result(false);
		$aGroups = array_map(function($aGroup) {
			return $aGroup['shop_group_id'];
		}, $aaGroups);
		$allGroups = [-1];
		foreach ($aGroups as $group_id) {
			/** @var Skynetcore_Shop_Group_Model $oGroups */
			$oGroups = Core_Entity::factory('Skynetcore_Shop_Group')->getById($group_id);
			$allGroups = array_merge($allGroups, $oGroups->getParentIDs());
		}
		parent::_setShopGroups();

		if(is_array($this->excludeShopGroups)) {
			$aExcludeGroupIDs = [];
			foreach ($this->excludeShopGroups as $ex_group_id) {
				/** @var Skynetcore_Shop_Group_Model $oExGroups */
				$oExGroups = Core_Entity::factory('Skynetcore_Shop_Group')->getById($ex_group_id);
				$aExcludeGroupIDs = array_merge($aExcludeGroupIDs, $oExGroups->getChilds());
			}
			if(count($aExcludeGroupIDs)) {
				$this
					->_Shop_Groups
					->queryBuilder()
					->where('shop_groups.id', 'NOT IN', array_unique($aExcludeGroupIDs))
				;
			}
		}
		$this
			->_Shop_Groups
			->queryBuilder()
			->where('shop_groups.id', 'IN', array_unique($allGroups))
		;

		return $this;
	}

	protected function _addShopItemConditions($oShop_Item)
	{
		parent::_addShopItemConditions($oShop_Item);

		/** @var $oShop_Item Shop_Item_Model */
		$oQB = $oShop_Item->queryBuilder();
		// Получаем
		$aWhere = $oQB->getWhere();
		// Очищаем
		$oQB->clearWhere();
		// Устанавливаем с корректировкой
		foreach ($aWhere as $aTmp) {
			$key = key($aTmp);
			$aWhere = current($aTmp);

			if(!($key == 'AND' && $aWhere[0] == 'shop_items.yandex_market')) {
				$oQB->setOperator($key);
				call_user_func_array(array($oQB, 'where'), $aWhere);
			}
		}

		if($this->_market_property_id > 0) {
			$oShop_Item
				->queryBuilder()
				->join(['property_value_ints', 'pvigoods'], 'pvigoods.entity_id', '=', 'shop_items.id', [
					['AND' => ['pvigoods.property_id', '=', $this->_market_property_id]]
				])
				->where('pvigoods.value', '=', 1)
			;
		}

		if($this->_pvi_nompunload > 0) {
			$oShop_Item
				->queryBuilder()
				->leftJoin(['property_value_ints', 'pvigoodsnompupload'], 'pvigoodsnompupload.entity_id', '=', 'shop_items.id', [
					['AND' => ['pvigoodsnompupload.property_id', '=', $this->_pvi_nompunload]]
				])
				->where( Core_QueryBuilder::expression('COALESCE(pvigoodsnompupload.value, 0)'), '=', 1)
			;
		}
//		$oShop_Item
//			->queryBuilder()
//			->where('marking', 'IN', ['PM20048'])
//		;
		return $this;
	}

	protected function _showOffer($oShop_Item)
	{
		$showOffer = true;
		if($this->checkBarcodes) {
			$aBarcodes = $oShop_Item->shop_item_barcodes->findAll(false);
			$showOffer = (isset($aBarcodes[0]->id) && $aBarcodes[0]->id>0 && trim($aBarcodes[0]->value) != '');
		}
		if($showOffer) {
			parent::_showOffer($oShop_Item);
		}
		return $this;
	}

	public function show()
	{
		$aDirsIDs = [-1];
		/** @var Property_Dir_Model $oPropertyDir */
		foreach ($this->_aProperty_dir_id as $propertyDir_id) {
			$oPropertyDir = Core_Entity::factory('Property_dir')->getById($propertyDir_id);
			if(!is_null($oPropertyDir)) {
				$aoPropertyDirs = $oPropertyDir->property_dirs->findAll(false);
				$aDirs = [$oPropertyDir->id*1];
				foreach ($aoPropertyDirs as $aoPropertyDir) {
					$aDirs[] = $aoPropertyDir->id*1;
				}
				$aDirsIDs = array_merge($aDirsIDs, $aDirs);
			}
		}

		$linkedObject = Core_Entity::factory('Shop_Item_Property_List', $this->_entity->id);
		$linkedObjectProperties = $linkedObject->Properties;
		$linkedObjectProperties
			->queryBuilder()
			->where('properties.property_dir_id', 'IN', $aDirsIDs)
		;
		$aProperties = $linkedObjectProperties->findAll(false);
		$aPropertiesIDs = [-1];
		foreach ($aProperties as $oProp) {
			$aPropertiesIDs[] = $oProp->id;
		}

		$this->itemsProperties(array_merge($aPropertiesIDs, $this->_aProperties));

		parent::show(); // TODO: Change the autogenerated stub
	}

	protected function _makeOfferTag($oShop_Item)
	{
		$oShop = $this->getEntity();

		/* Устанавливаем атрибуты тега <offer>*/
		$tag_bid = $oShop_Item->yandex_market_bid
			? ' bid="' . Core_Str::xml($oShop_Item->yandex_market_bid) . '"'
			: '';

		/*$tag_cbid = $oShop_Item->yandex_market_cid
			? ' cbid="' . Core_Str::xml($oShop_Item->yandex_market_cid) . '"'
			: '';*/

		$available = !$this->checkAvailable || $oShop_Item->getRest() > 0 ? 'true' : 'false';

		$sType = $this->type != 'offer'
			? ' type="' . Core_Str::xml($this->type) . '"'
			: '';

		$tagName = $this->offerIdTagType;
		$this->stdOut->write('<offer id="' . $oShop_Item->$tagName . '"'. $tag_bid . /*$tag_cbid . */$sType . " available=\"{$available}\">\n");

		return $this;
	}

	protected function _addPropertyValue($oShop_Item)
	{
		parent::_addPropertyValue($oShop_Item);
		$aProperty_Values = is_array($this->itemsProperties)
			? Property_Controller_Value::getPropertiesValues($this->itemsProperties, $oShop_Item->id, FALSE)
			: $oShop_Item->getPropertyValues(FALSE);

		$bAge = FALSE;

		foreach ($aProperty_Values as $oProperty_Value)
		{
			//$oProperty = $oProperty_Value->Property;
			$oProperty = $this->_getProperty($oProperty_Value->property_id);

			switch ($oProperty->type)
			{
				case 5: // ИС
					$value = $oProperty_Value->informationsystem_item->name;
					break;
				default:
					$value = NULL;
					break;
			}

			if (!is_null($value))
			{
				$sTagName = 'param';

				$sAttr = ' name="' . Core_Str::xml($oProperty->name) . '"';

				if ($value !== '')
				{
					if (!in_array($sTagName, $this->_aForbid))
					{
						$this->stdOut->write('<' . $sTagName . $sAttr . '>'
							. Core_Str::xml(html_entity_decode(strip_tags($value), ENT_COMPAT, 'UTF-8'))
							. '</' . $sTagName . '>'. "\n");
					}
				}
			}
		}
		return $this;
	}


}