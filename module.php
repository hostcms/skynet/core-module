<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');
/**
 * Skynet Core Module.
 *
 * @package HostCMS 6\Module
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © mikeborisov, m.u.borisov@gmail.com
 */
class Skynetcore_Module extends Core_Module
{
	/**
	 * Module version
	 * @var string
	 */
	public $version = '0.2';

	/**
	 * Module date
	 * @var date
	 */
	public $date = '2017-11-01';

	/**
	 * Skynetcore_Module constructor.
	 * @param string $version
	 */
	public function __construct()
	{
		parent::__construct();

		// Add template observer
		Core_Event::attach('Core.onBeforeInitConstants', array('Skynetcore_Observer_Core', 'onBeforeInitConstants'));
		Core_Event::attach('Core.onAfterInitConstants', array('Skynetcore_Observer_Core', 'onAfterInitConstants'));
		Core_Event::attach('Core.onAfterLoadModuleList', array('Skynetcore_Observer_Core', 'onAfterLoadModuleList'));
		Core_Event::attach('Core_Command_Controller_Default.onBeforeContentCreation', array('Skynetcore_Observer_Core', 'onBeforeContentCreation'));
//		Core_Event::attach('Skynetcore_Ajax_Controller_Shop.onBeforeSendResponse', array('Skynetcore_Observer_Core', 'onBeforeSendAjaxResponse'));

		if (Core::moduleIsActive('site'))
			Core_Event::attach('site.onBeforeGetXml', array('Skynetcore_Observer_Core', 'onBeforeGetXml'));
		if (Core::moduleIsActive('form'))
			Core_Event::attach('form.onBeforeGetXml', array('Skynetcore_Observer_Core', 'onBeforeGetXml'));
		if (Core::moduleIsActive('informationsystem'))
			Core_Event::attach('informationsystem.onBeforeGetXml', array('Skynetcore_Observer_Core', 'onBeforeGetXml'));
		if (Core::moduleIsActive('shop')) {
			Core_Event::attach('shop.onBeforeGetXml', array('Skynetcore_Observer_Core', 'onBeforeGetXml'));
			Core_Event::attach('shop_item.onBeforeGetXml', array('Skynetcore_Observer_Shop_Item', 'skynetOnBeforeGetItemXml'));
			Core_Event::attach('shop_item.onBeforeRedeclaredGetStdObject', array('Skynetcore_Observer_Shop_Item', 'skynetOnBeforeGetItemXml'));
			Core_Event::attach('shop_order.onBeforeSave', array('Skynetcore_Observer_Shop_Order', 'skynetOnBeforeSave'));
			Core_Event::attach('Shop_Cart_Controller.onAfterUpdate', array('Skynetcore_Observer_Shop_Cart_Controller', 'onAfterUpdate'));
		}
		if (Core::moduleIsActive('siteuser'))
			Core_Event::attach('siteuser.onBeforeGetXml', array('Skynetcore_Observer_Core', 'onBeforeGetXml'));

		Core_Event::attach('user.onBeforeGetXml', array('Skynetcore_Observer_User', 'onBeforeGetXml'));
		Core_Event::attach('property_value_int.onBeforeGetXml', array('Skynetcore_Observer_Property_Value', 'onBeforeGetXml'));
		Core_Event::attach('property_value_int.onBeforeRedeclaredGetXml', array('Skynetcore_Observer_Property_Value', 'onBeforeRedeclaredGet'));
		Core_Event::attach('property_value_int.onBeforeRedeclaredGetStdObject', array('Skynetcore_Observer_Property_Value', 'onBeforeRedeclaredGet'));
		Core_Event::attach('property_value_float.onBeforeGetXml', array('Skynetcore_Observer_Property_Value', 'onBeforeGetXml'));
		Core_Event::attach('property_value_string.onBeforeGetXml', array('Skynetcore_Observer_Property_Value', 'onBeforeGetXml'));
		Core_Event::attach('property_value_text.onBeforeGetXml', array('Skynetcore_Observer_Property_Value', 'onBeforeGetXml'));
		Core_Event::attach('property_value_file.onBeforeGetXml', array('Skynetcore_Observer_Property_Value', 'onBeforeGetXml'));
		Core_Event::attach('property_value_datetime.onBeforeGetXml', array('Skynetcore_Observer_Property_Value', 'onBeforeGetXml'));
		Core_Event::attach('Core_Page.onBeforeShowJs', array('Skynetcore_Observer_Core', 'onSiteBeforeShowJs'));
		Core_Event::attach('Core_Response.onBeforeShowBody', array('Skynetcore_Observer_Shop', 'onBeforeShowBody'));

		if(!is_null($user=Core_Entity::factory('User')->getCurrent()) && $user->id > 0) {
			Core_Skin::instance()->addJs('/modules/skynetcore/res/js/admin.js');
			Core_Skin::instance()->addCss('/modules/skynetcore/res/css/admin.css');
			Core_Event::attach('Core_Page.onBeforeShowCss', array('Skynetcore_Observer_Core', 'onBeforeShowCss'));
			Core_Event::attach('Core_Page.onBeforeShowJs', array('Skynetcore_Observer_Core', 'onBeforeShowJs'));
			if (Core::moduleIsActive('compression')) {
				Core_Event::attach('Core_Response.onBeforeCompress', array('Skynetcore_Admin_Panel', 'sitePanelShow'));
			} else {
				Core_Event::attach('Core_Response.onBeforeShowBody', array('Skynetcore_Admin_Panel', 'sitePanelShow'));
			}
			Core_Event::attach('site.onBeforeGetXml', array('Skynetcore_Observer_Common', 'onBeforeGetXmlAddUser'));
			Core_Event::attach('shop.onBeforeGetXml', array('Skynetcore_Observer_Common', 'onBeforeGetXmlAddUser'));
			Core_Event::attach('informationsystem.onBeforeGetXml', array('Skynetcore_Observer_Common', 'onBeforeGetXmlAddUser'));
			Core_Event::attach('Admin_Form_Action_Controller_Type_Edit.onAfterPrepareForm',
				array('Skynetcore_Observer_Admin_Form', 'onAfterPrepareForm'));
		}
		if(Core::moduleIsActive('siteuser') && !is_null($siteuser=Core_Entity::factory('Siteuser')->getCurrent()) && $siteuser->id > 0) {
			Core_Event::attach('site.onBeforeGetXml', array('Skynetcore_Observer_Common', 'onBeforeGetXmlAddSiteuser'));
			Core_Event::attach('shop.onBeforeGetXml', array('Skynetcore_Observer_Common', 'onBeforeGetXmlAddSiteuser'));
			Core_Event::attach('informationsystem.onBeforeGetXml', array('Skynetcore_Observer_Common', 'onBeforeGetXmlAddSiteuser'));
		}
		if (Core::moduleIsActive('compression')) {
			Core_Event::attach('Core_Response.onBeforeCompress', array('Skynetcore_Observer_Core', 'onBeforeShowBody'));
		} else {
			Core_Event::attach('Core_Response.onBeforeShowBody', array('Skynetcore_Observer_Core', 'onBeforeShowBody'));
		}
		Core_Event::attach('Core_Command_Controller_Default.onBeforeShowAction', array('Skynetcore_Observer_Core', 'onBeforeShowAction'));
		Core_Event::attach('Shop_Cart_Controller.onAfterDelete', array('Skynetcore_Observer_Shop_Cart_Controller', 'onAfterDelete'));

		Core_I18n::instance()->expandLng('list_item',
			array(
				'sky_i_case' => 'Поле именительного падежа',
				'sky_r_case' => 'Поле родительного падежа',
				'sky_d_case' => 'Поле дательного падежа',
				'sky_v_case' => 'Поле винительного падежа',
				'sky_t_case' => 'Поле творительного падежа',
				'sky_p_case' => 'Поле предложного падежа',
				'sky_once'   => 'Поле единственного числа',
				'sky_mch'    => 'Поле множественного числа',
			)
		);

		Core_Router::add('skynet_dadata_services', '/skynet/dadata/{service}/')
			->controller('Skynetcore_Dadata_Controller');
		Core_Router::add('skynet_process_services', '/skynet/process/{action}/')
			->controller('Skynetcore_Controller_Process');
		Core_Router::add('skynet_ajax_shop', '/skynet/ajax/shop/{action}/{params}/')
			->controller('Skynetcore_Ajax_Controller_Shop');
		Core_Router::add('skynet_ajax_mp_ozon', '/skynet/ajax/mp/ozon/{action}(/{params})/')
			->controller('Skynetcore_Ajax_Mp_Ozon_Controller');
	}
}