<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Restapi_Command_Controller extends Restapi_Command_Controller
{
	/** @var Siteuser_Model $_siteuser */
	protected $_siteuser;
	protected $_instance;

	public function __construct()
	{
		parent::__construct();

//		$this->_mode = 'json';
		$this->_instance = Core_Page::instance();
	}

	public function checkAuthorization()
	{
		if(parent::_checkAuthorization()) {
			$login = $this->_user->login;
			$qSiteuser = Core_Entity::factory('Siteuser');
			$this->_siteuser = $qSiteuser->getByLogin($login);
			$this->_siteuser->setCurrent();
			$this->_instance->skynet->siteuser = $this->_siteuser;
			$this->_instance->skynet->user = $this->_user;
		}
	}

}