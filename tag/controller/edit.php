<?php

class Skynetcore_Tag_Controller_Edit extends Tag_Controller_Edit {

	public static function getRelatedGroupTags($limit = false, $group_id = false) {
		$aRelatedEntity = Core::factory('Core_Xml_Entity');
		$aRelatedEntity
			->name('related_tags');
		$aReturnTags = [];
		$writeCommerce = Core_Array::getRequest('commerce', false) !== false;
		if(!$writeCommerce) {

//			Core_QueryBuilder::Ex
			/** @var Core_QueryBuilder_Select $qGroups */
			$qGroups = Core_QueryBuilder::select('sg.id')
				->select('sg.name')
				->select([Core_QueryBuilder::expression('GROUP_CONCAT(t.id ORDER BY t.name SEPARATOR \'~\')'), 'tag_ids'])
				->select([Core_QueryBuilder::expression('GROUP_CONCAT(t.name ORDER BY t.name SEPARATOR \'~\')'), 'tag_names'])
				->from(['shop_items', 'si'])
				->leftJoin(['shop_items', 'scat'], 'scat.id', '=', 'si.shortcut_id')
				->join(['shop_groups', 'sg'], 'sg.id', '=', 'si.shop_group_id')
				->join(['tag_shop_items', 'tsi'], 'tsi.shop_item_id', '=', Core_QueryBuilder::expression('COALESCE(scat.id, si.id)'))
				->join(['tags', 't'], 't.id', '=', 'tsi.tag_id')
				->where('si.modification_id', '=', 0)
				->where(Core_QueryBuilder::expression('COALESCE(scat.active, si.active)'), '=', 1)
				->where(Core_QueryBuilder::expression('COALESCE(scat.deleted, si.deleted)'), '=', 0)
				->where('si.shop_id', '=', 1)
				->where('sg.active', '=', 1)
				->where('sg.deleted', '=', 0)
				->open()
				->where('sg.id', '=', $group_id)
				->setOr()
				->where('sg.parent_id', '=', $group_id)
				->close()
				->groupBy('sg.id');
//					Skynetcore_Utils::tp($qGroups->build());
			$qGroups->execute('SET group_concat_max_len = 20480000;');
			$aGroups = $qGroups->asAssoc()->execute()->result();

			$aTmpGroups = [];
			$aTmpCompletedTags = $aCompletedTagsForSort = $aTmpCompletedTagsForSort = [];
			foreach ($aGroups as $group) {
				$aTmpTagIDs = explode('~', $group['tag_ids']);
				$aTmpTags = explode('~', $group['tag_names']);
//						Skynetcore_Utils::p($aTmpTags);
				foreach ($aTmpTagIDs as $tagKey => $tagId) {
					if (!isset($aTmpCompletedTags[$tagId])) {
						$aTmpCompletedTags[$tagId] = [];
					}
					$aTmpCompletedTags[$tagId][] = $aTmpTags[$tagKey];
				}
				$aTmpCompletedTagsForSort = array_map(function ($aTmpCompletedTag) {
					return count($aTmpCompletedTag);
				}, $aTmpCompletedTags);
			}
			uasort($aTmpCompletedTagsForSort, function ($a, $b) {
				if ($a == $b) {
					return 0;
				}
				return ($a > $b) ? -1 : 1;
			});
			foreach ($aTmpCompletedTagsForSort as $tmpCompletedTagsForSortKey => $tmpCompletedTagsForSort) {
				$aCompletedTagsForSort[$tmpCompletedTagsForSortKey] = [
					'id' => $tmpCompletedTagsForSortKey,
					'name' => $aTmpCompletedTags[$tmpCompletedTagsForSortKey][0],
					'count' => $tmpCompletedTagsForSort,
				];
				if (count($aCompletedTagsForSort) >= 100) break;
			}
			if (count($aCompletedTagsForSort)) {
				$aGroupTags = Core_Entity::factory('Tag')->getAllById(array_keys($aCompletedTagsForSort), false, 'IN');
				foreach ($aGroupTags as $oGroupTag) {
					$aCompletedTagsForSort[$oGroupTag->id]['element'] = json_decode(json_encode($oGroupTag->getStdObject()), true);
				}
				$aReturnTags = Skynetcore_Convert_Array_Xml_Entity::createInstance($aCompletedTagsForSort, 'tag', 'related_tags');
				return $aReturnTags;
			}
//			Skynetcore_Utils::tp($aReturnTags);
		}
		return $aRelatedEntity;
	}

	public static function getRelatedTags($limit = false, $tag_id = false) {
		$aRelatedEntity = Core::factory('Core_Xml_Entity');
		$aRelatedEntity
			->name('related_tags');
		$writeCommerce = Core_Array::getRequest('commerce', false) !== false;
		if(!$writeCommerce) {
			/** @var Tag_Model $qRelatedTags */
			$qRelatedTags = Core_Entity::factory('Tag');
			$qRelatedTags
				->queryBuilder()
				->clearSelect()
				->select('related_tags.*')
				->select([ Core_QueryBuilder::expression('COUNT(DISTINCT tsi_related_tags.id)'), 'datacntids'])
				->join(['tag_shop_items', 'tsi_exists'], 'tsi_exists.tag_id', '=', 'tags.id')
				->join(['shop_items', 'items_exists'], 'items_exists.id', '=', 'tsi_exists.shop_item_id', [
					['AND' => ['items_exists.active', '=', 1]],
					['AND' => ['items_exists.deleted', '=', 0]],
				])
				->join(['tag_shop_items', 'tsi_related_tags'], 'tsi_related_tags.shop_item_id', '=', 'items_exists.id')
				->join(['tags', 'related_tags'], 'related_tags.id', '=', 'tsi_related_tags.tag_id')
				->where('tags.id', '=', $tag_id)
				->orderBy('datacntids', 'DESC')
				->orderBy('related_tags.name')
				->groupBy('related_tags.id')
			;
			if($limit != false && $limit > 0) {
				$qRelatedTags->queryBuilder()->clearLimit()->limit($limit);
			}
			$aRelatedTags = $qRelatedTags->findAll(false);
			$aRelatedEntity
				->addEntities($aRelatedTags);

		}
		return $aRelatedEntity;
	}
}
