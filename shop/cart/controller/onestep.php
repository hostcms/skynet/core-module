<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Заказ в 1 шаг.
 *
 */
class Skynetcore_Shop_Cart_Controller_Onestep extends Shop_Cart_Controller_Onestep
{
	protected $_cartItems = [];
	protected $_oldCartItems = [];
	protected $_oldHostcmsOrder = [];
	protected $_oldStorage = [];

	/**
	 * @return array
	 */
	public function getCartItems(): array
	{
		return $this->_cartItems;
	}

	/**
	 * @param array $cartItems
	 */
	public function addCartItem($cartItemId, $cartItemQuantity = 1): Skynetcore_Shop_Cart_Controller_Onestep
	{
		$tmpItem = new stdClass();
		$tmpItem->shop_item_id = $cartItemId;
		$tmpItem->quantity = $cartItemQuantity;
		$this->_cartItems[] = $tmpItem;
		return $this;
	}

	public function processOrder(
		  $shop_delivery_condition_id = 0
		, $shop_payment_system_id = 0
		, $siteuser_id = false
		, $letters = [
			'admin_email' => '',
			'admin' => 'ПисьмоАдминистратору',
			'siteuser' => 'ПисьмоПользователю',
			'letter_subject' => '',
		]) {
		Core_Event::notify(get_class($this) . '.onBeforeProcessOrder', $this, [$siteuser_id, $letters]);
		$eventResult = Core_Event::getLastReturn();
		if(!is_null($eventResult) && is_array($eventResult)) {
			list($siteuser_id, $letters) = $eventResult;
		}
		if(Core_Array::getSession('last_order_id', 0) > 0) {
			$_SESSION['last_order_id'] = 0;
		}
		if(count($this->_cartItems)) {
			$oShop = $this->getEntity();
			$Shop_Cart_Controller = Shop_Cart_Controller::instance();
			$tmpCartItems = $Shop_Cart_Controller->siteuser_id($siteuser_id)->getAll($oShop);

			if($shop_delivery_condition_id == 0) {
				/** @var Shop_Delivery_Model $oShopDelivery */
				$oShopDelivery = $oShop->shop_deliveries->getByGuid('UNDEFINED-DELIVERY');
				if(isset($oShopDelivery->id) && $oShopDelivery->id > 0) {
					$oShopDeliveryCondition = Core_Array::get($oShopDelivery->shop_delivery_conditions->findAll(), 0, false);
					if(isset($oShopDeliveryCondition->id) && $oShopDeliveryCondition->id>0) {
						$shop_delivery_condition_id = $oShopDeliveryCondition->id;
					}
					$oShopPaymentSystem = Core_Array::get($oShopDelivery->shop_delivery_payment_systems->findAll(), 0, false);
					if(isset($oShopPaymentSystem->id) && $oShopPaymentSystem->id>0) {
						$shop_payment_system_id = $oShopPaymentSystem->shop_payment_system_id;
					}
				}
			}
//			Skynetcore_Utils::p($shop_delivery_condition_id, $shop_payment_system_id); die();
			/** @var Shop_Cart_Model $oShop_Cart */
			foreach ($tmpCartItems as $oShop_Cart) {
				$this->_oldCartItems[] = [
					'postpone' => $oShop_Cart->postpone,
					'quantity' => $oShop_Cart->quantity,
					'shop_item_id' => $oShop_Cart->shop_item_id,
					'marking' => $oShop_Cart->marking,
					'shop_id' => $oShop_Cart->shop_id,
					'shop_warehouse_id' => $oShop_Cart->shop_warehouse_id,
					'siteuser_id' => $oShop_Cart->siteuser_id,
				];

				if ($oShop_Cart->Shop_Item->id) {
					if($siteuser_id == 0) {
						$Shop_Cart_Controller
							->clear()
							->shop_item_id($oShop_Cart->shop_item_id)
							->delete();
					} else {
						$oShop_Cart->delete();
					}
				}
			}
			if($siteuser_id == 0 && isset($_SESSION['hostcmsCart'])) {
				unset($_SESSION['hostcmsCart']);
			}
			foreach ($this->_cartItems as $tmpCartItem) {
				$Shop_Cart_Controller
					->shop_item_id($tmpCartItem->shop_item_id)
					->quantity($tmpCartItem->quantity)
					->add();
			}
			$tmpOldStorage = $this->_oldStorage;
			Core_Event::notify(get_class($this) . '.onAfterSaveCartInProcessOrder', $this, [$tmpOldStorage]);
			$eventResult = Core_Event::getLastReturn();
			!is_null($eventResult) && $this->_oldStorage = $eventResult;

			Core_Page::instance()->skynet->carts = [];
			$skyCarts = Skynetcore_Shop_Cart_Controller::getMuticart();
			Core_Page::instance()->skynet->carts = $skyCarts->tmpCarts;
			Core_Page::instance()->skynet->cartTotals = $skyCarts->tmpCartTotals;

			$this->_oldHostcmsOrder = Core_Array::get($_SESSION, 'hostcmsOrder', []);
			$fio = Skynetcore_Utils::splitFio(Core_Array::getRequest('fio', Core_Array::getRequest('name', '')));
			$oPossibleLocationId = $oPossibleCountryId = $oPossibleCityId = 0;
			/** @var Shop_Country_Location_Model $oPossibleLocation */
//			$oPossibleLocation = Core_Entity::factory('Shop_Country_Location', $fio->country_id)->getById(Core_Array::getRequest('location', 0), false);
			$oPossibleLocation = Core_Entity::factory('Shop_Country_Location')
				->getById(Core_Array::getRequest('location', 0), false);
			if(isset($oPossibleLocation->id) && $oPossibleLocation->id > 0) {
				$oPossibleLocationId = $oPossibleLocation->id;
				$oPossibleCountryId = $oPossibleLocation->shop_country_id;
				$oPossibleCityId = Core_Array::getRequest('sel_city', 0);
			}
			$index = Core_Array::getRequest('index', '');
			$addrLocation = '';
			if($index != '') {
				$addrLocation = trim(Core_Array::getRequest('address_str', ''));
			}
			if($addrLocation == '') {
				$addrLocation = trim(Core_Array::getRequest('address', ''));
			}


			$_SESSION['hostcmsOrder'] = array();

			$_SESSION['hostcmsOrder']['shop_country_id'] = intval(Core_Array::getPost('shop_country_id', $oPossibleCountryId));
			$_SESSION['hostcmsOrder']['shop_country_location_id'] = intval(Core_Array::getPost('shop_country_location_id', $oPossibleLocationId));
			$_SESSION['hostcmsOrder']['shop_country_location_city_id'] = intval(Core_Array::getPost('shop_country_location_city_id', $oPossibleCityId));
			$_SESSION['hostcmsOrder']['shop_country_location_city_area_id'] = intval(Core_Array::getPost('shop_country_location_city_area_id', 0));
			$_SESSION['hostcmsOrder']['postcode'] = Core_Str::stripTags(strval(Core_Array::getPost('postcode', Core_Array::getPost('index'))));
			$_SESSION['hostcmsOrder']['address'] = Core_Str::stripTags(strval($addrLocation));
			$_SESSION['hostcmsOrder']['house'] = Core_Str::stripTags(strval(Core_Array::getPost('house', Core_Array::getPost('house', ''))));
			$_SESSION['hostcmsOrder']['flat'] = Core_Str::stripTags(strval(Core_Array::getPost('flat', Core_Array::getPost('room', ''))));
			$_SESSION['hostcmsOrder']['surname'] = Core_Str::stripTags(strval(Core_Array::getPost('surname', $fio->surname)));
			$_SESSION['hostcmsOrder']['name'] = Core_Str::stripTags(strval($fio->name));
			$_SESSION['hostcmsOrder']['patronymic'] = Core_Str::stripTags(strval(Core_Array::getPost('patronymic', $fio->patronymic)));
			$_SESSION['hostcmsOrder']['phone'] = Core_Str::stripTags(strval(Core_Array::getPost('phone')));
			$_SESSION['hostcmsOrder']['email'] = Core_Str::stripTags(strval(Core_Array::getPost('email')));
			$_SESSION['hostcmsOrder']['description'] = Core_Str::stripTags(strval(Core_Array::getPost('description')));

			// Additional order properties
			$_SESSION['hostcmsOrder']['properties'] = array();

			$oShop_Order_Property_List = Core_Entity::factory('Shop_Order_Property_List', $oShop->id);
			$aProperties = $oShop_Order_Property_List->Properties->findAll();
			foreach ($aProperties as $oProperty)
			{
				// Св-во может иметь несколько значений
				$aPropertiesValue = Core_Array::getPost('property_' . $oProperty->id);
				if(is_null($aPropertiesValue)) {
					$aTmpPropertiesValue = Core_Array::getFiles('property_' . $oProperty->id);
					if(is_array($aTmpPropertiesValue) && count($aTmpPropertiesValue)) {
						foreach ($aTmpPropertiesValue as $tmpPropertyKey => $tmpPropertyValue) {
							foreach ($tmpPropertyValue as $tmpKey => $tmpValue) {
								$aPropertiesValue[$tmpKey][$tmpPropertyKey] = $tmpValue;
							}
						}
					}
				}
				if (!is_null($aPropertiesValue))
				{
					!is_array($aPropertiesValue) && $aPropertiesValue = array($aPropertiesValue);
					foreach ($aPropertiesValue as $sPropertyValue)
					{
						$_SESSION['hostcmsOrder']['properties'][] = array($oProperty->id, $sPropertyValue);
					}
				}
			}

			$shop_delivery_condition_id = strval(Core_Array::getPost('shop_delivery_condition_id', $shop_delivery_condition_id));

			if (is_numeric($shop_delivery_condition_id))
			{
				$_SESSION['hostcmsOrder']['shop_delivery_condition_id'] = intval($shop_delivery_condition_id);

				$oShop_Delivery_Condition = Core_Entity::factory('Shop_Delivery_Condition', $_SESSION['hostcmsOrder']['shop_delivery_condition_id']);
				$_SESSION['hostcmsOrder']['shop_delivery_id'] = $oShop_Delivery_Condition->shop_delivery_id;
			}

			$shop_payment_system_id
				= $_SESSION['hostcmsOrder']['shop_payment_system_id']
				= $shop_payment_system_id; //-- Оплата по счету для юридических лиц
			/** @var Skynetcore_Shop_Payment_System_Handler $oShop_Payment_System_Handler */
			$oShop_Payment_System_Handler = Skynetcore_Shop_Payment_System_Handler::factory(
				Core_Entity::factory('Shop_Payment_System', $shop_payment_system_id)
			);
			if($letters['admin_email'] != ''
				&& Core_Valid::email($letters['admin_email'])
			) {
				$oShop_Payment_System_Handler
					->setAdminMailTo($letters['admin_email']);
			}
			if($letters['letter_subject'] != '') {
				$oShop_Payment_System_Handler
					->setMailTopic($letters['letter_subject']);
			}
			$oShop_Payment_System_Handler::$_letters = $letters;
			$oShop_Payment_System_Handler
				->orderParams($_SESSION['hostcmsOrder'])
				->execute()
//				->xslAdminMail(Core_Entity::factory('Xsl')->getByName(Core_Array::get($letters, 'admin')))
//				->xslSiteuserMail(Core_Entity::factory('Xsl')->getByName(Core_Array::get($letters, 'siteuser')))
//				->send()
			;
			if(isset($_SESSION['last_order_id'])) {
				$oShop_Order = Core_Entity::factory('shop_order')->getById($_SESSION['last_order_id'], false);
				if($siteuser_id !== false && $siteuser_id > 0) {
					$oShop_Order->siteuser_id = $siteuser_id;
					$oShop_Order->save();
				}
				Core_Event::notify(get_class($this) . '.onAfterProcessOrder', $this, [$oShop_Order,  $siteuser_id]);
				unset($_SESSION['last_order_id']);
			}

			$_SESSION['hostcmsOrder'] = $this->_oldHostcmsOrder;
			foreach ($this->_oldCartItems as $cartItem) {
				$Shop_Cart_Controller
					->postpone(Core_Array::get($cartItem, 'postpone', 0))
					->marking(Core_Array::get($cartItem, 'marking', ''))
					->shop_warehouse_id(Core_Array::get($cartItem, 'shop_warehouse_id', 0))
					->siteuser_id(Core_Array::get($cartItem, 'siteuser_id', 0))
					->shop_item_id(Core_Array::get($cartItem, 'shop_item_id'))
					->quantity(Core_Array::get($cartItem, 'quantity'))
					->add();
			}
			Core_Page::instance()->skynet->carts = [];
			$skyCarts = Skynetcore_Shop_Cart_Controller::getMuticart();
			Core_Page::instance()->skynet->carts = $skyCarts->tmpCarts;
			Core_Page::instance()->skynet->cartTotals = $skyCarts->tmpCartTotals;
			Core_Event::notify(get_class($this) . '.onAfterRestoreCartInProcessOrder', $this, [$this->_oldStorage]);
		}
		return $this;
	}
}