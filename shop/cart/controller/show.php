<?php

class Skynetcore_Shop_Cart_Controller_Show extends Shop_Cart_Controller_Show {

	public function show()
	{
		$this->addEntity(Skynetcore_Controller_Process::applyRequestToSessionOrder(true));
		return parent::show();
	}

}