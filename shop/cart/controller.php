<?php

class Skynetcore_Shop_Cart_Controller extends Shop_Cart_Controller_Default {

	public static function getMuticart()
	{
		$sCartEntities = new stdClass();
		$sCartEntities->tmpCarts = $sCartEntities->tmpCartTotals = [];
//-- UPD 16.09.2024. За каким-то лядлм была добавлена данная строка. За каким - фиг знает. Пока закомментировал --
//-- Возвращать не стоит потому что ломается расчет минимальной цены товара --
//		Core_Event::off('Shop_Item_Controller.onAfterCalculatePrice');
		$oSite = Core_Entity::factory('Site')->getById(CURRENT_SITE);
		$aShops = $oSite->shops->findAll(false);
		/** @var Shop_Cart_Controller $oCartInstance */
		$oCartInstance = Shop_Cart_Controller::instance();

		foreach ($aShops as $oShop) {
			$aCartItems = $oCartInstance->getAll($oShop);
			foreach ($aCartItems as $oCartItem) {
				$sCartEntities->tmpCarts[$oShop->id][$oCartItem->shop_item_id][] = $oCartItem;
			}
			$sCartEntities->tmpCartTotals[$oShop->id] = [
				'totalQuantity' => $oCartInstance->totalQuantity,
				'totalAmount' => $oCartInstance->totalAmount,
				'totalTax' => $oCartInstance->totalTax,
				'totalWeight' => $oCartInstance->totalWeight,
				'totalVolume' => $oCartInstance->totalVolume,
				'totalQuantityForPurchaseDiscount' => $oCartInstance->totalQuantityForPurchaseDiscount,
				'totalAmountForPurchaseDiscount' => $oCartInstance->totalAmountForPurchaseDiscount,
				'totalDiscountPrices' => $oCartInstance->totalDiscountPrices,
			];
//				* - totalQuantity общее количество неотложенного товара
//				* - totalAmount сумма неотложенного товара
//				* - totalTax налог неотложенного товара
//				* - totalWeight суммарный вес неотложенного товара
//				* - totalVolume суммарный объем неотложенного товара
//				* - totalQuantityForPurchaseDiscount общее количество неотложенного товара для расчета скидки от суммы заказа
//				* - totalAmountForPurchaseDiscount сумма неотложенного товара для расчета скидки от суммы заказа
//				* - totalDiscountPrices цены товаров для расчета скидки на N-й товар
		}
		return $sCartEntities;
	}

}