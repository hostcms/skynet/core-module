<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Shop_Compare_Controller extends Shop_Compare_Controller
{
	/**
	 * Register an existing instance as a singleton.
	 * @return object
	 */
	static public function instance()
	{
		if (is_null(self::$instance))
		{
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function getAll(Shop_Model $oShop)
	{
		$aCompares = parent::getAll($oShop);
		$aCompareShop_ItemIDs = array_map(function($tmpComp) {
			return $tmpComp->shop_item_id;
		}, $aCompares);

		if(count($aCompareShop_ItemIDs)) {
			$aItems = Core_Entity::factory('Shop_Item')->getAllById($aCompareShop_ItemIDs, false, 'IN');
			$aShop_ItemIDs = array_map(function($tmpComp) {
				return $tmpComp->id;
			}, $aItems);

			if((count($aItems) != count($aCompares))) {
				foreach ($aCompareShop_ItemIDs as $tmpCompItemId) {
					if( !in_array($tmpCompItemId, $aShop_ItemIDs) ) {
						$this->shop_item_id = $tmpCompItemId;
						$this->delete();
					}
				}
			}
		}
		return $aCompares;
	}

}