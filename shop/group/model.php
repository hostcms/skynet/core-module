<?php
use Skynetcore_Utils as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Shop_Group_Model extends Shop_Group_Model
{
	protected $_parentID = 0;
	protected $_parents = [];
	protected $_parentIDs = [];
	protected $_groupProperties = [];

	public function getParentIDs()
	{
		return array_keys($this->getParents());
	}

	public function currentModel()
	{
		return preg_replace('/\_Model$/i', '', get_class($this));
	}

	public function getChilds() {
		$this->queryBuilder()
			->where('parent_id', '=', $this->id);
		return array_merge([$this->id], $this->getGroupChildrenId(false));
	}

	public function getParents()
	{
		$current = $this;
		if ($this->parent_id > 0 && (count($this->_parents) == 0 || $this->_parentID != $this->parent_id)) {
			$this->_parentID = $this->parent_id;
			do {
				$this->_parents[$current->id] = $current;
			} while ($current = Core_Entity::factory($this->currentModel())->getById($current->parent_id, false));
			$this->_parents = array_reverse($this->_parents, true);
		} elseif ($this->parent_id == 0) {
			$this->_parents[$current->id] = $current;
		}
		$this->_parentIDs = array_keys($this->_parents);
		return $this->_parents;
	}

	public function getGroups($shop_id, $parent_id=0) {
		$oChilds = Core_Entity::factory($this->currentModel());
		$oChilds
			->queryBuilder()
			->where('shop_id', '=', $shop_id);
		return $oChilds->getAllByParent_id($parent_id);
	}

	public function getTop($level=0) {
		$ids = $this->getParentIDs();
		if(isset($ids[$level])) {
			return $this->getParents()[$ids[$level]];
		} else {
			return NULL;
		}
	}

	public function setChildEntities($lastLevelLimit, $levelTo=0, $level=0) {
		$oChilds = Core_Entity::factory($this->currentModel());
		$oChilds
			->queryBuilder()
			->where('parent_id', '=', $this->id*1)
			->where('shop_groups.shop_id', '=', $this->shop_id*1)
			->where('shop_groups.active', '=', 1)
			->where('shop_groups.deleted', '=', 0)
			->orderBy(Core_QueryBuilder::expression('CASE WHEN shop_groups.sorting=0 THEN 1000000 else shop_groups.sorting END'))
			->orderBy('shop_groups.name')
		;
		if($level == $levelTo) {
			$oChilds
				->queryBuilder()
				->sqlCalcFoundRows()
				->clearLimit()
				->limit($lastLevelLimit);
		}
		$aChilds = $oChilds->findAll();
		if($level == $levelTo) {
			$row = Core_QueryBuilder::select(array('FOUND_ROWS()', 'count'))->asAssoc()->execute()->current();
			$aChilds[] = Core::factory('Core_Xml_Entity')->name('more')->value($row['count']);
		}
		$this->addXmlProperties($this->_groupProperties);
		foreach ($aChilds as $aChild) {
			method_exists($aChild, 'addXmlProperties') && $aChild->addXmlProperties($this->_groupProperties);
			method_exists($aChild, 'addForbiddenTags') && $aChild->addForbiddenTags(['description']);
		}
		$this->addEntities($aChilds);
		if($level<$levelTo) {
			$this->setChildEntities($levelTo, $level+1);
		}
		return $this;
	}

	public function removeSeoTags()
	{
		$this->addForbiddenTags([
			'description',
			'seo_title',
			'seo_description',
			'seo_keywords',
			'seo_group_title_template',
			'seo_group_keywords_template',
			'seo_group_description_template',
			'seo_item_title_template',
			'seo_item_keywords_template',
			'seo_item_description_template',
		]);
		return $this;
	}

	public function removeImageTags()
	{
		$this->addForbiddenTags([
			'image_large',
			'image_small',
			'image_large_width',
			'image_large_height',
			'image_small_width',
			'image_small_height',
			'dir',
		]);
		return $this;
	}
}