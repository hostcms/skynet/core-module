<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Shop_Delivery_Controller_Show extends Shop_Delivery_Controller_Show
{
	protected function _setShopDeliveries()
	{
		parent::_setShopDeliveries();
		Core_Event::notify('Skynetcore_Shop_Delivery_Controller_Show.onBeforeSetShopDeliveries', $this, [$this->_Shop_Deliveries]);

		return $this;
	}

	public function show()
	{
		$this->addEntity(Skynetcore_Controller_Process::applyRequestToSessionOrder(true));
		$ret = parent::show();
		return $ret;
	}

	static public function onAfterAddShopDeliveryCondition($object, $args)
	{
		/** Shop_Delivery_Cond */
		list($oShop_Delivery_Condition) = $args;
	}
}