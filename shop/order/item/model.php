<?php

use Skynetcore_Utils as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Shop_Order_Item_Model extends Shop_Order_Item_Model
{
	/**
	 * Table name, e.g. 'books' for 'Book_Model'
	 * @var mixed
	 */
	protected $_tableName = 'shop_order_items';

	/**
	 * Model name, e.g. 'book' for 'Book_Model'
	 * @var mixed
	 */
	protected $_modelName = 'shop_order_item';

}