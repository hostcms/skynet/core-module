<?php
use GuzzleHttp\Client as GuzzleClient;
use Http\Factory\Guzzle\RequestFactory;
use Http\Factory\Guzzle\StreamFactory;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Shop_Item_Model extends Shop_Item_Model
{
	protected $_total = 0;
	protected $_limit = false;
	protected $_offset = false;
	protected $_sql_calc_found_rows = false;

	/**
	 * One-to-one relations
	 * @var array
	 */
	protected $_hasOneSkynet = array(
		'ozon_item' => array(
			'model' => 'Skynetcore_Mp_Ozon_Item',
		)
	);

	/**
	 * One-to-one relations
	 * @var array
	 */
	protected $_hasManySkynet = array(
		'ozon_item' => array(
			'model' => 'Skynetcore_Mp_Ozon_Item',
		)
	);

	public function __construct($id = NULL)
	{
		$this->_hasOne = array_merge($this->_hasOne, $this->_hasOneSkynet);
		$this->_hasMany = array_merge($this->_hasMany, $this->_hasManySkynet);
		parent::clearRelationModelCache($this->_modelName);
		parent::__construct($id);
	}

	/**
	 * Show ozon items in XML
	 * @var boolean
	 */
	protected $_showXmlOzon = FALSE;

	/**
	 * Add ozon items XML to item
	 * @param boolean $showXmlOzon mode
	 * @return self
	 */
	public function showXmlOzon($showXmlOzon = TRUE)
	{
		$this->_showXmlOzon = $showXmlOzon;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isSqlCalcFoundRows(): bool
	{
		return $this->_sql_calc_found_rows;
	}

	/**
	 * @param bool $sql_calc_found_rows
	 */
	public function setSqlCalcFoundRows(bool $sql_calc_found_rows = true)
	{
		$this->_sql_calc_found_rows = $sql_calc_found_rows;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getLimit(): int
	{
		return $this->_limit;
	}

	/**
	 * @param bool $limit
	 */
	public function setLimit(int $limit)
	{
		$this->_limit = $limit;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getOffset(): int
	{
		return $this->_offset;
	}

	/**
	 * @param bool $offset
	 */
	public function setOffset(int $offset)
	{
		$this->_offset = $offset;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getTotal(): int
	{
		return $this->_total;
	}

	public function getFirsPropertyValueByTagName($property_tag) {
		$aProperty_Tags = Core_Entity::factory('Property')->getAllByTag_Name(['group:'.$property_tag, 'item:'.$property_tag], false, 'IN');
		$aPropertyIDs = array_map(function($oProperty) {
			return $oProperty->id;
		}, $aProperty_Tags);

		if(count($aPropertyIDs)) {
			$aPropertyValues = $this->getPropertyValues(false, $aPropertyIDs);
			if(count($aPropertyValues)) {
				$oPropertyValue = Core_Array::get($aPropertyValues, 0);
				switch ($oPropertyValue->property->type) {
					case 3:
						$aPropertyValues = array_filter($aPropertyValues, function ($oPropertyValue) {
							return $oPropertyValue->value == 0 ? false : $oPropertyValue;
						});
						break;
				}
				if(count($aPropertyValues)) {
					return array_values($aPropertyValues);
				} else {
					/** @var Shop_Group_Model $oGroup */
					$oGroup = $this->modification_id > 0 ? $this->Modification->shop_group : $this->shop_group;

					$aGroups = []; // = $oGroup;
					do {
						$aGroups[] = $oGroup;
						$oGroup = $oGroup->getParent();
					} while (isset($oGroup->id) && $oGroup->id > 0);
					foreach ($aGroups as $oGroupTmp) {
						$aCategoryValues = $oGroupTmp->getPropertyValues(false, $aPropertyIDs);
						if(count($aCategoryValues) > 0) {
							$oCategoryValue = Core_Array::get($aCategoryValues, 0);
							switch ($oCategoryValue->property->type) {
								case 3:
									$aCategoryValues = array_filter($aCategoryValues, function ($oCategoryValue) {
										return $oCategoryValue->value == 0 ? false : $oCategoryValue;
									});
									break;
							}
							if(count($aCategoryValues)) {
								return array_values($aCategoryValues);
							}
						}
					}
				}
			}
		}
		return [];
	}

	public function getFirsPropertyValue($property_id) {
		$oOzonCategory = 0;

		$oOzonCategory = Core_Array::get($this->getPropertyValues(false,
			[Core_Page::instance()->skynet->config['integration']['ozon']['properties']['ozon_item_category']]),
			0,
			json_decode('{"value": 0}')
		)->value;
		if($oOzonCategory > 0) {
			return $oOzonCategory;
		}
		/** @var Shop_Group_Model $oGroup */
		$oGroup = $this->modification_id > 0 ? $this->Modification->shop_group : $this->shop_group;
//		Skynetcore_Utils::p($this->name);
//		Skynetcore_Utils::p($oGroup->name); die();
		$aGroups = []; // = $oGroup;
		do {
			$aGroups[] = $oGroup;
			$oGroup = $oGroup->getParent();
		} while (isset($oGroup->id) && $oGroup->id > 0);
		foreach ($aGroups as $oGroupTmp) {
			$oOzonCategory = $oGroupTmp->propertyValue($property_id);
			if($oOzonCategory != '') {
				break;
			}
		}
		return $oOzonCategory*1;
	}

	public function setPriceValue($priceGUID, $value) {
		$pr = Core_Entity::factory('Shop_Price')->getByGuid($priceGUID, FALSE);
		if(!is_null($pr)) {
			if(is_null($prValue = $this->shop_item_prices->getByPriceId($pr->id, FALSE))) {
				$prValue=Core_Entity::factory('Shop_Item_Price');
				$prValue->shop_item_id = $this->id;
				$prValue->shop_price_id = $pr->id;
				$prValue->save();
			}
			if($prValue->value != $value) {
				$prValue->value = $value;
				$prValue->save();
			}
		} else {
			throw new Core_Exception("Цена '{$priceGUID}' не существует");
		}
	}

	public function getPriceValue($priceGUID) {
		$returnPriceValue = 0;

		/** @var Shop_Item_Price_Model $pr */
		$pr = Core_Entity::factory('Shop_Price')->getByGuid($priceGUID, FALSE);
		if(!is_null($pr)) {
			/** @var Shop_Item_Price_Model $qItemPrice */
			$qItemPrice = $pr->shop_item_prices;
			$qItemPrice->queryBuilder()->where('shop_price_id', '=', $pr->id);
			$oItemPrice = $qItemPrice->getByShop_item_id($this->id, false);
			if(isset($oItemPrice->value)) {
				$returnPriceValue = $oItemPrice->value;
			}
		}
		return $returnPriceValue;
	}

	public function setWarehouseCount($warehouseGUID, $count) {
		$wh = Core_Entity::factory('Shop_Warehouse')->getByGuid($warehouseGUID,false);
		if(!is_null($wh)) {
			if(is_null($whValue = $this->shop_warehouse_items->getByWarehouseId($wh->id,false))) {
				$whValue=Core_Entity::factory('Shop_Warehouse_Item');
				$whValue->shop_item_id = $this->id;
				$whValue->shop_warehouse_id = $wh->id;
				$whValue->save();
			}
			if($whValue->count != $count) {
				$whValue->count = $count;
				$whValue->save();
			}
		} else {
			throw new Core_Exception("Склад '{$warehouseGUID}' не существует");
		}
	}

	public function getMarketExportJson($modificationAsItem = true) {
		$instance = Core_Page::instance();
		$currentShopItem = $this;
		if($modificationAsItem && isset($this->modification->id) && $this->modification->id > 0) {
			$currentShopItem = $this->modification;
		}

		$currentShopItem
			->showXmlBarcodes()
		;
		/** @var Skynetcore_Shop_Item_Model $oStdItem */
		$oStdItem = $this->getStdObject();
		$retValue = [
			"offerId" => $this->marking,
			"name" => $this->name,
		];
		if(isset($currentShopItem->shop_group->name) && $currentShopItem->shop_group->name != '') {
			$retValue['category'] = $currentShopItem->shop_group->name;
		}
		$barcode = '';
		if(isset($oStdItem->shop_item_barcode)) {
			if(!is_array($oStdItem->shop_item_barcode) && $oStdItem->shop_item_barcode instanceof stdClass) {
				$barcode = $oStdItem->shop_item_barcode->value;
			} else {
				$barcode = $oStdItem->shop_item_barcode[0]->value;
			}
		}
		if($barcode != '') {
			$retValue['barcodes'] = [ $barcode ];
			$retValue['vendorCode'] = $barcode;
		}

		/** @var Moto_Shop_Item_Property_List_Model $oShop_Item_Property_List */
		$oShop_Item_Property_List = Core_Entity::factory('Moto_Shop_Item_Property_List', $currentShopItem->shop->id);
		$oShop_Item_Property_List
			->setFilterProperties(['id' => '', 'name' => "REGEXP#^\/(ТТХ|Технические характеристики)\/"]);

		$aProperties = $oShop_Item_Property_List->getPropertiesForGroup($currentShopItem->shop_group->id);
		$aPropertyValues = [];
		/** @var Property_Model $oProperty */
		foreach ($aProperties as $oProperty) {
			$aTmpPropertyValues = $oProperty->getValues($currentShopItem->id, false);
			/** @var Property_Value_Int_Model $oTmpPropertyValue */
			foreach ($aTmpPropertyValues as $oTmpPropertyValue) {
				switch (true) {
					case ($oProperty->type == 3 && isset($oTmpPropertyValue->list_item->id) > 0 && $oTmpPropertyValue->list_item->value != ''):
						$aPropertyValues[$oProperty->name][] = $oTmpPropertyValue->list_item->value;
						break;
					default:
						switch (true) {
							case ($oTmpPropertyValue instanceof Property_Value_Int_Model && $oTmpPropertyValue->value != 0):
								if($oProperty->type == 7) {
									$aPropertyValues[$oProperty->name][] = $oTmpPropertyValue->value == 1;
								} else {
									$aPropertyValues[$oProperty->name][] = $oTmpPropertyValue->value;
								}
								break;
						}
				}
			}
//			Skynetcore_Utils::p($oProperty->name, $oProperty->id);
		}
		$aParams = [];
		foreach ($aPropertyValues as $propertyName => $propertyValue) {
			$tmpValue = implode(';', $propertyValue);
			if(is_bool($propertyValue[0])) {
				$tmpValue = ($propertyValue[0]===true ? 'Да' : 'Нет');
			}
			$aParams[] =
			[
				'name' => $propertyName,
				'value' => $tmpValue,
			];
		}
		if(count($aParams)) {
			$retValue['params'] = $aParams;
		}
//		Skynetcore_Utils::p($aPropertyValues);
//		$tmpPrice = Core_Array::get($currentShopItem->getPrices(false), 'price_tax', 0);
//		if($tmpPrice > 0) {
//			$retValue['price'] = $tmpPrice;
//		}

		if($currentShopItem->description != '') {
			$retValue['description'] = $currentShopItem->description;
		}
		if($oStdItem->currentimage != '') {
			$retValue['pictures'] = [ $instance->skynet->config['main_domain'] . '/1222x1222' . $oStdItem->currentimage ];
		}
		return $retValue;
	}

	public function getOzonExportJson($fillPrice = false) {
		$instance = Core_Page::instance();
		$this
			->showXmlBarcodes()
		;
		/** @var Skynetcore_Shop_Item_Model $oStdItem */
		$oStdItem = $this->getStdObject();
		$oStdItemParent = $oStdItem->modification_id == 0 ?
			$oStdItem :
			$this->Modification->getStdObject()
		;
		$barcode = (isset($oStdItem->shop_item_barcode)
				&& is_array($oStdItem->shop_item_barcode)
				&& count($oStdItem->shop_item_barcode)
			) ?
			$oStdItem->shop_item_barcode[0]->value :
			((isset($oStdItem->shop_item_barcode) && is_object($oStdItem->shop_item_barcode)) ? $oStdItem->shop_item_barcode->value : '');
		;
		$ozonCategory = $this->getFirsPropertyValue($instance->skynet->config['integration']['ozon']['properties']['ozon_category']);
		if($ozonCategory > 0) {
			$aReturn = [
				'status' => true,
				'message' => 'OK',
				'data' => [
					[
						'complex_id' => 0,
						'id' => 9048,
						'values' => [
							'value' => 'для кухни Start',
						]
					],
					"category_id" => $ozonCategory,
					"offer_id" => $oStdItem->marking,
					"name" => $oStdItem->name,
					"primary_image" => 'https://'.$instance->skynet->request->domain.$oStdItem->currentimage,
//					"old_price" => "1100",
//					"premium_price" => "900",
					"price" => (($fillPrice ? $oStdItemParent->price->value : 0)*1).'',
					"barcode" => $barcode,
					"images" => [ ],
					"images360" => [ ],
					"depth" => $oStdItemParent->length,
					"width" => $oStdItemParent->width,
					"height" => $oStdItemParent->height,
					"dimension_unit" => "mm",
					"weight" => $oStdItemParent->weight,
					"weight_unit" => "kg",
					"color_image" => "",
					"attributes" => [],
					"complex_attributes" => [ ],
					"pdf_list" => [ ],
					"vat" => "0.1",
				]
			];
//			/** @var Skynetcore_Mp_Ozon_Item_Model $qOzonItem */
//			$qOzonItem = Core_Entity::factory('Skynetcore_Mp_Ozon_Item');
//			$ozonItem = $qOzonItem->getByShop_item_id($oStdItem->_id);
////			if(isset($ozonItem->product_id) && $ozonItem->product_id != '') {
////				$aReturn['data']['product_id'] = $ozonItem->product_id;
////			}
			Core_Event::notify(get_class($this) . '.onBeforeGetOzonExportJsonReturn', $this, [$aReturn]);

			$eventResult = Core_Event::getLastReturn();

			if (is_array($eventResult))
			{
				$aReturn = $eventResult;
			}
		} else {
			$aReturn = [
				'status' => false,
				'message' => 'Не задана категория ОЗОН'
			];
		}
		return $aReturn;
	}

	protected function _prepareData()
	{
		parent::_prepareData();

		if($this->_showXmlOzon) {
			$aModifications = $this->Modifications->findAll();
			$oOzonItems = Core::factory('Core_Xml_Entity')
				->name('ozon_items');
			$oOzonItems
				->addEntity($this->ozon_item);
			foreach ($aModifications as $oModification) {
				$oOzonItems->addEntity(
					$oModification->ozon_item
				);
			}
			$this->addEntity($oOzonItems);
		}

		return $this;
	}

	public function findAll($bCache = TRUE)
	{
		$this
			->queryBuilder()
			->sqlCalcFoundRows($this->_sql_calc_found_rows)
		;
		if($this->_limit !== false) {
			$this
				->queryBuilder()
				->clearLimit()
				->clearOffset()
				->limit($this->_limit)
				->offset($this->_offset)
			;
		}
		$aItems = parent::findAll($bCache);
		if( $this->_sql_calc_found_rows ) {
			$this->_total = Core_QueryBuilder::select()->getFoundRows();
		}
		return $aItems;
	}


}