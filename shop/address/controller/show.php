<?php

class Skynetcore_Shop_Address_Controller_Show extends Shop_Address_Controller_Show {

	public function show()
	{
		/** @var Shop_Country_Model $oCountries */
		$oCountries = Core_Entity::factory('Shop_Country');
		$oCountries
			->queryBuilder()
			->clearOrderBy()
			->orderBy(Core_QueryBuilder::expression("CASE WHEN name = 'Россия' THEN '_1' WHEN name = 'Беларусь' THEN '_2' ELSE name END"));
		$aCountries = $oCountries->findAll();

		$aCountriesTmp = Skynetcore_Utils::array_map_assoc(function($oKey, $oItem) {
			return [$oKey, ['id' => $oItem->id*1, 'name' => $oItem->name]];
		}, $aCountries);

		$aLocations = Core_Entity::factory('Shop_Country_Location')->findAll();
		$aLocationsTmp = [];
		foreach ($aLocations as $oLocationTmp) {
			$aLocationsTmp[$oLocationTmp->shop_country_id][] = ['id' => $oLocationTmp->id*1, 'name' => $oLocationTmp->name];
		}

		$aCities = Core_Entity::factory('Shop_Country_Location_City')->findAll();
		$aCitiesTmp = [];
		foreach ($aCities as $oCityTmp) {
			$aCitiesTmp[$oCityTmp->shop_country_location_id][] = ['id' => $oCityTmp->id*1, 'name' => $oCityTmp->name];
		}

		$aAddressStructure = [];
		foreach ($aCountriesTmp as $countryTmp) {
			if(isset($aLocationsTmp[$countryTmp['id']])) {
				$aLocationsCountryTmp = $aLocationsTmp[$countryTmp['id']];
				foreach ($aLocationsCountryTmp as &$locationCountryTmp) {
					if(isset($aCitiesTmp[$locationCountryTmp['id']])) {
						$locationCountryTmp['cities'] = $aCitiesTmp[$locationCountryTmp['id']];
					}
				}
				$asLocationstTmp = $aLocationsCountryTmp;
				$countryTmp['regions'] = $asLocationstTmp;
				$aAddressStructure[] = $countryTmp;
			}
		}

//		Skynetcore_Utils::tp($aCountriesTmp);
//		Skynetcore_Utils::tp($aLocationsTmp);
//		Skynetcore_Utils::tp($aCitiesTmp);
//		Skynetcore_Utils::tp(json_encode($aAddressStructure));


		$this
			->addEntity(Core::factory('Core_Xml_Entity')->name('j_locations')->value(json_encode($aAddressStructure, JSON_UNESCAPED_UNICODE)))
			->addEntity(Skynetcore_Controller_Process::applyRequestToSessionOrder(true));
		return parent::show();
	}

}