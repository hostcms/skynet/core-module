<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Shop_Favorite_Controller extends Shop_Favorite_Controller
{
	/**
	 * Register an existing instance as a singleton.
	 * @return object
	 */
	static public function instance()
	{
		if (is_null(self::$instance))
		{
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function getAll(Shop_Model $oShop)
	{
		$aFavorites = parent::getAll($oShop);
		$aFavoriteShop_ItemIDs = array_map(function($tmpFav) {
			return $tmpFav->shop_item_id;
		}, $aFavorites);

		if(count($aFavoriteShop_ItemIDs)) {
			$aItems = Core_Entity::factory('Shop_Item')->getAllById($aFavoriteShop_ItemIDs, false, 'IN');
			$aShop_ItemIDs = array_map(function($tmpFav) {
				return $tmpFav->id;
			}, $aItems);

			if((count($aItems) != count($aFavorites))) {
				foreach ($aFavoriteShop_ItemIDs as $tmpFavItemId) {
					if( !in_array($tmpFavItemId, $aShop_ItemIDs) ) {
						$this->shop_item_id = $tmpFavItemId;
						$this->delete();
					}
				}
			}
		}
		return $aFavorites;
	}

}