<?php

/**
 */
class Skynetcore_Shop_Payment_System_Handler extends Shop_Payment_System_Handler
{
	static public $_adminMailTo = '';
	/** @var Skynetcore_Shop_Payment_System_Handler $_currentOrder */
	static public $_currentOrder = false;
	static public $_mailTopic = '';
	static public $_mailTopicAdmin = '';
	static public $_letters = [
		'admin_email' => '',
		'admin' => '',
		'siteuser' => '',
		'letter_subject' => '',
	];
	protected $_instance = '';
	protected $_domain = '';

	public function __construct(Shop_Payment_System_Model $oShop_Payment_System_Model)
	{
		parent::__construct($oShop_Payment_System_Model);

		$this->_instance = Core_Page::instance();
		$this->_domain = Core_Page::instance()->skynet->request->domain;
	}

	public function send()
	{
		parent::send();
		self::$_mailTopic = self::$_mailTopicAdmin = self::$_adminMailTo = '';
		return $this;
	}


	/**
	 * @param string $adminMailTo
	 */
	public function setAdminMailTo(string $adminMailTo): self
	{
		self::$_adminMailTo = $adminMailTo;
		return $this;
	}

	/**
	 * @param string $mailTopic
	 */
	public function setMailTopic(string $mailTopic): self
	{
		self::$_mailTopic = $mailTopic;
		return $this;
	}

	/**
	 * @param string $mailTopicAdmin
	 */
	public function setMailTopicAdmin(string $mailTopicAdmin): self
	{
		self::$_mailTopicAdmin = $mailTopicAdmin;
		return $this;
	}

	public function getAdminEmails()
	{
		$aAdminEmails = parent::getAdminEmails();
		if(self::$_adminMailTo != ''
			&& Core_Valid::email(self::$_adminMailTo)
		) {
			$aAdminEmails[] = self::$_adminMailTo;
			self::$_adminMailTo = '';
		}
		$aAdminEmails = array_unique($aAdminEmails);

		return $aAdminEmails;
	}

	public function sendSiteuserEmail(Core_Mail $oCore_Mail)
	{
		if(method_exists($this, 'onBeforeAdminSend')) {
			Core_Event::attach('Core_Mail.onBeforeSend', [$this, 'onBeforeSiteuserSend']);
		}
		$this->_shopOrder->showXmlCommentProperties();
		self::$_currentOrder = $this;
		parent::sendSiteuserEmail($oCore_Mail);
		if(method_exists($this, 'onBeforeAdminSend')) {
			Core_Event::detach('Core_Mail.onBeforeSend', [$this, 'onBeforeSiteuserSend']);
		}
		return $this;
	}

	public function sendAdminEmail(Core_Mail $oCore_Mail)
	{
		if(method_exists($this, 'onBeforeAdminSend')) {
			Core_Event::attach('Core_Mail.onBeforeSend', [$this, 'onBeforeAdminSend']);
		}
		$this->_shopOrder->showXmlCommentProperties();
		self::$_currentOrder = $this;
		parent::sendAdminEmail($oCore_Mail);
		if(method_exists($this, 'onBeforeAdminSend')) {
			Core_Event::detach('Core_Mail.onBeforeSend', [$this, 'onBeforeAdminSend']);
		}
		return $this;
	}

	/**
	 * Set XSLs to e-mail
	 * @return self
	 * @hostcms-event Skynetcore_Shop_Payment_System_Handler.onBeforeSetXSLs
	 * @hostcms-event Skynetcore_Shop_Payment_System_Handler.onAfterSetXSLs
	 */
	public function setXSLs()
	{
		Core_Event::notify('Skynetcore_Shop_Payment_System_Handler.onBeforeSetXSLs', $this);

//		Skynetcore_Utils::p($this::$_letters); die();
		$oShopOrder = $this->_shopOrder;
		$oStructure = $oShopOrder->Shop->Structure;
		$libParams = $oStructure->Lib->getDat($oStructure->id);

		$adminLetterXslName = Core_Array::get($this::$_letters, 'admin', '');
		$userLetterXslName = Core_Array::get($this::$_letters, 'siteuser', '');

		if($adminLetterXslName == '' ) {
			$adminLetterXslName = Core_Array::get($libParams, 'orderAdminNotificationXsl');
		}

		if($userLetterXslName == '' ) {
			$userLetterXslName = Core_Array::get($libParams, 'orderUserNotificationXsl');
		}

		$this->xslAdminMail(
			Core_Entity::factory('Xsl')->getByName($adminLetterXslName)
		)
			->xslSiteuserMail(
				Core_Entity::factory('Xsl')->getByName($userLetterXslName)
			);

		Core_Event::notify('Skynetcore_Shop_Payment_System_Handler.onAfterSetXSLs', $this);

		return $this;
	}

	protected function _processOrder()
	{
		parent::_processOrder();
		Core_Event::notify('Skynetcore_Shop_Payment_System_Handler.onAfterProcessOrder', $this);

		// Установка XSL-шаблонов в соответствии с настройками в узле структуры
		$this->setXSLs();

		// Отправка писем клиенту и пользователю
		$this->send();

		return $this;
	}
}