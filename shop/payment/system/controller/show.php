<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Shop_Payment_System_Controller_Show extends Shop_Payment_System_Controller_Show
{
	public function show()
	{
		$this->addEntity(Skynetcore_Controller_Process::applyRequestToSessionOrder(true));
		return parent::show();
	}
}