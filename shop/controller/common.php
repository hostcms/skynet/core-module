<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Skynetcore Module.
 *
 * @package HostCMS
 * @subpackage Redwings
 * @version 6.x
 * @author GoldDraft
 * @copyright © 2021 GoldDraft (info@golddraft.ru)
 */

class Skynetcore_Shop_Controller_Common extends Shop_Controller_Show
{
	/** @var Core_Page $_instance */
	protected $_instance = false;
	/** @var Shop_Group_Model $_currentGroup */
	protected $_currentGroup = false;
	/** @var Shop_Item_Model $_currentItem */
	protected $_currentItem = false;
	protected $_parentGroups = array();
	protected $_parentGroupIDs = array();
	protected $_groupXMLProperties = FALSE;

	public function __construct($oShop)
	{
		parent::__construct($oShop);

		$this->_instance = Core_Page::instance();
	}

	public function getFirstGroup()
	{
		$this->getParentGroups();
		return count($this->_parentGroups) > 0 ? $this->_parentGroups[array_keys($this->_parentGroups)[0]] : $this->_zero;
	}

	public function getLastGroup()
	{
		$this->getParentGroups();
		$parentGroupKeys = array_keys($this->_parentGroups);
		$parentGroupIndexID = end($parentGroupKeys);
		return count($this->_parentGroups) > 0 ? $this->_parentGroups[$parentGroupIndexID] : $this->_zero;
	}

	public function getParentGroupIDs()
	{
		return array_keys($this->getParentGroups());
	}

	public function getParentGroups()
	{
//		Skynetcore_Utils::p($this->group);
		if (count($this->_parentGroups) == 0 && $this->group > 0) {
			$currentGroup = Core_Entity::factory('Shop_Group')->getById($this->group);
			do {
				$currentGroup->showXmlProperties($this->_groupXMLProperties);
				$this->_parentGroups[$currentGroup->id] = $currentGroup;
			} while ($currentGroup = $currentGroup->getParent());
			$this->_parentGroups = array_reverse($this->_parentGroups, true);
		}
		$this->_parentGroupIDs = array_keys($this->_parentGroups);
		return $this->_parentGroups;
	}

	public function getChildGroups() {
		if(is_null($this->_childGroupIDs)) {
			$this->setChildGroups();
		}
		return $this->_childGroupIDs;
	}

	public function setChildGroups($fromLevel=1, $toLevel=10, $additionalGIDs=[])
	{
		$splitedPath = json_decode($this->_instance->srcrequest->splitedPath, true);
		$path = '/'.implode('/', array_slice($splitedPath, 0, $fromLevel)).'/';

		$subgroupsForMenu = Utils_Observers_Shop_Group::getGroups($this->getEntity()->id, $path, $toLevel);
		$childGroups = utl::readTreeIDsFromLevel($subgroupsForMenu, $this->getLastGroup()->id);
		$topChildGroups = utl::readTreeIDsFromLevel($subgroupsForMenu, $this->getFirstGroup()->id);

		$this->_childGroupIDs = count($childGroups)>0 ? $childGroups : array(-1);
		$allGroups = array_merge($this->_childGroupIDs);
		$restrictGroups = array_merge($this->_parentGroupIDs, $topChildGroups, $this->_childGroupIDs);
		$restrictGroups = array_map(function ($val) {
			return $val*1;
		}, $restrictGroups);
		if(count($additionalGIDs)) {
			$restrictGroups = array_merge($restrictGroups, $additionalGIDs);
		}
		sort($allGroups);
		if($this->item==0) {
			$this
				->group(false)
				->shopItems()
				->queryBuilder()
				->where('shop_items.shop_group_id', 'IN', array_unique($allGroups))
			;
		}
		$groupsBuilder = $this->shopGroups()->queryBuilder();
		$groupsBuilder
			->where('id', 'IN', array_unique($restrictGroups))
		;

		return $this;
	}

	public function getCurrentGroup()
	{
		if (is_null($this->_currentGroup) && $this->group > 0) {
			$this->_currentGroup = Core_Entity::factory('Utils_Shop_Group')->getById($this->group);
		} elseif (!is_object($this->_currentGroup)) {
			$this->_currentGroup = $this->_zero;
		}
		return $this->_currentGroup;
	}

	public function getCurrentItem()
	{
		if (is_null($this->_currentItem) && $this->item > 0) {
			$this->_currentItem = Core_Entity::factory('Utils_Shop_Item')->getById($this->item);
		} elseif ($this->item == 0) {
			$this->_currentItem = $this->_zero;
		}
		return $this->_currentItem;
	}

	public function parseUrl()
	{
		parent::parseUrl();
		Skynetcore_Controller_Common::redirectByConditions($this);

		return $this;
	}

}