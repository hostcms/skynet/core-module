<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Property_Controller_Value extends Property_Controller_Value
{
	public static function setPropertyValue($property_id, $entity_id, $value) {
		/** @var Property_Model $oProperty */
		$oProperty = Core_Entity::factory('Property')->getById($property_id);
		if(isset($oProperty->id) && $oProperty->id > 0) {
			$oEntityValue = Core_Array::get($oProperty->getValues($entity_id, false), 0, false);
			if($oEntityValue === false) {
				/** @var Property_Value_Int_Model $oEntityValue */
				$oEntityValue = $oProperty->createNewValue($entity_id);
			}
			if($oEntityValue->value != $value) {
				$oEntityValue->setValue($value);
				$oEntityValue->save();
			}
		}
	}
}
