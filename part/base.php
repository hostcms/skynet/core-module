<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Part_Base
{
	public static function getUserBarTop($type, $shop_id, $xsl_name)
	{
		$instance = Core_Page::instance();
		$oShop = Core_Entity::factory('Shop')->getById($shop_id, false);

		ob_start();
		$aTopBarNodes = [];
		if ($instance->skynet->$type[$oShop->id]->count > 0) {
			$aTopBarNodes = array_values($instance->skynet->$type[$oShop->id]->nodes);
		}
		$oShop->addEntities($aTopBarNodes);
		$favoritesText = Xsl_Processor::instance()
			->xml($oShop->getXml())
			->xsl(Core_Entity::factory('Xsl')->getByName($xsl_name))
			->process();
		echo $favoritesText;

		return ob_get_clean();
	}

//	public static function getLittleCart()
//	{
//		ob_start();
//		$Shop_Cart_Controller_Show = new Shop_Cart_Controller_Show(
//			Core_Entity::factory('Shop', 5)
//		);
//		$Shop_Cart_Controller_Show
//			->xsl(
//				Core_Entity::factory('Xsl')->getByName('Shoker. Common. КорзинаКраткая')
//			)
//			->couponText(isset($_SESSION) ? Core_Array::get($_SESSION, 'coupon_text') : '')
//			->show();
//
//		return ob_get_clean();
//	}
}