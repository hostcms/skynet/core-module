function dataset(el) {
    const data = {};
    const dataset = el.dataset;
    for (let i in dataset) {
        if (i !== 'container' && i !== 'fancyclose' && i !== 'url' && i !== 'context') {
            data[i] = dataset[i].trim();
        }
    }
    return data;
}
function msgBox(message, type) {
    let informerWrap = document.querySelector('#informer-wrap');
    if (!informerWrap) {
        informerWrap = document.createElement('div');
        informerWrap.id = 'informer-wrap';
        informerWrap.className = 'informer-wrap';
        document.body.appendChild(informerWrap);
        informerWrap.style.position = 'fixed';
        informerWrap.style.zIndex = '9999999';
        informerWrap.style.right = '10px';
        informerWrap.style.top = '10px';
        informerWrap.style.width = '270px';
        informerWrap.style.pointerEvents = 'none';
        // informerWrap.style.left = '10px';
    }
    let timeKey;
    const closeInformerHandler = function () {
        this.removeEventListener('click', closeInformerHandler, false);
        informerWrap.removeChild(this.parentNode);
        clearTimeout(timeKey);
        return this.parentNode;
    };

    const informer = document.createElement('div');
    const informerClose = document.createElement('button');
    informer.className = 'informer informer_' + type;
    informer.innerHTML = '<div class="informer__message" style="padding:15px;display:flex;justify-content:center;align-items:center;">' + message + '</div>';
    informerClose.className = 'informer__close';

    informer.style.position = 'relative';
    informer.style.textAlign = 'center';
    informer.style.color = '#fff';
    informer.style.background = 'rgba(34, 160, 239, 0.9)';
    informer.style.width = '100%';
    informer.style.marginTop = '10px';
    informer.style.fontSize = '14px';
    informer.style.lineHeight = '1';
    informer.style.boxShadow = '0 16px 35px -8px rgba(30, 55, 80, 0.2)';
    informer.style.pointerEvents = 'auto';
    if (type.toLowerCase() === 'error') {
        informer.style.background = 'rgba(252, 97, 128, 0.9)';
        informer.style.boxShadow = '0 16px 35px -8px rgba(252, 97, 128, 0.9)';
    }
    // if (type.toLowerCase() === 'info') {
    // 	informer.style.background = 'rgba(252, 97, 128, 0.9)';
    // 	informer.style.boxShadow = '0 16px 35px -8px rgba(252, 97, 128, 0.9)';
    // }
    if (type.toLowerCase() === 'warn' || type.toLowerCase() === 'warning') {
        informer.style.background = 'rgba(252, 163, 78, 0.9)';
        informer.style.boxShadow = '0 16px 35px -8px rgba(252, 163, 78, 0.2)';
    }

    informerClose.addEventListener('click', closeInformerHandler, false);
    informerClose.style.position = 'absolute';
    informerClose.style.top = '0';
    informerClose.style.right = '0';
    informerClose.style.width = '20px';
    informerClose.style.height = '20px';
    informerClose.style.lineHeight = '20px';
    informerClose.style.border = '0';
    informerClose.style.outline = '0';
    informerClose.style.cursor = 'pointer';
    informerClose.style.textAlign = 'center';
    informerClose.style.padding = '0';
    informerClose.style.fontSize = '14px';
    informerClose.style.color = '#fff';
    informerClose.style.background = 'rgba(0,0,0,.2)';
    informerClose.innerHTML = '<svg viewBox="0 0 32 32" style="fill:transparent;stroke:currentColor;stroke-width:1;"><path d="M10,10 L22,22 M22,10 L10,22"></path></svg>';

    informer.appendChild(informerClose);
    informerWrap.appendChild(informer);

    timeKey = setTimeout(function () {
        informerClose.removeEventListener('click', closeInformerHandler, false);
        informerWrap.removeChild(informer);
        delete timeKey;
        return informer;
    }, 4000);

    return informerWrap;
}

//skynet 2.0
const defaultSkynet = {
    excludeAttr: []
};
const skynetMethods = {
    sortTable(el){
        const obj = {
            element: el,
            siblings: null,
            table: null,
            //tbody: null,
            th: null,
            rows: [],
            index: null,
            sorted: []
        };
        let invert = false;
        obj.table = el.closest('table');
        obj.siblings = obj.table.querySelectorAll('.js-skynet[data-skynet="sort-table"]');
        obj.tbody = obj.table.querySelector('tbody');
        obj.th = el.closest('th');
        obj.index = [...obj.th.parentElement.children].indexOf(obj.th);

        if(!obj.element.classList.contains('asc') && !obj.element.classList.contains('desc')){
            obj.siblings.forEach(sib => {
                sib.classList.remove('asc');
                sib.classList.remove('desc');
            });
            invert = false;
            obj.element.classList.add('asc');
        }else if(obj.element.classList.contains('asc')){
            obj.element.classList.remove('asc');
            obj.element.classList.add('desc');
            invert = true;
        }else if(obj.element.classList.contains('desc')){
            obj.element.classList.remove('desc');
            obj.element.classList.add('asc');
            invert = false;
        }

        const tableGroups = {};
        let currentChild = obj.tbody.firstElementChild;
        let tmpType = '';
        let pos = 0;

        if(currentChild.classList.contains('mp-table-head')){
            while(currentChild){
                if(currentChild.classList.contains('mp-table-head')){
                    tmpType = currentChild.dataset.type;
                    tableGroups[tmpType] = {
                        ar: [],
                        head: currentChild,
                        position: pos
                    };
                    pos++;
                }else {
                    tableGroups[tmpType].ar.push(currentChild);
                }
                currentChild = currentChild.nextElementSibling;
            }


            for(let i in tableGroups){
                obj.sorted = [];
                obj.rows = [];
                tableGroups[i].ar.forEach(tr => {
                    const o = {
                        tr: null,
                        td: null
                    };
                    o.tr = tr;
                    [...tr.children].forEach((td, index) => {
                        if(index === obj.index){
                            o.td = td;
                        }
                    });
                    obj.rows.push(o);
                });
                obj.sorted = [...obj.rows];
                obj.sorted = sorter(obj.sorted);
                if(invert){
                    obj.sorted.reverse();
                }

                tableGroups[i].sorted = obj.sorted;
            }
            while (obj.tbody.firstChild) {
                obj.tbody.removeChild(obj.tbody.lastChild);
            }
            for(let i in tableGroups){
                obj.tbody.appendChild(tableGroups[i].head);
                let html = [];
                tableGroups[i].sorted.forEach(s => {
                    html.push(s.tr.outerHTML);
                });
                html = html.join('');
                obj.tbody.innerHTML += html;
            }
            return false;
        }else {
            obj.tbody.querySelectorAll('tbody tr:not(.mp-table-head)').forEach(tr => {
                const o = {
                    tr: null,
                    td: null
                };
                o.tr = tr;
                [...tr.children].forEach((td, index) => {
                    if(index === obj.index){
                        o.td = td;
                    }
                });
                obj.rows.push(o);
            });
            obj.sorted = [...obj.rows];

            obj.sorted = sorter(obj.sorted);
            if(invert){
                obj.sorted.reverse();
            }
            while (obj.tbody.firstChild) {
                obj.tbody.removeChild(obj.tbody.lastChild);
            }
            obj.sorted.forEach(s => {
                obj.tbody.appendChild(s.tr);
            });

        }

        function sorter(group){
            return group.sort((a,b)=>{
                const aSpan = (a.td.querySelector('span')) ? a.td.querySelector('span') : a.td;
                const bSpan = (b.td.querySelector('span')) ? b.td.querySelector('span') : b.td;
                const f = Number(aSpan.innerHTML.replaceAll(/\s/g,''));
                const s = Number(bSpan.innerHTML.replaceAll(/\s/g,''));
                const first = isNaN(f) ? 0 : f;
                const second = isNaN(s) ? 0 : s;
                return first - second;
            });
        }

        function isAlphanumeric(str) {
            let code, i, len;
            for (i = 0, len = str.length; i < len; i++) {
                code = str.charCodeAt(i);
                if (!(code > 47 && code < 58) &&
                    !(code > 64 && code < 91) &&
                    !(code > 96 && code < 123)) {
                    return false;
                }
            }
            return true;
        }
    }
};
class Skynet {
    constructor(options = {}) {
        this.options = this.extend(options, defaultSkynet);
        this.exclude = ['container', 'fancyclose', 'url', 'context', 'show_message', 'fill_form'].concat(this.options.excludeAttr);
        this.isInit = false;
        this.element = null;
        this.showAlert = 0;
        this.version = '2.24.11.06';
        this.init();
    }

    init(){
        if(!this.isInit){
            this.isInit = true;
            this.clickHandler = this.clickHandler.bind(this);
            this.changeHandler = this.changeHandler.bind(this);
            this.submitHandler = this.submitHandler.bind(this);
            document.addEventListener('click', this.clickHandler);
            document.addEventListener('change', this.changeHandler);
        }else {
            new Informer({
                text: 'SkyNet already init',
                type: 'info'
            }).show();
        }
    }
    // destroy(){
    // 	this.isInit = false;
    // 	document.removeEventListener('click', this.clickHandler);
    // }

    clickHandler(event){
        const { target } = event;
        let elementSendData = (target.classList.contains('js-skynet-send-data')) ? target : target.closest('.js-skynet-send-data');
        let element = (target.classList.contains('js-skynet')) ? target : target.closest('.js-skynet');
        let elementModal = (target.classList.contains('js-skynet-modal')) ? target : target.closest('.js-skynet-modal');
        if(elementSendData){
            this.element = elementSendData;
            this.showAlert = (this.element.dataset.hasOwnProperty('show_message')) ? Number(this.element.dataset.show_message) : 0;

            const isForm = this.element.dataset.form !== undefined && this.element.dataset.form === '1';

            if(isForm){
                const formEl = (this.element.tagName.toLowerCase() === 'form') ? this.element : this.element.form;
                if(formEl && formEl.dataset.submit !== undefined){
                    switch (formEl.dataset.submit){
                        case 'default':
                            if(formEl.getAttribute('init') === null){
                                formEl.setAttribute('init', 'init');
                                formEl.addEventListener('submit', this.submitHandler);
                            }
                            break;
                    }
                }else {
                    this.element.classList.add('is-wait');
                    this.element.classList.add('is-skynet-wait');
                    this.send(this.getData(), null, elementSendData);
                }
            }else {
                this.element.classList.add('is-wait');
                this.element.classList.add('is-skynet-wait');

                let run = true;
                if(typeof extendFunctionsPreSkyNet === 'function'){
                    run = extendFunctionsPreSkyNet.call(elementSendData);
                }
                if(run){
                    this.send(this.getData(), null, elementSendData);
                }else {
                    console.log('sky abort');
                    this.element.classList.remove('is-wait');
                    this.element.classList.remove('is-skynet-wait');
                }
                // this.send(this.getData(), null, elementSendData);
            }
        }
        if(element){
            const skynetType = element.dataset.skynet.split(',');
            skynetType.forEach(type => {
                switch (type.trim()){
                    case 'sort-table':
                        skynetMethods.sortTable(element);
                        break;
                    case 'sort-table-mp':
                        skynetMethods.sortTableMP(element);
                        break;
                }
            });
        }
        if(elementModal){
            this.element = elementModal;
            this.element.classList.add('is-wait');
            this.element.classList.add('is-skynet-wait');
            this.showAlert = (this.element.dataset.hasOwnProperty('show_message')) ? Number(this.element.dataset.show_message) : 0;
            this.appendContentToModal(elementModal);
        }
    }
    submitHandler(event){
        event.preventDefault();
        this.element.classList.add('is-wait');
        this.element.classList.add('is-skynet-wait');
        this.send(this.getData(), null, this.element);
    }
    changeHandler(event){
        const { target } = event;
        let element = (target.classList.contains('js-skynet-change-send-data')) ? target : target.closest('.js-skynet-change-send-data');

        if(element){
            this.element = element;
            this.send(this.getData());
        }
    }

    getObjElement(){
        const data = {};
        const dataset = this.element.dataset;
        for (let i in dataset) {
            if(!this.exclude.includes(i)){
                data[i] = dataset[i].trim();
            }
        }
        return data;
    }
    getObjOutside(){
        const data = {};
        const fillForm = this.element.dataset.fill_form.split(',');
        fillForm.forEach(el => {
            const target = document.querySelector(el);
            if(target){
                data[target.name] = target.value;
            }
        });
        return data;
    }
    getObjForm(){
        const form = (this.element.tagName.toLowerCase() === 'form') ? this.element : this.element.form;
        // const data = {};
        // if (form) {
        // 	for (let i = 0; i < form.length; i++) {
        // 		if (form[i].name !== undefined && form[i].name !== '' && form[i].type !== 'file') {
        // 			data[form[i].name] = form[i].value.trim();
        // 		}
        // 	}
        // }
        // return data;

        const data = {};
        if(form){
            const formData = new FormData(form);
            formData.forEach((value, key) => {
                if(!data.hasOwnProperty(key)){
                    data[key] = value;
                    return;
                }
                // if(!Reflect.has(data, key)){
                // 	data[key] = value;
                // 	return;
                // }
                if(!Array.isArray(data[key])){
                    data[key] = [data[key]];
                }
                data[key].push(value);
            });
        }
        return data;
    }

    getContext(){
        const contextArray = (this.element.dataset.hasOwnProperty('context')) ? this.element.dataset.context.split(',') : [];
        const re = new RegExp(/[\t\n\r]/, 'g');
        const objContext = {};
        contextArray.forEach((context)=>{
            const keys = context.split(':');
            const selectorName = keys[0];
            let target = document.querySelectorAll(selectorName);
            if(target.length){
                if(keys.length > 1) {
                    keys.forEach((key)=>{
                        switch (key){
                            case 'f':
                                // f -- не будет отправляться разметка контекста на сервер
                                objContext[selectorName] = '';
                                break;
                        }
                    });
                }else {
                    // заполняем html-разметкой объект ключ которого является контекст
                    // const regClear = new RegExp('/(?<=>)\\s+(?=<)/', 'g');
                    // const targetClearHTML = targetOuterHTML.replace(regClear, '');

                    //придумать для чекбоксов, их нужно будет поместить в ЗНАЧЕНИЕ все checked элементы
                    for(let i=0; i<target.length; i++){
                        if(target[i].tagName.toLowerCase() === 'input' && ['radio'].includes(target[i].type)){
                            if(target[i].checked){
                                const el = target[i];
                                target = [];
                                target.push(el);
                                break;
                            }
                        }
                    }

                    // console.log(target[0].outerHTML.toString().replace(/(<(pre|script|style|textarea)[^]+?<\/\2)|(^|>)\s+|\s+(?=<|$)/g, "$1$3"))
                    // objContext[selectorName] = target[0].outerHTML.toString().replace(re, '');
                    objContext[selectorName] = target[0].outerHTML.toString().replace(/(<(pre|script|style|textarea)[^]+?<\/\2)|(^|>)\s+|\s+(?=<|$)/g, "$1$3");
                    // objContext[selectorName] = target[0].outerHTML.toString().replace(re, '').replace(/(?<=>)\s+(?=<)/g, '');
                }
            }
        });
        return objContext;
    }

    createFormData(dataSet, formSet, isForm){
        function buildFormData(formData, data, parentKey) {
            if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
                Object.keys(data).forEach(key => {
                    buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
                });
            } else {
                const value = data == null ? '' : data;
                formData.append(parentKey, value);
            }
        }
        const form = (this.element.tagName.toLowerCase() === 'form') ? this.element : this.element.form;
        const data = {};
        Object.assign(data, dataSet);
        Object.assign(data, formSet);
        const fd = new FormData();

        for (let i in data) {
            if(typeof data[i] === 'object'){
                if(isForm === undefined){
                    if(i === 'context_html'){
                        const obj = data[i];
                        for(let j in obj){
                            fd.append(`${i}[${j}]`, obj[j]);
                        }
                    }
                }
            }else {
                //fd.append(i, data[i]);
            }
        }
        //проверить момент с context_html и file
        buildFormData(fd, data);

        //если есть input type=file, то добавляем данные в fd
        const formFiles = form.querySelectorAll('input[type="file"]');
        formFiles.forEach((fileInput)=>{
            const files = fileInput.files;
            for(let i=0; i<files.length; i++){
                fd.append(fileInput.name, files[i]);
            }
        });

        //TmpFiles - глобальная переменка для dropzone, если есть файлы для dropzone, то также добавленяем данные файлов в fd
        if(typeof TmpFiles !== 'undefined' && TmpFiles.length){
            const nameDropzoneFile = document.querySelector('.js-dropzone').dataset.name;
            // console.log(nameDropzoneFile);
            TmpFiles.map((file)=>{
                if(file.hasOwnProperty('dz_id')){
                    fd.append(`${file.dz_id}[]`, file);
                }else {
                    fd.append(`${nameDropzoneFile}[]`, file);
                }
            });
        }

        return fd;
    }

    getData(){
        const isForm = this.element.dataset.form !== undefined && this.element.dataset.form === '1';
        let formEl = null;
        let sendData = {};
        this.setInputsValue(this.getContext());
        sendData['context_html'] = this.getContext();
        Object.assign(sendData, this.getObjElement());
        if(this.element.dataset.hasOwnProperty('fill_form')){
            Object.assign(sendData, this.getObjOutside());
        }
        if(this.element.dataset.hasOwnProperty('context_values')){
            Object.assign(sendData, this.getOutDataFromContextValues(this.element));
        }
        if(isForm){
            formEl = (this.element.tagName.toLowerCase() === 'form') ? this.element : this.element.form;
            const formSet = this.getObjForm();
            sendData = this.createFormData(sendData, formSet, isForm);
        }
        return {
            isForm: isForm,
            formEl: formEl,
            url: this.element.dataset.url,
            data: sendData
        };
    }

    setInputsValue(contexts){
        const keys = Object.keys(contexts);
        const TYPES = ['text', 'email', 'tel'];
        keys.forEach(k => {
            document.querySelectorAll(k).forEach(el => {
                if(el.tagName.toLowerCase() === 'input'){
                    if(TYPES.includes(el.type.toString())){
                        el.setAttribute('value', el.value);
                    }
                }
            });
        });
    }

    getOutDataFromContextValues(el){
        if(el.dataset.context_values !== undefined && el.dataset.context_values.trim() !== '') {
            const obj = {};
            el.dataset.context_values.split(',').forEach(ctx => {
                document.querySelectorAll(ctx).forEach(node => {
                    const tag = node.tagName.toLowerCase();
                    if(['input', 'select', 'textarea'].includes(tag)){
                        obj[node.name] = node.value;
                    }
                })
            })
            return obj;
        }
    }

    serializeGet(obj, path = [], result = []){
        const array = Object.entries(obj).reduce( (accumulator, [ key, value ]) => {
            path.push(key);
            if(value instanceof Object){
                this.serializeGet(value, path, accumulator);
            }else {
                accumulator.push( `${path.map((n, i) => i
                    ? `%5B${encodeURIComponent(n.toString())}%5D`
                    : encodeURIComponent(n.toString())).join('')}=${encodeURIComponent(value.toString())}`
                );
            }
            path.pop();
            return accumulator;
        }, result );
        return array.join('&');
    }

    send(fd, callback, btn){
        const serialize = this.serializeGet(fd.data);
        let opt = {
            body: (fd.isForm) ? fd.data : serialize,
            method: 'post',
            cache: 'no-cache'
        };
        if(!fd.isForm){
            //если не форма, то будем жестко отправлять json и принимать json
            // opt.headers = {
            // 	// 'Accept': 'application/json, text/plain, */*',
            // 	'Accept': 'application/json',
            // 	'Content-Type': 'application/json; charset=UTF-8'
            // }
            opt.headers = {
                'Accept': '*/*',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                //'Access-Control-Expose-Headers': 'Content-Disposition'
            }
        }
        let filename = '';
        if(btn){
            const label = btn.closest('label');
            if(label){
                label.classList.add('is-skynet-wait');
            }
            btn.disabled = true;
        }
        fetch(fd.url, opt).then((response) => {
            if (!response.ok) {
                new Informer({
                    text: response.status,
                    type: 'error'

                }).show();
                this.clearWaitButtons();
                throw new Error("HTTP status " + response.status);
            }
            const contentType = response.headers.get("content-type");
            const contentDisposition = response.headers.get('content-disposition');
            if(contentType && contentType.indexOf('application/json') !== -1){
                //если всё ок, то преобразуем ответ в json
                return response.json();
            }else if(contentDisposition && contentDisposition.indexOf('attachment') !== -1){
                filename = contentDisposition.split(/;(.+)/)[1].split(/=(.+)/)[1];
                if (filename.toLowerCase().startsWith("utf-8''")){
                    filename = decodeURIComponent(filename.replace("utf-8''", ''))
                } else {
                    filename = filename.replace(/['"]/g, '');
                }
                return response.blob();
            } else {
                new Informer({
                    text: 'Данный тип ответа не поддерживается',
                    type: 'error'
                }).show();
                this.clearWaitButtons();
                return response.text();
                //throw new Error("ответ не JSON");
            }

        }).then((data)=>{
            if(btn){
                setTimeout(()=>{
                    const label = btn.closest('label');
                    if(label){
                        label.classList.remove('is-skynet-wait');
                    }
                    btn.disabled = false;
                },500);
            }
            if(data.type !== undefined && data.type.includes('application/')){
                const file = window.URL.createObjectURL(data);
                const a = document.createElement('a');
                a.download = decodeURIComponent(filename);
                a.href = file;
                document.body.appendChild(a);
                a.click();
                a.remove();
                this.clearWaitButtons();
                return false;
            }
            if(!data.hasOwnProperty('status')){
                new Informer({
                    text: 'Статус отсутствует, скрипт прерван',
                    type: 'error'
                }).show();
                this.clearWaitButtons();
                throw new Error('Статус отсутствует, скрипт прерван');
            }

            const status = data.status.toString().toLowerCase();
            if(status === 'error'){
                new Informer({
                    text: data.message,
                    type: 'error'
                }).show();
                this.clearWaitButtons();
                throw new Error('Статус-ответ ERROR');
            }else if(this.showAlert === 1){
                new Informer({
                    text: data.message,
                    type: data.status
                }).show();
            }

            //если status = TEST, то попадаем в ветку тестирования html
            if(status === 'test' && data.hasOwnProperty('html')){
                const context = this.getContext();
                for(let key in context){
                    const target = document.querySelectorAll(key);
                    target.forEach((t)=>{
                        t.outerHTML = data.html;
                    });
                }
                this.clearWaitButtons();
                return false;
            }

            if(fd.formEl){
                fd.formEl.reset();

                //если есть dropzone
                if(typeof TmpFiles !== 'undefined' && typeof dropzone !== 'undefined'){
                    TmpFiles = [];
                    dropzone.removeAllFiles();
                }
            }

            if(data.hasOwnProperty('html')){
                //если в html есть объект с контекстами, то прогоняем в цикле
                //иначе в html располагается уже готовая html-структура -- контекст берется изначальный у элемента
                if(data.html['try_redirect'] !== undefined && data.html['try_redirect'] !== ''){
                    window.location = data.html['try_redirect'];
                }
                if(data.html instanceof Object){
                    for(let key in data.html){
                        const target = document.querySelectorAll(key);
                        const newHTML = data.html[key];
                        target.forEach((t)=>{
                            t.outerHTML = newHTML;
                        });
                    }
                }else {
                    const context = this.getContext();
                    for(let key in context){
                        const target = document.querySelectorAll(key);
                        target.forEach((t)=>{
                            t.outerHTML = data.html;
                        });
                    }
                }
            }

            if(data.hasOwnProperty('trigger')){
                this.triggerSkynet(data.trigger);
            }
            if(data.hasOwnProperty('js_func')){
                data['js_func'].forEach( func => {
                    this.triggerFunction(func.toString())
                });
            }
            if(btn.dataset.cb !== undefined){
                this.triggerCallbackFunctions(btn.dataset.cb, data);
            }

            if(window.$ !== undefined && window.$.fancybox !== undefined){
                //если открыто модальное окно от fancybox (v3), то закрываем его
                // window.jQuery.fancybox.close();
            }
            this.clearWaitButtons(btn);

            if(typeof initPhoneMask === 'function') {
                initPhoneMask();
            }

            if(typeof extendFunctionsForSkyNet === 'function'){
                extendFunctionsForSkyNet.call(btn,data);
            }

            if(data.hasOwnProperty('url')){
                window.location = data.url;
            }

            if(callback) {
                callback(data);
            }
        });
    }

    clearWaitButtons(btn){
        if(btn){
            btn.classList.remove('is-wait');
            btn.classList.remove('is-skynet-wait');
            btn.disabled = false;
            const label = btn.closest('label');
            if(label){
                label.classList.remove('is-skynet-wait');
            }
        }else {
            document.querySelectorAll('.is-wait').forEach((el)=>{
                el.classList.remove('is-wait');
                el.classList.remove('is-skynet-wait');
                el.disabled = false;
                const label = el.closest('label');
                if(label){
                    label.classList.remove('is-skynet-wait');
                }
            });
        }
    }

    appendContentToModal(btn){
        this.send(this.getData(), (data)=>{
            const that = this;
            if(data.html.hasOwnProperty('modal')){
                if($?.fancybox !== undefined){
                    $.fancybox.open({
                        src: data.html.modal,
                        type: 'inline',
                        opts: {
                            afterLoad: function(){
                                const modal = this.$content[0];
                                modal.style.maxWidth = (that.element.dataset.width !== undefined) ? that.element.dataset.width : '100%';

                                if(typeof window['skyModalAfterInit'] === 'function'){
                                    skyModalAfterInit(modal);
                                }
                            }
                        }
                    });
                }
            }else {
                new Informer({
                    text: 'Для sky-modal нужно, чтобы контент размещался в ключе <br>{"html":{"modal":"CONTENT"}}',
                    type: 'error'
                }).show();
            }
        }, btn);
    }

    triggerSkynet(array, callback){
        for(let i=0; i<array.length; i++){
            const el = document.querySelector(array[i]);
            if(el){
                this.element = el;
                this.send(this.getData(), callback, el);
            }
        }
    }
    triggerFunction(str){
        if (str) {
            const callbackArrayFunc = str.split(';');
            for (let i = 0; i < callbackArrayFunc.length; i++) {
                // const pattern = /\[(.+?)\]/;
                const pattern = new RegExp(/\[(.+)\]/);
                let fn = callbackArrayFunc[i];
                let args = pattern.exec(fn);
                if (args !== null) {
                    fn = fn.replace(args[0], '');
                    args = args[1].split(',');
                }
                console.log('triggerFunction', typeof window[fn], fn);
                if (typeof window[fn] == 'function') {
                    const func = window[fn];
                    if (args) {
                        func.apply(null, args);
                    } else {
                        window[fn]();
                    }
                }
            }
        }else {
            console.info('callback функции не заданы');
        }
    }

    triggerSelector(selector, callback){
        selector = selector ?? null;
        //(typeof selector === 'string') ? document.querySelector(selector) : (typeof selector === 'object') ? selector : null;
        if(selector){
            if(typeof selector === 'object'){
                this.element = selector;
                this.element.classList.add('is-wait');
                this.element.classList.add('is-skynet-wait');
                this.showAlert = (this.element.dataset.hasOwnProperty('show_message')) ? Number(this.element.dataset.show_message) : 0;
                this.send(this.getData(), callback, this.element);
            }else if(typeof selector === 'string'){
                const elements = document.querySelectorAll(selector);
                elements.forEach(el => {
                    this.element = el;
                    this.element.classList.add('is-wait');
                    this.element.classList.add('is-skynet-wait');
                    this.showAlert = (this.element.dataset.hasOwnProperty('show_message')) ? Number(this.element.dataset.show_message) : 0;
                    this.send(this.getData(), callback, this.element);
                });
            }
        }else {
            console.info('selector отстутствует');
        }
    }

    triggerCallbackFunctions(str, data){
        if (str) {
            const callbackArrayFunc = str.split(';');
            for (let i = 0; i < callbackArrayFunc.length; i++) {
                let fn = callbackArrayFunc[i];
                if (typeof window[fn] == 'function') {
                    const func = window[fn];
                    func.call(data);
                }
            }
        }else {
            console.info('callback функции не заданы');
        }
    }

    extend(custom, defaults) {
        for (const key in defaults) {
            if (custom[key] == null) {
                custom[key] = defaults[key];
            }
        }
        return custom;
    }
}
const informerDefault = {
    delay: 5000,
    animationTime: 400,
    closeTime: 200,
    autoClose: true,
    // position: 2, //1 - left top, 2 - right top, 3 - bottom right, 4- left bottom
    class: 'msgBox',
    wrapperClass: 'msgBox-wrapper',
    text: 'default text',
    type: 'info' //error, success (ok), info, warn
};

class Informer {
    constructor(options = {}){
        this.options = this.extend(options, informerDefault);
        this.informer = null;
        this.wrap = null;
        this.timeId = null;
        this.render();
        this.init();
    }

    init() {
        this.clickHandler = this.clickHandler.bind(this);
        this.informer.addEventListener('click', this.clickHandler);
    }

    render(){
        this.wrap = document.querySelector(`.${this.options.wrapperClass}`);
        if (!this.wrap) {
            this.wrap = this.create('div');
            this.wrap.className = this.options.wrapperClass;
            this.wrap.style.position = 'fixed';
            this.wrap.style.zIndex = '9999999';
            // this.wrap.dataset.position = this.options.position;
            this.wrap.style.top = '10px';
            this.wrap.style.right = '20px';

            // switch(this.options.position){
            // 	case '1':
            // 		this.wrap.style.top = '10px';
            // 		this.wrap.style.left = '20px';
            // 		break;
            // 	case '2':
            // 		this.wrap.style.top = '10px';
            // 		this.wrap.style.right = '20px';
            // 		break;
            // 	case '3':
            // 		this.wrap.style.bottom = '10px';
            // 		this.wrap.style.right = '20px';
            // 		break;
            // 	case '4':
            // 		this.wrap.style.bottom = '10px';
            // 		this.wrap.style.left = '20px';
            // 		break;
            // 	default:
            // 		this.wrap.style.top = '10px';
            // 		this.wrap.style.right = '20px';
            // 		break;
            // }

            document.body.appendChild(this.wrap);

            this.addStyles(`
				.${this.options.class} {
					box-sizing: border-box;
					position: relative;
					display: flex;
					align-items: center;
					width: 400px;
					height: auto;
					background: #fff;
					padding: 15px;
					color: var(--msgBox-color);
					border-left: 2px solid var(--msgBox-color);
					box-shadow: 5px 5px 10px rgba(0,0,0,.1);
					border-radius: 4px;
					margin: 15px 0;
					user-select: none;
					opacity: 0;
					overflow: hidden;
				}
				.${this.options.class}.is-show {
					animation: msgBox-animate ${this.options.animationTime}ms cubic-bezier(0.39, 0.58, 0.57, 1);
					opacity: 1;
				}
				.${this.options.class}.is-hide {
					pointer-events:none;
					animation: msgBox-animate-hide ${this.options.closeTime}ms ease-in;
					opacity: 0;
				}
				.${this.options.class}__icon {
					margin-right: 15px;
					display: block;
					width: 32px;
					height: 32px;
					flex-shrink: 0;
					box-sizing: border-box;
					color: var(--msgBox-color);
					fill: currentColor;
				}
				.${this.options.class}__text {font-size:16px;line-height:1.4;font-weight:400;font-family:inherit;color:#000;}
				.${this.options.class}__close {
					position: absolute;
					top: 0;
					right: 0;
					width: 24px;
					height: 24px;
					outline: 0;
					cursor: pointer;
					border: 0;
					background: transparent;
					color: #000;
					padding: 0;
					box-sizing: border-box;
					opacity: .5;
				}
				@keyframes msgBox-animate {
					0% {
						transform: translateX(15px);
						opacity: 0;
					}
					100% {
						transform: translateY(0);
						opacity: 1;
					}
				}
				@keyframes msgBox-animate-hide {
					0% {
						transform: translateY(0);
						opacity: 1;
					}
					100% {
						transform: translateX(15px);
						opacity: 0;
					}
				}
				@media (max-width:500px){
				    .${this.options.class}{
				        width: 290px;
				    }
				    
				}
			`);
        }

        this.informer = this.create('div');
        switch (this.options.type.toString().toLowerCase()){
            case 'error':
                this.informer.style.setProperty('--msgBox-color', '#ff0000');
                break;
            case 'ok':
            case 'success':
                this.informer.style.setProperty('--msgBox-color', '#14bd00');
                break;
            case 'warn':
                this.informer.style.setProperty('--msgBox-color', '#ff9900');
                break;
            default:
                this.informer.style.setProperty('--msgBox-color', '#0095ff');
                break;
        }

        this.informer.className = this.options.class;
        const text = this.options.text.toString().replace(/\n/g, "<br />");
        this.informer.innerHTML = `
			<div class="${this.options.class}__icon">${this.icon(this.options.type)}</div>
			<div class="${this.options.class}__text">${text}</div>
			<button type="button" class="${this.options.class}__close" data-informer="close">
				<svg viewBox="0 0 32 32" style="fill:transparent;stroke:currentColor;stroke-width:1;"><path d="M10,10 L22,22 M22,10 L10,22"></path></svg>
			</button>
		`;

        this.wrap.appendChild(this.informer);
    }

    show(){
        this.informer.classList.add('is-show');
        if (this.options.autoClose) {
            this.timeId = setTimeout(() => {
                this.close();
            }, this.options.delay);
        }
    }
    close(){
        this.informer.classList.remove('is-show');
        this.informer.classList.add('is-hide');
        setTimeout(()=>{
            clearTimeout(this.timeId);
            this.timeId = null;
            this.destroy();
        }, this.options.closeTime);
    }
    destroy() {
        this.informer.removeEventListener('click', this.clickHandler);
        this.informer.innerHTML = '';
        this.informer.parentNode.removeChild(this.informer);
    }

    clickHandler(event){
        const { target } = event;
        const { informer } = target.dataset;
        let element;
        if (informer !== undefined) {
            element = target;
        } else {
            element = target.closest("[data-informer]");
        }
        if (element){
            switch (element.dataset.informer){
                case 'close':
                    this.close();
                    break;
            }
        }
    }

    icon(type){
        let html;
        switch(type.toString().toLowerCase()){
            case 'error':
                html = `<svg viewBox="0 0 512 512"><path d="M256 0C114.848 0 0 114.848 0 256s114.848 256 256 256 256-114.848 256-256S397.152 0 256 0zm0 480C132.48 480 32 379.52 32 256S132.48 32 256 32s224 100.48 224 224-100.48 224-224 224z"/><path d="M363.312 171.312l-22.624-22.624L256 233.376l-84.688-84.688-22.624 22.624L233.376 256l-84.688 84.688 22.624 22.624L256 278.624l84.688 84.688 22.624-22.624L278.624 256z"/></svg>`;
                break;
            case 'success':
                html = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="checkmark-circle-2"><rect width="24" height="24" opacity="0"/><path d="M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z"/><path d="M14.7 8.39l-3.78 5-1.63-2.11a1 1 0 0 0-1.58 1.23l2.43 3.11a1 1 0 0 0 .79.38 1 1 0 0 0 .79-.39l4.57-6a1 1 0 1 0-1.6-1.22z"/></g></g></svg>`;
                break;
            default:
                html = `<svg viewBox="0 0 512 512"><path d="M256 0C114.848 0 0 114.848 0 256s114.848 256 256 256 256-114.848 256-256S397.152 0 256 0zm0 480C132.48 480 32 379.52 32 256S132.48 32 256 32s224 100.48 224 224-100.48 224-224 224z"/><path d="M240 112h32v224h-32zM240 368h32v32h-32z"/></svg>`;
                break;
        }
        return html;
    }

    create(tag){
        return document.createElement(tag);
    }
    addStyles(styles){
        const style = document.createElement('style');
        document.head.append(style);
        style.innerHTML = styles;
    }

    extend(custom, defaults) {
        for (const key in defaults) {
            if (custom[key] == null) {
                custom[key] = defaults[key];
            }
        }
        return custom;
    }
}

window.sky = new Skynet();