<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Core_Xml_Entity extends Core_Xml_Entity
{
	/**
	 * @return array
	 */
	public function getEntities()
	{
		return $this->_childrenEntities;
	}
}