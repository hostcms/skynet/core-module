<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 14.06.2017
 * Time: 12:31
 */
use Skynetcore_Utils as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Core_Servant_Properties extends Core_Servant_Properties {

	protected $_tagName = 'propertytag';


	public function getAllowedProperties() {
		return $this->_allowedProperties;
	}

	public function toArray() {
		$returnes = array();
		foreach ($this->_allowedProperties as $allowedPropertyKey => $allowedProperty) {
			$returnes[$allowedPropertyKey] = $this->$allowedPropertyKey;
		}

		return json_decode(json_encode($returnes), true);
	}

	public function getXml() {
		Core_Event::notify(get_class($this) . '.onBeforeGetXml', $this);

		$xmlTranslator = Skynetcore_Convert_Array_Xml_Entity::createInstance($this->toArray(), '', $this->_tagName);
		Core_Event::notify(get_class($this) . '.onAfterGetXml', $this);
		return $xmlTranslator->getXml();
	}
}