<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 24.01.2017
 * Time: 12:22
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Core_Controller extends Core_Controller {

	public function get()
	{
		$return = parent::get();
		$return = Shortcode_Controller::instance()->applyShortcodes($return);

		return $return;
	}
}