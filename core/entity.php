<?php
use Utils_Utl as utl;
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 24.01.2017
 * Time: 12:22
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 *
 */
class Skynetcore_Core_Entity extends Core_Entity {

	public static function processEntityFields($aEntities, $allowedFields=['none']) {
		$aEntitiesTmp = $aEntities;
		if(!is_array($aEntities)) {
			$aEntitiesTmp = [$aEntities];
		}
		$aGroupTableColumns = [];
		if(count($aEntitiesTmp)) {
			$aGroupTableColumns = array_diff(array_keys($aEntitiesTmp[0]->getTableColums()), $allowedFields);
		}
		/** @var Shop_Group_Model $oShopGroupTmp */
		foreach ($aEntitiesTmp as $oEntityTmp) {
			$oEntityTmp->addForbiddenTags($aGroupTableColumns);
		}
//		foreach ($allowedFields as $allowedField) {
//
//		}
//		$oShopGroupTmp->removeForbiddenTag('active');
//		$oShopGroupTmp->removeForbiddenTag('name');

//		$oShopGroup->addEntity(
//			Core::factory('Core_Xml_Entity')
//				->name('subgroups')
//				->addEntities($aSubGroups)
//		);
		return $aEntitiesTmp;
	}

	public function save()
	{
		if(isset($this->update_time)) {
			$this->update_time = Core_Date::timestamp2sql(time());
		}

		return parent::save();
	}
}