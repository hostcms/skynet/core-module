<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 10.03.2017
 * Time: 8:36
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Convert_Array_Xml_Entity {
	protected $_arConverts;
	protected $_rootPath;
	public $name = '';

	function __construct($inArray = array(), $key='', $rootPath = 'newrecords', $attrkey = '') {
		$this->_rootPath = $rootPath;
		if (is_null($inArray)) {
			$inArray = [];
		}
		foreach ($inArray as &$inArrayValue) {
			if(is_null($inArrayValue)) $inArrayValue = '';
		}

		if($key != '') {
			if($attrkey != '') {
				foreach ($inArray as $arKey=>$arVal){
					if(is_array($arVal)) {
						$inArray[$arKey]['@attributes'] = array($attrkey => Core_Array::get($arVal, $attrkey, ''));
					}
				}
			}
			$this->_arConverts[$key] = $inArray;
		} else {
			$this->_arConverts = $inArray;
		}
		$this->name = $key;
	}

	public static function xml_attribute($object, $attribute)
	{
		if(isset($object[$attribute]))
			return (string) $object[$attribute];
	}

	public static function createInstance($inArray = array(), $key='', $rootPath = 'newrecords', $attrkey = '')
	{
		return new self($inArray, $key, $rootPath, $attrkey);
	}

	/**
	 * Build entity XML
	 *
	 * @return string
	 */
	public function getXml()
	{
		try {
			$arrayToXml = new \Spatie\ArrayToXml\ArrayToXml(
				$this->_arConverts,
				$this->_rootPath,
				false,
				"UTF-8",
				"1.0",
				[]
			);
			$xml = preg_replace("/<\?xml version=\"1.0\"( encoding=\"UTF-8\")?\?>/ui", "", $arrayToXml->prettify()->toXml());
		} catch (\Exception $e) {
			$xml = "<{$this->_rootPath}>\n	<error>{$e->getMessage()}</error>\n</{$this->_rootPath}>\n";
		}

		return $xml;
	}

	/**
	 * Build entity XML
	 *
	 * @return string
	 */
	public function getStdObject()
	{
		$name = (isset($this->name) && $this->name != '') ? $this->name : 'undefined';
		if(isset($this->_arConverts[$name])) {
			return $this->_arConverts[$name];
		} else {
			return [];
		}
	}

	/**
	 * @return string
	 */
	public function getRootPath(): string
	{
		return $this->_rootPath;
	}

	/**
	 * @return array
	 */
	public function getResult(): array
	{
		return $this->_arConverts;
	}
}