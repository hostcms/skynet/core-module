<?php

class Skynetcore_Form_Controller_Show extends Form_Controller_Show
{
	public function process()
	{
		parent::process();

		if(Core_Array::getRequest('usersend', false) !== false
			&& ($userEmail=Core_Array::getPost($this->mailFromFieldName, false)) !== false
		) {
			$adminEmail = Core_Array::get($this->_aEmails, 1, Core_Array::get($this->_aEmails, 0, false));
			if($adminEmail !== false) {
				$localValues = $this->values;
				$localValues[$this->mailFromFieldName] = $adminEmail;
				$this->values = $localValues;

				$this
					->clearEmails()
					->addEmail($userEmail)
					->sendEmail()
				;
			}
//			Skynetcore_Utils::v($this->getError(), 'errors');
//					$Form_Controller_Show->addEmail($userEmail);
		}
	}

}