<?php

use MoySklad\MoySklad;
use Skynetcore_Utils as utl;
//use Eseath\SxGeo as sx;

/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Observer_User
{
	static public function onBeforeGetXml($object, $args)
	{
		$object
			->addXmlTag('fullname', trim("{$object->surname} {$object->name} {$object->patronymic}"));
	}
}