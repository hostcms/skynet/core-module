<?php

use Skynetcore_Utils as utl;
use Astartsky\SypexGeo as sx;

/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Observer_Shop {

	static function onBeforeShowBody($oCore_Response, $args)
	{
		$body = $oCore_Response->getBody();
		$instance = Core_Page::instance();
		if(isset($instance->skynet)) {
			$aLdScheme = [];
			// Регулярное выражение для поиска img тегов с классом "target-class" и получения их src атрибута
			// $pattern = '/<img[^>]*class=["\'][^"\']*add_ld_scheme[^"\']*["\'][^>]*src=["\']([^"\']*)["\'][^>]*>/i';
			$pattern = '/<img[^>]*class=["\'][^"\']*add_ld_scheme[^"\']*["\'][^>]*src=["\']([^"\']*)["\'][^>]*alt=["\']([^"\']*)["\'][^>]*title=["\']([^"\']*)["\'][^>]*>/ui';
//Skynetcore_Utils::tp(htmlspecialchars($body));
			if (preg_match_all($pattern, (string) $body, $matches)) {
				// $matches[1] содержит src, $matches[2] содержит alt, $matches[3] содержит title
				$images = [];
				for ($i = 0; $i < count($matches[1]); $i++) {
					$images[] = [
						'src' => $matches[1][$i],
						'alt' => $matches[2][$i],
						'title' => $matches[3][$i],
					];
				}
				$aImagesLdScheme = [];
				foreach ($images as $image) {
					$aImagesLdScheme[] = [
						'@context' => 'http://schema.org',
						'@type' => "ImageObject",
						'contentUrl' => $image['src'],
						'url' => $image['src'],
//						'caption ' => $image['alt'],
//						'description ' => $image['title'],
						'name' => $image['title'],
//							'description' => $image['title'],
						'thumbnail' => str_replace('1920x1080', '460x460', $image['src']),
					];
				}
				if (count($aImagesLdScheme)) {
					$h1text = $pageDescription = '';
					$patternH1 = '/<h1[^>]*>(.*?)<\/h1>/i';
					if (preg_match_all($patternH1, $body, $matches)) {
						// $matches[1] содержит содержимое всех найденных тегов <h1>
						foreach ($matches[1] as $content) {
							$h1text = $content;
							break;
						}
					}

					$patternDescription = '/<([a-z1-6]+)[^>]*class=["\'][^"\']*add_ld_scheme_description[^"\']*["\'][^>]*>(.*?)<\/\1>/is';
					if (preg_match_all($patternDescription, $body, $matches)) {
						foreach ($matches[2] as $pageDescription) {
							if($pageDescription != '') break;
						}
					}

					$aLdScheme = [
						'@context' => 'https://schema.org',
						'@type' => 'ImageGallery',
						'name' => $h1text,
						'description' => strip_tags(html_entity_decode($pageDescription)),
						'url' => 'https://'.$instance->skynet->request->domain.$instance->skynet->request->pathNoPage,
						'image' => $aImagesLdScheme,
					];
				}
				if(count($aLdScheme)) {
					$body = preg_replace('/' . preg_quote('<script type="application/ld+json">', '/') . '/', '<script type="application/ld+json">'.
						json_encode($aLdScheme, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE).
						'</script>'."\n".'<script type="application/ld+json">', $body, 1);
//						$body = str_replace('<script type="application/ld+json">', '<script type="application/ld+json">'.
//							json_encode($aLdScheme, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE).
//						'</script><script type="application/ld+json">', $body);
				}
			} else {
//					echo "No matching images found.";
			}
		}
		$oCore_Response->changeBody($body);
	}
}