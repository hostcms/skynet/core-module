<?php

use MoySklad\MoySklad;
use Skynetcore_Utils as utl;
// use Eseath\SxGeo as sx;

/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Observer_Core {
	static $_utilsAllowedProperties = array(
		'skynet' => false,
		'extclass' => '',
		'change_template' => false,
	);
	static $_arBotIgnore =
		[
			'YandexBot/3.0',
			'YandexImages/3.0',
			'YandexMetrika/2.0',
			'Googlebot/2.1',
			'YaDirectFetcher/1.0',
			'Googlebot-Image'
		];

	static public function botStatusIgnore($userAgent = ''){
		$status = true;
		foreach (self::$_arBotIgnore as $bot){
			$pos = strpos($userAgent, $bot);
			if($pos) {
				$status = false;
				break;
			}
		}
		return $status;
	}

	static public function onBeforeInitConstants($object, $args) {
		if ((Core::$url['path'] == '/edit-in-place.php'
				|| Core::$url['path'] == '/robots.txt'
				|| Core::$url['path'] == '/users/administration/items-are-added/'
				|| Core_Array::getRequest('action', '') != ''
				|| preg_match('/sitemap.xml/', Core::$url['path'])==1
			) && Core_Array::getRequest('debug', '') == ''
		) {
			if(!defined('ALLOW_PANEL')) define('ALLOW_PANEL', FALSE);
		}
	}

	static public function onAfterInitConstants($object, $args) {
		$instance = Core_Page::instance();
		$instance
			->fileTimestamp(true);

		$instance->addAllowedProperties(array_keys(self::$_utilsAllowedProperties));
		foreach (self::$_utilsAllowedProperties as $projectAllowedProperty => $projectAllowedPropertyValue) {
			$instance->$projectAllowedProperty = $projectAllowedPropertyValue;
		}
		$skynet = new stdClass();

		$skynet->config = [];
		if(defined('CURRENT_SITE') && isset(Core_Config::instance()->get('site_config')[CURRENT_SITE])) {
			$skynet->config = Core_Config::instance()->get('site_config')[CURRENT_SITE];
		}
		if(!isset($skynet->config['settings']['do_not_use_sessions']) ||
			(isset($skynet->config['settings']['do_not_use_sessions'])
				&& $skynet->config['settings']['do_not_use_sessions']!==true
			)
		) {
			Core_Session::start();
		}

		$request = new Skynetcore_Entity_Request();
		$response = new Skynetcore_Entity_Response();

		$request->test = Core_Array::getRequest('test', false) !== false;
		if(isset($_SERVER['REQUEST_URI']) && isset($_SERVER['HTTP_HOST'])) {
			$requestMathches = array();
			preg_match('/([^?]*)\??(.*)/', strtolower($_SERVER['REQUEST_URI']), $requestMathches);
			$hostPort = explode(':', $_SERVER['HTTP_HOST']);
			$splitedPath = array_filter(explode('/', Core_Array::get($requestMathches, 1, '')));

			$pathPattern = '/\/page-([1-9]\d*)\/$/i';
			$pathSearch = Core_Array::get($requestMathches, 1, '');
			$pathNoPage = preg_replace($pathPattern, '/', $pathSearch);
			$pathMatches = array();
			preg_match($pathPattern, $pathSearch, $pathMatches);
			$numPage = ((isset($pathMatches[1]) ? $pathMatches[1] : 1)*1)-1;

			$request->remoteip = Core_Array::get($_SERVER, 'HTTP_X_FORWARDED_FOR', Core_Array::get($_SERVER, 'REMOTE_ADDR', '0.0.0.0'));
			$request->proto = Core_Array::get($_SERVER, 'HTTP_X_FORWARDED_PROTO', 'http');
//			$request->domain = $hostPort[0];
			$request->domain = preg_replace('/^www\./ui', '', Core_Array::get($hostPort, 0, ''));
			$request->port = Core_Array::get($hostPort, 1, '80');
			$request->path = Core_Array::get($requestMathches, 1, '');

			$request->pathNoPage = $pathNoPage;
			$request->pathdeep = count($splitedPath);
			$request->aSplitedPath = array_values($splitedPath);
			$request->jSplitedPath = json_encode(array_values($splitedPath));
			$request->crc32path = crc32(Core_Array::get($requestMathches, 1, ''));
			$request->crc32pathNoPage = crc32($pathNoPage);
			$request->isMain = (crc32($pathNoPage)==2043925204);
			$request->query = Core_Array::get($requestMathches, 2, '');
			$request->currentPage = $numPage;
			$request->utm = array(
				'source' => Core_Array::getGet('utm_source', Core_Array::get($_COOKIE, 'hostcms_source_service', '')),
				'medium' => Core_Array::getGet('utm_medium', Core_Array::get($_COOKIE, 'hostcms_source_medium', '')),
				'campaign' => Core_Array::getGet('utm_campaign', Core_Array::get($_COOKIE, 'hostcms_source_campaign', '')),
				'content' => Core_Array::getGet('utm_content', Core_Array::get($_COOKIE, 'hostcms_source_content', '')),
				'term' => Core_Array::getGet('utm_term', Core_Array::get($_COOKIE, 'hostcms_source_term', '')),
			);
			$browser = new Skynetcore_Browser_Info();
			$request->browser = $browser;
		}

		//-- Получаем город по geoip --
		$request->geoip = false;
		$ip = Core_Array::get($_SERVER, 'HTTP_X_REAL_IP',
			Core_Array::get($_SERVER, 'REMOTE_ADDR', '0.0.0.0')
		);
//		Skynetcore_Utils::tp($_SERVER);
//		$ip = "178.219.186.12";    //-- Москва
//		$ip = "212.34.48.15";      //-- Мытищи
//		$ip = "95.64.155.225";     //-- Балашиха
//		$ip = "109.110.68.10";     //-- Севастополь
//		$ip = "109.195.178.10";    //-- Тула
//		$ip = "176.50.86.59";      //-- Барнаул
//		$ip = "103.111.62.255";
//		$ip = "91.250.55.12";      //-- Украина
//		$ip = "192.168.1.20";      //-- Херь какая-то
//		$ip = "5.255.231.61";      //-- Херь какая-то
//		$ip = "95.108.213.162";    //-- Херь какая-то

		$cookieGeoLocation = Core_Array::get($_COOKIE, 'skynet_geolocation', false);
		if(    defined('CURRENT_SITE')
			&& $cookieGeoLocation === false
			&& isset($skynet->config['integration']['dadata']['cache_type'])
			&& $skynet->config['integration']['dadata']['cache_type'] === 'db'
		) {
			/** @var Skynetcore_Cache_Ip_Location_Model $qCacheIpLocations */
			$qCacheIpLocations = Core_Entity::factory('Skynetcore_Cache_Ip_Location');
			$qCacheIpLocations
				->queryBuilder()
				->where('site_id', CURRENT_SITE)
				->where('type', 'dadata');
			$oCacheIpLocation = $qCacheIpLocations->getByIp($ip, false);
			$oCurrentCacheIpLocation = null;
			$checkDate = time();
			if(!is_null($oCacheIpLocation)) {
				if (!($checkDate >= Core_Date::sql2timestamp($oCacheIpLocation->from)
					&& $checkDate <= Core_Date::sql2timestamp($oCacheIpLocation->to))) {
					$oCacheIpLocation->delete();
				} else {
					$oTmpCacheIpLocation = json_decode(base64_decode($oCacheIpLocation->location));
					if(json_last_error() === JSON_ERROR_NONE) {
						$cookieGeoLocation = json_encode($oTmpCacheIpLocation);
					}
				}
			}
		}
//		Skynetcore_Utils::v(self::botStatusIgnore($_SERVER['HTTP_USER_AGENT']));
		if(    $cookieGeoLocation === false
			&& isset($_SERVER['HTTP_USER_AGENT'])
			&& self::botStatusIgnore($_SERVER['HTTP_USER_AGENT']) === true
		) {
			try {
				if(isset($skynet->config['integration']['dadata']['use_geoip'])
					&& $skynet->config['integration']['dadata']['use_geoip'] === true
				) {
					Core_Event::notify('Skynetcore_Observer_Core.onBeforeDadataGet', NULL, []);
					$result = 'none';
					if (Core::moduleIsActive('cache'))
					{
						/** @var Cache_File $oCore_Cache */
						$oCore_Cache = Core_Cache::instance(Core::$mainConfig['defaultCache']);
						$cacheKey = "dadata-ip={$ip}";
						$inCache = $oCore_Cache->check($cacheKey, "default");
						if($inCache) {
							$result = json_decode($oCore_Cache->get($cacheKey), true);
							if(json_last_error() != JSON_ERROR_NONE) {
								$result = 'none';
							}
						}
					}
					if($result === 'none') {
						$token = $skynet->config['integration']['dadata']['key'];
						$dadata = new \Dadata\DadataClient($token, null);
						try {
							$result = $dadata->iplocate($ip);
						} catch (\Exception $e) {
//							Skynetcore_Utils::p($e->getMessage(), $result);
//							die();
						}
						if ($result !== 'none' && Core::moduleIsActive('cache')) {
							$oCore_Cache->set($cacheKey, json_encode($result));
						}
//						if(    defined('CURRENT_SITE')
//							&& $result !== 'none'
//							&& isset($skynet->config['integration']['dadata']['cache_type'])
//							&& $skynet->config['integration']['dadata']['cache_type'] === 'db'
//						) {
////							/** @var Skynetcore_Cache_Ip_Location_Model $qCacheIpLocations */
////							$qCacheIpLocations = Core_Entity::factory('Shop_Country_Location_City');
////							$qCacheIpLocations
////								->queryBuilder()
////								->where('site_id', CURRENT_SITE)
////								->where('type', 'dadata')
////							;
////							$aCacheIpLocations = $qCacheIpLocations->getAllByIp($ip, false);
////							$oCurrentCacheIpLocation = null;
////							$checkDate = strtotime('now');
////							foreach ($aCacheIpLocations as $oCacheIpLocation) {
////								if (!($checkDate >= $oCacheIpLocation->from && $checkDate <= $oCacheIpLocation->to)) {
////									$oCacheIpLocation->delete();
////								} else {
////									$oCurrentCacheIpLocation = $oCacheIpLocation;
////								}
////							}
////							if(is_null($oCurrentCacheIpLocation)) {
////								/** @var Skynetcore_Cache_Ip_Location_Model $oCurrentCacheIpLocation */
////								$oCurrentCacheIpLocation = Core_Entity::factory('Shop_Country_Location_City');
////								$oCurrentCacheIpLocation->site_id = CURRENT_SITE;
////								$oCurrentCacheIpLocation->type = 'dadata';
////								$oCurrentCacheIpLocation->ip = $ip;
////								$oCurrentCacheIpLocation->to = Core_Date::timestamp2sql(strtotime('now + 1 month'));
////								$oCurrentCacheIpLocation->location = base64_encode($result);
////								$oCurrentCacheIpLocation->save();
////							}
////							Skynetcore_Utils::p($oCurrentCacheIpLocation->toArray());
//						}
					}
					Core_Event::notify('Skynetcore_Observer_Core.onAfterDadataGet', NULL, [$ip, $result]);
					if(isset($result['data']['city'])
						&& ($city = $result['data']['city']) != ''
					) {
						/** @var  $oHostResult Shop_Country_Location_City_Model */
						$oHostResultCity = Core_Entity::factory('Shop_Country_Location_City')->getAllByName($city, false);
						if(isset($oHostResultCity[0])
							&& $oHostResultCity[0] instanceof Shop_Country_Location_City_Model) {
							if(count($oHostResultCity) > 1) {
								//-- TODO: Доработать одинаковые названия городов --
								$oHostResultCity = $oHostResultCity[0];
							} else {
								$oHostResultCity = $oHostResultCity[0];
							}
							$request->geoip = Array
							(
								'city' => Array (
									'id' => $oHostResultCity->id,
									'lat' => $result['data']['geo_lat'],
									'lon' => $result['data']['geo_lon'],
									'name_ru' => $oHostResultCity->name,
									'name_en' => $oHostResultCity->name_en,
								),
								'region' => Array (
									'id' => $oHostResultCity->shop_country_location->id,
									'name_ru' => $oHostResultCity->shop_country_location->name,
									'name_en' => $oHostResultCity->shop_country_location->name_en,
									'iso' => ''
								),
								'country' => Array (
									'id' => $oHostResultCity->shop_country_location->shop_country->id,
									'iso' => $oHostResultCity->shop_country_location->shop_country->alpha2,
									'lat' => 0,
									'lon' => 0,
									'name_ru' => $oHostResultCity->shop_country_location->shop_country->name,
									'name_en' => $oHostResultCity->shop_country_location->shop_country->name_en,
								)
							);
						}
					}
				} else {
//					if(is_file(CMS_FOLDER.'modules/skynetcore/geo/sx/SxGeoCity.dat')) {
//						$SxGeo = new sx\SxGeo(CMS_FOLDER.'modules/skynetcore/geo/sx/SxGeoCity.dat');
//						$request->geoip = $SxGeo->getCityFull($ip);
//					}
				}
			} catch (\Exception $ex) {
				$request->geoip = false;
			}
//Skynetcore_Utils::tp($request->geoip, '1');
			if(    is_null($request->geoip)
				|| $request->geoip === false
			) {
				if(!isset($skynet->config['integration']['undefined_moscow']) || ($skynet->config['integration']['undefined_moscow']
						&& $skynet->config['integration']['undefined_moscow']==0)) {
					$request->geoip = Array
					(
						'city' => Array (
							'id' => 1,
							'lat' => 55.75222,
							'lon' => 37.61556,
							'name_ru' => 'Москва',
							'name_en' => 'Moscow',
						),
						'region' => Array (
							'id' => 1,
							'name_ru' => 'Московская область',
							'name_en' => "Moskovskaya oblast'",
							'iso' => 'RU-MOW'
						),
						'country' => Array (
							'id' => 185,
							'iso' => 'RU',
							'lat' => 60,
							'lon' => 100,
							'name_ru' => 'Россия',
							'name_en' => 'Russia',
						)
					);
				} else {
					$request->geoip = Array
					(
						'city' => Array (
							'id' => 0,
							'lat' => 0,
							'lon' => 0,
							'name_ru' => 'Undefined',
							'name_en' => 'Undefined',
						),
						'region' => Array (
							'id' => 0,
							'name_ru' => 'Undefined',
							'name_en' => 'Undefined',
							'iso' => 'XX-XXX'
						),
						'country' => Array (
							'id' => 0,
							'iso' => 'RU',
							'lat' => 0,
							'lon' => 0,
							'name_ru' => 'Undefined',
							'name_en' => 'Undefined',
						)
					);
				}
			}
			if(!isset($_COOKIE['skynet_city'])) {
				if (isset($request->geoip['city']['name_ru'])) {
					$city = $request->geoip['city']['name_ru'];
					if (isset($request->geoip['region']['name_ru'])) {
						$city .= ', ' . $request->geoip['region']['name_ru'];
					}
					if (isset($request->geoip['country']['name_ru'])) {
						$city .= ', ' . $request->geoip['country']['name_ru'];
					}

					Core::setCookie("skynet_geolocation", json_encode($request->geoip), strtotime( "+1 month"), '/');
					Core::setCookie("skynet_region", $request->geoip['region']['name_ru'], strtotime( "+1 month"), '/');
					Core::setCookie("skynet_city", $city, strtotime( "+1 month"), '/');
				}
			}

			if(    defined('CURRENT_SITE')
				&& isset($skynet->config['integration']['dadata']['cache_type'])
				&& $skynet->config['integration']['dadata']['cache_type'] === 'db'
			) {
//				$ip = "109.195.178.10";
				/** @var Skynetcore_Cache_Ip_Location_Model $qCacheIpLocations */
				$qCacheIpLocations = Core_Entity::factory('Skynetcore_Cache_Ip_Location');
				$qCacheIpLocations
					->queryBuilder()
					->where('site_id', CURRENT_SITE)
					->where('type', 'dadata')
				;
				$aCacheIpLocations = $qCacheIpLocations->getAllByIp($ip, false);
				$oCurrentCacheIpLocation = null;
				$checkDate = time();
				foreach ($aCacheIpLocations as $oCacheIpLocation) {
//					Skynetcore_Utils::p(Core_Date::timestamp2datetime($checkDate), 'now');
//					Skynetcore_Utils::p(Core_Date::timestamp2datetime(Core_Date::sql2timestamp($oCacheIpLocation->from)), 'from');
//					Skynetcore_Utils::p(Core_Date::timestamp2datetime(Core_Date::sql2timestamp($oCacheIpLocation->to)), 'to');
					if (!($checkDate >= Core_Date::sql2timestamp($oCacheIpLocation->from)
							&& $checkDate <= Core_Date::sql2timestamp($oCacheIpLocation->to))) {
						$oCacheIpLocation->delete();
					} else {
						$oCurrentCacheIpLocation = $oCacheIpLocation;
					}
				}
				if(is_null($oCurrentCacheIpLocation)) {
					/** @var Skynetcore_Cache_Ip_Location_Model $oCurrentCacheIpLocation */
					$oCurrentCacheIpLocation = Core_Entity::factory('Skynetcore_Cache_Ip_Location');
					$oCurrentCacheIpLocation->site_id = CURRENT_SITE;
					$oCurrentCacheIpLocation->type = 'dadata';
					$oCurrentCacheIpLocation->ip = $ip;
					$oCurrentCacheIpLocation->to = Core_Date::timestamp2sql(strtotime('now + 1 month'));
					$oCurrentCacheIpLocation->location = base64_encode(json_encode($request->geoip));
					$oCurrentCacheIpLocation->useragent = isset($request->browser->user_agent) ? $request->browser->user_agent : '-';
					$oCurrentCacheIpLocation->save();
				}
			}
		} else {
			$request->geoip = json_decode($cookieGeoLocation, true);
		}
		$geohostName = trim(''.implode(', ', [
			Core_Array::get($request->geoip, 'city', ['name_ru' => 'Undefined'])['name_ru'],
			Core_Array::get($request->geoip, 'region', ['name_ru' => 'Undefined'])['name_ru'],
			Core_Array::get($request->geoip, 'country', ['name_ru' => 'Undefined'])['name_ru'],
		]));
//		if(($gTmp=trim(Core_Array::getCookie('skynet_city', ''))) != '') {
//			$geohostName = $gTmp;
//		}
		$request->geohost = utl::getAddress($geohostName);

		$skynet->response = $response;
		$skynet->request = $request;
		$instance->skynet = $skynet;

		$instance->skynet->request->isProd = ((
			   is_array($instance->skynet->config)
			&& isset($instance->skynet->config['prod'])
			&& in_array($instance->skynet->request->domain, $instance->skynet->config['prod'])
		) || (
			defined('SKYNET_ENVIRONMENT_TYPE') && SKYNET_ENVIRONMENT_TYPE == 'prod'
		));

		$instance->skynet->request->isSeo = ((
			   is_array($instance->skynet->config)
			&& isset($instance->skynet->config['seodomain'])
			&& in_array($instance->skynet->request->domain, $instance->skynet->config['seodomain'])
		) || (
			defined('SKYNET_ENVIRONMENT_TYPE') && SKYNET_ENVIRONMENT_TYPE == 'seodomain'
		));
		$instance->skynet->request->isBeta = ((
			   is_array($instance->skynet->config)
			&& isset($instance->skynet->config['beta'])
			&& in_array($instance->skynet->request->domain, $instance->skynet->config['beta'])
		) || (
			defined('SKYNET_ENVIRONMENT_TYPE') && SKYNET_ENVIRONMENT_TYPE == 'beta'
		));
		$instance->skynet->request->isManagement = ((
			   is_array($instance->skynet->config)
			&& isset($instance->skynet->config['manage'])
			&& in_array($instance->skynet->request->domain, $instance->skynet->config['manage'])
		) || (
			defined('SKYNET_ENVIRONMENT_TYPE') && SKYNET_ENVIRONMENT_TYPE == 'prod'
		));
		$instance->skynet->request->isAdmin = preg_match('/^\/admin\/.*/ui', $instance->skynet->request->path.'') === 1;
		$instance->skynet->request->envType = $instance->skynet->request->isProd ? 'prod' : 'dev';
//		if($instance->skynet->request->envType == 'dev') {
//			ob_start();
//			print_r($_SERVER);
//			print_r($instance->skynet->request);
//			$to_log = ob_get_clean();
//			file_put_contents(CMS_FOLDER.'_log/'.$instance->skynet->request->envType.'/file_'.microtime(false).'.txt', $to_log);
//		}
		if (Core::moduleIsActive('siteuser')) {
			$instance->skynet->siteuser = Core_Entity::factory('Skynetcore_Siteuser')->getCurrent();
			if(is_null($instance->skynet->siteuser)) {
				$instance->skynet->siteuser = false;
				$instance->skynet->siteuser_groups = [];
			} else {
				/** @var Siteuser_Group_Model $oSiteuserGroups */
				$oSiteuserGroups = Core_Entity::factory('Siteuser_Group');
				$instance->skynet->siteuser_groups = utl::array_map_assoc(
					function($groupKey, $oGroup) {
						return [$oGroup->id, $oGroup];
					}, $oSiteuserGroups->getForSiteuser($instance->skynet->siteuser->id)
				);
			}
		}
		if(isset($instance->skynet->config['integration']['ms'])) {
			$oMs = new stdClass(['instance' => false]);
			$oMs->instance = MoySklad::getInstance(
				$instance->skynet->config['integration']['ms']['basic']['user'],
				$instance->skynet->config['integration']['ms']['basic']['pass']
			);
			$instance->skynet->ms = $oMs;
		}
		$instance->skynet->user = Core_Entity::factory('User')->getCurrent();
		is_null($instance->skynet->user) && $instance->skynet->user = false;

		$skynet->skynetcore_checker = Core_Array::get(Core_Array::get($skynet->config, 'skynetcore_checker', []), 'use', false);
		if($skynet->skynetcore_checker && $skynet->user !== false) {
			$workers = Core_Array::get(Core_Array::get($skynet->config, 'skynetcore_checker', []), 'workers', []);
			foreach ($workers as $workerEvent => $worker) {
				Core_Event::attach($workerEvent, array(Core_Array::get($worker, 0), Core_Array::get($worker, 1)));
			}
		}

		$instance->skynet->carts = $tmpCarts = $tmpCartTotals = [];
		if (Core::moduleIsActive('shop') && defined('CURRENT_SITE')) {
			$skyCarts = Skynetcore_Shop_Cart_Controller::getMuticart();
			$instance->skynet->carts = $skyCarts->tmpCarts;
			$instance->skynet->cartTotals = $skyCarts->tmpCartTotals;
		}

		$instance->skynet->favorites = [];
		if (Core::moduleIsActive('shop')) {
			$aShops = Core_Entity::factory('Shop')->findAll();
			$tmpFavorites = [];
			foreach ($aShops as $oShop) {
				$tmpShopFavorites = Skynetcore_Shop_Favorite_Controller::instance()->getAll($oShop);
				$cShopFavorites = new stdClass();
				$cShopFavorites->count = count($tmpShopFavorites);
				$cShopFavorites->nodes = Skynetcore_Utils::array_map_assoc(function($oKey, $oItem) {
					return [$oItem->shop_item_id, $oItem];
				}, $tmpShopFavorites);
				$tmpFavorites[$oShop->id] = $cShopFavorites;
			}
			$instance->skynet->favorites = $tmpFavorites;
		}

		$instance->skynet->compares = [];
		if (Core::moduleIsActive('shop')) {
			$aShops = Core_Entity::factory('Shop')->findAll();
			$tmpCompares = [];
			foreach ($aShops as $oShop) {
				$tmpShopCompares = Skynetcore_Shop_Compare_Controller::instance()->getAll($oShop);
				$cShopCompares = new stdClass();
				$cShopCompares->count = count($tmpShopCompares);
				$cShopCompares->nodes = Skynetcore_Utils::array_map_assoc(function($oKey, $oItem) {
					return [$oItem->shop_item_id, $oItem];
				}, $tmpShopCompares);
				$tmpCompares[$oShop->id] = $cShopCompares;
			}
			$instance->skynet->compares = $tmpCompares;
		}
		self::checkCanonical();

		$settingHisories = Core_Array::get(Core_Array::get($instance->skynet->config, 'settings', []), 'histories_log', [
			'warehouse_items' => false,
		]);
		if(Core_Array::get($settingHisories, 'warehouse_items', false)) {
			Core_Event::attach('shop_warehouse_item.onBeforeSave', array('Skynetcore_Observer_History_Shop_Warehouse_Item', 'onBeforeSave'));
		}
	}

	static public function onAfterLoadModuleList($object, $args) {
		require_once(CMS_FOLDER . 'vendor/autoload.php');
		require_once(CMS_FOLDER . 'vendor/skynet/installers/phptoolcase/PtcHm.php');

		if(isset(Core_Page::instance()->srcrequest->path)) {
			Core_Event::detach('Core_Command_Controller_Default.onBeforeShowAction', array('Hostcms_Redirect_Observer', 'onBeforeShowAction'));
			$foundedISitem = CUtil::getIsItemByFullPath( Core_Page::instance()->srcrequest->path, 25 );
			if($foundedISitem == 0) {
				Core_Event::attach('Core_Command_Controller_Default.onBeforeShowAction', array('Hostcms_Redirect_Observer', 'onBeforeShowAction'));
			}
		}
	}

//-- не удалять. это для примера --
// Core_Event::attach('Skynetcore_Ajax_Controller_Shop.onBeforeSendResponse', array('Skynetcore_Observer_Core', 'onBeforeSendAjaxResponse'));
//	static public function onBeforeSendAjaxResponse($commandAjaxController, $args) {
//		list($aResult) = $args;
//
//		if(isset($aResult['html']['context_keys'])
//			&& is_array($aResult['html']['context_keys'])
//			&& array_search('#userbar__link_favorites', $aResult['html']['context_keys'])
//		) {
//			$aResult['html']['#userbar__link_favorites'] = Skynetcore_Part_Base::getUserBarTop('favorites', 5, 'Shoker. Common. '.Core_Str::ucfirst('favorites'));
//		}
//		if(isset($aResult['html']['context_keys'])
//			&& is_array($aResult['html']['context_keys'])
//			&& array_search('#userbar__link_compares', $aResult['html']['context_keys'])
//		) {
//			$aResult['html']['#userbar__link_compares'] = Skynetcore_Part_Base::getUserBarTop('compares', 5, 'Shoker. Common. '.Core_Str::ucfirst('compares'));
//		}
//		return $aResult;
//	}

	static public function onBeforeContentCreation() {
		$instance = Core_Page::instance();

		$checkUser = Core::moduleIsActive('siteuser') && $instance->skynet->siteuser;
		if(    isset($instance->skynet->config['settings'])
			&& isset($instance->skynet->config['settings']['checksiteusers'])
			&& is_array($instance->skynet->config['settings']['checksiteusers'])
			&& count($instance->skynet->config['settings']['checksiteusers'])
			&& $checkUser !== false
		) {
			$redirectToAuth = false;
			$checkSiteusers = $instance->skynet->config['settings']['checksiteusers'];
			$maxSessions = Core_Array::get($checkSiteusers, 'maxsessions', 10000000)*1;

			//-- Проверяем, не менялся ли пароль пользователя --
			/** @var Skynetcore_Siteuser_Session_Model $qSkynetcore_siteuser_current_session */
			$qSkynetcore_siteuser_current_session = Core_Entity::factory('Skynetcore_Siteuser_Session');
			$qSkynetcore_siteuser_current_session
				->queryBuilder()
				->select(['s.value', 'datavalue'])
				->join(['sessions', 's'], 's.id', '=', 'skynetcore_siteuser_sessions.session_id')
				->where('skynetcore_siteuser_sessions.exit_time', '>', Core_Date::timestamp2sql(time()))
				->where('skynetcore_siteuser_sessions.active', '=', 1)
			;
			$aSkynetcore_siteuser_current_sessions = $qSkynetcore_siteuser_current_session->getAllBySiteuser_id($checkUser->id, false);
			foreach ($aSkynetcore_siteuser_current_sessions as $oSkynetcore_siteuser_current_session) {
				if($oSkynetcore_siteuser_current_session->siteuser_pass != $checkUser->password) {
					Core_Session::destroy($oSkynetcore_siteuser_current_session->session_id);
					$oSkynetcore_siteuser_current_session->exit_type = 'password_changed';
					$oSkynetcore_siteuser_current_session->value = $oSkynetcore_siteuser_current_session->datavalue;
					$oSkynetcore_siteuser_current_session->active = 0;
					$oSkynetcore_siteuser_current_session->exit_time = Core_Date::timestamp2sql(time());
					$oSkynetcore_siteuser_current_session->save();
					$redirectToAuth = true;
				}
			}

			if($redirectToAuth) {
				$oCore_Response = new Core_Response();
				$oCore_Response
					->status(301)
					->header('Location', Core_Array::get($checkSiteusers, 'profile', '/users/'));
				$oCore_Response->sendHeaders();
				die();
			}
			//-- Проверяем, не менялся ли пароль пользователя --

			$checkedStructures = $checkSiteusers['structure'];
			if(    !$instance->skynet->request->isMain
				&& ( isset($checkUser->id) && $checkUser->id > 0)
				&& (   in_array($instance->structure->id, $checkedStructures)
					|| $instance->structure->getSiteuserGroupId() > 0
				)
				&& $maxSessions > 0
			) {
				$redirectToAuth = false;
				/** @var Skynetcore_Siteuser_Session_Model $qSkynetcore_siteuser_sessions */
				$qSkynetcore_siteuser_sessions = Core_Entity::factory('Skynetcore_Siteuser_Session');
				$qSkynetcore_siteuser_sessions
					->queryBuilder()
					->select(['s.value', 'datavalue'])
					->join(['sessions', 's'], 's.id', '=', 'skynetcore_siteuser_sessions.session_id')
					->where('skynetcore_siteuser_sessions.exit_time', '>', Core_Date::timestamp2sql(time()))
					->where('skynetcore_siteuser_sessions.active', '=', 1)
					->orderBy('skynetcore_siteuser_sessions.time', 'DESC')
				;
				$aSkynetcore_siteuser_sessions = $qSkynetcore_siteuser_sessions->getAllBySiteuser_id($checkUser->id, false);

				$skynetSessionKeyCounts = 0;
				//-- 1. Актуализируем сессии согласно ТП --
				foreach ($aSkynetcore_siteuser_sessions as $skynetSessionKey => $skynetSession) {
					if($skynetSession->active == 1) $skynetSessionKeyCounts++;
					if($skynetSessionKeyCounts > Core_Array::get($checkSiteusers, 'maxsessions', 10000000)*1) {
						$skynetSession->exit_type = 'too_many_sessions';
						$skynetSession->value = $skynetSession->datavalue;
						$skynetSession->active = 0;
						$skynetSession->save();
					}
				}
				//-- 2. Сбрасываем все текущие неактивные сессии --
				/** @var Skynetcore_Siteuser_Session_Model $qSkynetcore_siteuser_sessions_current */
				$qSkynetcore_siteuser_sessions_current = Core_Entity::factory('Skynetcore_Siteuser_Session');
				$qSkynetcore_siteuser_sessions_current
					->queryBuilder()
					->select(['s.value', 'datavalue'])
					->join(['sessions', 's'], 's.id', '=', 'skynetcore_siteuser_sessions.session_id')
					->where('skynetcore_siteuser_sessions.exit_time', '>', Core_Date::timestamp2sql(time()))
					->where('skynetcore_siteuser_sessions.active', '=', 0)
					->where('skynetcore_siteuser_sessions.session_id', '=', session_id())
				;
				$aSkynetcore_siteuser_sessions_current = $qSkynetcore_siteuser_sessions_current->getAllBySession_id(session_id(), false);
				if(count($aSkynetcore_siteuser_sessions_current)) {
					foreach ($aSkynetcore_siteuser_sessions_current as $aSkynetcore_siteuser_session_current) {
						$redirectToAuth = true;
						Core_Session::destroy($aSkynetcore_siteuser_session_current->session_id);
						$aSkynetcore_siteuser_session_current->exit_time = Core_Date::timestamp2sql(time());
						$aSkynetcore_siteuser_session_current->save();
						// Update last change time
					}
				}

				if($redirectToAuth) {
					$oCore_Response = new Core_Response();
					$oCore_Response
						->status(301)
						->header('Location', Core_Array::get($checkSiteusers, 'profile', '/users/'));
					$oCore_Response->sendHeaders();
					die();
				}
			}
		}
	}

	static public function onBeforeShowCss($object, $args) {
		Core_Page::instance()->css('/modules/skynetcore/res/css/frontend.css');
		Core_Page::instance()->css('/modules/skynetcore/res/css/togglepanel.css');
	}

	static public function onSiteBeforeShowJs($object, $args) {
		Core_Page::instance()->prependJs('/modules/skynetcore/front/js/common.js');
	}

	static public function onBeforeShowJs($object, $args) {
		Core_Page::instance()->js('/modules/skynetcore/res/js/frontend.js');
		Core_Page::instance()->js('/modules/skynetcore/res/js/togglepanel.js');
	}

	static public function onBeforeGetXml($oEntity, $args) {
		$hideExtended = method_exists($oEntity, 'getHideExtended') && $oEntity->getHideExtended();
		if(!$hideExtended) {
			$instance = Core_Page::instance();
			$oEntity
				->addEntity(
					Core::factory('Core_Xml_Entity')
						->name('type')
						->value($instance->skynet->request->browser->devicetype)
				)
				->addEntity(
					Core::factory('Core_Xml_Entity')
						->name('pathinfo')
						->addEntity(Core::factory('Core_Xml_Entity')->name('domain')->value($instance->skynet->request->domain))
						->addEntity(Core::factory('Core_Xml_Entity')->name('path')->value($instance->skynet->request->path))
						->addEntity(Core::factory('Core_Xml_Entity')->name('path_no_page')->value($instance->skynet->request->pathNoPage))
						->addEntity(Core::factory('Core_Xml_Entity')->name('pathdeep')->value($instance->skynet->request->pathdeep))
				)
			;
			if(isset($instance->skynet->request->browser)
				&& is_object($instance->skynet->request->browser)
			) {
//			Skynetcore_Utils::tp($instance->skynet->request->browser->getXml()); die();
				$oEntity
					->addEntity($instance->skynet->request->browser)
				;
			}
			if(isset($instance->skynet->request->geohost)
				&& is_object($instance->skynet->request->geohost)
			) {
				$oEntity
					->addEntity($instance->skynet->request->geohost)
				;
			}
			$oEntity
				->addEntity($instance->skynet->request->get)
				->addEntity($instance->skynet->request->post)
				->addEntity(
					Core::factory('Core_Xml_Entity')
						->name('sky_csrf')
						->value($instance->skynet->response->csrf->token)
				)
			;

			if(isset($instance->skynet->siteuser) && $instance->skynet->siteuser !== false) {
				switch (true) {
					case $oEntity instanceof Shop_Model:
						break;
					case $oEntity instanceof Siteuser_Model:
						$aSiteuserPeoples = $oEntity->siteuser_people->findAll(false);
						$oSiteUserPeople = Core_Array::get($aSiteuserPeoples, 0, false);

						if($oSiteUserPeople !== false) {
							$oEntity->addEntity(
								Core::factory('Core_Xml_Entity')
									->name('fullname')
									->value(trim(''.trim(''.$oSiteUserPeople->surname).' '.trim(''.$oSiteUserPeople->name).' '.trim(''.$oSiteUserPeople->patronymic)))
							);
						}
						break;
					case $oEntity instanceof Site_Model:
						$oEntity->addXmlTag('siteuser_id', $instance->skynet->siteuser->id);
//					Core_Event::detach('siteuser.onBeforeGetXml', array('Skynetcore_Observer_Core', 'onBeforeGetXml'));
						break;
					default:
						$oEntity->addEntity($instance->skynet->siteuser);
				}
//			Skynetcore_Utils::p($instance->skynet->request);
//			die();
				$oEntity->addEntity(
					Core::factory('Core_Xml_Entity')
						->name('siteuser_groups')
						->addEntities($instance->skynet->siteuser_groups)
				);
			}
		}
	}

	static public function checkCanonical() {
//		$instance = Core_Page::instance();
//
//		if($instance->srcrequest->currentPage > 0
//			|| $instance->srcrequest->query != ''
//		) {
//			$instance->canonical = $instance->srcrequest->pathNoPage;
//		}
	}

	static public function onBeforeShowAction($object)
	{
		$instance = Core_Page::instance();
		$settings = Core_Array::get($instance->skynet->config, 'settings', [
			'use_redirect_upper_to_lowercase' => false
		]);
		if(Core_Array::get($settings, 'use_redirect_upper_to_lowercase', false)) {
			$localUri = urldecode(Core_Array::get($_SERVER, 'REQUEST_URI', false));
			$localMethod = Core_Array::get($_SERVER, 'REQUEST_METHOD', false);
			$explodeUri = explode('?', $localUri);
			$localMatchResult = preg_match('/[A-ZА-ЯЁ\s]+/u', $explodeUri[0]);
			if($localUri !== false
				&& $localMethod == 'GET'
				&& $localMatchResult !== false
				&& $localMatchResult > 0
			) {
				$localUri = mb_strtolower($explodeUri[0]).(isset($explodeUri[1]) ? "?{$explodeUri[1]}" : '');
				$oCore_Response = new Core_Response();
				$oCore_Response
					->status(301)
					->header('Location', $localUri);
				$oCore_Response->sendHeaders();
				die();
			}
		}
	}

	static public function onBeforeShowBody($oCoreResponce) {
		/** @var Core_Response $oCoreResponce */
		$instance = Core_Page::instance();
		$tmpBody = $oCoreResponce->getBody().'';
		switch (true) {
			case (isset($instance->skynet->response->index)
				&& $instance->skynet->response->index !== true
				&& is_array($instance->skynet->response->index)
				&& count($instance->skynet->response->index)
				&& preg_match('/<head>/ui', $tmpBody)
			):
				$index_types = implode($instance->skynet->response->index);
				$tmpBody = preg_replace('/<head>/ui', '<head>'."\n\t".'<meta name="robots" content="'.$index_types.'" />', $tmpBody);
				$oCoreResponce->changeBody($tmpBody);
				break;
			case (isset($instance->skynet->response->csrf->token)
				&& $instance->skynet->response->csrf->token != ''
				&& preg_match('/<body /ui', $tmpBody)
			):
				$tmpBody = preg_replace('/<body /ui', '<body data-csrf="'.$instance->skynet->response->csrf->token.'" ', $tmpBody);
				$oCoreResponce->changeBody($tmpBody);
				break;
		}
	}
}