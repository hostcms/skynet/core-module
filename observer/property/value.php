<?php
use Skynetcore_Utils as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Observer_Property_Value
{
	static public function onBeforeGetXml($oValue, $args)
	{
		Core_Event::detach('property_value_int.onBeforeGetXml', array('Skynetcore_Observer_Property_Value', 'onBeforeGetXml'));
		/** @var Property_Model $oProperty */
		$oProperty = $oValue->property;
		$oProperty_Dir = $oProperty->property_dir;
		$oValue
			->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('original_value')
					->value(isset($oValue->value) ? $oValue->value : '')
			)
			->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('model_name')
					->value($oValue->getModelName())
			)
			->addEntity(
				Core::factory('Core_Xml_Entity')
					->addAttribute('id', $oProperty->id)
					->addAttribute('property_dir_id', $oProperty->property_dir_id)
					->addAttribute('sorting', $oProperty->sorting)
					->name('property_name')
					->value($oProperty->name)
			)
		;
		if($oProperty_Dir instanceof Property_Dir_Model) {
			$aTmpDirProperties = [];
			do {
				$aTmpDirProperties[] = [
					'crcid' => base64_encode($oProperty_Dir->id.'property_dir'),
					'id' => $oProperty_Dir->id,
					'parent_id' => $oProperty_Dir->parent_id,
					'sorting' => $oProperty_Dir->sorting,
					'name' => $oProperty_Dir->name,
					'description' => $oProperty_Dir->description,
				];
			} while(($oProperty_Dir = $oProperty_Dir->getParent()) instanceof Property_Dir_Model);
			$aTmpDirProperties = array_reverse($aTmpDirProperties);
			foreach ($aTmpDirProperties as $aProperty_Dir) {
				$oValue
					->addEntity(
						Core::factory('Core_Xml_Entity')
							->name('property_dir_name')
//							->addAttribute('crcid', base64_encode($aProperty_Dir->id.'property_dir'))
							->addAttribute('id', $aProperty_Dir['id'])
							->addAttribute('parent_id', $aProperty_Dir['parent_id'])
							->addAttribute('sorting', $aProperty_Dir['sorting'])
							->addAttribute('name', $aProperty_Dir['name'])
							->addEntity(
								Core::factory('Core_Xml_Entity')
									->name('crcid')
									->value(base64_encode($aProperty_Dir['id'].'property_dir'))
							)
							->addEntity(
								Core::factory('Core_Xml_Entity')
									->name('description')
									->value($aProperty_Dir['description'])
							)
					)
				;
			}
		}
		if($oValue->property->list_id > 0
			&& isset($object->list_item->id)
			&& $object->list_item->id > 0
		) {
			$oValue->addEntity($oValue->list_item);
		}
		Core_Event::attach('property_value_int.onBeforeGetXml', array('Skynetcore_Observer_Property_Value', 'onBeforeGetXml'));
	}

	static public function onBeforeRedeclaredGet($oValue, $args)
	{
		if($oValue->property->type == 3) {
//			$propertyConfig = Core_Config::instance()->get('property_config', array());
//			isset($propertyConfig['add_list_items']) && $propertyConfig['add_list_items'] = false;
//			$oValue->property->setConfig($propertyConfig);
		}
	}
}