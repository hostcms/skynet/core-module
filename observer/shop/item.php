<?php
use Skynetcore_Utils as utl;
use Astartsky\SypexGeo as sx;

/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Observer_Shop_Item {

	/** @var Shop_Item_Model $oShopItem */
	static public function skynetOnBeforeGetItemXml($oShopItem, $args) {
		$instance = Core_Page::instance();
		$oShopItem->datais_favorite = isset($instance->skynet->favorites[$oShopItem->shop->id]->nodes[$oShopItem->id])*1;
		$oShopItem->datais_compare = isset($instance->skynet->compares[$oShopItem->shop->id]->nodes[$oShopItem->id])*1;
//		Skynetcore_Utils::tp($instance->skynet->favorites[$oShopItem->shop->id]->nodes[$oShopItem->id]);
	}
}