<?php
use Skynetcore_Utils as utl;
use Astartsky\SypexGeo as sx;

/**
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Observer_Shop_Cart_Controller {

	/** @var Shop_Cart_Controller $oShopCartController */
	static public function onAfterDelete($oShopCartController) {
		$oShop_Item = Core_Entity::factory('Shop_Item')->find($oShopCartController->shop_item_id);
		if (isset($_SESSION['hostcmsCart'][$oShop_Item->shop_id])
			&& is_array($_SESSION['hostcmsCart'][$oShop_Item->shop_id])
		) {
			foreach ($_SESSION['hostcmsCart'][$oShop_Item->shop_id] as $shopCartKey => $shopCart) {
				if(preg_match("/^{$oShopCartController->shop_item_id}-?/", $shopCartKey)) {
					unset($_SESSION['hostcmsCart'][$oShop_Item->shop_id][$shopCartKey]);
				}
			}
		}
	}

	/** @var Shop_Cart_Controller $oShopCartController */
	static public function onAfterUpdate($oShopCartController) {
		Core_Event::detach('Shop_Cart_Controller.onAfterUpdate', array('Skynetcore_Observer_Shop_Cart_Controller', 'onAfterUpdate'));
		$instance = Core_Page::instance();
		$aCarts = $instance->skynet->carts;
		if(isset($instance->skynet->carts) && count($instance->skynet->carts)) {
			foreach ($instance->skynet->carts as $shopId => $shopCarts) {
				if(isset($shopCarts[$oShopCartController->shop_item_id])) {
					$shopCarts[$oShopCartController->shop_item_id][0]->quantity = $oShopCartController->quantity;
//					Skynetcore_Utils::v($oShopCartController);
//					Skynetcore_Utils::p($shopCarts[$oShopCartController->shop_item_id]);
				}
			}
		}
//		Skynetcore_Utils::p($oShopCartController);
//		Skynetcore_Utils::p($oShopCartController->quantity);
		Core_Event::attach('Shop_Cart_Controller.onAfterUpdate', array('Skynetcore_Observer_Shop_Cart_Controller', 'onAfterUpdate'));
	}
}