<?php
use Skynetcore_Utils as utl;
use Astartsky\SypexGeo as sx;

/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Observer_Shop_Order {

	/** @var Skynetcore_Observer_Shop_Order $oShopOrder */
	static public function skynetOnBeforeSave($oShopOrder) {
		Core_Event::detach('shop_order.onBeforeSave', array('Skynetcore_Observer_Shop_Order', 'skynetOnBeforeSave'));
		$instance = Core_Page::instance();
		$oOrder = Core_Entity::factory('Shop_Order')->getById($oShopOrder->id, false);

		if(is_null($oOrder)
			|| ($oOrder->phone == '' && $oShopOrder->phone != '')
		) {
			$oShopOrder->phone = preg_replace('/[^\d]+/ui', '', (string) $oShopOrder->phone);
			if(strlen($oShopOrder->phone) == 11) {
				$oShopOrder->phone = preg_replace('/^7/ui', '+7', (string) $oShopOrder->phone);
			}
		}
		Core_Event::attach('shop_order.onBeforeSave', array('Skynetcore_Observer_Shop_Order', 'skynetOnBeforeSave'));
	}
}