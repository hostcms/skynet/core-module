<?php
use Skynetcore_Utils as utl;
use Astartsky\SypexGeo as sx;

/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Observer_List_Item {

//	/** @var List_Item_Model $oListItem */
//	static public function onListItemBeforeGetXml($oListItem) {
//		$uploadDir = "/upload/lists/list_{$oListItem->list_id}/".Core_File::getNestingDirPath($oListItem->id)."/item_{$oListItem->id}/";
////		Skynetcore_Utils::p($uploadDir);
//		$oListItem->addEntity(
//			Core::factory('Core_Xml_Entity')
//				->name('upload_files')
//				->value($uploadDir)
//		);
//	}
}