<?php
/**
* Created by PhpStorm.
* User: MikeBorisov
* Date: 10.12.2014
* Time: 15:32
*/
use Utils_Utl as utl;
use HCMS\Images as img;

class Skynetcore_Observer_Admin_Form {

	static public function onAfterPrepareForm($controller, $args) {
		list($object, $Admin_Form_Controller) = $args;

		$objectFields = array_keys($object->getTableColums());
//		Skynetcore_Utils::p($objectFields, get_class($controller)); die();
		// Данное событие будет вызываться для всех форм, определяем с каким контроллером работаем
		switch (true)
		{
			case $controller instanceof List_Item_Controller_Edit:
				$oMainTab = $controller->getTab('main');
//				$oAdditionalTab = $controller->getTab('additional');

				$oListItemSEOTab = Admin_Form_Entity::factory('Tab')
					->caption('SEO')
					->name('tab_fields_seos');
				$controller->addTabAfter($oListItemSEOTab, $oMainTab);
				$oListItemSEOTab
					->add($oListItemSEOTabRow0 = Admin_Form_Entity::factory('Div')->class('row'))
					->add($oTabBlock1 = Admin_Form_Entity::factory('Div')->id('shop_tabs_block_1')->class('well with-header'))
					->add($oTabBlock2 = Admin_Form_Entity::factory('Div')->id('shop_tabs_block_1')->class('well with-header'))
				;
				$oTabBlock1
					->add(Admin_Form_Entity::factory('Div')
						->class('header bordered-warning')
						->value('Падежи')
					)
					->add($oListItemSEOTabRow1 = Admin_Form_Entity::factory('Div')->class('row'));
				$oTabBlock2
					->add(Admin_Form_Entity::factory('Div')
						->class('header bordered-warning')
						->value('Числительность')
					)
					->add($oListItemSEOTabRow2 = Admin_Form_Entity::factory('Div')->class('row'));

				foreach (['value_en', 'linkaddr',
							 'sky_i_case', 'sky_r_case', 'sky_d_case', 'sky_v_case', 'sky_t_case', 'sky_p_case',
							 'sky_once', 'sky_mch'
						 ] as $objectFieldName) {
					$objectFieldKey = array_search($objectFieldName, $objectFields);
//					Skynetcore_Utils::tp($objectFieldKey, $objectFieldKey);
//					Skynetcore_Utils::tv($objectFields, $objectFieldName);
					if($objectFieldKey!==FALSE) {
						for($i=$objectFieldKey; $i<count($objectFields); $i++) {
//							После обновления 6.8.8 начал валить в лог то что было в try/catch. Почему - непонятно
							try {
								$localTmpField = $controller->getField($objectFields[$i]);
//								Skynetcore_Utils::tv($localTmpField->name == $objectFieldName, $localTmpField->name . '=' . $objectFieldName);
								$oListItemSEOTabCommonRow = $oListItemSEOTabRow1;
								switch ($localTmpField->name) {
									case 'value_en':
									case 'sky_i_case':
									case 'sky_r_case':
									case 'sky_d_case':
									case 'sky_v_case':
									case 'sky_t_case':
									case 'sky_p_case':
										$localTmpField
											->divAttr(array('class' => 'form-group col-xs-12 col-sm-4'));
										break;
									case 'value_en':
									case 'linkaddr':
										$oListItemSEOTabCommonRow = $oListItemSEOTabRow0;
										break;
									case 'sky_once':
									case 'sky_mch':
										$localTmpField
											->divAttr(array('class' => 'form-group col-xs-12 col-sm-4'));
										$oListItemSEOTabCommonRow = $oListItemSEOTabRow2;
										break;
									default:
								}
								if($localTmpField->name == $objectFieldName) {
									// die();
									$oMainTab->move($localTmpField, $oListItemSEOTabCommonRow);
								}
							} catch (Core_Exception $e) {
//								Skynetcore_Utils::tp($e->getMessage(), $objectFields[$i]);
							}
						}
					}
				}
//				die();

				break;
//			case $controller instanceof Shop_Item_Controller_Edit:
//				$oMainTab = $controller->getTab('main');
//				$oLastTab = $controller->getTab('additional');
//				$oAdditionalTab = Admin_Form_Entity::factory('Tab')
//					->caption('Другие поля')
//					->name('tab_fields_others');
//
//				$controller->addTabAfter($oAdditionalTab, $oLastTab);
//				$oAdditionalTab
//					->add($oAdditionalRow1 = Admin_Form_Entity::factory('Div')->class('row'));
//				$objectFieldKey = array_search('deleted', $objectFields);
//				if($objectFieldKey!==FALSE) {
//					for($i=$objectFieldKey+1; $i<count($objectFields); $i++) {
////							После обновления 6.8.8 начал валить в лог то что было в try/catch. Почему - непонятно
//						try {
//							$oMainTab
//								->move($controller->getField($objectFields[$i]), $oAdditionalRow1);
//						} catch (Core_Exception $e) {}
//					}
//				}
//
//				switch (true) {
//					case $object instanceof Shop_Item_Model:
//						$groupId = (isset($object->modification->id) && $object->modification->id>0) ? $object->modification->shop_group_id : $object->shop_group_id;
//						$groupsNav = array_values(Utils_Shop_Group_Controller::getAllParenGroups($groupId));
//						$oProperty = Core_Entity::factory('Property')->getByTag_Name('utils_group_code_marking_generate_shop_'.$object->shop_id);
//						if(isset($groupsNav[0]->id) && $groupsNav[0]->id>0 && isset($oProperty->id) && $oProperty->id>0) {
//							$marking = $controller->getField('marking');
//							if (trim(''.$marking->value) == '' || trim(''.$marking->value) == '0') {
//								$marking->value = Utils_Shop_Item_Controller::generateMarking($object);
//								$marking->value == '0' && $marking->value = '';
//							}
//							if (strlen($marking->value) == 13) {
//								$marking->readonly('readonly');
////									$regFormat = '/^' . substr($marking->value, 0, 4) . '\d{9}$/';
////									$marking->format(
////										array(
////											'minlen' => array('value' => 1),
////											'reg' => array('value' => $regFormat, 'message' => 'Не соответствует формату "' . $regFormat . '"')
////										)
////									);
//							}
//							$name = $controller->getField('name');
//							$name->format(
//								array(
//									'minlen' => array('value' => 2),
//								)
//							);
//						}
//						break;
//				}
//				break;
		}

	}
}