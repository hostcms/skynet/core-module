<?
use Skynetcore_Utils as utl;
use Astartsky\SypexGeo as sx;

/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Observer_Common {

	static public function onBeforeGetXmlAddUser($object)
	{
		/** @var Shop_Model $object */
		Core_Event::detach($object->getModelName().'.onBeforeGetXml', array('Skynetcore_Observer_Common', 'onBeforeGetXmlAddUser'));
		$object->addEntity(Core_Page::instance()->skynet->user);
		Core_Event::attach($object->getModelName().'.onBeforeGetXml', array('Skynetcore_Observer_Common', 'onBeforeGetXmlAddUser'));
	}

	static public function onBeforeGetXmlAddSiteuser($object)
	{
		/** @var Shop_Model $object */
		Core_Event::detach($object->getModelName().'.onBeforeGetXml', array('Skynetcore_Observer_Common', 'onBeforeGetXmlAddSiteuser'));
		switch (true) {
			case $object instanceof Shop_Model:
			case $object instanceof Site_Model:
				break;
			default:
				$object->addEntity(Core_Page::instance()->skynet->siteuser);
		}
		Core_Event::attach($object->getModelName().'.onBeforeGetXml', array('Skynetcore_Observer_Common', 'onBeforeGetXmlAddSiteuser'));
	}
}