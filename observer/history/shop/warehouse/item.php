<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Observer_History_Shop_Warehouse_Item extends Core_Entity
{
	static public function onBeforeSave($oSWHItem) {
		$oldObjectItem = new stdClass();
		$oldObjectItem->shop_item_id = $oSWHItem->shop_item_id;
		if(isset($oSWHItem->id) && $oSWHItem->id > 0) {
			/** @var Shop_Warehouse_Item_Model $oldObjectItem */
			$oldObjectItem = Core_Entity::factory('Shop_Warehouse_Item')->getById($oSWHItem->id*1, false);
		} else {
			$oldObjectItem->id = 0;
			$oldObjectItem->shop_warehouse_id = 0;
			$oldObjectItem->count = 0;
		}
		$aChangedData = $oSWHItem->getChangedData();
		if(count($aChangedData)) {
			foreach ($aChangedData as $aChangedDataKey => $aChangedDataValue) {
				$changeCode = time();
				$aChangedDataValueOld = isset($oldObjectItem->$aChangedDataKey) ? $oldObjectItem->$aChangedDataKey : '';
				if($aChangedDataKey != 'shop_item_id'
					&& $aChangedDataValueOld != $aChangedDataValue
				) {
					/** @var Skynetcore_History_Shop_Warehouse_Item_Model $oWHItemHistory */
					$oWHItemHistory = Core_Entity::factory('Skynetcore_History_Shop_Warehouse_Item');
					$oWHItemHistory->code = $changeCode;
					$oWHItemHistory->shop_warehouse_item_id = $oldObjectItem->id*1;
					$oWHItemHistory->shop_item_id = $oldObjectItem->shop_item_id;
					$oWHItemHistory->entity_field = $aChangedDataKey;
					$oWHItemHistory->old_value = $aChangedDataValueOld;
					$oWHItemHistory->new_value = $aChangedDataValue;
					$oWHItemHistory->save();
				}
				usleep(1);
			}
		}
	}
}