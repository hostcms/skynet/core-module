<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 14.06.2017
 * Time: 12:31
 */
use Skynetcore_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Entity_Response extends Skynetcore_Core_Servant_Properties {

	/**
	 * Allowed object properties
	 * @var array
	 */
	protected $_allowedProperties = array(
		'ajax_status',
		'ajax_message',
		'csrf',
		'index',
	);

	protected $_tagName = 'sky_response';

	public function __construct()
	{
		parent::__construct();
		$this->setUnlimitedProperties();

		$csrf = new stdClass();
		$csrf->token = Skynetcore_Utils::getSimpleToken();
		$csrf->isValid = Core_Array::getRequest('csrf_key', false) == $csrf->token;
		$this->csrf = $csrf;
		$this->index = true;

		$this->ajax_status = 'ERROR';
		$this->ajax_message = 'Неизвестная ошибка';
	}
}