<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 14.06.2017
 * Time: 12:31
 */
use Skynetcore_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Entity_Request extends Skynetcore_Core_Servant_Properties {

	/**
	 * Allowed object properties
	 * @var array
	 */
	protected $_allowedProperties = array(
		'remoteip',
		'proto',
		'domain',
		'port',
		'path',
		'pathNoPage',
		'pathdeep',
		'aSplitedPath',
		'jSplitedPath',
		'crc32path',
		'crc32pathNoPage',
		'isMain',
		'isBeta',
		'isSeo',
		'isProd',
		'isManagement',
		'isAdmin',
		'envType',
		'query',
		'currentPage',
		'utm',
		'geoip',
		'geohost',
		'browser',
		'test',
		'get',
		'post',
		'storage',
	);

	protected $_tagName = 'sky_request';

	/**
	 * Utils_Path_Info constructor.
	 * @param string $_tagName
	 */
	public function __construct()
	{
		parent::__construct();

//		$this->test = false;
		if(isset($_GET)) {
			$noSortGet = $_GET;
			if(isset($noSortGet['sort'])) unset($noSortGet['sort']);
			if(isset($noSortGet['ajax'])) unset($noSortGet['ajax']);

			$localGet = $_GET;
			if(isset($_GET['context_html']) && is_array($_GET['context_html']) && count($_GET['context_html'])) {
				foreach ($_GET['context_html'] as $localKey => $localValue) {
					if(preg_match('/#/ui', $localKey)) {
						$localGet['context_html'][preg_replace('/#/ui', '', $localKey)] = $localValue;
						unset($localGet['context_html'][$localKey]);
					}
				}
			}
			$this->get = Skynetcore_Convert_Array_Xml_Entity::createInstance(
				array_merge([
					'query' => Core_Array::get($_SERVER, 'QUERY_STRING', ''),
					'queryshort' => rawurldecode(http_build_query($noSortGet)),
				], $localGet), '', 'get'
			);
		}
		if(isset($_POST)) {
			$localPost = $_POST;
			if(isset($_POST['context_html']) && is_array($_POST['context_html']) && count($_POST['context_html'])) {
				foreach ($_POST['context_html'] as $localKey => $localValue) {
					if(preg_match('/#/ui', $localKey)) {
						$localPost['context_html'][preg_replace('/#/ui', '', $localKey)] = $localValue;
						unset($localPost['context_html'][$localKey]);
					}
				}
			}
			$this->post = Skynetcore_Convert_Array_Xml_Entity::createInstance($localPost, '', 'post');
		}
//		if(isset($_SESSION['storage'])) {
//			$this->storage = Skynetcore_Convert_Array_Xml_Entity::createInstance($_SESSION['storage'], '', 'storage');
//		}
		$this->isMain = false;
		$this->isSeo = false;
		$this->isBeta = false;
		$this->isProd = false;
		$this->isManagement = false;
		$this->envType = 'dev';
	}

//	/**
//	 * Utils_Path_Info constructor.
//	 * @param string $_tagName
//	 */
//	public function __construct_XXX_bad()
//	{
//		parent::__construct();
//
//		if(isset($_GET)) {
//			$noSortGet = $_GET;
//			if(isset($noSortGet['sort'])) unset($noSortGet['sort']);
//			if(isset($noSortGet['ajax'])) unset($noSortGet['ajax']);
//
//			$this->get = Skynetcore_Convert_Array_Xml_Entity::createInstance(
//				array_merge([
//					'query' => Core_Array::get($_SERVER, 'QUERY_STRING', ''),
//					'queryshort' => rawurldecode(http_build_query($noSortGet)),
//				], $_GET), '', 'get'
//			);
//		}
//		if(isset($_POST)) {
////			Skynetcore_Utils::p($_POST);
//			$localPost = $_POST;
//			if(isset($_POST['context_html']) && is_array($_POST['context_html']) && count($_POST['context_html'])) {
//				foreach ($_POST['context_html'] as $localKey => $localValue) {
//					if(preg_match('/#/ui', $localKey)) {
//						$localPost['context_html'][preg_replace('/#/ui', '', $localKey)] = $localValue;
//						unset($localPost['context_html'][$localKey]);
//					}
//				}
//			}
//			$this->post = Skynetcore_Convert_Array_Xml_Entity::createInstance($localPost, '', 'post');
//		}
////		if(isset($_SESSION['storage'])) {
////			$this->storage = Skynetcore_Convert_Array_Xml_Entity::createInstance($_SESSION['storage'], '', 'storage');
////		}
//		$this->isMain = false;
//		$this->isProd = false;
//		$this->envType = 'dev';
//	}

	public static function queryFromArray($aQuery) : string {
		$query = '';
		foreach ($aQuery as $qQuery => $queryValue) {
			$query .= '&'.$qQuery.'='.$queryValue;
		}
		$query = trim($query, '&');
		strlen($query) > 0 && $query = "?{$query}";

		return $query;
	}
}