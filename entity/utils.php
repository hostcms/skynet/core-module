<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 14.06.2017
 * Time: 12:31
 */
use Skynetcore_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Entity_Utils extends Skynetcore_Core_Servant_Properties {

	public static function setShopItemQueryBuilderConditions(&$oQueryBuilder,
	                                                         $pseudo='shop_items',
	                                                         $inOptions = []) {
		$options = $inOptions + [
			'deep' => 9,
			'pseudo_foreign' => 'shop_group_id',
			'pseudo_table' => 'shop_groups',
			'has_active' => true,
			'has_deleted' => true,
			'path_fields' => ['id' => '', 'name' => ''],
		];
		$pathFields = array_keys(Core_Array::get($options, 'path_fields', []));
		$pathFieldValues = Core_Array::get($options, 'path_fields', []);
//		Skynetcore_Utils::p($options);
		/** @var Core_QueryBuilder_Select $oQueryBuilder */
		$oQueryBuilder
			->leftJoin([$options['pseudo_table'], "sky_condition_{$options['pseudo_table']}1"], "sky_condition_{$options['pseudo_table']}1.id", '=', "{$pseudo}.{$options['pseudo_foreign']}")
		;
		$deep = Core_Array::get($options, 'deep', 9);
		for ($index=2; $index <= $deep; $index++) {
			$oQueryBuilder
				->leftJoin([$options['pseudo_table'], "sky_condition_{$options['pseudo_table']}{$index}"], "sky_condition_{$options['pseudo_table']}{$index}.id", '=', "sky_condition_{$options['pseudo_table']}".($index-1).".parent_id")
			;
		}
		$sTmpQueryBuilderActive = $sTmpQueryBuilderDeleted = '';
		$aTmpQueryBuilderPathFields = [];
		$indexDeep = $deep;
		for ($index=1; $index <= $deep; $index++) {
			if(count($pathFields)) {
				foreach ($pathFields as $tmpPathField) {
					!isset($aTmpQueryBuilderPathFields[$tmpPathField]) && $aTmpQueryBuilderPathFields[$tmpPathField] = '';
					$index > 1 && $aTmpQueryBuilderPathFields[$tmpPathField] .= " , ";
					$aTmpQueryBuilderPathFields[$tmpPathField] .= "COALESCE(CONCAT('/', sky_condition_{$options['pseudo_table']}{$indexDeep}.{$tmpPathField}), '')";
				}
			}
			if($options['has_active']) {
				$index > 1 && $sTmpQueryBuilderActive .= " AND ";
				$sTmpQueryBuilderActive .= "COALESCE(sky_condition_{$options['pseudo_table']}{$index}.active, 1)";
			}
			if($options['has_deleted']) {
				$index > 1 && $sTmpQueryBuilderDeleted .= " OR ";
				$sTmpQueryBuilderDeleted .= "COALESCE(sky_condition_{$options['pseudo_table']}{$index}.deleted, 0)";
			}
			$indexDeep--;
		}
		if(count($pathFields)) {
			foreach ($pathFields as $tmpPathField) {
				$aTmpQueryBuilderPathFields[$tmpPathField] = "CONCAT($aTmpQueryBuilderPathFields[$tmpPathField])";
				$oQueryBuilder
					->select([Core_QueryBuilder::expression($aTmpQueryBuilderPathFields[$tmpPathField]), "datasky_field_{$tmpPathField}"]);
				if(isset($pathFieldValues[$tmpPathField]) && $pathFieldValues[$tmpPathField] != '') {
//					Skynetcore_Utils::p($pathFieldValues[$tmpPathField]);
					$pathFieldExpression = explode('#',$pathFieldValues[$tmpPathField]);
//					Skynetcore_Utils::p($pathFieldExpression);
					$oQueryBuilder
//						->where($aTmpQueryBuilderPathFields[$tmpPathField], $pathFieldExpression[0], Core_QueryBuilder::expression($pathFieldExpression[1]));
						->where($aTmpQueryBuilderPathFields[$tmpPathField], $pathFieldExpression[0], Core_QueryBuilder::expression($pathFieldExpression[1])->build());
				}
			}
		}
//		Skynetcore_Utils::p($oQueryBuilder->build());
//		Skynetcore_Utils::p($aTmpQueryBuilderPathFields);
		if($options['has_active']) {
			$oQueryBuilder
				->where(Core_QueryBuilder::expression("({$sTmpQueryBuilderActive})"), '=', 1)
			;
		}
		if($options['has_deleted']) {
			$oQueryBuilder
				->where(Core_QueryBuilder::expression("({$sTmpQueryBuilderDeleted})"), '=', 0)
			;
		}
		return true;
	}
}