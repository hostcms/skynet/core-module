<?php
use Skynetcore_Utils as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * @property string  $address
 * @property string  $city
 * @property string  $region
 * @property string  $country
 * @property integer $country_id
 * @property integer $location_id
 * @property integer $city_id
 * @property integer $area_id
 */
class Skynetcore_Entity_Address extends Skynetcore_Core_Servant_Properties {

	/**
	 * Allowed object properties
	 * @var array
	 */
	protected $_allowedProperties = array(
		'address',
		'country',
		'region',
		'city',
		'country_id',
		'location_id',
		'city_id',
		'area_id',
	);

	/**
	 * Name of the tag in XML
	 * @var string
	 */
	protected $_tagName = 'sky_address';

	public function __construct($address)
	{
		parent::__construct();

		$aLocations = explode(',', $address);
		$aLocations = array_map(function($location) {return trim($location); }, $aLocations);
		$this->country_id = $this->location_id = $this->city_id = $this->area_id = 0;
		$this->address = $address;
		switch (count($aLocations)) {
			case 1:
				list($this->city) = $aLocations;
				break;
			case 2:
				list($this->city, $this->country) = $aLocations;
				break;
			default:
				list($this->city, $this->region, $this->country) = $aLocations;
		}

		/** @var Shop_Country_Location_City_Model $oCity */
		$aCity = Core_Entity::factory('Shop_Country_Location_City')->getAllByName($this->city, false);
		$oCity = null;
		if(count($aCity)==1) {
			$oCity = $aCity[0];
		} else {
			$region = $this->region;
			$aResult = array_filter(
				$aCity,
				function ($location) use ($region) {
					return $location->shop_country_location->name==$region;
				}
			);
			if(count($aResult)==1) {
				$oCity = array_values($aResult)[0];
			}
		}
		if(!is_null($oCity)) {
			$this->city_id = $oCity->id;
			$this->location_id = $oCity->shop_country_location->id;
			$this->country_id = $oCity->shop_country_location->shop_country->id;
		}

	}
}