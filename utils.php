<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Skynet Core Module.
 *
 * @package HostCMS 6\Module
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © mikeborisov, m.u.borisov@gmail.com
 */
class Skynetcore_Utils
{
	public static $monthes = [
		'январь',
		'февраль',
		'март',
		'апрель',
		'май',
		'июнь',
		'июль',
		'август',
		'сентябрь',
		'октябрь',
		'ноябрь',
		'декабрь'
	];

	public static function addQbLimitLevels(&$queryBuilder, $tableName, $aliasPrefix, $aliasRelatePrefix, $selects=[], $maxLevel = 9) {
		/** @var Core_QueryBuilder_Select $queryBuilder */
		for($i = 1; $i <= $maxLevel; $i++) {
			if(count($selects)) {
				foreach ($selects as $select) {
					$queryBuilder
						->select([Core_QueryBuilder::expression("COALESCE({$aliasPrefix}_{$i}.{$select}, '')"), "data{$aliasPrefix}_{$i}_{$select}"])
					;
				}
			}
			$queryBuilder
				->leftJoin([$tableName, "{$aliasPrefix}_{$i}"], "{$aliasPrefix}_{$i}.id", "=", Core_QueryBuilder::expression("split_string({$aliasRelatePrefix}.path, '/', {$i})"));
		}
		return $queryBuilder;
	}

	public static function splitFio($fio) {
		$retValue = new stdClass();
		$aFIO = preg_replace('/ +/ui', ' ', trim(Core_Str::stripTags(strval($fio.''))));
		$aTmpSurname = explode(' ', $aFIO);
		$retValue->surname = Core_Array::get($aTmpSurname, 0, '');
		$retValue->name = Core_Array::get($aTmpSurname, 1, $fio);
		$retValue->patronymic = Core_Array::get($aTmpSurname, 2, '');

		return $retValue;
	}

	public static function RandomString($len = 11)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randstring = '';
		for ($i = 0; $i < $len; $i++) {
			$randstring .= $characters[rand(0, strlen($characters))];
		}
		return mb_strtoupper($randstring);
	}

	public static function getAllPremute($items, $perms = []) {
		static $results = [];

		if (empty($items)) {
			$results[] = $perms;
		} else {
			for ($i = 0; $i < count($items); $i++) {
				$newItems = $items;
				$newPerms = $perms;
				list($foo) = array_splice($newItems, $i, 1);
				array_push($newPerms, $foo);
				self::getAllPremute($newItems, $newPerms);
			}
		}

		return $results;
	}

	public static function getAllCombinations($array)
	{
		$result = [];
		$count = count($array);
		$total = 1 << $count; // 2^count - общее количество комбинаций

		for ($i = 0; $i < $total; $i++) {
			$combination = [];
			for ($j = 0; $j < $count; $j++) {
				if ($i & (1 << $j)) {
					$combination[] = $array[$j];
				}
			}
			$result[] = $combination;
		}

		return $result;
	}

/**
	 * Функция добавляет изображение к элементу или группе моделей
	 *   Shop_Group_Model
	 *   Shop_Item_Model
	 *
	 * @param $Shop_Group_Model, $Shop_Item_Model
	 * @param $sourceFile
	 * @param $uploadFolder
	 */
	public static function setShopImage($entity, $sourceFile, $currentShop, $destinationFolder='', $uploadFolder='', $ext='', $realFilename='', $saveOriginalFolder='') {
		$uploadFolder == '' && $uploadFolder = CMS_FOLDER . 'upload' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;
		$error = array(
			'status' => FALSE,
			'message' => 'Неизвестная ошибка',
		);
		$folderAddDir = '';
		$entityClass = get_class($entity);
		//-- Разрешенные типы изображений для конвертации --------------------------------------------------------------
		$imageTypes = array(
			IMAGETYPE_JPEG => 'jpg',
			IMAGETYPE_PNG => 'png',
			IMAGETYPE_GIF => 'gif',
			IMAGETYPE_WEBP => 'webp',
		);
		//-- Проверка на существование файла ---------------------------------------------------------------------------
		if( preg_match('/^http(s)?:\/\/.*$/', $sourceFile) > 0 ) {
			$tmpDirName = sys_get_temp_dir()."/".microtime(TRUE)."/";
			$tmpFileName = basename($sourceFile);
			if( ($fileContents = file_get_contents($sourceFile)) !== FALSE ) {
				self::checkFolder($tmpDirName);
				if(file_put_contents($tmpDirName.$tmpFileName, $fileContents)!==FALSE) {
					$sourceFile = $tmpDirName.$tmpFileName;
				}
			}
		}
		if (is_string($sourceFile) && $sourceFile != '' && is_file($sourceFile)) {
			!defined('CHMOD_FILE') && define('CHMOD_FILE', octdec(644));
			!defined('CHMOD') && define('CHMOD', octdec(777));

			if (is_object($entity)
				&& ($entity instanceof Shop_Item_Model
					|| $entity instanceof Shop_Group_Model
					|| $entity instanceof Property_Value_File_Model
					|| $entity instanceof Informationsystem_Item_Model
					|| $entity instanceof Shop_Producer_Model
				)
			) {
				$modelName = strtolower($entityClass);
				$processing = false;
				$imageType = '';
				if (filesize($sourceFile) > 11) {
					$imageType = exif_imagetype($sourceFile);
					try {
						if(!is_null($imageType) && $imageType!==FALSE) {
							$processing = array_key_exists($imageType, $imageTypes);
							if($processing) {
//								$picsize = Core_Image::getImageSize($sourceFile);
								$picsizetmp = getimagesize($sourceFile);
								$picsize['width'] = $picsizetmp[0];
								$picsize['height'] = $picsizetmp[1];
								$processing = isset($picsize['width'])&&isset($picsize['height'])&&(($picsize['width'])*1>0)&&(($picsize['height'])*1>0);
							}
						}
					} catch (Exception $exc) {
						$processing = false;
					}
				}
				$fileInfo = pathinfo($sourceFile);
				$sFileExt = $ext!='' ? $ext : (isset($imageTypes[$imageType]) ? $imageTypes[$imageType] : '');
				$sFileName = $fileInfo['filename'];
				$sFileDir = $fileInfo['dirname'];
				$uploadFolder = "{$uploadFolder}{$modelName}_{$currentShop->id}" . DIRECTORY_SEPARATOR . "{$entity->id}" . DIRECTORY_SEPARATOR;
				self::checkFolder($uploadFolder);
				$srcFullFile = $uploadFolder . $entity->id . '.' . $sFileExt;
				$tgtLargeFile = $uploadFolder . $entity->id . '_large.' . $sFileExt;
				$tgtSmallFile = $uploadFolder . $entity->id . '_small.' . $sFileExt;
				copy($sourceFile, $srcFullFile);
				$aPicturesParam = array();
				$aPicturesParam['large_image_source'] = $sourceFile;
				$aPicturesParam['large_image_name'] = $entity->id . '.' . $sFileExt;
				$aPicturesParam['create_small_image_from_large'] = FALSE;
				$aPicturesParam['small_image_target'] = $tgtSmallFile;
				$aPicturesParam['large_image_target'] = $tgtLargeFile;
				if ($processing) {

					$aPicturesParam['large_image_isset'] = TRUE;
					$aPicturesParam['watermark_file_path'] = $currentShop->getWatermarkFilePath();
					$aPicturesParam['watermark_position_x'] = $currentShop->watermark_default_position_x;
					$aPicturesParam['create_small_image_from_large'] = TRUE;
					$aPicturesParam['watermark_position_y'] = $currentShop->watermark_default_position_y;
					$aPicturesParam['large_image_preserve_aspect_ratio'] = $currentShop->preserve_aspect_ratio;
					$aPicturesParam['small_image_max_width'] = $currentShop->image_small_max_width;
					$aPicturesParam['small_image_max_height'] = $currentShop->image_small_max_height;
					$aPicturesParam['small_image_watermark'] = $currentShop->watermark_default_use_small_image;
					$aPicturesParam['small_image_preserve_aspect_ratio'] = $aPicturesParam['large_image_preserve_aspect_ratio'];
					$aPicturesParam['large_image_max_width'] = $currentShop->image_large_max_width;
					$aPicturesParam['large_image_max_height'] = $currentShop->image_large_max_height;
					$aPicturesParam['large_image_watermark'] = $currentShop->watermark_default_use_large_image;
				}
				try {
					$error['result'] = Core_File::adminUpload($aPicturesParam);
				} catch (Exception $exc) {
					$error = array(
						'message' => 'Ошибка обработки файла',
						'result' => array(
							'large_image' => FALSE,
							'small_image' => FALSE,
						)
					);
					return $error;
				}
				$folderAddDir = strtolower($entityClass).'/';
				switch (true) {
					case $entity instanceof Informationsystem_Item_Model:
						if ($error['result']['large_image']) {
							$entity->saveLargeImageFile($aPicturesParam['large_image_target'], 'informationsystem_items_catalog_image' . $entity->id . '.' . $sFileExt);
							$entity->setLargeImageSizes();
						}
						if ($error['result']['small_image']) {
							$entity->saveSmallImageFile($aPicturesParam['small_image_target'], 'small_informationsystem_items_catalog_image' . $entity->id . '.' . $sFileExt);
							$entity->setSmallImageSizes();
						}
						$error['message'] = 'OK';
						$error['status'] = TRUE;
						break;
					case $entity instanceof Shop_Item_Model:
						if ($error['result']['large_image']) {
							$entity->saveLargeImageFile($aPicturesParam['large_image_target'], 'shop_items_catalog_image' . $entity->id . '.' . $sFileExt);
							$entity->setLargeImageSizes();
						}
						if ($error['result']['small_image']) {
							$entity->saveSmallImageFile($aPicturesParam['small_image_target'], 'small_shop_items_catalog_image' . $entity->id . '.' . $sFileExt);
							$entity->setSmallImageSizes();
						}
						$error['message'] = 'OK';
						$error['status'] = TRUE;
						break;
					case $entity instanceof Shop_Group_Model:
						if ($error['result']['large_image']) {
							$entity->saveLargeImageFile($aPicturesParam['large_image_target'], 'shop_groups_catalog_image' . $entity->id . '.' . $sFileExt);
							$entity->setLargeImageSizes();
						}
						if ($error['result']['small_image']) {
							$entity->saveSmallImageFile($aPicturesParam['small_image_target'], 'small_shop_groups_catalog_image' . $entity->id . '.' . $sFileExt);
							$entity->setSmallImageSizes();
						}
						$error['message'] = 'OK';
						$error['status'] = TRUE;
						break;
					case $entity instanceof Shop_Producer_Model:
						if ($error['result']['large_image']) {
							$entity->saveLargeImageFile($aPicturesParam['large_image_target'], 'shop_producer_image_image' . $entity->id . '.' . $sFileExt);
						}
						if ($error['result']['small_image']) {
							$entity->saveSmallImageFile($aPicturesParam['small_image_target'], 'small_shop_producer_image_image' . $entity->id . '.' . $sFileExt);
						}
						$error['message'] = 'OK';
						$error['status'] = TRUE;
						break;
					case $entity instanceof Property_Value_File_Model:
						if ($destinationFolder == '') {
							$error['message'] = "Параметр DestinationFolder не задан";
							return $error;
						}
						$largeFileName = "shop_property_file_{$entity->property_id}_{$entity->id}.{$sFileExt}";
						$smallFileName = "small_" . $largeFileName;
						if ($error['result']['large_image']) {
							if(file_exists($aPicturesParam['large_image_target'])) {
								Core_File::upload($aPicturesParam['large_image_target'], $destinationFolder . $largeFileName);
							} else {
								if(file_exists($newFileName = str_replace('_large', '', $aPicturesParam['large_image_target']))) {
									Core_File::upload($newFileName, $destinationFolder . $largeFileName);
								}
							}
						}
						if ($error['result']['small_image']) {
							Core_File::upload($aPicturesParam['small_image_target'], $destinationFolder . $smallFileName);
						}
						$entity->file = $largeFileName;
						$entity->file_name = ($realFilename!='') ? $realFilename : $fileInfo['basename'];
						$entity->file_small = $smallFileName;
						$entity->file_small_name = $fileInfo['basename'];
						$entity->save();
						$error['message'] = 'OK';
						$error['status'] = TRUE;
						break;
				}

				if($saveOriginalFolder!='' && file_exists($saveOriginalFolder) && is_dir($saveOriginalFolder)) {
					$saveOriginalFolder = self::checkFolder($saveOriginalFolder.$folderAddDir);
					$fileName = basename($sourceFile);
					copy($sourceFile, $saveOriginalFolder.$entity->id.'_'.$fileName);
				}

				if(is_dir($uploadFolder)) Core_File::deleteDir($uploadFolder);

				$error['status'] = TRUE;
				$error['message'] = 'OK';
			} else {
				$error['message'] = "Параметр shopItem не является объектом Shop_Item_Model";
			}
		} else {
			$error['message'] = "Файл не существует";
		}
		return $error;
	}
	/**
	 */
	public static function processFilesType($aFilesVar)
	{
		foreach ($aFilesVar as $key => &$value) {
			if(!is_array($value)) {
				$value = [$value];
			}
		}
		$aTmpReturnVars = [];
		foreach ($aFilesVar as $key => $aValue) {
			foreach ($aValue as $keyValue => $aValues) {
				$aTmpReturnVars[$keyValue][$key] = $aValues;
			}
		}
		return $aTmpReturnVars;
	}

	/**
	 * Функция выводит print_r аргумента-объекта в блоке <pre> для отладки некоторых модулей/приложений
	 *
	 * @param $object
	 */
	public static function rp($object, $title = '', $options = [
		'show' => true,
		'lc' => true,
		'showHeader' => TRUE,
		'status' => 'NONE',
	])
	{
		$options['logline'] = Core_Array::get($options, 'logline', 1);
		if(Core_Array::getRequest('rp', false) !== false) {
			self::p($object, $title, $options);
		}
	}

	/**
	 * Функция выводит print_r аргумента-объекта в блоке <pre> для отладки некоторых модулей/приложений
	 *
	 * @param $object
	 */
	public static function rv($object, $title = '', $lastCaret = true)
	{
		if(Core_Array::getRequest('rv', false) !== false) {
			self::v($object, $title, $lastCaret);
		}
	}

	/**
	 * Функция выводит print_r аргумента-объекта в блоке <pre> для отладки некоторых модулей/приложений
	 *
	 * @param $object
	 */
	public static function p($object, $title = '', $options = [
		'show' => true,
		'lc' => true,
		'showHeader' => TRUE,
		'status' => 'NONE',
		'logline' => 0
	])
	{
		$show = Core_Array::get($options, 'show', true);
		$lastCaret = Core_Array::get($options, 'lc', true);
		$showHeader = Core_Array::get($options, 'showHeader', true);
		$status = Core_Array::get($options, 'status', 'NONE');
		if ($show === true || ($show === false && Core_Array::getRequest('show', false) !== false)) {
			echo isset($_SERVER['SERVER_NAME']) ? "<pre style='text-align: left;' class='debug'>" : ($showHeader ? "\n" : "");
			if ($showHeader) {
				$filesource = Core_Array::get($f = debug_backtrace(), Core_Array::get($options, 'logline', 0), []);
				$sFileSource = "== [".Core_Array::get($filesource, 'line', '').'] '.Core_Array::get($filesource, 'file', '').' ==';
				echo $sFileSource."\n";
				echo str_pad((strlen($title) > 0 ? ' ' : '') . $title . (strlen($title) > 0 ? ' ' : ''), 100, '=', STR_PAD_BOTH) . ($lastCaret && !$showHeader ? "\n" : "");
			}
			echo($showHeader ? "\n" : "");
			print_r($object);
			echo isset($_SERVER['SERVER_NAME']) ? "</pre>\n" : "\n";
		}
	}

	/**
	 * Функция выводит var_dump аргумента-объекта в блоке <pre> для отладки некоторых модулей/приложений
	 *
	 * @param $object
	 */
	public static function v($object, $title = '', $lastCaret = true)
	{
		echo isset($_SERVER['SERVER_NAME']) ? "<pre style='text-align: left;' class='debug'>" : "\n";
//		if ($showHeader) {
		$filesource = Core_Array::get($f = debug_backtrace(), 0, []);
		$sFileSource = "== [".Core_Array::get($filesource, 'line', '').'] '.Core_Array::get($filesource, 'file', '').' ==';
		echo $sFileSource."\n";
//		}
		echo str_pad((strlen($title) > 0 ? ' ' : '') . $title . (strlen($title) > 0 ? ' ' : ''), 100, '=', STR_PAD_BOTH) . ($lastCaret ? "\n" : "");
		var_dump($object);
		echo isset($_SERVER['SERVER_NAME']) ? "</pre>\n" : "\n";
	}

	/**
	 * Функция выводит print_r аргумента-объекта в блоке <textarea> для отладки некоторых модулей/приложений
	 *
	 * @param $object
	 */
	public static function tp($object, $title = '', $height = 400, $lastCaret = true)
	{
		if($title != '') {
			echo str_pad((strlen($title) > 0 ? ' ' : '') . $title . (strlen($title) > 0 ? ' ' : ''), 100, '=', STR_PAD_BOTH) . ($lastCaret ? "\n" : "");
		}
		echo "<textarea style='width: 100%; height: {$height}px; overflow: auto; text-align: left' class='debug'>";
		print_r($object);
		echo "</textarea>";
	}

	/**
	 * Функция выводит var_dump аргумента-объекта в блоке <textarea> для отладки некоторых модулей/приложений
	 *
	 * @param $object
	 */
	public static function tv($object, $title = '', $height = 400, $lastCaret = true)
	{
		if($title != '') {
			echo str_pad((strlen($title) > 0 ? ' ' : '') . $title . (strlen($title) > 0 ? ' ' : ''), 100, '=', STR_PAD_BOTH) . ($lastCaret ? "\n" : "");
		}
		echo "<textarea style='width: 100%; height: {$height}px; text-align: left' class='debug'>";
		var_dump($object);
		echo "</textarea>";
	}

	/**
	 * Функция возвращает number аргумента в формате денежной единицы XXX XXX,XX
	 *
	 * @param $object
	 */
	public static function money($number, $currency = 'руб.')
	{
		$number = $number * 1;
		return trim(number_format($number, (($number * 1 - floor($number * 1)) == 0 ? 0 : 2), ',', ' ') . " $currency");
	}

	/**
	 * Функция обрезает строку $string на заданное количество символов $limit до ближайшего пробела после $limit
	 *
	 * @param $string
	 * @param $limit
	 */
	public static function cropStr($string, $limit)
	{
		if (strlen($string) >= $limit) {
			$substring_limited = substr($string, 0, $limit);
			return substr($substring_limited, 0, strrpos($substring_limited, ' '));
		} else {
			//Если количество символов строки меньше чем задано, то просто возращаем оригинал
			return $string;
		}
	}

	// перевод цвета из HEX в RGB
	public static function hexToRgb($color) {
		// проверяем наличие # в начале, если есть, то отрезаем ее
		if ($color[0] == '#') {
			$color = substr($color, 1);
		}

		// разбираем строку на массив
		if (strlen($color) == 6) { // если hex цвет в полной форме - 6 символов
			list($red, $green, $blue) = array(
				$color[0] . $color[1],
				$color[2] . $color[3],
				$color[4] . $color[5]
			);
		} elseif (strlen($color) == 3) { // если hex цвет в сокращенной форме - 3 символа
			list($red, $green, $blue) = array(
				$color[0]. $color[0],
				$color[1]. $color[1],
				$color[2]. $color[2]
			);
		}else{
			return false;
		}

		// переводим шестнадцатиричные числа в десятичные
		$red = hexdec($red);
		$green = hexdec($green);
		$blue = hexdec($blue);

		// вернем результат
		return array($red, $green, $blue);
	}

	// перевод цвета из RGB в HEX
	public static function rgbToHex($color) {
		$red = dechex($color[0]);
		$green = dechex($color[1]);
		$blue = dechex($color[2]);
		return "#" . mb_strtoupper($red . $green . $blue);
	}

	/**
	 * Функция проверяет папку по пути $pathToFolder на существование, и при необходимости создает данную папку по
	 * указанному пути с правами $rules
	 *
	 * @param $string
	 * @param $rules
	 */
	public static function checkFolder($pathToFolder, $rules = 0755)
	{
		if (!file_exists($pathToFolder)) {
			mkdir($pathToFolder, 0755, true);
		}
		return $pathToFolder;
	}

	/**
	 * Функция преобразует адрес от
	 * Google Maps
	 * и возвращает объект Skynetcore_Entity_Address
	 *
	 * @param $rules
	 */
	public static function getAddress($address)
	{
		return new Skynetcore_Entity_Address($address);
	}

	/**
	 * Compare two arrays and return a list of items only in array1 (deletions) and only in array2 (insertions)
	 *
	 * @param array $array1 The 'original' array, for comparison. Items that exist here only are considered to be deleted (deletions).
	 * @param array $array2 The 'new' array. Items that exist here only are considered to be new items (insertions).
	 * @param array $keysToCompare A list of array key names that should be used for comparison of arrays (ignore all other keys)
	 * @return array[] array with keys 'insertions' and 'deletions'
	 */
	public static function arrayDifference(array $array1, array $array2, array $keysToCompare = null) {
		$serialize = function (&$item, $idx, $keysToCompare) {
			if (is_array($item) && $keysToCompare) {
				$a = array();
				foreach ($keysToCompare as $k) {
					if (array_key_exists($k, $item)) {
						$a[$k] = $item[$k];
					}
				}
				$item = $a;
			}
			$item = serialize($item);
		};

		$deserialize = function (&$item) {
			$item = unserialize($item);
		};

		array_walk($array1, $serialize, $keysToCompare);
		array_walk($array2, $serialize, $keysToCompare);

		// Items that are in the original array but not the new one
		$deletions = array_diff($array1, $array2);
		$insertions = array_diff($array2, $array1);

		array_walk($insertions, $deserialize);
		array_walk($deletions, $deserialize);

		return array('insertions' => $insertions, 'deletions' => $deletions);
	}

	/**
	 * Compare two arrays and return a list of items only in array1 (deletions) and only in array2 (insertions)
	 *
	 * @param array $array1 The 'original' array, for comparison. Items that exist here only are considered to be deleted (deletions).
	 * @param array $array2 The 'new' array. Items that exist here only are considered to be new items (insertions).
	 * @param array $keysToCompare A list of array key names that should be used for comparison of arrays (ignore all other keys)
	 * @return array[] array with keys 'insertions' and 'deletions'
	 */
	public static function arrayDifferenceWithKeys(array $array1, array $array2, array $keysToCompare = null) {
		$serialize = function (&$item, $idx, $keysToCompare) {
			$tmpItem = null;
			if (is_array($item) && $keysToCompare) {
				$a = array();
				foreach ($keysToCompare as $k) {
					if (array_key_exists($k, $item)) {
						$a[$k] = $item[$k];
					}
				}
				$item = $a;
			}
			$tmpItem['value'] = $item;
			$tmpItem['key'] = $idx;
			$item = serialize($tmpItem);
		};

		$deserialize = function (&$item) {
			$item = Core_Array::get(unserialize($item), 'value', 0);
		};

		array_walk($array1, $serialize, $keysToCompare);
		array_walk($array2, $serialize, $keysToCompare);

		// Items that are in the original array but not the new one
		$deletions = array_diff($array1, $array2);
		$insertions = array_diff($array2, $array1);

		array_walk($insertions, $deserialize);
		array_walk($deletions, $deserialize);

		$updates = [];
		foreach ($insertions as $insertionKey => $insertion) {
			if (array_key_exists($insertionKey, $deletions)) {
				$updates[$insertionKey] = [
					'old' => $deletions[$insertionKey],
					'new' => $insertion,
				];
				unset($insertions[$insertionKey]);
				unset($deletions[$insertionKey]);
			}
		}

		return array('insertions' => $insertions, 'deletions' => $deletions, 'updates' => $updates);
	}

	public static function wsAutoSize(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet &$sheet)
	{
		$list = $sheet->rangeToArray('A1:' . $sheet->getHighestColumn() . $sheet->getHighestRow(), '', TRUE, TRUE, TRUE);
		$colDims = [];

		foreach ($list as $rowIndex => $rowData) {
			foreach ($rowData as $colIndex => $colData) {
				$charWidth = mb_strlen($colData);

				if (empty($colDims[$colIndex]))
					$colDims[$colIndex] = $charWidth;
				elseif (!empty($colDims[$colIndex]) && $charWidth > $colDims[$colIndex])
					$colDims[$colIndex] = $charWidth;
			}
		}

		foreach ($colDims as $colIndex => $charWidth) {
			$sheet->getColumnDimension($colIndex)->setWidth($charWidth + 3); // padding: 3-char width
		}
	}

	/**
	 *  $aGoods = Skynetcore_Utils::array_map_assoc(function($oKey, $oItem) {
			return [$oItem->id, $oItem];
		}, $requestParms->goods);
	 *
	 */
	public static function array_map_assoc(callable $f, array $a) {
		return array_column(array_map($f, array_keys($a), $a), 1, 0);
	}

	public static function getSimpleToken() {
		/** @var Core_Hash_Md5 $hashInstance */
		$hashInstance = new Core_Hash_Md5();
		$hashInstance->salt(date('Y-m-d'));
		$value = 'api.chelznak.ru'.date('Y-m-d').':';
//		return $hashInstance->hash(Core_Array::get($_SERVER, 'REMOTE_ADDR', microtime()).':'.Core_Array::get($_SERVER, 'SERVER_NAME', microtime()).date('Y-m-d').':');
		return $hashInstance->hash($value);
	}

	public static function setPropertyItemPathByItemID($propertyId, $entityId, $value)
	{
		$propertyModel = Core_Entity::factory('Property', $propertyId);
		if (!is_null($propertyModel->type)) {
			$propertyValues = $propertyModel->getValues($entityId, false);
			if (count($propertyValues) == 0) {
				$propertyValues[] = $propertyModel->createNewValue($entityId);
			}
			if (count($propertyValues) == 1) {
				$propertyValues[0]->value = $value;
				$propertyValues[0]->save();
			}
		}
	}

	public static function monByNum($num) {
		return self::$monthes[$num-1];
	}

	public static function setPropertyValues($aPropertyValues, $entity_id)
	{
		if($entity_id > 0) {
			foreach ($aPropertyValues as $propertyId => $propertyValue) {
				/** @var Property_Model $oProperty */
				$oProperty = Core_Entity::factory('Property')->getById($propertyId, false);
				$model = false;
//				Skynetcore_Utils::p($oProperty->type, $oProperty->id);
				switch ($oProperty->type) {
					case 0:
					case 7:
						$model = 'Property_Value_Int';
						if($oProperty->type == 7) {
							$propertyValue = $propertyValue*1;
						}
						break;
					case 1:
						$model = 'Property_Value_String';
						break;
					case 4:
					case 6:
						$model = 'Property_Value_Text';
//						Skynetcore_Utils::p($propertyValue);
						break;
					case 9:
						/** @var Property_Value_Datetime_Model $model */
						$model = 'Property_Value_Datetime';
						$propertyValue = Core_Date::timestamp2sql(strtotime($propertyValue));
						break;
				}
				if($model !== false) {
					/** @var Property_Value_Int_Model $qPropertyValue */
					$qPropertyValue = Core_Entity::factory($model);
					$qPropertyValue
						->queryBuilder()
						->where('property_id', '=', $oProperty->id);
					$oPropertyValue = $qPropertyValue
						->getByEntity_id($entity_id, false);
					if(!(isset($oPropertyValue->id) && $oPropertyValue->id > 0)) {
						$oPropertyValue = Core_Entity::factory($model);
						$oPropertyValue->property_id = $oProperty->id;
						$oPropertyValue->entity_id = $entity_id;
						$oPropertyValue->save();
					}
					$oPropertyValue->value = $propertyValue;
					$oPropertyValue->save();
				}
			}
		}
	}

	public static function getDocumentTemplate($documentName, $aParams = [])
	{
		if($aParams instanceof stdClass) {
			$aParams = json_decode(json_encode($aParams), true);
		}
		/** @var Document_Model $oDocument */
		$oDocument = Core_Entity::factory('Document')->getByName($documentName);
		ob_start();
		$oDocument->execute();
		$letterTemplate = ob_get_clean();

		foreach ($aParams as $paramKey => $param ) {
			$letterTemplate = preg_replace("/\{$paramKey\}/ui", $param, $letterTemplate);
		}

		return $letterTemplate;
	}

	public static function timestampToMonthYearDay($timestamp, $show_time = false)
	{
		if (empty($timestamp)) {
			return '-';
		} else {
			$now   = explode(' ', date('Y n j H i'));
			$value = explode(' ', date('Y n j H i', $timestamp));

			if ($now[0] == $value[0] && $now[1] == $value[1] && $now[2] == $value[2]) {
				return 'Сегодня в ' . $value[3] . ':' . $value[4];
			} else {
				$month = array(
					'', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
					'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'
				);
				$out = $value[2] . ' ' . $month[$value[1]] . ' ' . $value[0];
				if ($show_time) {
					$out .= ' в ' . $value[3] . ':' . $value[4];
				}
				return $out;
			}
		}
	}

	public static function timestampToMonthYear($ts, $firstLetterUp = false, $monthOnly = false) {
		$month = date('m', $ts)*1;
		$year = '';
		$monthName = self::$monthes[$month-1];
		if($firstLetterUp) {
			$monthName = Core_Str::ucfirst($monthName);
		}
		if($monthOnly !== true) {
			$year = date('Y', $ts)*1;
		}
		return trim($monthName.' '.$year);
	}

	public static function objectGetAllParents($object) {
		$aObjects = []; // = $oObject;
		$oObject = $object;
		do {
			$aObjects[] = $oObject;
			$oObject = $oObject->getParent();
		} while (isset($oObject->id) && $oObject->id > 0);
		return $aObjects;
	}


	/**
	 * Санитизация входных данных с помощью регулярного выражения
	 * @param string $input Входная строка
	 * @param string $pattern Регулярное выражение для проверки (по умолчанию: безопасные символы)
	 * @return string Очищенная строка
	 */
	public static function sanitize_regexp_input($input, $pattern = '/[^a-zA-Z0-9_\-\s\.\,\@\!\?\:]/u')
	{
		// Удаляем невидимые символы и потенциальные вредоносные конструкции
		$input = preg_replace('/[\x00-\x1F\x7F-\x9F]/u', '', $input); // Убираем управляющие символы ASCII
		$input = trim($input); // Обрезаем лишние пробелы в начале и конце строки

		// Если входные данные содержат запрещённые символы — фильтруем
		if (!preg_match($pattern, $input)) {
			// Если подходящего шаблона нет, оставляем только допустимые символы
			$sanitized = preg_replace($pattern, '', $input);
		} else {
			$sanitized = $input; // Если соответствуют, данные считаются "безопасными"
		}

		// Возвращаем результат после проверки
		return htmlspecialchars($sanitized, ENT_QUOTES, 'UTF-8'); // Преобразуем спецсимволы в виды HTML.
	}
}
