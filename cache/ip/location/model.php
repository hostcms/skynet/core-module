<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Skynetcore_siteuser_Session_Model
 *
 */
class Skynetcore_Cache_Ip_Location_Model extends Core_Entity
{
	const T_DADATA = 'dadata';
//	const T_DADATA = 'dadata';

	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

	/**
	 * Default sorting for models
	 * @var array
	 */
	protected $_sorting = array(
		'skynetcore_cache_ip_locations.id' => 'ASC',
	);

	/**
	 * Constructor.
	 * @param int $id entity ID
	 */
	public function __construct($id = NULL)
	{
		parent::__construct($id);

		if (is_null($id) && !$this->loaded())
		{
			$this->_preloadValues['location'] = '{}';
			$this->_preloadValues['from'] = Core_Date::timestamp2sql(time());
			$this->_preloadValues['useragent'] = '';
		}
	}
}