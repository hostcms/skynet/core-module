<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');

use Skynetcore_Utils as utl;
use KubAT\PhpSimple\HtmlDomParser;

class Skynetcore_Ajax_Controller_Shop extends Skynetcore_Command_Controller
{
//->itemsProperties(Core_Array::get($options, 'itemsProperties', FALSE))
//->itemsPropertiesList(Core_Array::get($options, 'itemsPropertiesList', FALSE))
//->itemsForbiddenTags(array('description','text','shop_producer'))
//->warehousesItems(Core_Array::get($options, 'warehousesItems', FALSE))

	public function base($baseAction, $shop_item_id, $couponText = '', $options = [
		'littleCart' => [
			'itemsProperties' => false,
			'itemsPropertiesList' => false,
			'itemsForbiddenTags' => ['description','text','shop_producer'],
			'warehousesItems' => false,
		],
		'cart' => [
			'itemsProperties' => false,
			'itemsPropertiesList' => false,
			'itemsForbiddenTags' => ['description','text','shop_producer'],
			'warehousesItems' => false,
		],
	]) {
		$instance = Core_Page::instance();
		$localHtml = [];
		$shop_id = 0;
		$isExcludedIDtypes = in_array($shop_item_id, ['apply', 'clear']);
//		Skynetcore_Utils::p("\n$baseAction");
		if((is_numeric($shop_item_id) && ($shop_item_id *= 1) > 0) || $isExcludedIDtypes) {
			$oCartXsl = $oLittleCartXsl = false;
			$littleCartTemplate = base64_decode(Core_Array::getRequest('tmpl', false));
			if($littleCartTemplate !== '') {
				$oLittleCartXsl = Core_Entity::factory('Xsl')->getByName($littleCartTemplate, false);
			}
			$cartTemplate = base64_decode(Core_Array::getRequest('tmpl_cart', false));
			if($cartTemplate !== '') {
				$oCartXsl = Core_Entity::factory('Xsl')->getByName($cartTemplate, false);
			}
			/** @var Shop_Item_Model $oShopItem */
			$oShopItem = Core_Entity::factory('Shop_Item')->getById($shop_item_id);
			if((isset($oShopItem->id) && $oShopItem->id == $shop_item_id) || $isExcludedIDtypes ) {
				$shop_id = Core_Array::getRequest('shop_id', (isset($oShopItem->id) && $oShopItem->id > 0) ? $oShopItem->shop->id : Core_Array::getRequest('shop_id', 0));
				switch ($baseAction)
				{
					case 'cart':
						$action = Core_Array::getRequest('action', 'none');
						$quantity = Core_Array::getRequest('quantity', 1)*1;
						switch ($action) {
							case 'add':
								$oShop_Cart_Controller = Shop_Cart_Controller::instance();
								$oShop_Cart_Controller->shop_item_id($shop_item_id);
								$oShop_Cart_Controller_Item = $oShop_Cart_Controller->get();
								if(($oShop_Cart_Controller_Item->quantity + $quantity) > 0) {
									$oShop_Cart_Controller
										->shop_item_id($shop_item_id)
										->quantity($oShop_Cart_Controller_Item->quantity + $quantity)
										->update();
								}
								$this->_message = "Добавлено в корзину";
								break;
							case 'remove':
								$oShop_Cart_Controller = Shop_Cart_Controller::instance();
								$oShop_Cart_Controller
									->shop_item_id($shop_item_id)
									->delete();
								$this->_message = "Удалено из корзины";
								break;
							case 'clearcart':
								$oShop_Cart_Controller = Shop_Cart_Controller::instance();
								$aCartItems = $oShop_Cart_Controller->getAll(Core_Entity::factory('Shop', $shop_id));
								/** @var Shop_Cart_Model $oCartItem */
								foreach ($aCartItems as $oCartItem) {
									$oShop_Cart_Controller
										->shop_item_id($oCartItem->shop_item_id)
										->delete();
								}
								$this->_message = "Удалено из корзины";
								break;
						}
						Skynetcore_Controller_Process::clearSessionDeliveryConditionOrder();
//						'shop_delivery_condition_id' => 0,
//						'shop_delivery_id' => 0,
//						'shop_payment_system_id' => 0, // payment system id
//						if(isset($_SESSION['hostcmsOrder'])
//							&& isset($_SESSION['hostcmsOrder']['shop_delivery_condition_id'])
//						) {
//							$_SESSION['hostcmsOrder']['shop_delivery_condition_id'] = 0;
//						}
						$this->_status = 'OK';
						break;
					case 'coupon':
						$_SESSION['hostcmsOrder']['coupon_text'] = trim(strval(Core_Array::getPost('coupon_text', $couponText)));
						$this->_message = "Выполнено успешно";
						$this->_status = 'OK';
						break;
					case 'compares':
					case 'favorites':
						$classNameUserbar = ".userbar_{$baseAction}";
						$className = ".def_{$baseAction}_{$shop_item_id}";
						$classNameCommon = ".{$baseAction}_common_{$shop_item_id}";
						$classNameCount = "#items_{$baseAction}_main";
						$classProcessing = "Skynetcore_Shop_".Core_Str::ucfirst(preg_replace('/s$/ui', '', $baseAction))."_Controller";

						$tmpLocalHtml = Core_Array::get($this->_context_html, $className, false);
						$tmpCommonHtml = Core_Array::get($this->_context_html, $classNameCommon, false);
						$tmpUserbarHtml = Core_Array::get($this->_context_html, $classNameUserbar, false);
						$tmpCountHtml = Core_Array::get($this->_context_html, $classNameCount, false);
						if($tmpLocalHtml === false && $tmpCommonHtml === false) {
							$this->_message = "Класс {$className} или {$classNameCommon} не найден";
						} else {
							/** @var \simple_html_dom\simple_html_dom $dom */
							$dom = ($tmpLocalHtml !== false) ? HtmlDomParser::str_get_html( $tmpLocalHtml ) : false;
							$domCommon = ($tmpCommonHtml !== false) ? HtmlDomParser::str_get_html( $tmpCommonHtml ) : false;
							$domUserbar = HtmlDomParser::str_get_html( $tmpUserbarHtml );
							$comp = $dom !== false ? Core_Array::get($dom->find($className), 0, false) : false;
							$compCommon = $domCommon !== false ? Core_Array::get($domCommon->find($classNameCommon), 0, false) : false;
//							Skynetcore_Utils::v($comp);
							/** @var \simple_html_dom\simple_html_dom_node $comp */
							if($comp !== false || $compCommon !== false) {
								/** @var Shop_Compare_Controller $compareController */
								$tmpActionController = $classProcessing::instance();
								$tmpActionController->shop_item_id = $oShopItem->id;
								$comp !== false && $comp->removeClass('is-skynet-wait');
								$comp !== false && $comp->removeClass('is-wait');
								$comp !== false && $comp->removeClass('is-active');
								$tmpActionController->add();
								$aTmpActionItems = $tmpActionController->getAll($oShopItem->shop);

								$foundedTmpAction = Core_Array::get(array_values(array_filter($aTmpActionItems, function($tmpCompareItem) use ($tmpActionController) {
									return $tmpCompareItem->shop_item_id == $tmpActionController->shop_item_id;
								})), 0, false);
								$aTmpChecked = is_object($foundedTmpAction);
								$heartMatches = [];
								if($dom !== false && !is_null($xLinkFounded = $dom->find($className.' use', 0))
									&& preg_match('/^(.*)((heart|compare)(-fill)?)$/ui', $xLinkFounded->getAttribute('xlink:href'), $heartMatches)
								) {
									$xLinkFounded->setAttribute('xlink:href', Core_Array::get($heartMatches, 1).Core_Array::get($heartMatches, 3).($aTmpChecked ? '-fill' : ''));
								}
								if(is_object($domCommon) && (($compCommon=Core_Array::get($domCommon->find($classNameCommon), 0, false)) !== false)) {
									$compCommon->removeClass('is-wait');
									$compCommon->removeClass('is-active');
								}

								if($domCommon !== false) {
									$heartCommonMatches = [];
									if(!is_null($xLinkCommonFounded = $domCommon->find($classNameCommon.' use', 0))
										&& preg_match('/^(.*)((heart|compare)(-fill)?)$/ui', $xLinkCommonFounded->getAttribute('xlink:href'), $heartCommonMatches)
									) {
										$xLinkCommonFounded->setAttribute('xlink:href', Core_Array::get($heartCommonMatches, 1).Core_Array::get($heartCommonMatches, 3).($aTmpChecked ? '-fill' : ''));
									}
								}

//								if($compCommon !== false) {
//									$heartCommonMatches = [];
//									if(!is_null($xLinkCommonFounded = $domCommon->find($classNameCommon.' use', 0))
//										&& preg_match('/^(.*)(heart(-fill)?)$/ui', $xLinkCommonFounded->getAttribute('xlink:href'), $heartCommonMatches)
//									) {
//										$xLinkCommonFounded->setAttribute('xlink:href', Core_Array::get($heartCommonMatches, 1).'heart'.($aTmpChecked ? '-fill' : ''));
//									}
//								}

								if($aTmpChecked) {
									$comp !== false && $comp->addClass('is-active');
									if($compCommon !== false) {
										$compCommon->addClass('is-active');
									}
								}
								$oShopItem->clearCache();
								$dom !== false && $tmpLocalHtml = $dom->save();
								if($compCommon !== false) {
									$tmpCommonHtml = $domCommon->save();
									$localHtml[$classNameCommon] = $tmpCommonHtml;
								}
							}
							$localHtml[$className] = $tmpLocalHtml;

							if($tmpCountHtml !== false && isset($aTmpActionItems)) {
								/** @var \simple_html_dom\simple_html_dom $domCount */
								$domCount = HtmlDomParser::str_get_html( $tmpCountHtml );
								/** @var \simple_html_dom\simple_html_dom_node $domCountText */
								$domCountText = $domCount->find($classNameCount, 0);
								if(is_object($domCountText) && isset($domCountText->innertext)) {
									$tmpCounts = count($aTmpActionItems);
									$domCountText->innertext = $tmpCounts;
									$domCountText->setAttribute('data-counts', $tmpCounts);
									$domCountText->setAttribute('data-hide', ($tmpCounts==0)*1);
								}
								$heartUserbarMatches = [];
								if(false
									&& $tmpUserbarHtml !== false
									&& $domUserbar !== false
									&& !is_null($xLinkUserbarFounded = $domUserbar->find($classNameUserbar.' use', 0))
									&& preg_match('/^(.*)((heart|compare)(-fill)?)$/ui', $xLinkUserbarFounded->getAttribute('xlink:href'), $heartUserbarMatches)
								) {
									$xLinkUserbarFounded->setAttribute('xlink:href', Core_Array::get($heartUserbarMatches, 1).Core_Array::get($heartUserbarMatches, 3).(($tmpCounts > 0) ? '-fill' : ''));
									$tmpUserbarHtml = $domUserbar->save();
									$localHtml[$classNameUserbar] = $tmpUserbarHtml;
								}

								$tmpCountHtml = $domCount->save();
								$localHtml[$classNameCount] = $tmpCountHtml;
							}

							$aShops = Core_Entity::factory('Shop')->findAll();
							$tmpFavorites = [];
							foreach ($aShops as $oShop) {
								$tmpShopFavorites = $classProcessing::instance()->getAll($oShop);
								$cShopFavorites = new stdClass();
								$cShopFavorites->count = count($tmpShopFavorites);
								$cShopFavorites->nodes = Skynetcore_Utils::array_map_assoc(function($oKey, $oItem) {
									return [$oItem->shop_item_id, $oItem];
								}, $tmpShopFavorites);
								$tmpFavorites[$oShop->id] = $cShopFavorites;
							}
							$instance->skynet->$baseAction = $tmpFavorites;
//Skynetcore_Utils::p($this->_context_html);
							if(is_array($this->_context_html)) {
								$localHtml['context_keys'] = array_keys($this->_context_html);
							}
							$this->_status = 'OK';
							$this->_message = "OK";
						}
						break;
				}
//				Skynetcore_Utils::p($_SESSION);
				if($shop_id > 0) {
					$couponText = Core_Array::get(
						Core_Array::get($_SESSION, 'hostcmsOrder', array('coupon_text' => $couponText))
						, 'coupon_text');
//					Skynetcore_Utils::p($couponText);
					if($oCartXsl !== false) {
						ob_start();
						$Shop_Cart_Controller_Show = new Shop_Cart_Controller_Show(
							Core_Entity::factory('Shop', $shop_id)
						);
//							if(($additionalClass = Core_Array::getRequest('additional_class','')) != '') {
//								$Shop_Cart_Controller_Show
//									->addEntity(
//										Core::factory('Core_Xml_Entity')
//											->name('additional_class')
//											->value($additionalClass)
//									)
//								;
//							}
						$Shop_Cart_Controller_Show
							->addEntity(Core::factory('Core_Xml_Entity')->name('step')->value(Core_Array::getRequest('step', 1)))
							->itemsProperties(Core_Array::get($options['cart'], 'itemsProperties', FALSE))
							->itemsPropertiesList(Core_Array::get($options['cart'], 'itemsPropertiesList', FALSE))
							->itemsForbiddenTags(Core_Array::get($options['cart'], 'itemsForbiddenTags', FALSE))
							->warehousesItems(Core_Array::get($options['cart'], 'warehousesItems', FALSE))
							->xsl($oCartXsl)
							->couponText($couponText)
							->show();
						$cartHtml = ob_get_clean();

						$localHtml['.sky-cart'] = $cartHtml;
					}
					if($oLittleCartXsl !== false) {
						ob_start();
						$Shop_Little_Cart_Controller_Show = new Shop_Cart_Controller_Show(
							Core_Entity::factory('Shop', $shop_id)
						);
						if(($additionalClass = Core_Array::getRequest('additional_class','')) != '') {
							$Shop_Little_Cart_Controller_Show
								->addEntity(
									Core::factory('Core_Xml_Entity')
										->name('additional_class')
										->value($additionalClass)
								)
							;
						}
						$Shop_Little_Cart_Controller_Show
							->itemsProperties(Core_Array::get($options['littleCart'], 'itemsProperties', FALSE))
							->itemsPropertiesList(Core_Array::get($options['littleCart'], 'itemsPropertiesList', FALSE))
							->itemsForbiddenTags(Core_Array::get($options['littleCart'], 'itemsForbiddenTags', FALSE))
							->warehousesItems(Core_Array::get($options['littleCart'], 'warehousesItems', FALSE))
							->addEntity(Core::factory('Core_Xml_Entity')->name('step')->value(Core_Array::getRequest('step', 1)))
							->xsl($oLittleCartXsl)
							->couponText($couponText)
							->show()
						;
						$littleCartHtml = ob_get_clean();

						$localHtml['.sky-little-cart'] = $littleCartHtml;
					}
				}
			} else {
				$this->_message = "Такой товар не найден";
			}

			$this->_html = $localHtml;
//			$this->_message = "Выполнено успешно";
//			$this->_status = "OK";
		} else {
			$this->_message = "Такого ID не существует";
		}

		$this->_status_code = 200;
		$this->_html = $localHtml;
		return $this->sendResponse();
	}

	public function favoritesAction() {
		return $this->base('favorites', $this->params);
	}

	public function comparesAction() {
		return $this->base('compares', $this->params);
	}

	public function cartAction() {
		return $this->base('cart', $this->params);
	}

	public function couponAction() {
		/** @var \simple_html_dom\simple_html_dom $dom */
		$dom = HtmlDomParser::str_get_html( '<div>'.Core_Array::get(Core_Array::getRequest('context_html'), '#coupon_text', '').'</div>' );
		/** @var \simple_html_dom\simple_html_dom_node $coupon */
		$coupon = $dom->find('#coupon_text', 0);
		$couponValue = '';
		if($coupon instanceof \simple_html_dom\simple_html_dom_node) {
			$couponValue = $coupon->getAttribute('value');
		}
		return $this->base('coupon', $this->params, $couponValue);
	}

	public function orderAction($itemIDs=[],
								$shop_delivery_condition_id = 0,
								$shop_payment_system_id = 0,
								$makeSiteuser = false
	) {
		$this->checkCSRF();
		$localHtml = [];
		switch ($this->params) {
			case 'fast':
				$aItems = Core_Array::getRequest('item', $itemIDs);

				if(count($aItems) == 0) {
					$this->_message = "Не указан ни один товар";
				} else {
					$oSiteuser = $this->_instance->skynet->siteuser;
					$siteuser_id = 0;
					if($makeSiteuser && $oSiteuser == false) {
						$email = Core_Array::getPost('email');
						$phone = Core_Array::getPost('phone');
						$phone = preg_replace('/[^0-9]+/', '', $phone);

						$oSiteuser = Core_Entity::factory('Skynetcore_Siteuser')->getCurrent();

						if( !(isset($oSiteuser->id) && $oSiteuser->id > 0) ) {
							/** @var Chelznak_Siteuser_Model $oSiteuser */
							$oSiteuser = Core_Entity::factory('Chelznak_Siteuser')->getByLogin($phone, false);
							if(!(isset($oSiteuser->id) && $oSiteuser->id > 0)) {
								$oSiteuser = Core_Entity::factory('Chelznak_Siteuser');
								$oSiteuser->active = 1;
								$oSiteuser->login = $phone;
								$oSiteuser->email = $email;
								$oSiteuser = $oSiteuser->save();
								/** @var Siteuser_Group_Model $qSiteuserGroups */
								$qSiteuserGroups = Core_Entity::factory('Siteuser_Group');
								/** @var Siteuser_Group_Model $defaultGroup */
								$defaultGroup = $qSiteuserGroups->getDefault();
								/** @var Siteuser_Group_List_Model $oSiteuserGroup */
								$oSiteuserGroup = Core_Entity::factory('Siteuser_Group_List');
								$oSiteuserGroup->siteuser_id = $oSiteuser->id;
								$oSiteuserGroup->siteuser_group_id = $defaultGroup->id;
								$oSiteuserGroup->save();
							}
						} else {
							$oSiteuser->email = $email;
							$oSiteuser->save();
						}
						$siteuser_id = $oSiteuser->id;
						$fio = Skynetcore_Utils::splitFio(Core_Array::getRequest('fio', Core_Array::getRequest('name', '')));
						/** @var Siteuser_Person_Model $qSiteuserPeople */
						$qSiteuserPeople = Core_Entity::factory('Siteuser_Person');
						$qSiteuserPeople
							->queryBuilder()
							->where('surname', '=', $fio->surname)
							->where('name', '=', $fio->name)
							->where('patronymic', '=', $fio->patronymic)
						;
						/** @var Siteuser_Person_Model $oSiteuserPeople */
						$oSiteuserPeople = $qSiteuserPeople->getBySiteuser_id($oSiteuser->id, false);
						if(is_null($oSiteuserPeople)) {
							$oSiteuserPeople = Core_Entity::factory('Siteuser_Person');
							$oSiteuserPeople->siteuser_id = $oSiteuser->id;
							$oSiteuserPeople->name = $fio->name;
							$oSiteuserPeople->surname = $fio->surname;
							$oSiteuserPeople->patronymic = $fio->patronymic;
							$oSiteuserPeople = $oSiteuserPeople->save();
						}
						/** @var Directory_Email_Model $oSiteuserPeopleEmail */
						$oSiteuserPeopleEmail = Core_Entity::factory('Directory_Email')->getByValue(Core_Array::getRequest('email', ''));
						if(is_null($oSiteuserPeopleEmail)) {
							$oSiteuserPeopleEmail = Core_Entity::factory('Directory_Email');
							$oSiteuserPeopleEmail->value = Core_Array::getRequest('email', '');
							if($oSiteuserPeopleEmail->value != '') {
								$oSiteuserPeopleEmail = $oSiteuserPeopleEmail->save();
							}
						}
						if($oSiteuserPeopleEmail->value != '') {
							/** @var Siteuser_Person_Directory_Email_Model $qSiteuserPeopleEmail */
							$qSiteuserPeopleEmail = Core_Entity::factory('Siteuser_Person_Directory_Email');
							$qSiteuserPeopleEmail
								->queryBuilder()
								->where('siteuser_person_id', '=', $oSiteuserPeople->id)
							;
							$oSiteuserPeopleDirectoryEmail = $qSiteuserPeopleEmail->getByDirectory_email_id($oSiteuserPeopleEmail->id, false);
							if(is_null($oSiteuserPeopleDirectoryEmail)) {
								$oSiteuserPeopleDirectoryEmail = Core_Entity::factory('Siteuser_Person_Directory_Email');
								$oSiteuserPeopleDirectoryEmail->siteuser_person_id = $oSiteuserPeople->id;
								$oSiteuserPeopleDirectoryEmail->directory_email_id = $oSiteuserPeopleEmail->id;
								$oSiteuserPeopleDirectoryEmail->save();
							}
						}
					} else {
						(isset($oSiteuser->id) && $oSiteuser->id > 0) &&  $siteuser_id = $oSiteuser->id;
					}
					$oShopItem0 = Core_Entity::factory('Shop_Item')->getById(Core_Array::get(array_keys($aItems), 0, 0));

					if(isset($oShopItem0->id) && $oShopItem0->id > 0) {
						$shopCartControllerOnestep = new Skynetcore_Shop_Cart_Controller_Onestep($oShopItem0->shop);
						ob_start();
						foreach ($aItems as $itemId => $itemQuantity) {
							$shopCartControllerOnestep->addCartItem($itemId, $itemQuantity);
						}
						$shopCartControllerOnestep->processOrder(
							  Core_Array::getRequest('shop_delivery_condition_id', $shop_delivery_condition_id)
							, Core_Array::getRequest('shop_payment_system_id', $shop_payment_system_id)
							, $siteuser_id);
						$processedOrder = ob_get_clean();
						$this->_status = "OK";
						$this->_message = "Выполнено успешно";
						$matches = [];
						preg_match("/&order_id=(.*?)['|&]/ui", $processedOrder, $matches);
						if (isset($matches[1]) && $matches[1] != '') {
							/** @var Chelznak_Shop_Order_Model $oOrder */
							$oOrder = Chelznak_Shop_Order_Model::getOrderByHash($matches[1]);
							if(isset($oOrder->id) && $oOrder->id > 0) {
								$localHtml['try_redirect'] = "/p/".Chelznak_Shop_Order_Model::getOrderCRC32($oOrder)."/";
							}
						}
						preg_match("/invoice=(.*?)['|&]/ui", $processedOrder, $matches);
						if (isset($matches[1]) && $matches[1] != '') {
							$localHtml['try_redirect'] = "/shop/cart/?invoice=".$matches[1]."";
						}
						$localHtml['processed_order'] = $processedOrder;
					}
				}
				break;
		}
		$this->_status_code = 200;
		$this->_html = $localHtml;
		return $this->sendResponse();
	}
}