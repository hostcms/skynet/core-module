<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');

use Skynetcore_Utils as utl;
use PHPHtmlParser\Dom;
use \PhpOffice\PhpSpreadsheet\IOFactory as exl;
use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\IOFactory;
use \PhpOffice\PhpSpreadsheet\Cell\Cell;
use \PhpOffice\PhpSpreadsheet\Cell\DataType;
use \PhpOffice\PhpSpreadsheet\Style\Alignment;
use \PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use \PhpOffice\PhpSpreadsheet\Style\Color;
use \PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use GuzzleHttp\Client as GuzzleClient;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use Http\Factory\Guzzle\RequestFactory;
use Http\Factory\Guzzle\StreamFactory;
use Gam6itko\OzonSeller\Service\V2 as psV2;
use Gam6itko\OzonSeller\Service\V1 as psV1;

class Skynetcore_Ajax_Mp_Ozon_Controller extends Skynetcore_Command_Controller
{
	public function exportAction($itemMarkings = false) {
		$instance = Core_Page::instance();
		if($itemMarkings === false) {
			$itemMarkings = $this->params;
		}
		if(!is_array($itemMarkings)) {
			$itemMarkings = [$itemMarkings];
		}
		$aImportToOzonItems = [];
//		Skynetcore_Utils::p($itemMarkings);
		foreach ($itemMarkings as $itemMarking) {
			/** @var Skynetcore_Shop_Item_Model $oItem */
			$qItem = Core_Entity::factory('Skynetcore_Shop_Item');
			$oItem = $qItem->getByMarking($itemMarking, false);
//			Skynetcore_Utils::p($oItem->toArray(), $itemMarking);
			$aItem = $oItem->getOzonExportJson(true);
//			Skynetcore_Utils::p($aItem);
			if($aItem['status']) {
				$aImportToOzonItems[] = $aItem['data'];

				$requestFactory = new RequestFactory();
				$streamFactory = new StreamFactory();
				$adapter = new GuzzleAdapter(new GuzzleClient());
				/** @var \Gam6itko\OzonSeller\Service\V2\ProductService $svcProduct */
				$svcProduct = new psV2\ProductService(
					$instance->skynet->config['integration']['ozon'][$instance->skynet->request->envType],
					$adapter,
					$requestFactory,
					$streamFactory
				);
			} else {
//				$this->_message = $aItem['message'];
			}
		}
//		Skynetcore_Utils::p($aImportToOzonItems);
//		die();
//		$this->_status_code = 200;
		$aProductImport = $svcProduct->import($aImportToOzonItems);
		/** @var Skynetcore_Mp_Ozon_Item_Sync_Model $oTaskItem */
		$oTaskItem = Core_Entity::factory('Skynetcore_Mp_Ozon_Item_Sync')->getByShop_item_id($oItem->id, false);
		if(isset($aProductImport['task_id']) && $aProductImport['task_id'] > 0) {
			if(!(isset($oTaskItem->id) && $oTaskItem->id>0)) {
				$oTaskItem = Core_Entity::factory('Skynetcore_Mp_Ozon_Item_Sync');
				$oTaskItem->shop_item_id = $oItem->id;
			}
			$oTaskItem->transaction_id = $aProductImport['task_id'];
			$oTaskItem->response = '';
			$oTaskItem->active = 1;
			$oTaskItem->save();
//			Skynetcore_Utils::p($oTaskItem);
		}
//		Skynetcore_Utils::tp($aProductImport);
		sleep(5);
		/** @var \Gam6itko\OzonSeller\Service\V1\ProductService $svcProductV1 */
		$svcProductV1 = new psV1\ProductService(
			$instance->skynet->config['integration']['ozon'][$instance->skynet->request->envType],
			$adapter,
			$requestFactory,
			$streamFactory
		);
		$aProductImportTask = $svcProductV1->importInfo($oTaskItem->transaction_id);
//		Skynetcore_Utils::tp($aProductImportTask);
		if(is_array($aProductImportTask)
			&& isset($aProductImportTask['items'])
			&& count($aProductImportTask['items'])
		) {
			foreach ($aProductImportTask['items'] as $ozonOffer) {
				$tryUmOzonItem = Core_Entity::factory('Shop_Item')->getByMarking($ozonOffer['offer_id'], false);

				/** @var Skynetcore_Mp_Ozon_Item_Model $tryMpOzonItemTmp */
				$tryMpOzonItemTmp = Core_Entity::factory('Skynetcore_Mp_Ozon_Item');
				$tryMpOzonItem = $tryMpOzonItemTmp
					->getByShop_item_id($tryUmOzonItem->id, false);
//				Skynetcore_Utils::p($tryMpOzonItemTmp->queryBuilder()->build(), $ozonOfferId);
				if(!(isset($tryMpOzonItem->id) && $tryMpOzonItem->id > 0)) {
					$tryMpOzonItem = Core_Entity::factory('Skynetcore_Mp_Ozon_Item');
					$tryMpOzonItem->shop_item_id = $tryUmOzonItem->id;
					$tryMpOzonItem->create();
				}
				if(isset($ozonOffer['id']) && $ozonOffer['id'] != '') {
					$tryMpOzonItem->product_id = $ozonOffer['offer_id'];
					$tryMpOzonItem->state = $ozonOffer['status'];
//					$tryMpOzonItem->state_failed = $ozonOffer['status']['state_failed'];
//					$tryMpOzonItem->validation_state = $ozonOffer['status']['validation_state'];
//					$tryMpOzonItem->state_name = $ozonOffer['status']['state_name'];
//					$tryMpOzonItem->state_description = $ozonOffer['status']['state_description'];
//					$tryMpOzonItem->is_failed = $ozonOffer['status']['is_failed']*1;
//					$tryMpOzonItem->is_created = $ozonOffer['status']['is_created']*1;
					$tryMpOzonItem->item_errors = json_encode(Core_Array::get($ozonOffer['errors'], 'item_errors', []), JSON_UNESCAPED_UNICODE);
					$tryMpOzonItem->state_updated_at = Core_Date::timestamp2sql(time());
					$tryMpOzonItem->save();
				}
			}
			$oTaskItem->code = 200;
//			$oTaskItem->active = 0;
		} else {
			$oTaskItem->code = 400;
		}
		$oTaskItem->response = json_encode($aProductImportTask, JSON_UNESCAPED_UNICODE);
		$oTaskItem->save();
		if($oTaskItem->code == 200) {
			$this->_status_code = 200;
			$this->_status = 'OK';
			$this->_message = 'OK';
		} else {
//			$this->_message = $aProductImportTask;
		}

		return $this->sendResponse();
	}

	public function linkShopItemsAction($item_markings=[[-1]], $search_id=0, $property_id=0) {
		$item_markings = Core_Array::getRequest('item_id', $item_markings);
		$searchId = Core_Array::getRequest('category_id', $search_id);
		$propertyId = Core_Array::getRequest('property_id', $property_id);

//		Skynetcore_Utils::p("{$searchId}, {$propertyId}", '');
//		die();
		$aItemMarkings = array_map(function($aItemMarking) {
			return Core_Array::get(array_values($aItemMarking), 0, -1);
		}, $item_markings);
		if(count($aItemMarkings)) {
			$aItems = Core_Entity::factory('Shop_Item')->getAllByMarking($aItemMarkings, false, 'IN');
			if(count($aItems)) {
				$aItemIDs = array_map(function($oTmpItem) {
					return $oTmpItem->id;
				}, $aItems);
				foreach ($aItemIDs as $itemID) {
					$this->linkShopItemAction($itemID, $searchId, $propertyId);
				}
//				Skynetcore_Utils::p($aItemIDs, '$aItemIDs');
			}
		}
		$this->_status_code = 200;
		$this->_status = 'OK';
		$this->_message = 'OK';

		return $this->sendResponse();
	}

	public function linkShopItemAction($item_id=0, $search_id=0, $property_id=0) {
		$itemId = (isset($this->params) && $this->params != '') ? $this->params*1 : $item_id*1;
		$searchId = Core_Array::getRequest('search_id', $search_id);
		$propertyId = Core_Array::getRequest('property_id', $property_id);
		if($search_id > 0 && $property_id > 0) {

		}
		/** @var Property_Model $qProperty */
		$qProperty = Core_Entity::factory('Property');
		/** @var Property_Model $oProperty */
		$oProperty = $qProperty->getById($propertyId, false);
		$aProperties = $oProperty->getValues($itemId);
//		Skynetcore_Utils::p($aProperties);
		$oPropertyValue = Core_Array::get($aProperties, 0, $oProperty->createNewValue($itemId) );
		$oPropertyValue->entity_id = $itemId;
		$oPropertyValue->property_id = $propertyId;
		$oPropertyValue->value = $searchId;
		$oPropertyValue->save();

		$this->_status_code = 200;
		$this->_status = 'OK';
		$this->_message = 'OK';

		return $this->sendResponse();
	}

	public function linkShopGroupAction($group_id=0, $search_id=0, $property_id=0) {
		$groupId = $this->params != '' ? $this->params*1 : $group_id*1;
		$searchId = Core_Array::getRequest('search_id', $search_id);
		$propertyId = Core_Array::getRequest('property_id', $property_id);
		/** @var Property_Model $qProperty */
		$qProperty = Core_Entity::factory('Property');
		/** @var Property_Model $oProperty */
		$oProperty = $qProperty->getById($propertyId, false);
		$aProperties = $oProperty->getValues($groupId);
		$oPropertyValue = Core_Array::get($aProperties, 0, $oProperty->createNewValue($groupId) );
		$oPropertyValue->entity_id = $groupId;
		$oPropertyValue->property_id = $propertyId;
		$oPropertyValue->value = $searchId;
		$oPropertyValue->save();

		$this->_status_code = 200;
		$this->_status = 'OK';
		$this->_message = 'OK';

		return $this->sendResponse();
	}

	public function searchGroupAction() {
		$this->_query = Core_Array::getRequest('query', '');

		/** @var Skynetcore_Mp_Ozon_Category_Model $qOzonQueries */
		$qOzonQueries = Core_Entity::factory('Skynetcore_Mp_Ozon_Category');
		$qOzonQueries
			->queryBuilder()
			->where('skynetcore_mp_ozon_categories.title', 'like', "%{$this->_query}%")
			->limit(20)
			->orderBy('skynetcore_mp_ozon_categories.title')
		;
		$aOzonQueries = $qOzonQueries->findAll(false);

		$aSuggestions = [];
		foreach ($aOzonQueries as $oOzonQuery) {
			$aSuggestions[] = [
				"value" => $oOzonQuery->title,
//				"price" => "0",
//				"data" => "/auto-parts/tesla/model-x/seats/3rd-row-seat-controllers/3rd-row-lh-switch-asy/",
//				"img" => "/images/no-image.png",
				"id" => $oOzonQuery->id,
//				"type" => "items"
			];
		}

		$this->_suggestions = $aSuggestions;
		$this->_status_code = 200;
		$this->_status = 'OK';
		$this->_message = 'OK';

		return $this->sendResponse();
	}
}
