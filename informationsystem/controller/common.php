<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Skynetcore Module.
 *
 * @package HostCMS
 * @subpackage Redwings
 * @version 6.x
 * @author GoldDraft
 * @copyright © 2021 GoldDraft (info@golddraft.ru)
 */

class Skynetcore_Informationsystem_Controller_Common extends Informationsystem_Controller_Show
{
	/** @var Core_Page $_instance */
	protected $_instance = false;
	/** @var Informationsystem_Group_Model $_currentGroup */
	protected $_currentGroup = false;
	/** @var Informationsystem_Item_Model $_currentItem */
	protected $_currentItem = false;

	public function __construct(Informationsystem_Model $oInformationsystem)
	{
		parent::__construct($oInformationsystem);

		$this->_instance = Core_Page::instance();
	}

	public function parseUrl()
	{
		parent::parseUrl();
		$aEntities = Skynetcore_Controller_Common::redirectByConditions($this);
		$this->_currentGroup = Core_Array::get($aEntities, 'group', false);
		$this->_currentItem = Core_Array::get($aEntities, 'item', false);

		return $this;
	}

}