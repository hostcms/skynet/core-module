<?php
use Skynetcore_Utils as utl;
use Telegram\Bot\Api;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetcore_Chat_Telegram_Controller extends Core_Servant_Properties
{
	protected $_instance = NULL;

	public function __construct()
	{
		$this->_instance = Core_Page::instance();
	}

	public static function sendOk($message = '', $env=false, $ch=false) {
		self::sendMessage($message, 'ok', $env, $ch);
	}

	public static function sendError($message = '', $env=false, $ch=false) {
		self::sendMessage($message, 'error', $env, $ch);
	}

	public static function sendWarn($message = '', $env=false, $ch=false) {
		self::sendMessage($message, 'warning', $env, $ch);
	}

	public static function sendMessage($message = '', $type='info', $env=false, $ch=false) {
		$instance = Core_Page::instance();
		$oSite = Core_Entity::factory('Site')->getById(CURRENT_SITE, false);
		$domain = isset($instance->skynet->request->domain) ? $instance->skynet->request->domain : 'no-domain';
		if(trim($message) != '' && isset($instance->skynet->config['integration']['telegram'])) {
			$env = ($env !== false ? $env : ($instance->skynet->request->isProd ? 'prod' : 'dev'));
			$tConf = $instance->skynet->config['integration']['telegram'];
			if(isset($tConf['ch'][$type]) && isset($tConf['ch'][$type][$env])) {
				$ch === false && $ch = $tConf['ch'][$type][$env];
				$telegram = new Api($tConf['botkey']);
				$f = $telegram->sendMessage([
					'chat_id' => $ch,
					'text' => "[{$type}][{$oSite->name}][{$domain}]\n{$message}",
				]);
			}
		}
	}
}